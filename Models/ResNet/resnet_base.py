import torch
import torch
import torch.nn as nn
import numpy as np
from Models.model_core import *


class ResNet(nn.Module):
    def __init__(self, params):
        super(ResNet, self).__init__()


        input_shape = params.input_shape
        num_in_channels = params.num_in_channels
        # num_out_channels = params.num_out_channels
        num_classes = params.num_classes

        base_filter = params.base_filter

        architecture = params.architecture

        kernel_size = params.kernel_size
        batch_normalization = params.batch_normalization
        instance_normalization = params.instance_normalization
        norm_track_running_states = params.norm_track_running_states

        # pool_size = params.pool_size
        general_stride = params.general_stride
        conv_block_stride = params.conv_block_stride
        dilation = params.dilation
        # dropout = params.dropout

        num_fully_connected_features = params.num_fully_connected_features

        # residuals = params.residuals
        normalization_momentum = params.normalization_momentum

        self.classify = params.classify
        self.return_output_layer_blocks = params.return_output_layer_blocks

        self.down_layer_list = nn.ModuleList()
        self.up_layer_list = nn.ModuleList()

        assert architecture in ['resnet50', 'resnet101']

        channel_list_1 = np.multiply(base_filter, [1, 2, 4, 8])
        channel_list_2 = np.multiply(base_filter * 4, [1, 2, 4, 8])

        total_channel_list = np.stack([channel_list_1, channel_list_1, channel_list_2], axis=1)

        input_shape = np.array(input_shape)

        # Stage 1
        self.init_padding = nn.ZeroPad2d((3, 3, 3, 3))

        self.init_conv = Conv2D(in_channels=num_in_channels,
                                out_channels=int(channel_list_1[0]),
                                kernel_size=7,
                                stride=2,
                                padding=None,
                                batch_normalization=batch_normalization,
                                instance_normalization=instance_normalization,
                                normalization_momentum=normalization_momentum,
                                norm_track_running_states=norm_track_running_states,
                                activation='relu',
                                input_shape=input_shape,
                                )

        input_shape = input_shape / 2

        self.init_max_pool_padding = PadTensor(padding='same', input_shape=input_shape, operation_type='max_pooling',
                                               kernel_size=3, stride=2, dilation=1,
                                               output_shape=input_shape / 2)

        self.init_max_pool = nn.MaxPool2d(3, stride=2)


        input_shape = input_shape / 2
        # Stage 2
        self.res_block_1 = ResNetBlock(in_channels=base_filter,
                                       out_channel_list=list(total_channel_list[0]),
                                       input_shape=input_shape,
                                       num_ident_blocks=2,
                                       kernel_size=kernel_size,
                                       padding='same',
                                       conv_block_stride=general_stride,
                                       general_stride=general_stride,
                                       dilation=dilation,
                                       batch_normalization=batch_normalization,
                                       instance_normalization=instance_normalization,
                                       normalization_momentum=normalization_momentum,
                                       norm_track_running_states=norm_track_running_states,
                                       activation='relu')

        input_shape = input_shape / 2
        # Stage 3
        self.res_block_2 = ResNetBlock(in_channels=total_channel_list[0][-1],
                                       out_channel_list=list(total_channel_list[1]),
                                       input_shape=input_shape,
                                       num_ident_blocks=3,
                                       kernel_size=kernel_size,
                                       padding='same',
                                       conv_block_stride=conv_block_stride,
                                       general_stride=general_stride,
                                       dilation=dilation,
                                       batch_normalization=batch_normalization,
                                       instance_normalization=instance_normalization,
                                       normalization_momentum=normalization_momentum,
                                       norm_track_running_states=norm_track_running_states,
                                       activation='relu')


        input_shape = input_shape / 2
        # Stage 4
        num_ident_blocks = {'resnet50': 5, 'resnet101': 22}[architecture]

        self.res_block_3 = ResNetBlock(in_channels=total_channel_list[1][-1],
                                       out_channel_list=list(total_channel_list[2]),
                                       input_shape=input_shape,
                                       num_ident_blocks=num_ident_blocks,
                                       kernel_size=kernel_size,
                                       padding='same',
                                       conv_block_stride=conv_block_stride,
                                       general_stride=general_stride,
                                       dilation=dilation,
                                       batch_normalization=batch_normalization,
                                       instance_normalization=instance_normalization,
                                       normalization_momentum=normalization_momentum,
                                       norm_track_running_states=norm_track_running_states,
                                       activation='relu')

        input_shape = input_shape / 2
        # Stage 5
        self.res_block_4 = ResNetBlock(in_channels=total_channel_list[2][-1],
                                       out_channel_list=list(total_channel_list[3]),
                                       input_shape=input_shape,
                                       num_ident_blocks=2,
                                       kernel_size=kernel_size,
                                       padding='same',
                                       conv_block_stride=conv_block_stride,
                                       general_stride=general_stride,
                                       dilation=dilation,
                                       batch_normalization=batch_normalization,
                                       instance_normalization=instance_normalization,
                                       normalization_momentum=normalization_momentum,
                                       norm_track_running_states=norm_track_running_states,
                                       activation='relu')

        input_shape = input_shape / 2

        self.avg_pooling = nn.AvgPool2d((1, 1))

        num_fully_connected_input_features = total_channel_list[3][-1]

        self.fully_connected_layer = nn.Linear(in_features=num_fully_connected_input_features,
                                               out_features=num_fully_connected_features)

        self.classify_layer = nn.Linear(in_features=num_fully_connected_features,
                                        out_features=num_classes)

        self.final_activation = ActivationLayer('softmax', dim=1)


    def forward(self, x):
        # Stage 1
        x1 = self.init_padding(x)
        x2 = self.init_conv(x1)
        x3 = self.init_max_pool_padding(x2)
        C1 = x = self.init_max_pool(x3)


        # Stage 2
        C2 = x = self.res_block_1(x)

        # Stage 3
        C3 = x = self.res_block_2(x)

        # Stage 4
        C4 = x = self.res_block_3(x)

        # Stage 5
        C5 = x = self.res_block_4(x)

        if self.classify is True:
            x = self.avg_pooling(x)
            x = self.fully_connected_layer(x)
            x = self.classify_layer(x)
            x = self.final_activation(x)

            if self.return_output_layer_blocks is True:
                return [x, C1, C2, C3, C4, C5]

            else:
                return x
        else:
            return [C1, C2, C3, C4, C5]


class ResNetBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channel_list,
                 input_shape,
                 num_ident_blocks,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 conv_block_stride=2,
                 general_stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 max_pooling=True,
                 activation='relu',
                 ):
        super(ResNetBlock, self).__init__()

        self.ident_block_list = nn.ModuleList()

        conv_block = ConvBlock(in_channels=in_channels,
                               out_channel_list=out_channel_list,
                               input_shape=input_shape,
                               kernel_size=kernel_size,
                               stride=conv_block_stride,
                               padding=padding,
                               dilation=dilation,
                               batch_normalization=batch_normalization,
                               instance_normalization=instance_normalization,
                               normalization_momentum=normalization_momentum,
                               norm_track_running_states=norm_track_running_states,
                               activation=activation)

        input_shape = input_shape / 2

        self.conv_block = conv_block

        for idx in range(num_ident_blocks):

            ident_block = IdentityBlock(in_channels=out_channel_list[-1],
                                        out_channel_list=out_channel_list,
                                        input_shape=input_shape,
                                        kernel_size=kernel_size,
                                        stride=general_stride,
                                        padding=padding,
                                        dilation=dilation,
                                        batch_normalization=batch_normalization,
                                        instance_normalization=instance_normalization,
                                        normalization_momentum=normalization_momentum,
                                        norm_track_running_states=norm_track_running_states,
                                        activation=activation)

            self.ident_block_list.append(ident_block)


        # self.ident_block = nn.Sequential(*ident_block_list)

        # self.total_conv = nn.Sequential(conv_block, *ident_block_list)

    def forward(self, x):
        # x = self.total_conv(x)

        x = self.conv_block(x)
        for ident_block in self.ident_block_list:
            x = ident_block(x)

        return x


class IdentityBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channel_list,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 max_pooling=True,
                 activation='relu',
                 residuals=False
                 ):
        super(IdentityBlock, self).__init__()

        in_channels = int(in_channels)

        out_channel_1, out_channel_2, out_channel_3 = out_channel_list

        out_channel_1 = int(out_channel_1)
        out_channel_2 = int(out_channel_2)
        out_channel_3 = int(out_channel_3)

        self.conv1 = Conv2D(in_channels=in_channels,
                            out_channels=out_channel_1,
                            kernel_size=1,
                            stride=stride,
                            dilation=dilation,
                            batch_normalization=batch_normalization,
                            instance_normalization=instance_normalization,
                            normalization_momentum=normalization_momentum,
                            norm_track_running_states=norm_track_running_states,
                            activation=activation,
                            padding=padding,
                            input_shape=input_shape
                            )

        self.conv2 = Conv2D(in_channels=out_channel_1,
                            out_channels=out_channel_2,
                            kernel_size=kernel_size,
                            stride=stride,
                            dilation=dilation,
                            batch_normalization=batch_normalization,
                            normalization_momentum=normalization_momentum,
                            activation=activation,
                            padding=padding,
                            input_shape=input_shape
                            )


        self.conv3 = Conv2D(in_channels=out_channel_2,
                            out_channels=out_channel_3,
                            kernel_size=1,
                            stride=stride,
                            dilation=dilation,
                            batch_normalization=batch_normalization,
                            normalization_momentum=normalization_momentum,
                            activation=None,
                            padding=padding,
                            input_shape=input_shape
                            )

        self.final_act = ActivationLayer('relu')

    def forward(self, x):

        x1 = self.conv1(x)
        x2 = self.conv2(x1)
        x3 = self.conv3(x2)

        x = torch.add(x3, x)

        x = self.final_act(x)
        return x


class ConvBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channel_list,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=2,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 max_pooling=True,
                 activation='relu',
                 ):
        super(ConvBlock, self).__init__()

        in_channels = int(in_channels)

        out_channel_1, out_channel_2, out_channel_3 = out_channel_list

        out_channel_1 = int(out_channel_1)
        out_channel_2 = int(out_channel_2)
        out_channel_3 = int(out_channel_3)

        self.conv1 = Conv2D(in_channels=in_channels,
                            out_channels=out_channel_1,
                            kernel_size=1,
                            stride=stride,
                            dilation=dilation,
                            batch_normalization=batch_normalization,
                            instance_normalization=instance_normalization,
                            normalization_momentum=normalization_momentum,
                            norm_track_running_states=norm_track_running_states,
                            activation=activation,
                            padding=padding,
                            input_shape=input_shape
                            )

        output_shape = input_shape / 2

        self.conv2 = Conv2D(in_channels=out_channel_1,
                            out_channels=out_channel_2,
                            kernel_size=kernel_size,
                            stride=1,
                            dilation=dilation,
                            batch_normalization=batch_normalization,
                            normalization_momentum=normalization_momentum,
                            activation=activation,
                            padding=padding,
                            input_shape=output_shape
                            )


        self.conv3 = Conv2D(in_channels=out_channel_2,
                            out_channels=out_channel_3,
                            kernel_size=1,
                            stride=1,
                            dilation=dilation,
                            batch_normalization=batch_normalization,
                            normalization_momentum=normalization_momentum,
                            activation=None,
                            padding=padding,
                            input_shape=output_shape
                            )

        self.short_cut_conv = Conv2D(in_channels=in_channels,
                                     out_channels=out_channel_3,
                                     kernel_size=1,
                                     stride=stride,
                                     dilation=dilation,
                                     batch_normalization=batch_normalization,
                                     normalization_momentum=normalization_momentum,
                                     activation=None,
                                     padding=padding,
                                     input_shape=input_shape
                                     )

        self.final_act = ActivationLayer('relu')

    def forward(self, x):

        x1 = self.conv1(x)
        x2 = self.conv2(x1)
        x3 = self.conv3(x2)

        short_cut = self.short_cut_conv(x)

        x = torch.add(x3, short_cut)

        x = self.final_act(x)
        return x
