import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
from Models.model_core import *


class ResNetBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channel_list,
                 input_shape,
                 num_ident_blocks,
                 dim=3,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 conv_block_stride=2,
                 general_stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 max_pooling=True,
                 activation='relu',
                 ):
        super(ResNetBlock, self).__init__()

        self.ident_block_list = nn.ModuleList()

        conv_block = ConvBlock(in_channels=in_channels,
                               out_channels=out_channel_list,
                               dim=dim,
                               input_shape=input_shape,
                               kernel_size=kernel_size,
                               stride=conv_block_stride,
                               padding=padding,
                               dilation=dilation,
                               batch_normalization=batch_normalization,
                               instance_normalization=instance_normalization,
                               normalization_momentum=normalization_momentum,
                               norm_track_running_states=norm_track_running_states,
                               activation=activation)

        input_shape = input_shape / 2

        self.conv_block = conv_block

        for idx in range(num_ident_blocks):

            ident_block = IdentityBlock(in_channels=out_channel_list[-1],
                                        out_channel_list=out_channel_list,
                                        input_shape=input_shape,
                                        kernel_size=kernel_size,
                                        stride=general_stride,
                                        padding=padding,
                                        dilation=dilation,
                                        batch_normalization=batch_normalization,
                                        instance_normalization=instance_normalization,
                                        normalization_momentum=normalization_momentum,
                                        norm_track_running_states=norm_track_running_states,
                                        activation=activation)

            self.ident_block_list.append(ident_block)


        # self.ident_block = nn.Sequential(*ident_block_list)

        # self.total_conv = nn.Sequential(conv_block, *ident_block_list)

    def forward(self, x):
        # x = self.total_conv(x)

        x = self.conv_block(x)
        for ident_block in self.ident_block_list:
            x = ident_block(x)

        return x


class IdentityBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 dim=3,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 max_pooling=True,
                 activation='relu',
                 residuals=False
                 ):
        super(IdentityBlock, self).__init__()

        in_channels = int(in_channels)

        if isinstance(out_channels, list) or isinstance(out_channels, tuple):

            out_channel_1, out_channel_2, out_channel_3 = out_channels
        else:
            out_channel_1, out_channel_2, out_channel_3 = out_channels, out_channels, out_channels

        out_channel_1 = int(out_channel_1)
        out_channel_2 = int(out_channel_2)
        out_channel_3 = int(out_channel_3)

        conv_block = Conv3D if dim == 3 else Conv2D

        out_channel_1 = int(out_channel_1)
        out_channel_2 = int(out_channel_2)
        out_channel_3 = int(out_channel_3)

        self.conv1 = conv_block(in_channels=in_channels,
                                out_channels=out_channel_1,
                                kernel_size=1,
                                stride=stride,
                                dilation=dilation,
                                batch_normalization=batch_normalization,
                                instance_normalization=instance_normalization,
                                normalization_momentum=normalization_momentum,
                                norm_track_running_states=norm_track_running_states,
                                activation=activation,
                                padding=padding,
                                input_shape=input_shape,
                                )

        self.conv2 = conv_block(in_channels=out_channel_1,
                                out_channels=out_channel_2,
                                kernel_size=kernel_size,
                                stride=stride,
                                dilation=dilation,
                                batch_normalization=batch_normalization,
                                normalization_momentum=normalization_momentum,
                                activation=activation,
                                padding=padding,
                                input_shape=input_shape,
                                )


        self.conv3 = conv_block(in_channels=out_channel_2,
                                out_channels=out_channel_3,
                                kernel_size=1,
                                stride=stride,
                                dilation=dilation,
                                batch_normalization=batch_normalization,
                                normalization_momentum=normalization_momentum,
                                activation=None,
                                padding=padding,
                                input_shape=input_shape,
                                )

        self.final_act = ActivationLayer('relu')

    def forward(self, x):

        x1 = self.conv1(x)
        x2 = self.conv2(x1)
        x3 = self.conv3(x2)

        x = torch.add(x3, x)

        x = self.final_act(x)
        return x


class ConvBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 dim=3,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=2,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.1,
                 norm_track_running_states=False,
                 max_pooling=True,
                 activation='relu',
                 ):
        super(ConvBlock, self).__init__()

        in_channels = int(in_channels)

        if isinstance(out_channels, list) or isinstance(out_channels, tuple):

            out_channel_1, out_channel_2, out_channel_3 = out_channels
        else:
            out_channel_1, out_channel_2, out_channel_3 = out_channels, out_channels, out_channels

        out_channel_1 = int(out_channel_1)
        out_channel_2 = int(out_channel_2)
        out_channel_3 = int(out_channel_3)

        conv_block = Conv3D if dim == 3 else Conv2D

        self.conv1 = conv_block(in_channels=in_channels,
                                out_channels=out_channel_1,
                                kernel_size=1,
                                stride=stride,
                                dilation=dilation,
                                batch_normalization=batch_normalization,
                                instance_normalization=instance_normalization,
                                normalization_momentum=normalization_momentum,
                                norm_track_running_states=norm_track_running_states,
                                activation=activation,
                                padding=padding,
                                input_shape=input_shape,
                                )

        output_shape = input_shape / 2

        self.conv2 = conv_block(in_channels=out_channel_1,
                                out_channels=out_channel_2,
                                kernel_size=kernel_size,
                                stride=1,
                                dilation=dilation,
                                batch_normalization=batch_normalization,
                                normalization_momentum=normalization_momentum,
                                activation=activation,
                                padding=padding,
                                input_shape=output_shape,
                                )


        self.conv3 = conv_block(in_channels=out_channel_2,
                                out_channels=out_channel_3,
                                kernel_size=1,
                                stride=1,
                                dilation=dilation,
                                batch_normalization=batch_normalization,
                                normalization_momentum=normalization_momentum,
                                activation=None,
                                padding=padding,
                                input_shape=output_shape,
                                )

        self.short_cut_conv = conv_block(in_channels=in_channels,
                                         out_channels=out_channel_3,
                                         kernel_size=1,
                                         stride=stride,
                                         dilation=dilation,
                                         batch_normalization=batch_normalization,
                                         normalization_momentum=normalization_momentum,
                                         activation=None,
                                         padding=padding,
                                         input_shape=input_shape,
                                         )

        self.final_act = ActivationLayer('relu')

    def forward(self, x):

        x1 = self.conv1(x)
        x2 = self.conv2(x1)
        x3 = self.conv3(x2)

        short_cut = self.short_cut_conv(x)

        x = torch.add(x3, short_cut)

        x = self.final_act(x)
        return x