import torch
import torch.nn as nn
from Models.model_core import *


class GenerateRPNProposals(nn.Module):
    """Given the anchors and GT boxes, compute overlaps and identify positive
    anchors and deltas to refine them to match their corresponding GT boxes.

    anchors: [num_anchors, (x1, y1, x2, y2)]
    gt_class_ids: [num_gt_boxes] Integer class IDs.
    gt_boxes: [num_gt_boxes, (x1, y1, x2, y2)]

    params : Parameters.ParameterBuilder.Parameters

    Returns:
    rpn_match: [N] (int32) matches between anchors and GT boxes.
               1 = positive anchor, -1 = negative anchor, 0 = neutral
    rpn_class_match: [N] (int32) class that corresponds to the positive or neutral anchor overlaps.
    rpn_bbox: [N, (dx, dy, log(dw), log(dh))] Anchor bbox deltas.
    """
    def __init__(self,
                 params,
                 ):
        """

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """
        super(GenerateRPNProposals, self).__init__()
        self.params = params

    def forward(self, gt_boxes, gt_class_ids, anchors):


            rpn_match = torch.zeros((anchors.shape[0], anchors.shape[1]),
                                    dtype=torch.int8, device=gt_boxes.device)

            # RPN bounding boxes: [max anchors per image, (dx, dy, log(dw), log(dh))]
            rpn_bbox = torch.zeros((gt_boxes.shape[0], self.params.rpn_train_anchors_per_image, 4),
                                   dtype=torch.float32, device=gt_boxes.device)

            rpn_class_ids = torch.zeros((gt_class_ids.shape[0], self.params.rpn_train_anchors_per_image),
                                        dtype=torch.float32, device=gt_boxes.device)

            # # Reshape gt_boxes to work with the overlap function.
            # gt_boxes = torch.reshape(gt_boxes, (gt_boxes.shape[0], -1, gt_boxes.shape[-1]))

            # Compute overlaps [num_anchors, num_gt_boxes]

            # gt_boxes = gt_boxes[:, :12]

            overlaps = compute_overlap(anchors, gt_boxes)

            # Match anchors to GT Boxes
            # If an anchor overlaps a GT box with IoU >= 0.7 then it's positive.
            # If an anchor overlaps a GT box with IoU < 0.3 then it's negative.
            # Neutral anchors are those that don't match the conditions above,
            # and they don't influence the loss function.
            # However, don't keep any GT box unmatched (rare, but happens). Instead,
            # match it to the closest anchor (even if its max IoU is < 0.3).
            #
            # 1. Set negative anchors first. They get overwritten below if a GT box is
            # matched to them. Skip boxes in crowd areas.

            # Determine the best anchor for each gt_bbox
            # anchor_iou_argmax = torch.argmax(overlaps, dim=-1)
            # anchor_iou_max = overlaps[torch.arange(overlaps.shape[0]), anchor_iou_argmax]

            # ie, for each proposal, what was the maximum IoU
            roi_iou_max = torch.max(overlaps, dim=-1)
            anchor_iou_max = roi_iou_max[0]


            # The max IoU for each anchor have already been selected. If the max values are below the specified threshold
            # set the rpn id to -1.

            # rpn_match specifies what bbox is positive, negative or neutral for a particular anchor box.


            rpn_match[(anchor_iou_max < self.params.neg_iou_threshold)] = -1


            # 2. Set an anchor for each GT box (regardless of IoU value).
            # These may be overwritten later if the max value is also above the pos_iou_threshold

            gt_iou_argmax = torch.argmax(overlaps, dim=1)

            gt_iou_argmax_batch = torch.tensor(list(range(gt_iou_argmax.shape[0])) * gt_iou_argmax.shape[1],
                                               device=overlaps.device).reshape(gt_iou_argmax.shape[0], -1)

            idx = (gt_iou_argmax_batch.flatten(), gt_iou_argmax.flatten())

            # rpn_match[idx] = 1


            # 3. Set anchors with high overlap as positive.
            rpn_match[anchor_iou_max >= self.params.pos_iou_threshold] = 1

            for batch_idx in range(self.params.batch_size):
                pos_batch_tot = torch.where(rpn_match[batch_idx] == 1,
                                            torch.tensor(1).to(rpn_match),
                                            torch.tensor(0).to(rpn_match))

                if pos_batch_tot.sum() == 0:
                    for id_idx, id in enumerate(gt_class_ids[batch_idx]):
                        if id == 0:
                            continue
                        rpn_match[batch_idx, gt_iou_argmax[batch_idx, id_idx]] = 1



            # Subsample to balance positive and negative anchors
            # Don't let positives be more than half the anchors. Matters for rpn_match
            ids_positive = torch.where(rpn_match == 1)

            _, positive_amount_per_batch = torch.unique(ids_positive[0], return_counts=True)

            # If more positive anchors exists than the amount of positive anchors per image,
            # randomly set additional ones to zero
            extra_positive = positive_amount_per_batch - (self.params.rpn_train_anchors_per_image // 2)

            for batch_idx, extra_positive_per_batch in enumerate(extra_positive):
                if extra_positive_per_batch > 0:
                    # Reset the extra ones to neutral

                    batch_pos = torch.where(ids_positive[0] == batch_idx)[0]
                    pos_idx_hold = ids_positive[1][batch_pos]
                    random_pos_idx = torch.randint(0, pos_idx_hold.shape[-1], size=(extra_positive_per_batch,))
                    rpn_match[batch_idx, pos_idx_hold[random_pos_idx]] = 0


                    # ids_positive = torch.randint(ids_positive, extra_positive_per_batch)
                    # rpn_match[batch_idx, ids_positive] = 0

            # Same for negative proposals
            ids_negative = torch.where(rpn_match == -1)
            _, negative_amount_per_batch = torch.unique(ids_negative[0], return_counts=True)

            try:
                extra_negative = negative_amount_per_batch - (self.params.rpn_train_anchors_per_image - positive_amount_per_batch)
            except:
                a = 1

            for batch_idx, extra_negative_per_batch in enumerate(extra_negative):
                if extra_negative_per_batch > 0:
                    # Rest the extra ones to neutral

                    batch_neg = torch.where(ids_negative[0] == batch_idx)[0]
                    neg_idx_hold = ids_negative[1][batch_neg]
                    random_neg_idx = torch.randint(0, neg_idx_hold.shape[-1], size=(extra_negative_per_batch,))
                    rpn_match[batch_idx, neg_idx_hold[random_neg_idx]] = 0

            # For positive anchors, compute shift and scale needed to transform them
            # to match the corresponding GT boxes.
            ids = torch.where(rpn_match == 1)
            ix = 0  # index into rpn_bbox

            for batch_idx in range(self.params.batch_size):

                ids_batch = torch.where(ids[0] == batch_idx)

                ids_batch = ids[1][ids_batch]

                a = anchors[batch_idx, ids_batch]


                rpn_class_ids[batch_idx, :a.shape[0]] = gt_class_ids[batch_idx, roi_iou_max[1][batch_idx, ids_batch] ]

                # Closest gt box (it might have IoU < 0.7)

                # gt are in [x1, y1, x2, y2]
                gt = gt_boxes[batch_idx, roi_iou_max[1][batch_idx, ids_batch]]



                # Convert coordinates to center plus width/height.

                # GT Box
                gt_w = gt[:, 2] - gt[:, 0]
                gt_h = gt[:, 3] - gt[:, 1]
                gt_center_x = gt[:, 0] + 0.5 * gt_w
                gt_center_y = gt[:, 1] + 0.5 * gt_h

                # Anchor
                a_w = a[:, 2] - a[:, 0]
                a_h = a[:, 3] - a[:, 1]
                a_center_x = a[:, 0] + 0.5 * a_w
                a_center_y = a[:, 1] + 0.5 * a_h

                # rpn_bbox_delta_hold = torch.stack([(gt_center_x - a_center_x) / a_w,
                #                                    (gt_center_y - a_center_y) / a_h,
                #                                    torch.log(gt_w / a_w),
                #                                    torch.log(gt_h / a_h)], dim=1)
                #
                #
                # rpn_bbox.append(rpn_bbox_delta_hold)

                ix = ix + 1

                rpn_bbox[batch_idx, :a.shape[0]] =  torch.stack([(gt_center_x - a_center_x) / a_w,
                                                                 (gt_center_y - a_center_y) / a_h,
                                                                 torch.log(gt_w / a_w),
                                                                 torch.log(gt_h / a_h)], dim=1)

            # rpn_bbox_2 = torch.cat(rpn_bbox, dim=0)

            rpn_bbox = (rpn_bbox - torch.tensor(self.params.rpn_bbox_avg).to(rpn_bbox)) / torch.tensor(self.params.rpn_bbox_std_dev).to(rpn_bbox)

            return rpn_match, rpn_bbox, rpn_class_ids



def compute_overlap(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (x1, y1, x2, y2)].
    For better performance, pass the largest set first and the smaller second.
    """

    # Reshape and tile so every coordinate pair in box1 can be compared
    # to every coordinate pair in box2 without the need of loops.
    # Use rehape and tile since tf does not have something similar to np.repeat

    # Boxes1 and boxes2 are repeated and reshaped so
    b1 = torch.repeat_interleave(boxes1, boxes2.shape[1], dim=1)
    b2 = boxes2.repeat(1, boxes1.shape[1], 1)

    b1_x1, b1_y1, b1_x2, b1_y2 = torch.chunk(b1, 4, dim=-1)
    b2_x1, b2_y1, b2_x2, b2_y2 = torch.chunk(b2, 4, dim=-1)

    y1 = torch.max(b1_y1, b2_y1)
    x1 = torch.max(b1_x1, b2_x1)

    y2 = torch.min(b1_y2, b2_y2)
    x2 = torch.min(b1_x2, b2_x2)

    zero_tensor = torch.zeros(1).to(y1)

    # The use of zero to make sure negatives are not used
    intersection = torch.max(x2 - x1, zero_tensor) * torch.max(y2 - y1, zero_tensor)


    # 3. Compute union between boxes1 and boxes2
    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    union = b1_area + b2_area - intersection

    # Compute intersection-over-union (IoU) and reshape to [boxes1, boxes2]
    iou = intersection / union

    # This type of shape allows for each proposals overlap with every gt bounding box
    overlaps = iou.contiguous().view((iou.shape[0], boxes1.shape[1], boxes2.shape[1]))

    # b1 = boxes1[0]
    #
    # overlaps = []
    #
    # for box2_batch in boxes2:
    #     overlaps_batch = []
    #     for b2 in box2_batch:
    #
    #         b1_x1, b1_y1, b1_x2, b1_y2 = torch.chunk(b1, 4, dim=-1)
    #         b2_x1, b2_y1, b2_x2, b2_y2 = torch.chunk(b2, 4, dim=-1)
    #
    #         y1 = torch.max(b1_y1, b2_y1)
    #         x1 = torch.max(b1_x1, b2_x1)
    #
    #         y2 = torch.min(b1_y2, b2_y2)
    #         x2 = torch.min(b1_x2, b2_x2)
    #
    #         zero_tensor = torch.zeros(1).to(y1)
    #
    #         # The use of zero to make sure negatives are not used
    #         intersection = torch.max(x2 - x1, zero_tensor) * torch.max(y2 - y1, zero_tensor)
    #
    #
    #         # 3. Compute union between boxes1 and boxes2
    #         b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    #         b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)
    #
    #         union = b1_area + b2_area - intersection
    #
    #         # Compute intersection-over-union (IoU) and reshape to [boxes1, boxes2]
    #         iou = intersection / union
    #
    #         # # This type of shape allows for each proposals overlap with every gt bounding box
    #         # overlaps = iou.contiguous().view((iou.shape[0], boxes1.shape[1], boxes2.shape[1]))
    #
    #         overlaps_batch.append(iou)
    #
    #     overlaps.append(torch.cat(overlaps_batch, dim=1))
    #
    # overlaps = torch.stack(overlaps, dim=0)
    #
    # a = 1

    return overlaps

