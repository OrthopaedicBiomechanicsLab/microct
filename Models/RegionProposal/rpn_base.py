import torch
import torch.nn as nn
from Models.model_core import *


class RegionProposalBase(nn.Module):
    def __init__(self,
                 params,
                 input_shapes
                 ):
        super(RegionProposalBase, self).__init__()

        self.num_fpn_feature_maps = params.num_fpn_input_feature_maps + 1

        depth = params.top_down_pyramid_size
        shared_channels = params.rpn_shared_channels
        anchors_per_location = len(params.anchor_ratios)
        anchor_stride = params.anchor_strides

        self.rpn_block_list = nn.ModuleList()

        for idx in range(self.num_fpn_feature_maps):

            rpn_block = RegionProposalBlock(depth=depth,
                                            shared_channels=shared_channels,
                                            anchors_per_location=anchors_per_location,
                                            anchor_stride=anchor_stride,
                                            input_shape=input_shapes[idx])

            self.rpn_block_list.append(rpn_block)

    def forward(self, fpn_feature_list):

        rpn_class_logits_list = []
        rpn_class_probs_list = []
        rpn_bbox_reg_list = []

        for idx in range(self.num_fpn_feature_maps):

            rpn_logits, rpn_probs, rpn_reg = self.rpn_block_list[idx](fpn_feature_list[idx])

            rpn_class_logits_list.append(rpn_logits)
            rpn_class_probs_list.append(rpn_probs)
            rpn_bbox_reg_list.append(rpn_reg)


        rpn_class_logits = torch.cat(rpn_class_logits_list, dim=1)
        rpn_class_probs = torch.cat(rpn_class_probs_list, dim=1)
        rpn_bbox_reg = torch.cat(rpn_bbox_reg_list, dim=1)


        return rpn_class_logits, rpn_class_probs, rpn_bbox_reg



class RegionProposalBlock(nn.Module):
    def __init__(self,
                 depth,
                 shared_channels,
                 anchors_per_location,
                 anchor_stride,
                 input_shape):
        super(RegionProposalBlock, self).__init__()

        self.shared_conv = Conv2D(in_channels=depth,
                                  out_channels=shared_channels,
                                  kernel_size=3,
                                  input_shape=input_shape,
                                  stride=anchor_stride,
                                  batch_normalization=False,
                                  padding='same',
                                  activation=None)


        # self.rpn_class_conv = Conv2D(in_channels=shared_channels,
        #                              out_channels=2 * anchors_per_location,
        #                              kernel_size=1,
        #                              stride=1,
        #                              batch_normalization=False,
        #                              padding=None,
        #                              activation=None)

        self.rpn_class_conv = Conv2D(in_channels=shared_channels,
                                     out_channels=anchors_per_location,
                                     kernel_size=1,
                                     stride=1,
                                     batch_normalization=False,
                                     padding=None,
                                     activation=None)

        # self.rpn_class_probs_activation = ActivationLayer('softmax', dim=1)
        self.rpn_class_probs_activation = ActivationLayer('sigmoid')


        self.rpn_bbox_conv = Conv2D(in_channels=shared_channels,
                                    out_channels=anchors_per_location * 4,
                                    kernel_size=1,
                                    stride=1,
                                    batch_normalization=False,
                                    padding=None,
                                    activation=None)
    def forward(self, x):

        """

        Parameters
        ----------
        x : torch.Tensor

        Returns
        -------

        """

        shared = self.shared_conv(x)

        rpn_class_logits = self.rpn_class_conv(shared)
        # rpn_class_logits = rpn_class_logits.contiguous().view((rpn_class_logits.shape[0], -1, 2))

        rpn_class_logits = rpn_class_logits.permute(0, 2, 3, 1).contiguous().view((rpn_class_logits.shape[0], -1))
        # pred.permute(0, 2, 3, 4, 1).contiguous().view(-1, pred.shape[1])
        rpn_class_probs = self.rpn_class_probs_activation(rpn_class_logits)

        rpn_bbox_reg = self.rpn_bbox_conv(shared)
        rpn_bbox_reg = rpn_bbox_reg.permute(0, 2, 3, 1).contiguous().view(rpn_bbox_reg.shape[0], -1, 4)
        # rpn_bbox_reg = rpn_bbox_reg.contiguous().view((rpn_bbox_reg.shape[0], -1, 4))

        # return rpn_class_logits, rpn_class_probs, rpn_bbox_reg
        return rpn_class_probs, rpn_class_probs, rpn_bbox_reg

