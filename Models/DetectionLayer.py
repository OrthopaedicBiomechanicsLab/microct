import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Models.model_core import *
import Models.utils as utils


class DetectionLayer(nn.Module):
    """Refine classified proposals and filter overlaps and return final
    detections.
    Inputs:
    :param rois: [N, (x1, y1, x2, y2)] in normalized coordinates
    :param probs: [N, num_classes]. Class probabilities.
    :param deltas: [N, num_classes, (dx, dy, log(dw), log(dh))]. Class-specific
                bounding box deltas.
    :param window: (x1, y1, x2, y2) in image coordinates. The part of the image
            that contains the image excluding the padding.
    Returns
    :returns detections shaped: [N, (x1, y1, x2, y2, class_id, score)] where
        coordinates are normalized.
    """

    def __init__(self, params):
        """
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """
        super(DetectionLayer, self).__init__()

        self.params = params


    def forward(self, rois, probs, deltas):

        # Class IDs per ROI
        class_ids = torch.argmax(probs, dim=-1, keepdim=True)

        # Class probability of the top class of each ROI
        # For the index of each ROI, what is the predicted class
        # indices = tf.stack([class_ids, tf.range(probs.shape[1])], axis=1)
        indices_stacked = torch.stack([class_ids] * 4, dim=3)

        # For each index and predicted class, what is the probability score for each ROI being its predicted class
        # class_scores = tf.gather_nd(probs, indices)

        class_scores = torch.gather(probs, dim=2, index=class_ids).squeeze(-1)

        # Class-specific bounding box deltas
        # What are the predicted deltas that correspond to the class with the highest probability for each ROI
        deltas_specific = torch.gather(deltas, dim=2, index=indices_stacked).squeeze(-2)

        # Apply bounding box deltas
        # Shape: [boxes, (x1, y1, x2, y2)] in normalized coordinates
        # For each of the ROI, apply the class-specific deltas determined above to each ROI-proposals
        refined_rois = utils.apply_box_deltas_torch(
            rois,
            (deltas_specific * torch.tensor(self.params.bbox_std_dev).to(deltas_specific)) + torch.tensor(self.params.rpn_bbox_avg).to(deltas_specific))

        return refined_rois, class_scores

        # # Clip boxes to image window
        # refined_rois = clip_boxes_graph(refined_rois, window)

        # Filter out background boxes
        # Determine the indices for the boxes and classes that are not for the background
        keep = torch.where(class_ids.squeeze() > 0)

        keep_hold = torch.stack(keep, dim=1)

        # Filter out low confidence boxes
        # Remove boxes with probability scores below 0.7, or what config.DETECTION_MIN_CONFIDENCE is.
        if self.params.detect_min_confidence:
            # Determine indices for the scores that meet this threshold

            # Output is a tuple. First is batch dimension. Second is sample.
            conf_keep = torch.where(class_scores >= self.params.detect_min_confidence)

            conf_keep_hold = torch.stack(conf_keep, dim=1)

            # Determine the ROI box and class indices that are both above the DETECTION_MIN_CONFIDENCE threshold
            # and are not background.
            # Need to do intersection because it is possible that output has background sample above threshold.

            total_keep = []
            for conf_keep_element in conf_keep_hold:
                if conf_keep_element in keep_hold:
                    total_keep.append(conf_keep_element)


            keep = torch.stack(total_keep, dim=0)

        # Apply per-class NMS
        # 1. Prepare variables
        # Gather indices of class_ids, scores and ROIs (RPN-proposals with the deltas applied) of non-background
        # ROIs that surpass the DETECTION_MIN_CONFIDENCE (usually 0.7) threshold.

        keep_splice = (keep[:, 0], keep[:, 1])


        pre_nms_class_ids = class_ids.squeeze()[keep_splice]
        pre_nms_scores = class_scores[keep_splice]
        pre_nms_rois = refined_rois[keep_splice]

        # pre_nms_class_ids = torch.gather(class_ids.squeeze(), dim=1, index=keep)
        # pre_nms_scores = torch.gather(class_scores, dim=1, index=keep)
        # pre_nms_rois = torch.gather(refined_rois, keep)

        batch_idx = keep[:, 0]

        # Determine which classes are left after above filtering.
        # Ideally, all classes should be found here
        unique_pre_nms_class_ids = torch.unique(pre_nms_class_ids)

        # Define the Non-Max Suppression for filtering overlapping bounding boxes
        def nms_keep_map(class_id):
            """Apply Non-Maximum Suppression on ROIs of the given class."""

            # Indices of ROIs of the given class is class_id.
            # class_id is a single class_id based on the unique class_ids that passed the above threshold requirements.
            ixs = tf.where(tf.equal(pre_nms_class_ids, class_id))[:, 0]

            # Apply NMS
            # Perform the NMS for the ROIS for the particular class_id
            class_keep = tf.image.non_max_suppression(
                tf.gather(pre_nms_rois, ixs),
                tf.gather(pre_nms_scores, ixs),
                max_output_size=config.DETECTION_MAX_INSTANCES,
                iou_threshold=config.DETECTION_NMS_THRESHOLD)

            # Map indices
            # Collect the indices based on the NMS and the above requirements. Need to as keep from above holds to true
            # indices for each ROI. class_keep has the indices based on ixs which does not hold the true ROI indices.
            class_keep = tf.gather(keep, tf.gather(ixs, class_keep))

            # Pad with -1 so returned tensors have the same shape
            # Determine if padding is necessary and pad class_keep with -1 constant values to keep consistent output size
            gap = config.DETECTION_MAX_INSTANCES - tf.shape(class_keep)[0]

            class_keep = tf.pad(class_keep, [(0, gap)], mode='CONSTANT', constant_values=-1)

            # Set shape so map_fn() can infer result shape
            class_keep.set_shape([config.DETECTION_MAX_INSTANCES])
            return class_keep

        # 2. Map over class IDs
        nms_keep = tf.map_fn(nms_keep_map, unique_pre_nms_class_ids, dtype=tf.int32)

        # 3. Merge results into one list, and remove -1 padding
        nms_keep = tf.reshape(nms_keep, [-1])
        nms_keep = tf.gather(nms_keep, tf.where(nms_keep > -1)[:, 0])

        # 4. Compute intersection between keep and nms_keep
        # Don't believe this is necessary as nms_keep is already based on keep. What this seems to do is re-order nms_keep.
        keep = tf.sets.set_intersection(tf.expand_dims(keep, 0), tf.expand_dims(nms_keep, 0))
        keep = tf.sparse_tensor_to_dense(keep)[0]

        # Keep top detections
        # changed from keep to nms_keep as the above set_intersection with keep and nms_keep is commented out
        # Determine the size of output with roi_count
        roi_count = config.DETECTION_MAX_INSTANCES

        # Gather class_scores based keep
        class_scores_keep = tf.gather(class_scores, keep)

        # Determine if the amount of ROIs or maximum ROI amount is lower. IE, see if padding is needed or if sampling from
        # ROIs is needed if too many.
        num_keep = tf.minimum(tf.shape(class_scores_keep)[0], roi_count)

        # Get the indices of the top scoring ROIs. If less than roi_count, will just return the same as before, just sorted
        top_ids = tf.nn.top_k(class_scores_keep, k=num_keep, sorted=True)[1]
        # Gather top scoring ROIs source indices
        keep = tf.gather(keep, top_ids)

        # Arrange output as [N, (y1, x1, y2, x2, class_id, score)]
        # Coordinates are normalized.
        detections = tf.concat([
            tf.gather(refined_rois, keep),
            tf.to_float(tf.gather(class_ids, keep))[..., tf.newaxis],
            tf.gather(class_scores, keep)[..., tf.newaxis]
        ], axis=1)

        # Pad with zeros if detections < DETECTION_MAX_INSTANCES
        gap = config.DETECTION_MAX_INSTANCES - tf.shape(detections)[0]
        detections = tf.pad(detections, [(0, gap), (0, 0)], "CONSTANT")
        return detections

