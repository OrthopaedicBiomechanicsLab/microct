import torch
import torch.nn as nn
from torch.nn import functional as F
from Models.weight_initializer import init_weights
from .GridAttentionBlock import GridAttentionBlock3D

class MultiAttentionBlock(nn.Module):
    def __init__(self, in_channels, gate_size, inter_size, non_local_mode, sub_sample_factor,
                 num_gate_blocks=1,
                 align_corners=True,
                 init_weights_type='kaiming'):

        super(MultiAttentionBlock, self).__init__()

        self.num_gate_blocks = num_gate_blocks

        assert self.num_gate_blocks in [1, 2]

        self.gate_block_1 = GridAttentionBlock3D(in_channels=in_channels, gating_channels=gate_size,
                                                 inter_channels=inter_size, non_local_mode=non_local_mode,
                                                 sub_sample_factor=sub_sample_factor,
                                                 init_weights_type=init_weights_type,
                                                 align_corners=align_corners)

        self.gate_block_2 = GridAttentionBlock3D(in_channels=in_channels, gating_channels=gate_size,
                                                 inter_channels=inter_size, non_local_mode=non_local_mode,
                                                 sub_sample_factor=sub_sample_factor,
                                                 init_weights_type=init_weights_type,
                                                 align_corners=align_corners)

        if self.num_gate_blocks == 1:

            self.combine_gates = nn.Sequential(nn.Conv3d(in_channels, in_channels,
                                                         kernel_size=1, stride=1, padding=0),
                                               nn.BatchNorm3d(in_channels),
                                               nn.ReLU(inplace=True)
                                               )

        elif self.num_gate_blocks == 2:
            self.combine_gates = nn.Sequential(nn.Conv3d(in_channels * 2, in_channels,
                                                         kernel_size=1, stride=1, padding=0),
                                               nn.BatchNorm3d(in_channels),
                                               nn.ReLU(inplace=True)
                                               )

        # # initialise the blocks
        # for m in self.children():
        #     if m.__class__.__name__.find('GridAttentionBlock3D') != -1:
        #         continue
        #     init_weights(m, init_type=init_weights_type)

    def forward(self, input, gating_signal):
        if self.num_gate_blocks == 2:
            gate_1, attention_1 = self.gate_block_1(input, gating_signal)
            gate_2, attention_2 = self.gate_block_2(input, gating_signal)
            return self.combine_gates(torch.cat([gate_1, gate_2], 1)), torch.cat([attention_1, attention_2], 1)
        elif self.num_gate_blocks == 1:
            gate_1, attention_1 = self.gate_block_1(input, gating_signal)
            return self.combine_gates(gate_1), attention_1
