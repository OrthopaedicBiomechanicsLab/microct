import torch
import torch.nn as nn
from torch.nn import functional as F
from Models.weight_initializer import init_weights



class GridAttentionBlockND(nn.Module):
    def __init__(self, in_channels,
                 gating_channels,
                 inter_channels=None,
                 dimension=3,
                 non_local_mode='concatenation',
                 sub_sample_factor=(2, 2, 2),
                 init_weights_type='kaming',
                 align_corners=True):

        super(GridAttentionBlockND, self).__init__()

        assert dimension in [2, 3]
        assert non_local_mode in ['concatenation', 'concatenation_debug', 'concatenation_residual']

        # Down-sampling rate for the input featur emap
        if isinstance(sub_sample_factor, tuple):
            self.sub_sample_factor = sub_sample_factor
        elif isinstance(sub_sample_factor, list):
            self.sub_sample_factor = tuple(sub_sample_factor)
        else:
            self.sub_sample_factor = tuple([sub_sample_factor]) * dimension

        # Default parameter set
        self.mode = non_local_mode
        self.dimension = dimension
        self.sub_sample_kernel_size = self.sub_sample_factor

        # Number of channels (pixel dimensions)
        self.in_channels = in_channels
        self.gating_channels = gating_channels
        self.inter_channels = inter_channels

        self.align_corners = align_corners

        if self.inter_channels is None:
            self.inter_channels = in_channels // 2
            if self.inter_channels == 0:
                self.inter_channels = 1

        if dimension == 3:
            conv_nd = nn.Conv3d
            bn = nn.BatchNorm3d
            self.upsample_mode = 'trilinear'
        elif dimension == 2:
            conv_nd = nn.Conv2d
            bn = nn.BatchNorm2d
            self.upsample_mode = 'bilinear'
        else:
            raise NotImplemented

        # Output transform
        self.W = nn.Sequential(conv_nd(in_channels=self.in_channels, out_channels=self.in_channels,
                                       kernel_size=1, stride=1, padding=0),
                               bn(self.in_channels),
                               )

        # Theta^T * x_ij + Phi^T * gating_signal + bias


        # Why is theta not a 1x1x1 convolution?
        # Maybe because 2x2x2, but with stride of 2x2x2?
        # Or because of the initial 1x1x1 conv in the UNetGridGatingSignal3 class
        self.theta = conv_nd(in_channels=self.in_channels, out_channels=self.inter_channels,
                             kernel_size=self.sub_sample_kernel_size, stride=self.sub_sample_factor,
                             padding=0, bias=False)

        self.phi = conv_nd(in_channels=self.gating_channels, out_channels=self.inter_channels,
                           kernel_size=1, stride=1, padding=0, bias=True)

        self.psi = conv_nd(in_channels=self.inter_channels, out_channels=1, kernel_size=1, stride=1,
                           padding=0, bias=True)

        # Initialise weights
        for m in self.children():
            init_weights(m, init_type=init_weights_type)

        # # Define the operation
        # if non_local_mode == 'concatenation':
        #     self.operation_function = self._concatenation
        # elif non_local_mode == 'concatenation_debug':
        #     self.operation_function = self._concatenation_debug
        # elif non_local_mode == 'concatenation_residual':
        #     self.operation_function = self._concatenation_residual
        # else:
        #     raise NotImplementedError('Unknown operation function.')

    # def forward(self, x, g):
    #     '''
    #     :param x: (b, c, t, h, w)
    #     :param g: (b, g_d)
    #     :return:
    #     '''
    #
    #     output = self.operation_function(x, g)
    #     return output


    def forward(self, x, g):
        input_size = x.size()
        batch_size = input_size[0]
        assert batch_size == g.size(0)

        # theta => (b, c, t, h, w) -> (b, i_c, t, h, w) -> (b, i_c, thw)
        # phi   => (b, g_d) -> (b, i_c)
        theta_x = self.theta(x)
        theta_x_size = theta_x.size()

        # g (b, c, t', h', w') -> phi_g (b, i_c, t', h', w')
        #  Relu(theta_x + phi_g + bias) -> f = (b, i_c, thw) -> (b, i_c, t/s1, h/s2, w/s3)
        # phi_g = F.upsample(self.phi(g), size=theta_x_size[2:], mode=self.upsample_mode, align_corners=True)

        phi_g = F.interpolate(self.phi(g), size=theta_x_size[2:], mode=self.upsample_mode, align_corners=self.align_corners)

        f = F.relu(theta_x + phi_g, inplace=True)

        #  psi^T * f -> (b, psi_i_c, t/s1, h/s2, w/s3)
        sigm_psi_f = torch.sigmoid(self.psi(f))

        # upsample the attentions and multiply
        # sigm_psi_f = F.upsample(sigm_psi_f, size=input_size[2:], mode=self.upsample_mode)
        sigm_psi_f = F.interpolate(sigm_psi_f, size=input_size[2:], mode=self.upsample_mode, align_corners=self.align_corners)

        y = sigm_psi_f.expand_as(x) * x

        W_y = self.W(y)

        return W_y, sigm_psi_f

    def _concatenation(self, x, g):
        input_size = x.size()
        batch_size = input_size[0]
        assert batch_size == g.size(0)

        # theta => (b, c, t, h, w) -> (b, i_c, t, h, w) -> (b, i_c, thw)
        # phi   => (b, g_d) -> (b, i_c)
        theta_x = self.theta(x)
        theta_x_size = theta_x.size()

        # g (b, c, t', h', w') -> phi_g (b, i_c, t', h', w')
        #  Relu(theta_x + phi_g + bias) -> f = (b, i_c, thw) -> (b, i_c, t/s1, h/s2, w/s3)
        # phi_g = F.upsample(self.phi(g), size=theta_x_size[2:], mode=self.upsample_mode, align_corners=self.align_corners)

        phi_g = F.interpolate(self.phi(g), size=theta_x_size[2:], mode=self.upsample_mode, align_corners=self.align_corners)

        f = F.relu(theta_x + phi_g, inplace=True)

        #  psi^T * f -> (b, psi_i_c, t/s1, h/s2, w/s3)
        sigm_psi_f = torch.sigmoid(self.psi(f))

        # upsample the attentions and multiply
        # sigm_psi_f = F.upsample(sigm_psi_f, size=input_size[2:], mode=self.upsample_mode, align_corners=self.align_corners)
        sigm_psi_f = F.interpolate(sigm_psi_f, size=input_size[2:], mode=self.upsample_mode, align_corners=self.align_corners)

        y = sigm_psi_f.expand_as(x) * x

        W_y = self.W(y)

        return W_y, sigm_psi_f

    def _concatenation_debug(self, x, g):
        input_size = x.size()
        batch_size = input_size[0]
        assert batch_size == g.size(0)

        # theta => (b, c, t, h, w) -> (b, i_c, t, h, w) -> (b, i_c, thw)
        # phi   => (b, g_d) -> (b, i_c)
        theta_x = self.theta(x)
        theta_x_size = theta_x.size()

        # g (b, c, t', h', w') -> phi_g (b, i_c, t', h', w')
        #  Relu(theta_x + phi_g + bias) -> f = (b, i_c, thw) -> (b, i_c, t/s1, h/s2, w/s3)
        phi_g = F.upsample(self.phi(g), size=theta_x_size[2:], mode=self.upsample_mode)
        f = F.softplus(theta_x + phi_g)

        #  psi^T * f -> (b, psi_i_c, t/s1, h/s2, w/s3)
        sigm_psi_f = F.sigmoid(self.psi(f))

        # upsample the attentions and multiply
        sigm_psi_f = F.upsample(sigm_psi_f, size=input_size[2:], mode=self.upsample_mode)
        y = sigm_psi_f.expand_as(x) * x
        W_y = self.W(y)

        return W_y, sigm_psi_f

    def _concatenation_residual(self, x, g):
        input_size = x.size()
        batch_size = input_size[0]
        assert batch_size == g.size(0)

        # theta => (b, c, t, h, w) -> (b, i_c, t, h, w) -> (b, i_c, thw)
        # phi   => (b, g_d) -> (b, i_c)
        theta_x = self.theta(x)
        theta_x_size = theta_x.size()

        # g (b, c, t', h', w') -> phi_g (b, i_c, t', h', w')
        #  Relu(theta_x + phi_g + bias) -> f = (b, i_c, thw) -> (b, i_c, t/s1, h/s2, w/s3)
        phi_g = F.upsample(self.phi(g), size=theta_x_size[2:], mode=self.upsample_mode)
        f = F.relu(theta_x + phi_g, inplace=True)

        #  psi^T * f -> (b, psi_i_c, t/s1, h/s2, w/s3)
        f = self.psi(f).view(batch_size, 1, -1)
        sigm_psi_f = F.softmax(f, dim=2).view(batch_size, 1, *theta_x.size()[2:])

        # upsample the attentions and multiply
        sigm_psi_f = F.upsample(sigm_psi_f, size=input_size[2:], mode=self.upsample_mode)
        y = sigm_psi_f.expand_as(x) * x
        W_y = self.W(y)

        return W_y, sigm_psi_f



class GridAttentionBlock3D(GridAttentionBlockND):
    def __init__(self,
                 in_channels,
                 gating_channels,
                 inter_channels=None,
                 non_local_mode='concatenation',
                 sub_sample_factor=(2, 2, 2),
                 init_weights_type='kaiming',
                 align_corners=True):

        super(GridAttentionBlock3D, self).__init__(in_channels,
                                                   inter_channels=inter_channels,
                                                   gating_channels=gating_channels,
                                                   dimension=3, non_local_mode=non_local_mode,
                                                   sub_sample_factor=sub_sample_factor,
                                                   init_weights_type=init_weights_type,
                                                   align_corners=align_corners,
                                                   )
