import torch.nn as nn


class TimeDistributed(nn.Module):
    def __init__(self, module, batch_dim=0, distributed_dim=1):
        super(TimeDistributed, self).__init__()
        self.module = module
        self.batch_dim = batch_dim
        self.distributed_dim = distributed_dim

    def forward(self, x):

        if len(x.size()) <= 2:
            return self.module(x)

        x_shape = list(x.shape)

        if self.batch_dim > self.distributed_dim:
            reshape_dim = x_shape.pop(self.batch_dim) * x_shape.pop(self.distributed_dim)
        else:
            reshape_dim = x_shape.pop(self.distributed_dim) * x_shape.pop(self.batch_dim)

        reshape_size = [reshape_dim] + x_shape

        # Squash samples and timesteps into a single axis
        x_reshape = x.contiguous().view(reshape_size)
        # x_reshape = x.view(reshape_size)
        # (samples * timesteps, input_size)

        y = self.module(x_reshape)

        y_feature_shape = list(y.shape)
        y_feature_shape.pop(0)

        y = y.contiguous().view((x.shape[self.batch_dim],) + (-1,) + tuple(y_feature_shape))  # (samples, timesteps, output_size)
        # y = y.view((x.shape[self.batch_dim],) + (-1,) + tuple(y_feature_shape))  # (samples, timesteps, output_size)
        return y