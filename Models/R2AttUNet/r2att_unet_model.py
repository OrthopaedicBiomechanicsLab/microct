from ..BaseModel import BaseModel
from .r2att_unet_base import R2AttU_Net
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *
import importlib
import os
import pandas as pd
from torch.cuda.amp import autocast, GradScaler


class R2AttUNetModel(BaseModel):
    def __init__(self, params, logger=None):
        super(R2AttUNetModel, self).__init__(params, logger)


        # BaseModel.__init__(self, params, logger)

        self.model_base_fn = R2AttU_Net
        self.load_model_from_params()

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)
            self.setup_lr_scheduler(params)

        self.df_columns = ['bone_sample', 'dsc']

        self.during_training_df = pd.DataFrame(columns=self.df_columns)


        self.scaler = GradScaler()

    def set_input(self, input, gpu=None):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """

        if gpu is None:

            if len(input.items()) == 1:
                self.image = input['image'].to(self.device)
            else:
                self.image = input['image'].to(self.device)
                self.label = input['label'].to(self.device)


        else:
            if isinstance(gpu, float) or isinstance(gpu, int):
                gpu = 'cuda:{}'.format(gpu)

            if len(input.items()) == 1:
                self.image = input['image'].to(gpu)
            else:
                self.image = input['image'].to(gpu)
                self.label = input['label'].to(gpu)


    def forward(self):

        with autocast():
            self.pred = self.net(self.image)

        if self.net.training is False:
            if self.pred.shape[1] > 1:
                self.pred = torch.softmax(self.pred, dim=1)
                self.pred = torch.argmax(self.pred, dim=1, keepdim=True)


    def backward(self):
        """Calculate model loss"""
        with autocast():
            self.loss = self.calc_loss()

        # self.loss.backward()

        self.scaler.scale(self.loss).backward()

    def optimize_model(self):
        # self.forward()                   # compute prediction
        # self.set_requires_grad(self.net, True)  # enable backprop for D
        # self.optimizer.zero_grad()     # set gradients to zero
        # self.backward()                # calculate gradients
        # self.optimizer.step()          # update weights


        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.scaler.step(self.optimizer)          # update weights

        self.scaler.update()

    def model_specific_training(self, sample, epoch):
        pass


    def reset_model_specific_training(self):
        pass

    def save_in_training_df(self):
        self.during_training_df.to_csv(self.save_df_name + '.csv', index=False)

    def SagShapeLoss(self, **fn_params):
        return BaseLossMetric(sag_shape_loss, device=self.device, **fn_params)
