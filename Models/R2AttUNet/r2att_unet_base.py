import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import init
from Models.model_core import *
from Models.weight_initializer import init_weights


# inspired by: https://github.com/LeeJunHyun/Image_Segmentation/

class R2AttU_Net(nn.Module):
    def __init__(self,params):
        super(R2AttU_Net,self).__init__()


        img_ch = params.num_in_channels
        output_ch = params.num_out_channels
        input_shape = params.input_shape

        base_filter = params.base_filter

        kernel_size = params.kernel_size
        batch_normalization = True#params.batch_normalization
        pool_size = params.pool_size
        stride = params.stride
        dilation = params.dilation
        dropout = params.dropout
        init_weights_type = params.init_weights_type
        t = params.t



        input_shape = np.array(input_shape, dtype = np.float64) 


        self.Maxpool = nn.MaxPool3d(kernel_size=2,stride=2)
        self.Upsample = nn.Upsample(scale_factor=2)
        input_shape *= 2

        self.RRCNN1 = RRCNN_block(input_shape, ch_in=img_ch,ch_out=64, t=t)
        input_shape /= 2

        self.RRCNN2 = RRCNN_block(input_shape, ch_in=64,ch_out=128,t=t)
        input_shape /= 2
        
        self.RRCNN3 = RRCNN_block(input_shape, ch_in=128,ch_out=256,t=t)
        input_shape /= 2
        
        self.RRCNN4 = RRCNN_block(input_shape, ch_in=256,ch_out=512,t=t)
        input_shape /= 2
        
        self.RRCNN5 = RRCNN_block(input_shape, ch_in=512,ch_out=1024,t=t)
        input_shape /= 2
        

        self.Up5 = up_conv(input_shape, ch_in=1024,ch_out=512)
        input_shape *= 2

        self.Att5 = Attention_block(input_shape, F_g=512,F_l=512,F_int=256)
        self.Up_RRCNN5 = RRCNN_block(input_shape, ch_in=1024, ch_out=512,t=t)
        
        self.Up4 = up_conv(input_shape, ch_in=512,ch_out=256)
        input_shape *= 2

        self.Att4 = Attention_block(input_shape, F_g=256,F_l=256,F_int=128)
        self.Up_RRCNN4 = RRCNN_block(input_shape, ch_in=512, ch_out=256,t=t)
        
        self.Up3 = up_conv(input_shape, ch_in=256,ch_out=128)
        input_shape *= 2

        self.Att3 = Attention_block(input_shape, F_g=128,F_l=128,F_int=64)
        self.Up_RRCNN3 = RRCNN_block(input_shape, ch_in=256, ch_out=128,t=t)
        
        self.Up2 = up_conv(input_shape, ch_in=128,ch_out=64)
        input_shape *= 2

        self.Att2 = Attention_block(input_shape, F_g=64,F_l=64,F_int=32)
        self.Up_RRCNN2 = RRCNN_block(input_shape, ch_in=128, ch_out=64,t=t)

        self.Conv_1x1 =Conv3D(in_channels=64,
                       out_channels=output_ch,
                       kernel_size=1,
                       stride=1,
                       padding=0,
                       input_shape=input_shape)
        #self.Conv_1x1 = nn.Conv2d(64,output_ch,kernel_size=1,stride=1,padding=0)


    def forward(self,x):
        # encoding path
        x1 = self.RRCNN1(x)

        x2 = self.Maxpool(x1)
        x2 = self.RRCNN2(x2)
        
        x3 = self.Maxpool(x2)
        x3 = self.RRCNN3(x3)

        x4 = self.Maxpool(x3)
        x4 = self.RRCNN4(x4)

        x5 = self.Maxpool(x4)
        x5 = self.RRCNN5(x5)

        # decoding + concat path
        d5 = self.Up5(x5)
        x4 = self.Att5(g=d5,x=x4)
        d5 = torch.cat((x4,d5),dim=1)
        d5 = self.Up_RRCNN5(d5)
        
        d4 = self.Up4(d5)
        x3 = self.Att4(g=d4,x=x3)
        d4 = torch.cat((x3,d4),dim=1)
        d4 = self.Up_RRCNN4(d4)

        d3 = self.Up3(d4)
        x2 = self.Att3(g=d3,x=x2)
        d3 = torch.cat((x2,d3),dim=1)
        d3 = self.Up_RRCNN3(d3)

        d2 = self.Up2(d3)
        x1 = self.Att2(g=d2,x=x1)
        d2 = torch.cat((x1,d2),dim=1)
        d2 = self.Up_RRCNN2(d2)

        d1 = self.Conv_1x1(d2)

        return d1

class conv_block(nn.Module):
    def __init__(self, input_shape, ch_in,ch_out):
        super(conv_block,self).__init__()
        self.conv = nn.Sequential(
            # nn.Conv2d(ch_in, ch_out, kernel_size=3,stride=1,padding=1,bias=True),
            # nn.BatchNorm2d(ch_out),
            Conv3D(in_channels=ch_in,
                       out_channels=ch_out,
                       kernel_size=3,
                       stride=1,
                       padding=1,
                       input_shape=input_shape,
                       normalization = True),
            nn.ReLU(inplace=True),
            # nn.Conv2d(ch_out, ch_out, kernel_size=3,stride=1,padding=1,bias=True),
            # nn.BatchNorm2d(ch_out),
            Conv3D(in_channels=ch_out,
                       out_channels=ch_out,
                       kernel_size=3,
                       stride=1,
                       padding=1,
                       
                       input_shape=input_shape,
                       normalization = True),
            nn.ReLU(inplace=True)
        )


    def forward(self,x):
        x = self.conv(x)
        return x

class up_conv(nn.Module):
    def __init__(self, input_shape, ch_in,ch_out):
        super(up_conv,self).__init__()
        self.up = nn.Sequential(
            nn.Upsample(scale_factor=2),
            #nn.Conv2d(ch_in,ch_out,kernel_size=3,stride=1,padding=1,bias=True),
		    #nn.BatchNorm2d(ch_out),
			Conv3D(in_channels=ch_in,
                       out_channels=ch_out,
                       kernel_size=3,
                       stride=1,
                       padding=1,
                       
                       input_shape=input_shape,
                       normalization = True),
            nn.ReLU(inplace=True)
        )

    def forward(self,x):
        x = self.up(x)
        return x

class Recurrent_block(nn.Module):
    def __init__(self, input_shape, ch_out,t=2):
        super(Recurrent_block,self).__init__()
        self.t = t
        self.ch_out = ch_out
        self.conv = nn.Sequential(
            #nn.Conv2d(ch_out,ch_out,kernel_size=3,stride=1,padding=1,bias=True),
		    Conv3D(in_channels=ch_out,
                       out_channels=ch_out,
                       kernel_size=3,
                       stride=1,
                       padding=1,
                       
                       input_shape=input_shape,
                       normalization = True),
            #nn.BatchNorm2d(ch_out),
			nn.ReLU(inplace=True)
        )

    def forward(self,x):
        for i in range(self.t):

            if i==0:
                x1 = self.conv(x)
            
            x1 = self.conv(x+x1)
        return x1
        
class RRCNN_block(nn.Module):
    def __init__(self, input_shape, ch_in,ch_out, t=2, init_weights_type='kaiming'):
        super(RRCNN_block,self).__init__()
        self.RCNN = nn.Sequential(
            Recurrent_block(input_shape, ch_out,t=t),
            Recurrent_block(input_shape, ch_out,t=t)
        )
        # self.Conv_1x1 = nn.Conv2d(ch_in,ch_out,kernel_size=1,stride=1,padding=0)
        self.Conv_1x1 = Conv3D(in_channels=ch_in,
                       out_channels=ch_out,
                       kernel_size=1,
                       stride=1,
                       padding=0,
                       input_shape=input_shape,
                       init_weights_type=init_weights_type)

    def forward(self,x):
        x = self.Conv_1x1(x)
        x1 = self.RCNN(x)
        return x+x1


class single_conv(nn.Module):
    def __init__(self,input_shape, ch_in,ch_out):
        super(single_conv,self).__init__()
        self.conv = nn.Sequential(
            #nn.Conv2d(ch_in, ch_out, kernel_size=3,stride=1,padding=1,bias=True),
            #nn.BatchNorm2d(ch_out),
            Conv3D(in_channels=ch_in,
                       out_channels=ch_out,
                       kernel_size=3,
                       stride=1,
                       padding=1,
                       
                       input_shape=input_shape,
                       normalization = True),
            nn.ReLU(inplace=True)
        )

    def forward(self,x):
        x = self.conv(x)
        return x

class Attention_block(nn.Module):
    def __init__(self, input_shape, F_g,F_l,F_int):
        super(Attention_block,self).__init__()
        self.W_g = nn.Sequential(
            #nn.Conv2d(F_g, F_int, kernel_size=1,stride=1,padding=0,bias=True),
            #nn.BatchNorm2d(F_int)

            Conv3D(in_channels=F_g,
                       out_channels=F_int,
                       kernel_size=1,
                       stride=1,
                       padding=0,
                       
                       input_shape=input_shape,
                       normalization = True),
            )
        
        self.W_x = nn.Sequential(
            #nn.Conv2d(F_l, F_int, kernel_size=1,stride=1,padding=0,bias=True),
            #nn.BatchNorm2d(F_int)
            Conv3D(in_channels=F_l,
                       out_channels=F_int,
                       kernel_size=1,
                       stride=1,
                       padding=0,
                       
                       input_shape=input_shape,
                       normalization = True),
        )

        self.psi = nn.Sequential(
            #nn.Conv2d(F_int, 1, kernel_size=1,stride=1,padding=0,bias=True),
            #nn.BatchNorm2d(1),
            Conv3D(in_channels=F_int,
                       out_channels=1,
                       kernel_size=1,
                       stride=1,
                       padding=0,
                       
                       input_shape=input_shape,
                       normalization = True),
            nn.Sigmoid()
        )
        
        self.relu = nn.ReLU(inplace=True)
        
    def forward(self,g,x):
        g1 = self.W_g(g)
        x1 = self.W_x(x)
        psi = self.relu(g1+x1)
        psi = self.psi(psi)

        return x*psi

