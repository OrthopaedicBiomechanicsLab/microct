from ..BaseModel import BaseModel
from .unet_class_patch_base import UNetClassifierPatch
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *

class UNetClassifierPatchModel(BaseModel):
    def __init__(self, params, logger=None):
        # super(UNetModel, self).__init__(params)
        BaseModel.__init__(self, params, logger)

        self.model_base_fn = UNetClassifierPatch
        self.load_model_from_params()

        # self.net = UNet(params)

        # self.multi_class_loss_keys = ['seg', 'class']
        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

        self.vert_encode = {
            'C1': 1,
            'C2': 2,
            'C3': 3,
            'C4': 4,
            'C5': 5,
            'C6': 6,
            'C7': 7,

            'T1': 8,
            'T2': 9,
            'T3': 10,
            'T4': 11,
            'T5': 12,
            'T6': 13,
            'T7': 14,
            'T8': 15,
            'T9': 16,
            'T10': 17,
            'T11': 18,
            'T12': 19,

            'L1': 20,
            'L2': 21,
            'L3': 22,
            'L4': 23,
            'L5': 24,
            'L6': 25}

        self.vert_decode = {
            1: 'C1',
            2: 'C2',
            3: 'C3',
            4: 'C4',
            5: 'C5',
            6: 'C6',
            7: 'C7',

            8: 'T1',
            9: 'T2',
            10: 'T3',
            11: 'T4',
            12: 'T5',
            13: 'T6',
            14: 'T7',
            15: 'T8',
            16: 'T9',
            17: 'T10',
            18: 'T11',
            19: 'T12',

            20: 'L1',
            21: 'L2',
            22: 'L3',
            23: 'L4',
            24: 'L5',
            25: 'L6'}

        seg_tensor_shape = (self.params.batch_size, 1)

        self.seg_pred_blank_input = torch.zeros(seg_tensor_shape + self.params.input_shape, device=self.device,
                                                dtype=torch.float32)
        self.seg_pred_reset = True

        self.gt_value = torch.tensor(0.5, device=self.device, dtype=torch.float32)

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """
        self.image = input['image'].to(self.device)
        self.label = [x.to(self.device) for x in input['label']]
        # self.complete = self.label[2]
        # self.label = input['label'].to(self.device)
        # self.seg_label = label[0].to(self.device)
        # self.class_label = label[1].to(self.device)

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""

        # if self.seg_pred_reset is True:
        #     self.seg_pred_input = self.seg_pred_blank_input
        #     self.seg_pred_reset = False
        # else:
        #     self.seg_pred_input = self.seg_pred_input + self.seg_pred
        #
        # self.seg_pred_input = self.seg_pred_blank_input
        # self.input_image = torch.cat([self.image, self.seg_pred_input], dim=1)

        # self.pred = self.net(self.input_image)
        self.pred = self.net(self.image)
        # self.seg_pred = self.pred[0]
        # self.class_logits = self.pred[1]
        # self.class_pred = self.pred[2]
        # self.stop_logits = self.pred[3]
        # self.stop_pred = self.pred[4]

        # # Reset if patch completed
        # if self.stop_pred[0][1].gt(self.gt_value):
        #     self.seg_pred_reset = True

        # # Reset if patch completed
        # if self.complete.gt(self.gt_value):
        #     self.seg_pred_reset = True


    def backward(self):
        """Calculate UNet model loss"""
        self.loss = self.calc_loss()
        self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.optimizer.step()          # update weights




    # def model_specific_metrics(self, batch_metrics):
    #     pred_label = self.class_pred.argmax(dim=-1)
    #     for single_pred_label in pred_label:
    #         vert_pred = self.vert_decode[int(single_pred_label)]
    #         batch_metrics['pred_vertebra'] = vert_pred
    #     return batch_metrics

    def DSCLoss(self, **fn_params):
        return BaseLossMetric(dsc_loss, pred_index=0, label_index=0, **fn_params).to(self.device)

    def BCELossSeg(self, **fn_params):
        # if self.params.num_classes > 1:
        #     self.logger.info('Number of classes set >1, therefore using cross entropy loss instead.')
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(bce_loss, pred_index=0, label_index=0, **fn_params).to(self.device)

    def BCELoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     self.logger.info('Number of classes set >1, therefore using cross entropy loss instead.')
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(bce_loss, pred_index=0, label_index=0, **fn_params).to(self.device)

    def BCELossClass(self, **fn_params):
        # if self.params.num_classes > 1:
        #     self.logger.info('Number of classes set >1, therefore using cross entropy loss instead.')
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(bce_loss, pred_index=3, label_index=2, **fn_params).to(self.device)

    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, pred_index=0, label_index=0, **fn_params).to(self.device)

    def CONCURRENCY(self, **fn_params):
        return BaseLossMetric(concurrency, pred_index=0, label_index=0, **fn_params).to(self.device)

    def ACCURACYClass(self, **fn_params):
        return BaseLossMetric(accuracy, pred_index=2, label_index=1, **fn_params).to(self.device)

    def ACCURACYComp(self, **fn_params):
        return BaseLossMetric(accuracy, pred_index=4, label_index=2, **fn_params).to(self.device)

    def CrossEntropyLoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     self.logger.info('Number of classes set to 1, therefore using binary cross entropy loss instead')
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(ce_loss, pred_index=1, label_index=1, **fn_params).to(self.device)

    def initialize_weights(self):
        self.net.apply(self.__weights_init)

    def __weights_init(self, m):
        from torch.nn import init
        if isinstance(m, nn.Conv3d):
            init.xavier_normal_(m.weight.data)
            if m.bias is not None:
                # init.constant_(m.bias, 0)
                fan_in, _ = init._calculate_fan_in_and_fan_out(m.weight.data)
                import math
                bound = 1 / math.sqrt(fan_in)
                init.uniform_(m.bias, -bound, bound)










