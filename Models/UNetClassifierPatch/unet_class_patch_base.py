import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from Models.model_core import *

class UNetClassifierPatch(nn.Module):
    def __init__(self, params):
        super(UNetClassifierPatch, self).__init__()

        origin_input_shape = params.input_shape
        num_in_channels = params.num_in_channels
        num_out_channels = params.num_out_channels
        num_classes = params.num_classes
        base_filter = params.base_filter
        unet_depth = params.depth

        kernel_size = params.kernel_size
        batch_normalization = params.batch_normalization
        instance_normalization = params.instance_normalization

        pool_size = params.pool_size
        stride = params.stride
        dilation = params.dilation
        dropout = params.dropout

        residuals = params.residuals
        normalization_momentum = params.normalization_momentum
        norm_track_running_states = params.norm_track_running_states
        num_fcn_blocks = params.num_fcn_blocks
        dense_layer_size = params.dense_layer_size


        self.down_layer_list = nn.ModuleList()
        self.up_layer_list = nn.ModuleList()
        self.class_down_layer_list = nn.ModuleList()

        input_shape = np.array(origin_input_shape)

        # going down
        for layer_depth in range(unet_depth):

            if layer_depth == 0:
                max_pooling = False
                in_channels = num_in_channels
                input_shape = input_shape
            else:
                max_pooling = True
                in_channels = base_filter * (2 ** (layer_depth - 1))
                input_shape = input_shape / 2


            x = UNetConvModule(
                in_channels=in_channels,
                out_channels=base_filter * (2 ** layer_depth),
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout,
                batch_normalization=batch_normalization,
                instance_normalization=instance_normalization,
                normalization_momentum=normalization_momentum,
                norm_track_running_states=norm_track_running_states,
                max_pooling=max_pooling,
                residuals=residuals,
                activation='relu')

            # hold = nn.ModuleList()
            # hold.append(layer_depth, x)
            self.down_layer_list.append(x)

        bottle_neck_shape = input_shape
        # going up
        for layer_depth in range(unet_depth - 2, -1, -1):
            if layer_depth == unet_depth - 2:
                input_shape = input_shape
            else:
                input_shape = input_shape * 2

            x = UNetUpBlock(
                in_channels=base_filter * (2 ** (layer_depth + 1)),
                out_channels=base_filter * (2 ** layer_depth),
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout,
                batch_normalization=batch_normalization,
                instance_normalization=instance_normalization,
                normalization_momentum=normalization_momentum,
                norm_track_running_states=norm_track_running_states,
                residuals=residuals,
                activation='relu')


            self.up_layer_list.append(x)


        # classification FCN

        # in_channels = None
        # out_channels = None
        # class_base_filter = base_filter * (2 ** (unet_depth - 1))
        # for class_layer_depth in range(num_fcn_blocks):
        #     in_channels = int(class_base_filter * (2 ** (class_layer_depth)))
        #     out_channels = int(class_base_filter * (2 ** (class_layer_depth + 1)))
        #     if class_layer_depth == 0:
        #         max_pooling = False
        #         input_shape = bottle_neck_shape
        #     else:
        #         max_pooling = True
        #         input_shape = (input_shape) / 2
        #
        #     if all(input_shape >= 3):
        #         down_kernel_size = 3
        #     elif all(input_shape > 2):
        #         down_kernel_size = 2
        #     else:
        #         down_kernel_size = 1
        #     x = UNetConvModule(
        #         in_channels=in_channels,
        #         out_channels=out_channels,
        #         input_shape=input_shape,
        #         kernel_size=down_kernel_size,
        #         padding='same',
        #         pool_size=pool_size,
        #         stride=stride,
        #         dilation=dilation,
        #         dropout=dropout,
        #         batch_normalization=batch_normalization,
        #         normalization_momentum=normalization_momentum,
        #         max_pooling=max_pooling,
        #         residuals=False,
        #         activation='relu')
        #
        #     self.class_down_layer_list.append(x)

        final_seg_conv = nn.Conv3d(in_channels=base_filter,
                                   out_channels=num_out_channels,
                                   kernel_size=1)
        if num_out_channels == 1:
            output_activation = ActivationLayer('sigmoid')
        elif num_out_channels > 1:
            output_activation = ActivationLayer('softmax', dim=1)
            # output_activation = ActivationLayer('sigmoid')
        else:
            output_activation = ActivationLayer('sigmoid')

        self.seg_output_layer = nn.Sequential(final_seg_conv, output_activation)

        # Classification output block(s)

        # self.final_class_layer = nn.Linear(in_features=class_base_filter * (2 ** (num_fcn_blocks - 1)),
        #                                    out_features=dense_layer_size)

        # n_size = int(np.prod(input_shape) * out_channels)
        #
        # class_dense1 = nn.Linear(in_features=n_size,
        #                         out_features=dense_layer_size)
        # correct_labels1 = ActivationLayer('relu')
        # class_dense2 = nn.Linear(in_features=dense_layer_size,
        #                         out_features=dense_layer_size)
        # correct_labels2 = ActivationLayer('relu')
        # class_final_layer = nn.Linear(in_features=dense_layer_size,
        #                               out_features=num_classes + 1)
        #
        # self.final_class_block = nn.Sequential(class_dense1, correct_labels1,
        #                                        class_dense2, correct_labels2,
        #                                        class_final_layer)
        #
        # self.class_activation = ActivationLayer('softmax', dim=1)


        self.class_block = ClassNetwork(base_filter=base_filter,
                                        num_fcn_blocks=num_fcn_blocks,
                                        input_bottle_neck_shape=bottle_neck_shape,
                                        unet_depth=unet_depth,
                                        dense_layer_size=dense_layer_size,
                                        num_classes=num_classes,
                                        kernel_size=kernel_size,
                                        padding='same',
                                        pool_size=pool_size,
                                        stride=stride,
                                        dilation=dilation,
                                        dropout=dropout,
                                        batch_normalization=batch_normalization,
                                        instance_normalization=instance_normalization,
                                        normalization_momentum=normalization_momentum,
                                        norm_track_running_states=norm_track_running_states,
                                        activation='relu',
                                        residuals=residuals)


        self.stop_block = ClassNetwork(base_filter=base_filter,
                                       num_fcn_blocks=num_fcn_blocks,
                                       input_bottle_neck_shape=bottle_neck_shape,
                                       unet_depth=unet_depth,
                                       dense_layer_size=dense_layer_size,
                                       num_classes=1,
                                       kernel_size=kernel_size,
                                       padding='same',
                                       pool_size=pool_size,
                                       stride=stride,
                                       dilation=dilation,
                                       dropout=dropout,
                                       batch_normalization=batch_normalization,
                                       instance_normalization=instance_normalization,
                                       normalization_momentum=normalization_momentum,
                                       norm_track_running_states=norm_track_running_states,
                                       activation='relu',
                                       residuals=residuals)

    def forward(self, x):
        down_blocks = []
        for layer_depth, down_conv in enumerate(self.down_layer_list):
            x = down_conv(x)
            down_blocks.append(x)


        for layer_depth, up_conv in enumerate(self.up_layer_list):
            x = up_conv(x, down_blocks[-layer_depth - 2])

        final_unet_seg_output = x

        seg_pred = self.seg_output_layer(final_unet_seg_output)

        return seg_pred, None, None, None, None

        class_logits, class_pred = self.class_block(down_blocks[-1])

        stop_logits, stop_pred = self.stop_block(down_blocks[-1])

        # for fcn_depth, down_conv in enumerate(self.class_down_layer_list):
        #     if fcn_depth == 0:
        #         x = down_blocks[-1]
        #     x = down_conv(x)
        #
        # final_class_feature_map = x
        #
        # final_class_feature_map = final_class_feature_map.view((final_class_feature_map.size(0), -1))
        #
        # class_logits = self.final_class_block(final_class_feature_map)
        # class_pred = self.class_activation(class_logits)


        return seg_pred, class_logits, class_pred, stop_logits, stop_pred


class UNetConvModule(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.99,
                 instance_normalization=False,
                 norm_track_running_states=False,
                 max_pooling=True,
                 activation=None,
                 residuals=False):
        """

        Parameters
        ----------
        in_channels
        out_channels
        input_shape
        kernel_size
        padding : int, str
        pool_size
        stride
        dilation
        dropout
        batch_normalization
        normalization_momentum
        max_pooling
        activation
        residuals
        """
        super(UNetConvModule, self).__init__()

        self.residuals = residuals

        # unet_conv_module = []
        self.max_pooling = max_pooling
        if max_pooling:
            self.pooling_layer = nn.MaxPool3d(pool_size)
            # unet_conv_module += [pooling_layer]

        conv1 = Conv3D(in_channels=in_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       instance_normalization=instance_normalization,
                       normalization_momentum=normalization_momentum,
                       norm_track_running_states=norm_track_running_states,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape)

        conv2 = Conv3D(in_channels=out_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       instance_normalization=instance_normalization,
                       normalization_momentum=normalization_momentum,
                       norm_track_running_states=norm_track_running_states,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape)

        unet_conv_module = [conv1, conv2]

        if dropout > 0:
            dp_layer = nn.Dropout3d(dropout)
            unet_conv_module += [dp_layer]

        self.unet_conv_module = nn.Sequential(*unet_conv_module)

        if residuals:
            self.short_cut = Conv3D(in_channels=in_channels,
                                    out_channels=out_channels,
                                    kernel_size=1,
                                    stride=stride,
                                    dilation=dilation,
                                    batch_normalization=batch_normalization,
                                    instance_normalization=instance_normalization,
                                    normalization_momentum=normalization_momentum,
                                    norm_track_running_states=norm_track_running_states,
                                    activation=None,
                                    padding=padding,
                                    input_shape=input_shape)

    def forward(self, x):
        input_tensor = x

        if self.max_pooling:
            input_tensor = self.pooling_layer(x)
        #
        # x = self.conv1(x)
        # x = self.conv2(x)

        x = self.unet_conv_module(input_tensor)
        if self.residuals:
            short_cut = self.short_cut(input_tensor)
            x = short_cut + x
            x = F.relu(x)
        return x

class UNetUpBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.99,
                 norm_track_running_states=False,
                 activation=None,
                 residuals=False):

        super(UNetUpBlock, self).__init__()

        self.up_conv = UpConv3D(in_channels=in_channels,
                                out_channels=out_channels,
                                kernel_size=2,
                                stride=2,
                                dilation=1,
                                input_shape=input_shape,
                                batch_normalization=False,
                                normalization_momentum=normalization_momentum,
                                instance_normalization=instance_normalization,
                                norm_track_running_states=norm_track_running_states)

        # print(sum(p.numel() for p in self.up_conv.parameters()))

        self.conv_block = UNetConvModule(
            in_channels=in_channels,
            out_channels=out_channels,
            input_shape=input_shape * 2,
            kernel_size=kernel_size,
            padding=padding,
            pool_size=pool_size,
            stride=stride,
            dilation=dilation,
            dropout=dropout,
            batch_normalization=batch_normalization,
            instance_normalization=instance_normalization,
            normalization_momentum=normalization_momentum,
            norm_track_running_states=norm_track_running_states,
            max_pooling=False,
            residuals=residuals,
            activation=activation)

    def forward(self, x1, x2):
        """
        Parameters
        ----------
        x1 : torch.Tensor
            Tensor from up_conv layer.
        x2 : torch.Tensor
            Tensor from down_conv layer that gets concatenated with after up convolution.

        Returns
        -------

        """
        x_up = self.up_conv(x1)
        x = torch.cat([x_up, x2], dim=1)
        x = self.conv_block(x)
        return x


class ClassNetwork(nn.Module):
    def __init__(self,
                 base_filter,
                 num_fcn_blocks,
                 input_bottle_neck_shape,
                 unet_depth,
                 dense_layer_size,
                 num_classes,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 instance_normalization=False,
                 normalization_momentum=0.99,
                 norm_track_running_states=False,
                 activation=None,
                 residuals=False):

        super(ClassNetwork, self).__init__()

        input_shape = None
        in_channels = None
        out_channels = None

        self.class_down_layer_list = nn.ModuleList()


        class_base_filter = base_filter * (2 ** (unet_depth - 1))
        for class_layer_depth in range(num_fcn_blocks):
            in_channels = int(class_base_filter * (2 ** (class_layer_depth)))
            out_channels = int(class_base_filter * (2 ** (class_layer_depth + 1)))
            if class_layer_depth == 0:
                max_pooling = False
                input_shape = input_bottle_neck_shape
            else:
                max_pooling = True
                input_shape = (input_shape) / 2

            if all(input_shape >= kernel_size):
                down_kernel_size = kernel_size
            elif all(input_shape > 3):
                down_kernel_size = 3
            elif all(input_shape > 2):
                down_kernel_size = 2
            else:
                down_kernel_size = 1

            x = UNetConvModule(
                in_channels=in_channels,
                out_channels=out_channels,
                input_shape=input_shape,
                kernel_size=down_kernel_size,
                padding=padding,
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout,
                batch_normalization=batch_normalization,
                instance_normalization=instance_normalization,
                normalization_momentum=normalization_momentum,
                norm_track_running_states=norm_track_running_states,
                max_pooling=max_pooling,
                residuals=residuals,
                activation=activation)

            self.class_down_layer_list.append(x)


        n_size = int(np.prod(input_shape) * out_channels)

        class_dense1 = nn.Linear(in_features=n_size,
                                 out_features=dense_layer_size)
        correct_labels1 = ActivationLayer('relu')
        class_dense2 = nn.Linear(in_features=dense_layer_size,
                                out_features=dense_layer_size)
        correct_labels2 = ActivationLayer('relu')

        # if num_classes > 1:
        #     class_final_layer = nn.Linear(in_features=dense_layer_size,
        #                                   out_features=num_classes + 1)
        # else:
        #     class_final_layer = nn.Linear(in_features=dense_layer_size,
        #                                   out_features=num_classes)

        class_final_layer = nn.Linear(in_features=dense_layer_size,
                                      out_features=num_classes + 1)

        self.final_class_block = nn.Sequential(class_dense1, correct_labels1,
                                               class_dense2, correct_labels2,
                                               class_final_layer)


        self.class_activation = ActivationLayer('softmax', dim=1)


        # if num_classes == 1:
        #     self.class_activation = ActivationLayer('sigmoid')
        # elif num_classes > 1:
        #     self.class_activation = ActivationLayer('softmax', dim=1)
        #     # output_activation = ActivationLayer('sigmoid')
        # else:
        #     self.class_activation = ActivationLayer('sigmoid')

    def forward(self, x):

        for fcn_depth, down_conv in enumerate(self.class_down_layer_list):
            x = down_conv(x)

        final_class_feature_map = x

        final_class_feature_map = final_class_feature_map.view((final_class_feature_map.size(0), -1))

        class_logits = self.final_class_block(final_class_feature_map)
        class_pred = self.class_activation(class_logits)

        return class_logits, class_pred