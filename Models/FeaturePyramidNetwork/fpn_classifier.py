import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Models.model_core import *
import Models.utils as utils
from Models.pyramid_roi_align import PyramidROIAlign
from Models.TimeDistributedLayer import TimeDistributed

class FPNClassifier(nn.Module):
    """Builds the computation graph of the feature pyramid network classifier
    and regressor heads.
    :param rois: [batch, num_rois, (y1, x1, y2, x2)] Proposal boxes in normalized
          coordinates.
    :param feature_maps: List of feature maps from different layers of the pyramid,
                  [P2, P3, P4, P5]. Each has a different resolution.
    :param image_meta: [batch, (meta data)] Image details. See compose_image_meta()
    :param config: Configuration file with necessary parameters
    :param pool_size: The width of the square feature map generated from ROI Pooling.
    :param num_classes: number of classes, which determines the depth of the results
    :param train_bn: Boolean. Train or freeze Batch Norm layers
    :param fc_layers_size: Size of the 2 FC layers

    Returns:
    :returns logits: [N, NUM_CLASSES] classifier logits (before softmax)
    :returns probs: [N, NUM_CLASSES] classifier probabilities
    :returns bbox_deltas: [N, (dy, dx, log(dh), log(dw))] Deltas to apply to
                     proposal boxes
    """

    def __init__(self, params):
        """

         Parameters
         ----------
         params : Parameters.ParameterBuilder.Parameters
             Parameters for model creating and training purposes.
         """

        super(FPNClassifier, self).__init__()
        self.params = params

        # ROI Pooling
        # Shape: [batch, num_boxes, pool_height, pool_width, channels]
        self.pyramid_roi_align = PyramidROIAlign(params)

        self.conv_block1 = TimeDistConv(in_channels=self.params.top_down_pyramid_size,
                                        out_channels=self.params.fpn_class_fc_layer,
                                        kernel_size=self.params.pool_size,
                                        batch_normalization=self.params.batch_normalization,
                                        normalization_momentum=self.params.normalization_momentum,
                                        instance_normalization=self.params.instance_normalization,
                                        norm_track_running_states=self.params.norm_track_running_states,
                                        activation='relu',)

        self.conv_block2 = TimeDistConv(in_channels=self.params.fpn_class_fc_layer,
                                        out_channels=self.params.fpn_class_fc_layer,
                                        kernel_size=1,
                                        batch_normalization=self.params.batch_normalization,
                                        normalization_momentum=self.params.normalization_momentum,
                                        instance_normalization=self.params.instance_normalization,
                                        norm_track_running_states=self.params.norm_track_running_states,
                                        activation='relu',)

        self.class_layer = nn.Linear(in_features=self.params.fpn_class_fc_layer,
                                     out_features=self.params.num_classes)

        self.class_activation = ActivationLayer('softmax', dim=-1)

        self.bbox_reg = nn.Linear(in_features=self.params.fpn_class_fc_layer,
                                  out_features=self.params.num_classes * 4)


    def forward(self, boxes, feature_maps):

        x = self.pyramid_roi_align(boxes, feature_maps)
        x = self.conv_block1(x)
        x = self.conv_block2(x)

        # shared = x.squeeze()

        shared = x.squeeze(-1).squeeze(-1)

        mrcnn_class_logits = self.class_layer(shared)
        mrcnn_probs = self.class_activation(mrcnn_class_logits)

        mrcnn_bbox = self.bbox_reg(shared)
        mrcnn_bbox = mrcnn_bbox.contiguous().view(mrcnn_bbox.shape[0], mrcnn_bbox.shape[1], self.params.num_classes, 4)

        return mrcnn_class_logits, mrcnn_probs, mrcnn_bbox



class TimeDistConv(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 instance_normalization=False,
                 norm_track_running_states=False,
                 activation=None,
                 ):
        super(TimeDistConv, self).__init__()



        self.conv = Conv2D(in_channels=in_channels,
                           out_channels=out_channels,
                           kernel_size=kernel_size,
                           batch_normalization=batch_normalization,
                           normalization_momentum=normalization_momentum,
                           instance_normalization=instance_normalization,
                           norm_track_running_states=norm_track_running_states,
                           activation=activation)

        self.activation = ActivationLayer('relu')

        self.time_distributed_conv = TimeDistributed(self.conv)

    def forward(self, x):

        x = self.time_distributed_conv(x)
        x = self.activation(x)
        return x
