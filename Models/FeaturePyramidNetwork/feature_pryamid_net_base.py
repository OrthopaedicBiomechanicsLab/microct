import numpy as np
import torch
import torch.nn as nn
from Models.model_core import *

class FPNForward(nn.Module):
    def __init__(self, params):
        super(FPNForward, self).__init__()

        resnet_base_filter = params.base_filter

        num_fpn_input_feature_maps = params.num_fpn_input_feature_maps

        top_down_pyramid_size = params.top_down_pyramid_size

        input_scale_factor_for_fpn = params.input_scale_factor_for_fpn[-num_fpn_input_feature_maps:]

        input_shape = np.array(params.input_shape)

        resnet_block_channel_list = np.multiply(resnet_base_filter * 4, [1, 2, 4, 8])

        self.down_rpn_conv_list = nn.ModuleList()
        self.up_rpn_conv_list = nn.ModuleList()

        self.final_conv_list = nn.ModuleList()

        for idx, in_channel in enumerate(resnet_block_channel_list[::-1]):

            conv = Conv2D(in_channels=in_channel,
                          out_channels=top_down_pyramid_size,
                          kernel_size=1,
                          stride=1,
                          padding='same',
                          input_shape=input_shape / input_scale_factor_for_fpn[-1 - idx],
                          batch_normalization=False)

            self.down_rpn_conv_list.append(conv)

        for idx_up in range(num_fpn_input_feature_maps - 1):
            up_conv = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.up_rpn_conv_list.append(up_conv)

        for idx_final in range(num_fpn_input_feature_maps):

            conv = Conv2D(in_channels=top_down_pyramid_size,
                          out_channels=top_down_pyramid_size,
                          kernel_size=3,
                          stride=1,
                          padding='same',
                          input_shape=input_shape / input_scale_factor_for_fpn[idx_final],
                          batch_normalization=False)

            self.final_conv_list.append(conv)

        self.final_feature_map = nn.MaxPool2d(kernel_size=1, stride=2)

        self.fpn_output_sizes = [input_shape / input_scale_factor_for_fpn[idx]
                                 for idx in range(num_fpn_input_feature_maps)]

        self.fpn_output_sizes.append(np.ceil(self.fpn_output_sizes[-1] / 2))

    def forward(self, resnet_feature_map_list):

        C2, C3, C4, C5 = resnet_feature_map_list

        P5 = self.down_rpn_conv_list[0](C5)

        P4 = torch.add(self.up_rpn_conv_list[0](P5), self.down_rpn_conv_list[1](C4))
        P3 = torch.add(self.up_rpn_conv_list[1](P4), self.down_rpn_conv_list[2](C3))
        P2 = torch.add(self.up_rpn_conv_list[2](P3), self.down_rpn_conv_list[3](C2))

        P2 = self.final_conv_list[0](P2)
        P3 = self.final_conv_list[1](P3)
        P4 = self.final_conv_list[2](P4)
        P5 = self.final_conv_list[3](P5)

        P6 = self.final_feature_map(P5)

        return P2, P3, P4, P5, P6



