from ..BaseModel import BaseModel
from .unet_class_base import UNetClassifier
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *

class UNetClassifierModel(BaseModel):
    def __init__(self, params, logger=None):
        # super(UNetModel, self).__init__(params)
        BaseModel.__init__(self, params, logger)

        self.model_base_fn = UNetClassifier
        self.load_model_from_params()

        # self.net = UNet(params)

        # self.multi_class_loss_keys = ['seg', 'class']
        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

        self.vert_encode = {
            'T6': 1,
            'T7': 2,
            'T8': 3,
            'T9': 4,
            'T10': 5,
            'T11': 6,
            'T12': 7,

            'L1': 8,
            'L2': 9,
            'L3': 10,
            'L4': 11,
            'L5': 12}

        self.vert_decode = {
            1: 'T6',
            2: 'T7',
            3: 'T8',
            4: 'T9',
            5: 'T10',
            6: 'T11',
            7: 'T12',

            8: 'L1',
            9: 'L2',
            10: 'L3',
            11: 'L4',
            12: 'L5'}

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """
        self.image = input['image'].to(self.device)
        self.label = [x.to(self.device) for x in input['label']]
        # self.label = input['label'].to(self.device)
        # self.seg_label = label[0].to(self.device)
        # self.class_label = label[1].to(self.device)

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        self.pred = self.net(self.image)
        self.seg_pred = self.pred[0]
        self.class_logits = self.pred[1]
        self.class_pred = self.pred[2]

    def backward(self):
        """Calculate UNet model loss"""
        self.loss = self.calc_loss()
        self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.optimizer.step()          # update weights

    def model_specific_metrics(self, batch_metrics):
        pred_label = self.class_pred.argmax(dim=-1)
        for single_pred_label in pred_label:
            vert_pred = self.vert_decode[int(single_pred_label)]
            batch_metrics['pred_vertebra'] = vert_pred
        return batch_metrics

    def DSCLoss(self, **fn_params):
        return BaseLossMetric(dsc_loss, pred_index=0, label_index=0, **fn_params).to(self.device)

    def BCELoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     self.logger.info('Number of classes set >1, therefore using cross entropy loss instead.')
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(bce_loss, pred_index=0, label_index=0, **fn_params).to(self.device)

    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, pred_index=0, label_index=0, **fn_params).to(self.device)

    def CONCURRENCY(self, **fn_params):
        return BaseLossMetric(concurrency, pred_index=0, label_index=0, **fn_params).to(self.device)

    def ACCURACY(self, **fn_params):
        return BaseLossMetric(accuracy, pred_index=2, label_index=1, **fn_params).to(self.device)

    def CrossEntropyLoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     self.logger.info('Number of classes set to 1, therefore using binary cross entropy loss instead')
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(ce_loss, pred_index=1, label_index=1, **fn_params).to(self.device)

    def initialize_weights(self):
        self.net.apply(self.__weights_init)

    def __weights_init(self, m):
        from torch.nn import init
        if isinstance(m, nn.Conv3d):
            init.xavier_normal_(m.weight.data)
            if m.bias is not None:
                # init.constant_(m.bias, 0)
                fan_in, _ = init._calculate_fan_in_and_fan_out(m.weight.data)
                import math
                bound = 1 / math.sqrt(fan_in)
                init.uniform_(m.bias, -bound, bound)










