import os
from abc import ABC, abstractmethod
from collections import OrderedDict
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.multiprocessing as mp
from torch.multiprocessing import log_to_stderr, get_logger
import torch.distributed as dist
from torch.utils.data.distributed import DistributedSampler
import numpy as np

import LossMetricFunctions
from LossMetricFunctions import BaseLossMetric
from Dataset import CustomDataLoader
from Models import model_utils
from Visualization.Utils import ProgressBar
import pandas as pd
import importlib
from logging.handlers import QueueHandler, QueueListener
import logging
import threading
import copy

from time import time

import SimpleITK as sitk

class BaseModel(ABC):
    """
    This is an abstract base class (ABC) for models.
    To create a model, the following functions must be implemented.

    -- <__init__>: To initialize the model and pass the appropriate parameters.
        First call BaseModel.__init__(self, params).
    -- <self_input>: Unpack the data from the dataset and apply preprocessing.
    -- <forward>: Produce intermediate results.
    -- <optimize_model>: Calculate losses, gradients, and update network weights.

    """

    def __init__(self, params, logger=None):
        """
        Initialize the BaseModel class.

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.

        When building a model based on this class, the initialization of the model is required.
        When implementing, BaseModel.__init__(self, params) should be called first.

        Initialization requires certain parameters be defined and are:
            params.loss
            params.model
            params.optimizer

        params.loss is not initialized when creating the parameters class and must be specified by the user.
        """

        self.params = params
        self.net = None
        self.model_base_fn = None
        self.best_epoch = None
        self.logger = logger
        self.schedulers = None

        self.model = params.model
        self.optimizer = [params.optimizer]

        self.optimizer_dict = None

        self.gpu_ids = params.gpu_ids
        self.isTrain = params.isTrain


        self.visual_names = []
        self.image_paths = []


        if (self.gpu_ids is not None) and (len(self.gpu_ids) > 0):
            os.environ['CUDA_VISIBLE_DEVICES'] = ','.join([str(x) for x in self.gpu_ids])

            self.num_gpus = len(self.gpu_ids) if self.gpu_ids is not None else 0

            self.gpu_ids = [str(idx) for idx in range(len(self.gpu_ids))]

        else:
            self.num_gpus = 0

        self.device = torch.device(('cuda:{}').format(self.gpu_ids[0]) if self.gpu_ids is not None else 'cpu')


        # torch.backends.cudnn.benchmark = True

        # torch.backends.cudnn.deterministic = True
        # torch.backends.cudnn.benchmark = False

        self.loss_fns = None
        self.loss_fns_weights = None
        self.metric_names = None

        if self.params.loss_dict is not None:
            self.get_loss_functions()

        if self.params.metrics_dict is not None:
            self.metric_names = self.get_metric_functions()



        self.min_loss = None
        self.loss = None
        self.label = None
        self.image_np = None

    @abstractmethod
    def set_input(self, input, gpu=None):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): includes the data itself and its metadata information.
        """
        pass

    @abstractmethod
    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        self.pred = None

    @abstractmethod
    def optimize_model(self):
        """Calculate losses, gradients, and update network weights; called in every training iteration"""
        pass

    def setup_lr_scheduler(self, params):
        if self.isTrain:
            self.schedulers = model_utils.model_learning_scheduler(self.optimizer, params)

    def calc_loss(self):
        loss = 0
        loss_idx = 0


        for loss_idx, name in enumerate(self.loss_fns.keys()):
            if isinstance(self.label, list) or isinstance(self.label, tuple):
                label = self.label[loss_idx]
                pred = self.pred[loss_idx]
            else:
                label = self.label
                pred = self.pred
                image = self.image_np # use image_np if not in GPU
            if name=='volume_ratio_loss':
                loss = loss + (self.loss_fns[name](pred, label, image=image) * self.loss_fns_weights[name]) 
            else:
                loss = loss + (self.loss_fns[name](pred, label) * self.loss_fns_weights[name])

        # for loss_idx, name in enumerate(self.loss_fns.keys()):
        #     loss += (self.loss_fns[name](self.pred, self.label) * self.loss_fns_weights[name])
        loss = loss / (loss_idx + 1)
        return loss

    def calc_metrics(self):
        # calc_metrics_dict = dict()
        # for metric_idx, name in enumerate(self.metric_fns.keys()):
        #     metric = float(self.metric_fns[name](self.pred, self.label))
        #     calc_metrics_dict[name] = metric
        # return calc_metrics_dict



        calc_metrics_dict = dict()
        for metric_idx, name in enumerate(self.metric_fns.keys()):
            if isinstance(self.label, list) or isinstance(self.label, tuple):
                label = self.label[metric_idx]
                pred = self.pred[metric_idx]
            else:
                image = self.image_np # use image_np if not in GPU
                label = self.label
                pred = self.pred
            if name=='volume_ratio_gt':
                metric = float(self.metric_fns[name](pred, label, image=image, mode = 'gt').data)
            elif name == 'volume_ratio_pred':
                metric = float(self.metric_fns[name](pred, label, image=image, mode = 'pred').data)
            else:
                metric = float(self.metric_fns[name](pred, label).data)
            calc_metrics_dict[name] = metric
        return calc_metrics_dict

    def calc_metrics_with_inputs(self, pred, label):
        pred = pred.to(self.device)
        label = label.to(self.device)
        calc_metrics_dict = dict()
        for metric_idx, name in enumerate(self.metric_fns.keys()):
            metric = float(self.metric_fns[name](pred, label))
            calc_metrics_dict[name] = metric
        return calc_metrics_dict

    def model_specific_metrics(self, metrics):
        """Use for model specific metrics. """
        return metrics

    def model_specific_training(self, sample, epoch):
        pass

    def reset_model_specific_training(self):
        pass

    def setup_optim(self, opt_name):
        try:
            optimizer = getattr(torch.optim, opt_name)
            return optimizer
        except:
            print('Pytorch has no optimizer with the name: ', opt_name, '. Please ensure case sensitivity.')



    def fit(self, train_dataset, val_dataset=None, save_model_hist_while_training=False, history_filename=None,
            verbose=1):
        """ Train the model based on the inputs."""

        if self.net is None:
            raise AssertionError('No network has been initialized or loaded.')

        # os.environ["MASTER_ADDR"] = "127.0.0.1"
        os.environ["MASTER_ADDR"] = "localhost"
        os.environ["MASTER_PORT"] = "29500"


        self.params.num_process_per_node = self.num_gpus
        self.params.num_nodes = 1
        self.params.node_rank = 0  # Ranking within the different nodes. This would need to be edited for multi-node.
        self.params.world_size = self.params.num_process_per_node * self.params.num_nodes
        self.params.num_gpus = self.num_gpus

        # import copy
        # model = copy.deepcopy(self.net)
        # model.share_memory()
        # optimizer = copy.deepcopy(self.optimizer)

        self.params.is_distributed = self.params.is_distributed if self.num_gpus > 1 else False
        self.params.is_distributed = self.params.is_distributed if self.gpu_ids is not None else False

        training_input = {'train_dataset': train_dataset,
                          'val_dataset': val_dataset,
                          'save_model_hist_while_training': save_model_hist_while_training,
                          'history_filename': history_filename,
                          'verbose': verbose,

                          # 'model': model,
                          # 'optimizer': optimizer,
                          }


        n_train = int((len(train_dataset) + 1) / self.params.num_gpus) if self.params.is_distributed else (len(train_dataset) + 1)
        self.params.batch_size = self.params.batch_size // self.num_gpus if self.params.is_distributed else self.params.batch_size

        self.pbar = ProgressBar(self.params.batch_size, n_train, self.params.epochs, verbose=verbose)


        if self.metric_names is not None:
            training_history_columns = ['epoch'] + ['loss'] + self.metric_names
        else:
            training_history_columns = ['epoch'] + ['loss']

        if val_dataset:
            if self.metric_names is not None:
                for name in self.metric_names:
                    training_history_columns = training_history_columns + ['val_' + name]
            training_history_columns = training_history_columns + ['val_loss']

        self.df_tairning_history = pd.DataFrame(columns=training_history_columns)
        # mp.spawn(self.ddp_train_dummy, nprocs=self.num_gpus, args=(self.params, training_input))
        # mp.spawn(ddp_train_dummy, nprocs=self.num_gpus, args=(self.params, training_input))


        assert self.num_gpus == torch.cuda.device_count(), 'Both should return the same number of GPUs available.'

        # self.load_weights('hold_state_dict.pt')

        if self.params.is_distributed:

            # self.net.share_memory()


            mp.set_start_method('spawn')

            queue_logger = mp.Queue()

            # state_dict_manager = mp.Queue()
            state_dict_manager = mp.Manager().dict()



            pbar_queue = mp.Queue()
            batch_metric_queue = mp.Queue()

            listener_thread = threading.Thread(target=self.logger_thread, args=(queue_logger,))
            listener_thread.start()

            condition = mp.Condition()

            state = mp.Value('i', 1)
            gpu_state = mp.Value('i', 0)


            after_epoch_process = threading.Thread(target=self.after_epoch_updates,
                                                   args=(condition, state,
                                                         self.params, save_model_hist_while_training,
                                                         history_filename, verbose, pbar_queue))


            after_epoch_process.start()

            mp.spawn(self.distributed_training, nprocs=self.num_gpus, args=(self.params, training_input,
                                                                            queue_logger,
                                                                            condition, state, gpu_state,
                                                                            pbar_queue,
                                                                            batch_metric_queue,
                                                                            state_dict_manager))



            dist_state_dict_clone = copy.deepcopy(state_dict_manager['state_dict'])
            self.net.load_state_dict(dist_state_dict_clone)

            with condition:
                state.value = -1
                condition.notify_all()




            queue_logger.put(None)
            after_epoch_process.join()
            listener_thread.join()





        else:
            self.distributed_training(0, self.params, training_input)

        # # if importlib.util.find_spec('apex') is not None:
        # #     import apex
        # #     from apex import amp
        # #
        # #     self.net.to('cuda')
        # #
        # #     self.net, self.optimizer = amp.initialize(self.net, self.optimizer, opt_level=self.params.apex_opt_level)
        #
        #
        # # if (torch.cuda.device_count() > 1) and (self.gpu_ids is not None) and (
        # #         isinstance(self.net, torch.nn.parallel.DataParallel) is False):
        # #     if importlib.util.find_spec('apex') is not None:
        # #         import apex
        # #         from apex import amp
        # #         self.net = apex.parallel.DistributedDataParallel(self.net)
        # #     else:
        # #         self.net = torch.nn.DataParallel(self.net)
        # #         self.net.to(self.device)
        # #
        # # elif (torch.cuda.device_count() == 1) and (self.gpu_ids is not None) and (
        # #         isinstance(self.net, torch.nn.parallel.DataParallel) is False):
        # #     self.net.to(self.device)
        #
        # epochs = self.params.epochs
        # batch_size = self.params.batch_size
        #
        # drop_last_train = not len(train_dataset) % batch_size == 0
        #
        # train_generator = CustomDataLoader(self.params, train_dataset,
        #                                    num_threads=self.params.num_threads, shuffle=self.params.shuffle,
        #                                    drop_last=drop_last_train)
        #
        # if self.metric_names is not None:
        #     training_history_columns = ['epoch'] + ['loss'] + self.metric_names
        # else:
        #     training_history_columns = ['epoch'] + ['loss']
        #
        # if val_dataset:
        #     drop_last_val = not len(val_dataset) % batch_size == 0
        #     val_generator = CustomDataLoader(self.params, val_dataset,
        #                                      num_threads=self.params.num_threads, shuffle=False,
        #                                      drop_last=drop_last_val)
        #     if self.metric_names is not None:
        #         for name in self.metric_names:
        #             training_history_columns = training_history_columns + ['val_' + name]
        #     training_history_columns = training_history_columns + ['val_loss']
        # else:
        #     val_generator = None
        #
        # self.df_tairning_history = pd.DataFrame(columns=training_history_columns)
        #
        # n_train = int(len(train_dataset) + 1)
        # pbar = ProgressBar(batch_size, n_train, epochs, self.logger, verbose=verbose)
        # period = None
        # if self.params.relative_save_weight_period is not None:
        #     period = int(epochs / self.params.relative_save_weight_period)
        #
        # prev_dsc = 0
        # run_current_epoch = False
        # for epoch in range(epochs):
        #
        #     np.random.seed(epoch * self.params.num_threads)
        #
        #     pbar.initialize_progbar(epoch)
        #     self.net.train()
        #
        #     # The batch_step is increased by 1 before it is used by the averaging functions
        #     batch_step = 0
        #
        #     if self.metric_names is not None:
        #         train_metrics = {metric: [] for metric in self.metric_names}
        #         running_metrics = {metric: [] for metric in self.metric_names}
        #     else:
        #         train_metrics = {}
        #         running_metrics = {}
        #     train_metrics['loss'] = []
        #
        #     running_metrics['loss'] = []
        #
        #     for idx_train, batch in enumerate(train_generator):
        #
        #         self.set_input(batch)
        #         self.optimize_model()
        #
        #         if self.metric_names is not None:
        #             batch_metric = self.calc_metrics()
        #         else:
        #             batch_metric = {}
        #
        #         batch_metric['loss'] = self.loss.item()
        #
        #         self.loss_value = self.loss.item()
        #
        #
        #         for metric in batch_metric.keys():
        #             running_metrics[metric].append(batch_metric[metric])
        #
        #         batch_step += 1
        #         pbar.update_values(batch_step, running_metrics)
        #         pbar.print_update()
        #
        #         if self.params.imgs_during_training:
        #             if ((epoch + 1) % 25 == 0) or (epoch == 0) or (epoch == 1) or (epoch == 2):
        #                 self.model_specific_training(batch, epoch + 1)
        #                 run_current_epoch = True
        #
        #             elif ((np.mean(running_metrics['dsc']) - prev_dsc) < - 0.08) or run_current_epoch:
        #                 self.model_specific_training(batch, epoch + 1)
        #                 run_current_epoch = True
        #
        #         del self.loss
        #         del self.pred
        #
        #     if self.params.imgs_during_training:
        #         if run_current_epoch:
        #             self.reset_model_specific_training()
        #         prev_dsc = np.mean(running_metrics['dsc'])
        #         run_current_epoch = False  # Reset this
        #
        #
        #     if val_generator:
        #         val_batch_step = 0
        #
        #         # Initialize running metrics for validation.
        #         # Need to first get the names then change the size of running_metrics
        #         running_metric_names = list(running_metrics.keys())
        #         for name in running_metric_names:
        #             running_metrics['val_' + name] = []
        #
        #         with torch.set_grad_enabled(False):
        #             for batch in val_generator:
        #                 self.set_input(batch)
        #                 self.forward()
        #                 batch_metric = self.calc_metrics()
        #                 batch_metric['loss'] = float(self.calc_loss())
        #
        #                 del self.pred
        #
        #                 for metric in batch_metric.keys():
        #                     val_metric_name = 'val_' + metric
        #                     running_metrics[val_metric_name].append(batch_metric[metric])
        #                 val_batch_step += 1
        #
        #             pbar.update_values(val_batch_step, running_metrics, partial_normalize='val')
        #             pbar.print_update()
        #
        #     pbar.close()
        #     pbar.to_logger()
        #     epoch_history = pbar.return_dict()
        #     epoch_history['epoch'] = epoch + 1
        #
        #     self.df_tairning_history = self.df_tairning_history.append(epoch_history, ignore_index=True)
        #
        #     if period is not None:
        #         if ((epoch + 1) % period == 0) and ((epoch + 1) != epochs):
        #             self.__save_model_during_training(epoch=epoch)
        #
        #     if self.params.save_best_epoch is True:
        #         self.__save_best_epoch_during_training(epoch=epoch)
        #
        #     if ((self.params.history_while_training is True) or (save_model_hist_while_training is True)) \
        #             and (history_filename is not None):
        #         try:
        #             self.save_training_history(history_filename)
        #         except PermissionError:
        #             pass
        #
        #     self.update_learning_rate()

    @staticmethod
    def logger_thread(q):
        while True:
            record = q.get()
            if record is None:
                break
            logger = logging.getLogger(record.name)
            logger.handle(record)

    def distributed_training(self, gpu, params, input_params,
                             log_queue=None, cond=None, state=None, gpu_state=None,
                             pbar_queue=None, batch_metric_queue=None, state_dict_manager=None):

        if params.is_distributed:
            queue_handler = logging.handlers.QueueHandler(log_queue)

            root = logging.getLogger()
            root.setLevel(logging.NOTSET)
            root.addHandler(queue_handler)

            logger = logging.getLogger()
            logger.addHandler(queue_handler)

            logger.info('Processes launched.')
        else:
            logger = logging.getLogger()

        train_dataset = input_params['train_dataset']
        val_dataset = input_params['val_dataset']
        save_model_hist_while_training = input_params['save_model_hist_while_training']
        history_filename = input_params['history_filename']
        verbose = input_params['verbose']


        batch_size = params.batch_size
        epochs = params.epochs
        starting_epoch = params.starting_epoch


        #
        # # if (torch.cuda.device_count() > 1) and (self.gpu_ids is not None) and (
        # #         isinstance(self.net, torch.nn.parallel.DataParallel) is False):
        # #     if importlib.util.find_spec('apex') is not None:
        # #         import apex
        # #         from apex import amp
        # #         self.net = apex.parallel.DistributedDataParallel(self.net)
        # #     else:
        # #         self.net = torch.nn.DataParallel(self.net)
        # #         self.net.to(self.device)
        # #
        # # elif (torch.cuda.device_count() == 1) and (self.gpu_ids is not None) and (
        # #         isinstance(self.net, torch.nn.parallel.DataParallel) is False):
        # #     self.net.to(self.device)
        #

        rank = None
        if (params.num_gpus > 1) and (self.gpu_ids is not None):
            if params.is_distributed:
                rank = params.node_rank * params.num_gpus + gpu

                dist.init_process_group(
                    backend='nccl',
                    init_method='env://',
                    world_size=params.world_size,
                    rank=rank)

                training_device = 'cuda:{}'.format(gpu)
                self.net.to(training_device)
                self.net = torch.nn.parallel.DistributedDataParallel(self.net, device_ids=[rank], find_unused_parameters=True)
                # self.net = torch.nn.parallel.DistributedDataParallel(self.net, device_ids=[rank])

            else:
                training_device = self.device
                self.net = torch.nn.DataParallel(self.net)
                self.net.to(self.device)

        elif (params.num_gpus == 1) and (self.gpu_ids is not None):
            training_device = self.device
            self.net.to(self.device)
        else:
            # Put on CPU
            training_device = self.device


        drop_last_train = not len(train_dataset) % batch_size == 0

        if params.is_distributed:
            train_sampler = DistributedSampler(train_dataset, num_replicas=params.world_size, rank=rank, shuffle=True)
            if val_dataset:
                val_sampler = DistributedSampler(val_dataset, num_replicas=params.world_size, rank=rank)
            else:
                val_sampler = None
        else:
            train_sampler = None
            val_sampler = None



        train_generator = CustomDataLoader(params, train_dataset,
                                           num_threads=params.num_threads,
                                           shuffle=params.shuffle if train_sampler is None else False,
                                           drop_last=drop_last_train,
                                           sampler=train_sampler)()
        if val_dataset:
            drop_last_val = not len(val_dataset) % batch_size == 0
            val_generator = CustomDataLoader(params, val_dataset,
                                             num_threads=params.num_threads, shuffle=False,
                                             drop_last=drop_last_val,
                                             sampler=val_sampler)()
        else:
            val_generator = None



        prev_dsc = 0
        run_current_epoch = False

        if params.is_distributed:
            gpu_state.value = 0
        save_img_idx = 0
        for epoch in range(starting_epoch, epochs):
            self.net.train()

            if params.is_distributed is True:
                train_sampler.set_epoch(epoch)

            np.random.seed(epoch * params.num_threads + gpu)

            running_metrics = dict()
            if (gpu == 0) or (params.is_distributed is False):
                self.pbar.initialize_progbar(epoch)

                if self.metric_names is not None:
                    for metric in self.metric_names:
                        running_metrics[metric] = []

                running_metrics['loss'] = []

                if val_dataset:
                    # Initialize running metrics for validation.
                    # Need to first get the names then change the size of running_metrics
                    running_metric_names = list(running_metrics.keys())
                    for name in running_metric_names:
                        running_metrics['val_' + name] = []

            running_metrics_print = copy.deepcopy(running_metrics)


            # The batch_step is increased by 1 before it is used by the averaging functions
            batch_step = 0

            t0 = time()

            for idx_train, batch in enumerate(train_generator):

                self.set_input(batch, gpu=training_device)
                self.optimize_model()

                if self.metric_names is not None:
                    batch_metric = self.calc_metrics()
                else:
                    batch_metric = {}

                batch_metric['loss'] = self.loss.item()

                if params.is_distributed:
                    batch_metric_queue.put(batch_metric)


                if (params.is_distributed is False) or (params.is_distributed is True and gpu == 0):
                # if False:

                    # if self.pred[batch_idx].max() > 0:
                    # if True:
                    if False:
                        # pred_seg = self.pred[0].detach().cpu().numpy()
                        # pred_seg = np.where(pred_seg > 0.5, 1, 0)
                        #
                        #
                        #
                        # heat_pred_np = self.pred[1].detach().cpu().numpy()
                        # heat_pred = np.where(heat_pred_np > 0.85, 1, 0)
                        # heat_pred = np.argmax(heat_pred, axis=1)
                        #
                        #
                        # heat_gt_np = self.label[1].detach().cpu().numpy()
                        # heat_gt = np.where(heat_gt_np > 0.85, 1, 0)
                        # heat_gt = np.argmax(heat_gt, axis=1)

                        batch_idx = 0

                        # pred_seg = self.pred[0].detach().cpu().numpy()
                        # pred_seg = np.where(pred_seg > 0.5, 1, 0)
                        #
                        # gt_seg = self.label[0].detach().cpu().numpy()
                        #
                        # heat_pred_np = self.pred[1].detach().cpu().numpy()
                        # heat_pred = np.where(heat_pred_np > 0.6, 1, 0)
                        # heat_pred = np.argmax(heat_pred, axis=1)
                        #
                        #
                        # heat_gt_np = self.label[1].detach().cpu().numpy()
                        # heat_gt = np.where(heat_gt_np > 0.85, 1, 0)
                        # heat_gt = np.argmax(heat_gt, axis=1)

                        heat_gt_np = self.label[1].detach().cpu().numpy() if isinstance(self.label, list) else self.label.detach().cpu().numpy()
                        heat_pred_np = self.pred[1].detach().cpu().numpy() if isinstance(self.pred, list) else self.pred.detach().cpu().numpy()

                        if heat_gt_np.shape[1] > 1:
                            raw_gt_point = np.argwhere(heat_gt_np == 1)
                            raw_gt = heat_gt_np[raw_gt_point[0][0], raw_gt_point[0][1]]
                            raw_pred = heat_pred_np[raw_gt_point[0][0], raw_gt_point[0][1]]
                        else:
                            raw_gt = heat_gt_np[batch_idx, 0]
                            raw_pred = heat_pred_np[batch_idx, 0]


                        img = batch['image'].detach().cpu().numpy()[batch_idx, 0].astype(np.float)

                        # Save image
                        sitk.WriteImage(
                            sitk.GetImageFromArray(img),
                            self.params.model_output_directory + f'/hold_{save_img_idx}.nii.gz')

                        # # Save seg
                        # sitk.WriteImage(
                        #     sitk.GetImageFromArray(pred_seg[batch_idx, 0].astype(np.float)),
                        #     self.params.model_output_directory + f'/hold_seg_pred_{save_img_idx}.nii.gz')
                        #
                        # sitk.WriteImage(
                        #     sitk.GetImageFromArray(gt_seg[batch_idx, 0].astype(np.float)),
                        #     self.params.model_output_directory + f'/hold_seg_{save_img_idx}.nii.gz')

                        # # Save heatmap
                        # sitk.WriteImage(
                        #     sitk.GetImageFromArray(heat_gt[batch_idx].astype(np.float)),
                        #     self.params.model_output_directory + f'/hold_heat_{save_img_idx}.nii.gz')
                        #
                        # sitk.WriteImage(
                        #     sitk.GetImageFromArray(heat_pred[batch_idx].astype(np.float)),
                        #     self.params.model_output_directory + f'/hold_heat_pred_{save_img_idx}.nii.gz')


                        sitk.WriteImage(
                            sitk.GetImageFromArray(raw_pred.astype(np.float)),
                            self.params.model_output_directory + f'/hold_raw_heat_pred_{save_img_idx}.nii.gz')

                        sitk.WriteImage(
                            sitk.GetImageFromArray(raw_gt.astype(np.float)),
                            self.params.model_output_directory + f'/hold_raw_heat_{save_img_idx}.nii.gz')

                        save_img_idx += 1

                        if save_img_idx > 4:
                            save_img_idx = 0

                    a = 1
                # self.loss_value = self.loss.item()

                if (gpu == 0) and (params.is_distributed):
                    for metric in batch_metric.keys():
                        running_metrics_print[metric].append(batch_metric[metric])
                        # running_metrics[metric].append(batch_metric[metric])

                    batch_step += 1

                    self.pbar.update_values(running_metrics_print)
                    self.pbar.print_update()


                # if (gpu == 0) and (params.is_distributed):
                #     batch_metric_hold = batch_metric_queue.get()
                #     for metric in batch_metric_hold.keys():
                #         running_metrics[metric].append(batch_metric_hold[metric])
                #
                #     batch_step += 1
                #
                #     self.pbar.update_values(running_metrics)
                #     self.pbar.print_update()



                elif params.is_distributed is False:

                    for metric in batch_metric.keys():
                        running_metrics[metric].append(batch_metric[metric])

                    batch_step += 1
                    self.pbar.update_values(running_metrics)
                    self.pbar.print_update()


                # if (gpu == 0) or (params.is_distributed is False):
                #     self.pbar.print_update()

                if False:
                    if params.imgs_during_training:
                        if ((epoch + 1) % 25 == 0) or (epoch == 0) or (epoch == 1) or (epoch == 2):
                            self.model_specific_training(batch, epoch + 1)
                            run_current_epoch = True

                        elif ((np.mean(running_metrics['dsc']) - prev_dsc) < - 0.08) or run_current_epoch:
                            self.model_specific_training(batch, epoch + 1)
                            run_current_epoch = True

                del self.loss
                del self.pred

                # torch.cuda.ipc_collect()
                # torch.cuda.empty_cache()

            # if self.params.checkpoint_save:
            #     self.__save_checkpoint(epoch)

            if False:
                if params.imgs_during_training:
                    if run_current_epoch:
                        self.reset_model_specific_training()
                    prev_dsc = np.mean(running_metrics['dsc'])
                    run_current_epoch = False  # Reset this


            if val_generator:

                with torch.set_grad_enabled(False):
                    for batch in val_generator:
                        self.set_input(batch, training_device)
                        self.forward()
                        batch_metric = self.calc_metrics()
                        batch_metric['loss'] = float(self.calc_loss())


                        if params.is_distributed:
                            batch_metric_val = dict()
                            for metric in batch_metric.keys():
                                val_metric_name = 'val_' + metric
                                batch_metric_val[val_metric_name] = batch_metric[metric]
                            batch_metric_queue.put(batch_metric_val)

                        else:
                            for metric in batch_metric.keys():
                                val_metric_name = 'val_' + metric
                                running_metrics[val_metric_name].append(batch_metric[metric])
                            # self.pbar.update_values(running_metrics)
                            # self.pbar.print_update()

                        del self.pred

            t1 = time()
            epoch_time = t1 - t0
            running_metrics['epoch_time'] = np.array([epoch_time])


            # TODO this part may start before the other processes finish. Potential of batch_metric_queue emptying then
            #  having more values added. This should be put in the "after_epoch_updates" section to ensure epoch is
            #  complete.
            if (gpu == 0) and (params.is_distributed):
                while (batch_metric_queue.empty() is False):
                    batch_metric_hold = batch_metric_queue.get()
                    for metric in batch_metric_hold.keys():
                        running_metrics[metric].append(batch_metric_hold[metric])


                self.loss_value = np.mean(running_metrics['loss'])

                self.pbar.update_values(running_metrics)
                self.pbar.print_update()
                pbar_queue.put(self.pbar.return_dict())


            elif params.is_distributed is False:
                self.loss_value = np.mean(running_metrics['loss'])
                self.pbar.update_values(running_metrics)
                self.pbar.print_update()

            if params.is_distributed:
                gpu_state.value += 1

                if gpu_state.value == params.num_gpus:
                    state.value = 0

                with cond:
                    cond.notify_all()
                    cond.wait_for(lambda: state.value == 1)

                    gpu_state.value = 0

                    if (gpu == 0) or (params.is_distributed is False):
                        # gpu_state.value = 0
                        self.pbar.close()
                        self.pbar.to_logger(logger)


            else:
                self.pbar.close()
                self.pbar.to_logger(logger)
                period = None
                if params.relative_save_weight_period is not None:
                    period = int(epochs / params.relative_save_weight_period)

                epoch_history = self.pbar.return_dict()
                epoch_history['epoch'] = epoch + 1

                self.df_tairning_history = self.df_tairning_history.append(epoch_history, ignore_index=True)

                # self.__save_model_during_training(epoch=epoch)
                # logger.info(f'Model saved after epoch: {epoch + 1}')

                if period is not None:
                    if ((epoch + 1) % period == 0) and ((epoch + 1) != epochs):
                        self.__save_model_during_training(epoch=epoch)

                if self.params.save_best_epoch is True:
                    self.__save_best_epoch_during_training(epoch=epoch)

                if self.params.checkpoint_save:
                    self.__save_checkpoint(epoch)

                if ((self.params.history_while_training is True) or (save_model_hist_while_training is True)) \
                        and (history_filename is not None):
                    try:
                        self.save_training_history(history_filename)
                    except PermissionError:
                        pass



            self.update_learning_rate()

        if params.is_distributed and gpu == 0:
            state_dict_hold = self.net.state_dict()
            new_state_dict = OrderedDict()
            for k, v in state_dict_hold.items():
                if k.startswith('module.'):
                    name = k[7:]  # remove 'module'.
                    new_state_dict[name] = v.cpu()
                else:
                    new_state_dict[k] = v.cpu()


            state_dict_manager['state_dict'] = new_state_dict

        # torch.save(self.net.state_dict(), 'hold_state_dict.pt')


    def after_epoch_updates(self, cond, state,
                            params, save_model_hist_while_training=False, history_filename=None, verbose=1,
                            pbar_queue=None):

        epoch = 0
        with cond:
            while(True):
                cond.wait_for(lambda: state.value != 1)

                if state.value == 0:

                    epochs = params.epochs

                    period = None
                    if params.relative_save_weight_period is not None:
                        period = int(epochs / params.relative_save_weight_period)

                    epoch_history = pbar_queue.get()
                    epoch_history['epoch'] = epoch + 1

                    self.loss_value = epoch_history['loss']

                    self.df_tairning_history = self.df_tairning_history.append(epoch_history, ignore_index=True)

                    if period is not None:
                        if ((epoch + 1) % period == 0) and ((epoch + 1) != epochs):
                            self.__save_model_during_training(epoch=epoch)

                    if self.params.save_best_epoch is True:
                        self.__save_best_epoch_during_training(epoch=epoch)

                    if self.params.checkpoint_save:
                        self.__save_checkpoint(epoch)

                    if ((self.params.history_while_training is True) or (save_model_hist_while_training is True)) \
                            and (history_filename is not None):
                        try:
                            self.save_training_history(history_filename)
                        except PermissionError:
                            pass

                    state.value = 1
                    cond.notify_all()

                    epoch += 1
                else:
                    state.value = -1
                    break



    def __normal_training(self, gpu):
        pass

    def eval(self, input):
        """Make models eval mode during test time"""
        self.set_input(input)
        with torch.no_grad():
            self.forward()
        return self.pred

    def test(self, input):
        """Forward function used in test time.
        This function wraps <forward> function in no_grad() so we don't save intermediate steps for backprop
        """
        self.set_input(input)
        self.net.eval()
        with torch.no_grad():
            self.forward()
        metrics = self.calc_metrics()
        loss = self.calc_loss()

        metrics['loss'] = float(loss)

        metrics = self.model_specific_metrics(metrics)
        return metrics

    def test_and_eval(self, input):
        """Both runs both evaluation and test to output prediction and metrics"""
        self.set_input(input, self.device)
        self.net.eval()
        with torch.no_grad():
            self.forward()
        metrics = self.calc_metrics()
        metrics = self.model_specific_metrics(metrics)
        return self.pred, metrics

    def update_learning_rate(self):
        """Update learning rates for all the networks; called at the end of every epoch"""
        if self.schedulers is None:
            return

        if self.params.lr_scheduler == 'plateau':
            self.schedulers.step(self.loss_value)
        else:
            self.schedulers.step()
        lr = self.optimizer.param_groups[0]['lr']
        self.logger.info('learning rate = %.7f' % lr)

    def save_weights(self, filename, path=None):
        if path:
            file_path = path + '/' + filename
        else:
            file_path = filename
            path = os.path.dirname(filename)

        try:
            torch.save(self.net.state_dict(), file_path)
        except OSError:
            if os.path.exists(self.params.default_server_dir + '/' + path) is False:
                os.makedirs(self.params.default_server_dir + '/' + path)

            torch.save(self.net.state_dict(), self.params.default_server_dir + '/' + file_path)

            self.logger(f'Model weights saved to secondary default server location: '
                        f'{self.params.default_server_dir + "/" + file_path}')

    def load_weights(self, filename=None, state_dict=None):

        if self.net is None:
            raise LookupError('Net has not yet been made yet. Initialize first.')

        if state_dict is None:
            try:
                state_dict = torch.load(filename)

            except FileNotFoundError:
                filename = filename.split('.pth')[0] + '.pt'
                state_dict = torch.load(filename)

            except RuntimeError:
                state_dict = torch.load(filename, map_location=torch.device('cpu'))

        # Remove all 'module' in state_dict
        from collections import OrderedDict
        new_state_dict = OrderedDict()
        for k, v in state_dict.items():
            continue_val = 0

            if self.params.skip_load_layers is not None:
                for skip_layer in self.params.skip_load_layers:
                    if skip_layer in k:
                        continue_val = 1

            if continue_val == 1:
                continue

            if k.startswith('module.'):
                name = k[7:]  # remove 'module'.
                new_state_dict[name] = v
            else:
                new_state_dict[k] = v

        self.net.load_state_dict(new_state_dict, strict=True)
        self.net.to(self.device)

    def save_training_history(self, filename):
        """
        this method saves the training history to a file
        """
        self.df_tairning_history.to_csv(filename, index=False)

    def reinitialize_model(self):
        assert self.net is not None, 'Ensure a model already exists.'

        if isinstance(self.net, torch.nn.parallel.DataParallel):

            self.net.module.load_state_dict(self.base_state_dict)
        else:
            self.net.load_state_dict(self.base_state_dict)

    def load_model_from_params(self, *args, **kwargs):
        if self.params.pre_trained_model is True:
            self.net = self.model_base_fn(self.params, *args, **kwargs)
            self.load_weights(self.params.pre_trained_model_weights)
            self.base_state_dict = self.net.state_dict()

        elif self.params.load_from_checkpoint is True:
            self.net = self.model_base_fn(self.params, *args, **kwargs)
            self.__load_from_checkpoint(self.params.checkpoint_file)
            self.base_state_dict = self.net.state_dict()

        elif self.params.pre_trained_model is False:
            self.net = self.model_base_fn(self.params, *args, **kwargs)
            # self.initialize_weights()
            self.base_state_dict = self.net.state_dict(keep_vars=True)
            # self.base_state_dict = self.net.state_dict()

    def __save_model_during_training(self, epoch):
        model_weights_filename = self.params.model_name + '_' + str(epoch + 1) + '_weights.pt'
        filename = self.params.model_output_directory + '/' + model_weights_filename
        self.save_weights(filename=filename)

    def __save_best_epoch_during_training(self, epoch):
        # Initialize minimum loss to be slightly larger than loss of first epoch
        if self.min_loss is None:
            self.min_loss = self.loss_value + 1

        if self.loss_value < self.min_loss:
            self.best_epoch = epoch
            model_weights_filename = self.params.model_name + '_best_weights.pt'
            filename = self.params.model_output_directory + '/' + model_weights_filename
            self.save_weights(filename=filename)

    def __save_checkpoint(self, epoch):

        model_weights_filename = self.params.model_name + '_checkpoint.pt'
        filename = self.params.model_output_directory + '/' + model_weights_filename

        torch.save({
            'epoch': epoch,
            'model_state_dict': self.net.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
        }, filename)

    def __load_from_checkpoint(self, checkpoint_file):

        checkpoint = torch.load(checkpoint_file, map_location=torch.device('cpu'))
        state_dict = checkpoint['model_state_dict']
        # opt_state_dict = checkpoint['model_state_dict']
        epoch = checkpoint['epoch']

        if isinstance(self.optimizer, list):
            self.optimizer_dict = checkpoint['optimizer_state_dict']
        else:
            self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])


        self.load_weights(state_dict=state_dict)

        self.params.starting_epoch = epoch


    def set_requires_grad(self, nets, requires_grad=False):
        """Set requies_grad=Fasle for all the networks to avoid unnecessary computations
        Parameters:
            nets (network list)   -- a list of networks
            requires_grad (bool)  -- whether the networks require gradients or not
        """
        if not isinstance(nets, list):
            nets = [nets]
        for net in nets:
            if net is not None:
                for idx_child, child in enumerate(net.children()):
                    for layer_idx, (name, param) in enumerate(child.named_parameters()):
                        if (self.params.pre_trained_model == True) and (self.params.fine_tune_full == False):
                        # if self.params.pre_trained_model == True:
                            if name.__contains__('output_layer') is True:
                                param.requires_grad = True
                            elif name.__contains__('up_conv') is True:
                                param.requires_grad = True
                            else:
                                param.requires_grad = False
                        else:
                            param.requires_grad = requires_grad


    def get_loss_functions(self):
        # if self.params.multi_class_learning is True:
        #     multi_class_loss_keys = list(self.params.loss_dict.keys())
        #     self.loss_fns = {}
        #     self.loss_fns_weights = {}
        #     self.loss_names = []
        #     for key in multi_class_loss_keys:
        #         sub_loss_dict = self.params.loss_dict[key]
        #         loss_dict_args, loss_dict_names = self.loss_metric_name_mapping(sub_loss_dict, additional_name=key)
        #         for loss_name in loss_dict_args.keys():
        #             loss_params = loss_dict_args[loss_name]
        #             loss_display_name = loss_dict_names[loss_name]
        #             try:
        #                 loss_fn = getattr(self, loss_name)(**loss_params)
        #             except AttributeError:
        #                 loss_fn = getattr(self, loss_name + key.capitalize())(**loss_params)
        #             self.loss_fns[loss_display_name] = loss_fn
        #             self.loss_names.append(loss_display_name)
        # else:
        #     self.loss_fns = {}
        #     self.loss_fns_weights = {}
        #     self.loss_names = []
        #     loss_dict_args, loss_dict_names = self.loss_metric_name_mapping(self.params.loss_dict)
        #     for loss_name in loss_dict_args.keys():
        #         loss_params = loss_dict_args[loss_name]
        #         loss_display_name = loss_dict_names[loss_name]
        #         loss_fn = getattr(self, loss_name)(**loss_params)
        #         self.loss_fns[loss_display_name] = loss_fn
        #         self.loss_fns_weights[loss_display_name] = self.params.loss_weights_dict[loss_display_name]
        #         self.loss_names.append(loss_display_name)




        self.loss_fns = {}
        self.loss_fns_weights = {}
        self.loss_names = []
        # loss_dict_args, loss_dict_names = self.loss_metric_name_mapping(self.params.loss_dict)

        for loss_idx, loss_name in enumerate(self.params.loss_dict):
            ends_with_loss = loss_name.endswith('_loss')
            if hasattr(torch.nn.functional, loss_name):
                loss_fn = getattr(torch.nn.functional, loss_name)

            elif ends_with_loss is True:
                if hasattr(LossMetricFunctions, loss_name):
                    loss_fn = getattr(LossMetricFunctions, loss_name)
            elif ends_with_loss is False:
                if hasattr(LossMetricFunctions, loss_name + '_loss'):
                    name_proper = loss_name + '_loss'
                    loss_fn = getattr(LossMetricFunctions, name_proper)

            else:
                name_proper = LossMetricFunctions.dict_function_name_mappings[loss_name]
                if hasattr(torch.nn.functional, name_proper):
                    loss_fn = getattr(torch.nn.functional, name_proper)

                elif hasattr(LossMetricFunctions, name_proper):
                    loss_fn = getattr(LossMetricFunctions, name_proper)

                elif hasattr(self, name_proper):
                    loss_fn = getattr(self, name_proper)
                else:
                    raise NameError(f'{name_proper} is not found in any of the loss or metric functions.')

            self.loss_fns[loss_name] = BaseLossMetric(fn=loss_fn, fn_params=self.params.loss_dict[loss_name], device = self.device)
            self.loss_fns_weights[loss_name] = self.params.loss_weights_dict[loss_name]


        # for loss_name in loss_dict_args.keys():
        #     loss_params = loss_dict_args[loss_name]
        #     loss_display_name = loss_dict_names[loss_name]
        #     loss_fn = getattr(self, loss_name)(**loss_params)
        #     self.loss_fns[loss_display_name] = loss_fn
        #     self.loss_fns_weights[loss_display_name] = self.params.loss_weights_dict[loss_display_name]
        #     self.loss_names.append(loss_display_name)


    def get_metric_functions(self):
        # self.metric_fns = {}
        # metric_names = []
        # metric_dict_args, metric_dict_names = self.loss_metric_name_mapping(self.params.metrics_dict)
        # for metric_name in metric_dict_args.keys():
        #     metric_params = metric_dict_args[metric_name]
        #     metric_display_name = metric_dict_names[metric_name]
        #     metric_fn = getattr(self, metric_name)(**metric_params)
        #     self.metric_fns[metric_display_name] = metric_fn
        #     metric_names.append(metric_display_name)
        #
        # return metric_names


        self.metric_fns = {}
        metric_names = []



        for metric_idx, metric_name in enumerate(self.params.metrics_dict):
            secondary_name = LossMetricFunctions.dict_function_name_mappings[metric_name]

            if hasattr(torch.nn.functional, metric_name):
                metric_fn = getattr(torch.nn.functional, metric_name)
            # elif hasattr(self, metric_name):
            #     metric_fn = getattr(self, metric_name)
            #
            # # elif hasattr(self, secondary_name):
            # #     metric_fn = getattr(self, secondary_name)

            elif hasattr(LossMetricFunctions, metric_name):
                metric_fn = getattr(LossMetricFunctions, metric_name)

            else:
                name_proper = LossMetricFunctions.dict_function_name_mappings[metric_name]
                if hasattr(torch.nn.functional, name_proper):
                    metric_fn = getattr(torch.nn.functional, name_proper)

                elif hasattr(LossMetricFunctions, name_proper):
                    metric_fn = getattr(LossMetricFunctions, name_proper)

                elif hasattr(self, name_proper):
                    metric_fn = getattr(self, name_proper)
                else:
                    raise NameError(f'{name_proper} is not found in any of the loss or metric functions.')

            self.metric_fns[metric_name] = BaseLossMetric(fn=metric_fn, fn_params=self.params.metrics_dict[metric_name])
            metric_names.append(metric_name)


        return metric_names


    # @staticmethod
    # def loss_metric_name_mapping(loss_name, additional_name=None):
    #     """
    #     Function to change loss names to the appropriate format to work with rest of workflow.
    #     Parameters
    #     ----------
    #     fn_dict : dict
    #         dictionary of metric and loss function names.
    #
    #     Returns
    #     -------
    #
    #     """
    #     name_proper = LossMetricFunctions.dict_function_name_mappings[name]
    #
    #
    #     # Dictionary of accepted typos and names with corresponding loss functions.
    #     fn_dict_name = {}
    #     fn_dict_params = {}
    #     for name in fn_dict.keys():
    #         # Check to see if loss function defined already (either in torch or custom)
    #         # Make sure the given string is defined or can be mapped to the proper string
    #         name_proper = LossMetricFunctions.dict_function_name_mappings[name]
    #         fn_dict_params[name_proper] = fn_dict[name]
    #         fn_dict_name[name_proper] = name if additional_name is None else name + '_' + str(additional_name)
    #     return fn_dict_params, fn_dict_name

