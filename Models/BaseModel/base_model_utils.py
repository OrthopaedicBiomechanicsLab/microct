import os


def initialize_log_and_dir(time_stamp_dir):
    if os.path.exists(time_stamp_dir) is False:
        os.makedirs(time_stamp_dir)

    else:
        time_idx = 0
        while (os.path.exists(time_stamp_dir) is True):
            time_stamp_dir = time_stamp_dir + '_' + str(time_idx)
        os.makedirs(time_stamp_dir)

    if training_log_name is None:
        training_log_name = top_output_directory + '/' + time_stamp + '/train.log'
    else:
        training_log_name = top_output_directory + '/' + time_stamp + '/' + training_log_name
    logging_dict = logging_dict_config(training_log_name)

    logging_dictConfig(self.logging_dict)
    logger = logging.getLogger(__name__)
    logger_format = logging.Formatter(fmt=logging_dict['formatters']['f']['format'],
                                      datefmt=logging_dict['formatters']['f']['datefmt'])

    logger.info('Training data can be found in: ' + spine_main_path)


def build_filenames(self):

    self.model_output_directory = self.output_directory

    self.model_json_filename = FileNamesUtil.create_model_json_filename(
        self.model_output_directory,
        self.keyword)

    self.model_weights_filename = FileNamesUtil.create_model_weights_filename(
        self.model_output_directory,
        self.keyword)

    self.training_history_filename = FileNamesUtil.create_training_history_filename(
        self.model_output_directory,
        self.keyword)

    self.model_training_params = FileNamesUtil.create_training_params_filename(
        self.model_output_directory,
        self.keyword)

    self.logger.info('model json file is ' + str(self.model_json_filename))
    self.logger.info('model weights file is ' + str(self.model_weights_filename))
    self.logger.info('training history file is ' + str(self.training_history_filename))


def save_training_history(self):
    """
    this method saves the training history to a file
    """

    try:
        self.history_df.to_csv(self.training_history_filename, index=False)
        self.logger.info('history was saved successfully to ' + self.training_history_filename)
    except:
        self.logger.error('history file could not be saved to ' + self.training_history_filename)


def save_evaluation_filename(self):
    """
    this method creates a filename for the evaluation results.
    the file is located in the self.output_directory and it is
    built based on the name of the self.model_weights_filename

    Example:
        if the following values are set for the output_directory and the
        model_weights_filename,
        self.output_directory = 'd:/output/'
        self.model_weights_filename = 'd:/weights/weights123.h5'
        then, the self.evaluation_filename will be
        'd:/output/weights123.csv'
    """

    # create a csv filename inside the output_directory


    self.evaluation_filename = os.path.join(
        self.model_output_directory,
        'evaluation_' + self.keyword)

    self.measures_df.to_csv(self.evaluation_filename + '.csv', index=False)
    self.measures_df.to_pickle(self.evaluation_filename + '.pickle')

    self.logger.info('evaluation measures are saved to:' + self.evaluation_filename)



def initialize_log_and_dir_cv(self):


    if not os.path.exists(self.time_stamp_dir):
        os.makedirs(self.time_stamp_dir)

    if self.training_log_name is None:
        self.training_log_name = self.top_output_directory + '/' + self.time_stamp + '/train.log'

    else:
        self.training_log_name = self.top_output_directory + '/' + self.time_stamp + '/' + self.training_log_name

    self.logging_dict = logging_dict_config(self.training_log_name)

    logging_dictConfig(self.logging_dict)
    self.logger = logging.getLogger(__name__)
    self.logger_format = logging.Formatter(fmt=self.logging_dict['formatters']['f']['format'],
                                           datefmt=self.logging_dict['formatters']['f']['datefmt'])

    self.logger.info('Training data can be found in: ' + self.spine_main_path)


def build_model_param_directory_cv(self, model_type, grid_search):

    self.keyword = FileNamesUtil.build_filename_keyword(model_type, grid_search)

    self.output_directory = FileNamesUtil.create_output_directory(self.top_output_directory,
                                                                  os.path.join(self.time_stamp, self.keyword))


def build_filenames_cv(self):

    self.model_output_directory = self.output_directory


    self.model_json_filename = FileNamesUtil.create_model_json_filename(
        self.model_output_directory,
        self.keyword)

    self.model_training_params = FileNamesUtil.create_training_params_filename(
        self.model_output_directory,
        self.keyword)

    self.logger.info('model json file is ' + str(self.model_json_filename))

    self.logger.info(
        'training history file is ' + str(self.training_history_filename))


def build_model_weight_filename_cv(self, k_fold):

    self.model_weights_filename = FileNamesUtil.create_model_weights_filename(
        self.model_output_directory,
        self.keyword,
        k_fold=k_fold)

    self.logger.info(
        'model weights file is ' + str(self.model_weights_filename))


def save_training_history_cv(self, k_fold):
    """
    this method saves the training history to a file
    """

    self.training_history_filename = FileNamesUtil.create_training_history_filename(
        self.model_output_directory,
        self.keyword,
        k_fold=k_fold)

    try:
        self.history_df.to_csv(self.training_history_filename, index=False)
        self.logger.info(
            'history was saved successfully to ' + self.training_history_filename)
    except:
        self.logger.error(
            'history file could not be saved to ' + self.training_history_filename)

def save_total_grid_search_params_cv(self):

    self.training_params[GS_Util.MODEL_PARAMS()] = self.model_params
    self.training_params[GS_Util.OPT_PARAMS()] = self.optimizer_params

    params_json_file = json.dumps(self.training_params)
    with open(self.model_training_params, 'w') as json_file:
        json_file.write(params_json_file)


def save_model_to_json_cv(self):
    """
    this method saves the model's json file, required for loading the
    model later, properly
    """
    model_json = self.model.to_json()

    if os.path.exists(self.model_json_filename) is False:

        with open(self.model_json_filename, 'w') as jason_file:
            jason_file.write(model_json)
            self.logger.info(
                'model saved successfully to ' + self.model_json_filename)


def save_evaluation_filename_cv(self, k_fold=None):
    """
    this method creates a filename for the evaluation results.
    the file is located in the self.output_directory and it is
    built based on the name of the self.model_weights_filename

    Example:
        if the following values are set for the output_directory and the
        model_weights_filename,
        self.output_directory = 'd:/output/'
        self.model_weights_filename = 'd:/weights/weights123.h5'
        then, the self.evaluation_filename will be
        'd:/output/weights123.csv'
    """

    # create a csv filename inside the output_directory

    if k_fold is None:
        self.evaluation_filename = os.path.join(
            self.model_output_directory,
            'evaluation_' + self.keyword)

        self.measures_df.to_csv(self.evaluation_filename + '.csv', index=False)
        self.measures_df.to_pickle(self.evaluation_filename + '.pickle')

        self.logger.info(
            'evaluation measures are saved to:' + self.evaluation_filename)

    else:
        self.evaluation_filename_kfold = os.path.join(
            self.model_output_directory,
            'evaluation_' + self.keyword + '_k_fold_' + str(k_fold))

        self.measures_df_k_fold.to_csv(self.evaluation_filename_kfold + '.csv', index=False)
        self.measures_df_k_fold.to_pickle(self.evaluation_filename_kfold + '.pickle')


        self.logger.info(
            'evaluation measures are saved to:' + self.evaluation_filename_kfold)
