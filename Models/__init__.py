import importlib
from Models.BaseModel import BaseModel

from . import model_core

def find_model_from_name(params):

    dict_of_models = {'UNet': 'UNet',
                      'unet': 'UNet',
                      'ResUNet': 'ResUNet',
                      'UNetClassifier': 'UNetClassifier',
                      'UNetClass': 'UNetClassifier',
                      'unet_class': 'UNetClassifier',
                      'unet_patch': 'UNetClassifierPatch',
                      'UNetPatch': 'UNetClassifierPatch',
                      'UNetClassifierPatch': 'UNetClassifierPatch',
                      'AGUNet': 'AGUNet',
                      'AGUNetDetect': 'AGUNetDetect',
                      'FasterRCNN': 'FasterRCNN',
                      'FasterRCNNTorch': 'FasterRCNN',
                      'RetinaUNet': 'RetinaUNet',
                      'SPNet': 'SPNet',
                      'RetinaUNetHeatmap': 'RetinaUNetHeatmap',
                      'R2AttUNet': 'R2AttUNet',}

    # model_filename = dict_of_models[params.model]

    model_filename = dict_of_models[params.model]
    model_filename = 'Models.' + model_filename
    model_lib = importlib.import_module(model_filename)

    model = None
    for name, cls in model_lib.__dict__.items():
        if (name.lower() == params.model.lower()) and (issubclass(cls, BaseModel)):
            model = cls

    if model is None:
        raise ImportError('No accepted model name has been passed. Please refer to documentation of a list of accepted.'
                          ' model names, with appropriate formatting and case sensitivity.')

    return model

def create_model(params, logger=None):
    """
    This will create and build the model based on the specific parameters passed which need to be defined prior.

    Parameters
    ----------
    params : Parameters.Parameters
        The object for the parameters used to define and build the model.

    Returns
    -------

    """
    model = find_model_from_name(params)
    instance = model(params, logger)
    return instance


if __name__ == '__main__':

    create_model('UNet')