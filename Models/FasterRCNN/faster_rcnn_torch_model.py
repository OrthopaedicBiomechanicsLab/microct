from Models.BaseModel import BaseModel
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *
from torchvision.models.detection import fasterrcnn_resnet50_fpn
import importlib

class FasterRCNNTorchModel(BaseModel):
    def __init__(self, params, logger=None):
        # super(UNetModel, self).__init__(params)
        BaseModel.__init__(self, params, logger)

        if params.isTrain == True:
            self.net = fasterrcnn_resnet50_fpn(pretrained=False, progress=True, num_classes=13,
                                               pretrained_backbone=True)
        else:
            self.net = fasterrcnn_resnet50_fpn(pretrained=False, progress=False, num_classes=13,
                                               pretrained_backbone=False)
            self.net.eval()

        # self.load_model_from_params()

        # torch.autograd.set_detect_anomaly(True)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)
            self.setup_lr_scheduler(params)


    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """
        self.image = input['image'].to(self.device)
        # self.label = [x.to(self.device) for x in input['label']]


        self.label = []
        for batch_idx in range(self.params.batch_size):
            label_dict = {}
            label_dict['labels'] = input['label'][0][batch_idx].type(torch.int64).to(self.device)
            label_dict['boxes'] = input['label'][1][batch_idx].to(self.device)

            self.label.append(label_dict)

        a = 1

        # label = [rpn_match, rpn_bbox_delta, gt_class_ids, gt_bbox]



    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        try:
            self.pred = self.net(self.image, self.label)
        except:
            a = 1
        # import SimpleITK
        # import SimpleITK as sitk
        # hold = self.image.cpu().detach().numpy().squeeze()
        # img = sitk.GetImageFromArray(hold)
        # sitk.WriteImage(img, 'hold.nii.gz')
        # hold.max()

        a = 1
        # pred = (rpn_class_logits, rpn_class_probs, rpn_bbox_reg,
        #         rcnn_class_logits, rcnn_class, rcnn_bbox,
        #         target_class_ids, target_bbox_deltas)

    def backward(self):
        """Calculate UNet model loss"""
        # self.loss = self.calc_loss()

        self.loss = sum(self.pred.values())

        self.loss.backward()

        # if importlib.util.find_spec('deepspeed') is not None:
        #     import deepspeed
        #     self.net.backward(self.loss)
        #     self.net.step()
        # else:
        #     self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.optimizer.step()          # update weights


    def DSCLoss(self, **fn_params):
        return BaseLossMetric(dsc_loss, **fn_params)

    def BCELoss(self, **fn_params):
        return BaseLossMetric(bce_loss, **fn_params)

    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, **fn_params)

    def CONCURRENCY(self, **fn_params):
        return BaseLossMetric(concurrency, **fn_params)

    def CrossEntropyLoss(self, **fn_params):
        return BaseLossMetric(ce_loss, **fn_params)

    def L1Loss(self, **fn_params):
        return BaseLossMetric(l1_loss, device=self.device, **fn_params)

    def L2Loss(self, **fn_params):
        return BaseLossMetric(l2_loss, device=self.device, **fn_params)

    def L12Loss(self, **fn_params):
        return BaseLossMetric(l12_loss, device=self.device, **fn_params)


    def RPNClassLoss(self, **fn_params):
        return BaseLossMetric(rpn_class_loss, label_index=0, pred_index=0, device=self.device, **fn_params)

    def RPNBoxLoss(self, **fn_params):
        return BaseLossMetric(rpn_bbox_loss, label_index=[0, 1], pred_index=2, device=self.device, **fn_params)

    def RCNNClassLoss(self, **fn_params):
        return BaseLossMetric(frcnn_class_loss_graph, label_index='skip', pred_index=[6, 3], device=self.device, **fn_params)

    def RCNNBoxLoss(self, **fn_params):
        return BaseLossMetric(frcnn_bbox_loss_graph, label_index='skip', pred_index=[7, 6, 5], device=self.device, **fn_params)










