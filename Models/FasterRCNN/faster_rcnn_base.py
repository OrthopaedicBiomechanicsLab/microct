from Models.model_core import *

from Models.RegionProposal import RPNBase
from Models.RegionProposal import GenerateRPNProposals
from Models.ResNet import ResNetBase
from Models.FeaturePyramidNetwork import FPNForward
from Models.Anchors2D import get_anchors
from Models.ProposalLayer import PropsalLayer
from Models.DetectionTargetLayer import DetectionTargetLayer
from Models.DetectionLayer import DetectionLayer
from Models.FeaturePyramidNetwork import FPNClassifier

import Models.utils as utils

class FasterRCNN(nn.Module):
    def __init__(self, params):
        """

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """

        super(FasterRCNN, self).__init__()

        self.params = params

        input_shape = params.input_shape
        input_shape = np.array(input_shape)

        self.resnet_backbone = ResNetBase(params)


        self.fpn_forward = FPNForward(params)

        fpn_output_sizes = self.fpn_forward.fpn_output_sizes

        self.rpn_graph = RPNBase(params, fpn_output_sizes)

        self.proposal_layer = PropsalLayer(params)

        self.detection_target_layer = DetectionTargetLayer(params)

        self.fpn_classifier = FPNClassifier(params)

        self.generate_rpn_proposals = GenerateRPNProposals(params)

        self.final_detection = DetectionLayer(params)

    def forward(self, x, label=None):

        anchors = torch.stack([torch.tensor(get_anchors(self.params, normalize=False),
                                            dtype=torch.float32,
                                            device=x.device)] * x.shape[0], dim=0)


        gt_class_ids, gt_bbox = label



        rpn_match, rpn_gt_bbox, rpn_gt_class_ids = self.generate_rpn_proposals(gt_bbox, gt_class_ids, anchors)

        # rpn_match, rpn_gt_bbox, gt_class_ids, gt_bbox = label


        # gt_bbox = self.normalize_input_coords(gt_bbox)
        # anchors = utils.normalize_coordinates2D_torch(anchors, self.params)

        _, C2, C3, C4, C5 = self.resnet_backbone(x)

        P2, P3, P4, P5, P6 = self.fpn_forward([C2, C3, C4, C5])

        rpn_class_logits, rpn_class_probs, rpn_bbox_reg = self.rpn_graph([P2, P3, P4, P5, P6])

        # return rpn_class_logits, rpn_class_probs, rpn_bbox_reg


        rpn_rois = self.proposal_layer(rpn_class_probs, rpn_bbox_reg, anchors)

        # rcnn_feature_maps = [P2, P3, P4, P5]
        # if self.training:
        #     rcnn_class_logits, rcnn_class, rcnn_bbox = self.forward_train(rpn_rois,
        #                                                                   gt_class_ids,
        #                                                                   gt_bbox,
        #                                                                   rcnn_feature_maps)
        # else:


        rois, target_class_ids, target_bbox_deltas, target_bbox_center = self.detection_target_layer(rpn_rois,
                                                                                                     gt_class_ids,
                                                                                                     gt_bbox)

        rcnn_feature_maps = [P2, P3, P4, P5]

        rcnn_class_logits, rcnn_class, rcnn_bbox_deltas = self.fpn_classifier(rois, rcnn_feature_maps)


        # Detections
        # output is [batch, num_detections, (x1, y1, x2, y2, class_id, score)]
        # detections = self.final_detection(rois, rcnn_class, rcnn_bbox_deltas)


        return (rpn_match, rpn_gt_bbox, rpn_gt_class_ids,
                rpn_class_logits, rpn_class_probs, rpn_bbox_reg,
                rcnn_class_logits, rcnn_class, rcnn_bbox_deltas,
                target_class_ids, target_bbox_deltas, rois, rpn_rois, rcnn_feature_maps)



    # def forward_train(self, rpn_rois,
    #                        gt_class_ids,
    #                        gt_bbox,
    #                        rcnn_feature_maps):
    #
    #     rois, target_class_ids, target_bbox_deltas, target_bbox_center = self.detection_target_layer(rpn_rois,
    #                                                                                                  gt_class_ids,
    #                                                                                                  gt_bbox)
    #
    #     # rcnn_feature_maps = [P2, P3, P4, P5]
    #
    #     rcnn_class_logits, rcnn_class, rcnn_bbox = self.fpn_classifier(rois, rcnn_feature_maps)


    # def inference(self,
    #               rois,
    #               rcnn_feature_maps
    #               ):
    #     rcnn_class_logits, rcnn_class, rcnn_bbox = fpn_classifier(rpn_rois, rcnn_feature_maps)
    #
    #
    #     # Detections
    #     # output is [batch, num_detections, (y1, x1, y2, x2, class_id, score)] in
    #     # normalized coordinates
    #     detections = DetectionLayer(rpn_rois, rcnn_class, rcnn_bbox)

    def normalize_input_coords(self, x):

        zero_idx = torch.where(torch.sum(x, dim=-1) == 0)

        x_norm = utils.normalize_coordinates2D_torch(x, self.params)

        # Re-zero padded portions
        x_norm[zero_idx] = 0

        return x_norm


