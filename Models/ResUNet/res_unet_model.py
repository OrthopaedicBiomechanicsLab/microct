from ..BaseModel import BaseModel
from .res_unet_base import ResUNet
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *
import importlib
import os
import pandas as pd

class ResUNetModel(BaseModel):
    def __init__(self, params, logger=None):
        # super(UNetModel, self).__init__(params)


        BaseModel.__init__(self, params, logger)

        self.model_base_fn = ResUNet
        self.load_model_from_params()
        #
        # self.net = UNet(params)


        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)
            self.setup_lr_scheduler(params)

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

        self.df_columns = ['spine_sample', 'dsc']

        self.during_training_df = pd.DataFrame(columns=self.df_columns)

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """

        if len(input.items()) == 1:
            self.image = input['image'].to(self.device)
        else:
            self.image = input['image'].to(self.device)
            self.label = input['label'].to(self.device)

        # if self.net.training is True:
        #     self.image = input['image'].to(self.device)
        #     self.label = input['label'].to(self.device)
        #
        # if self.net.training is False:
        #     if len(input.items()) == 1:
        #         self.image = input['image'].to(self.device)
        #     else:
        #         self.image = input['image'].to(self.device)
        #         self.label = input['label'].to(self.device)

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        self.pred = self.net(self.image)

        if self.net.training is False:
            if self.pred.shape[1] > 1:
                self.pred = torch.nn.functional.softmax(self.pred, dim=1)
                self.pred = torch.argmax(self.pred, dim=1, keepdim=True)


    def backward(self):
        """Calculate UNet model loss"""
        self.loss = self.calc_loss()

        if importlib.util.find_spec('apex') is not None:
            from apex import amp
            with amp.scale_loss(self.loss, self.optimizer) as scaled_loss:
                scaled_loss.backward()

        else:
            self.loss.backward()

        # if importlib.util.find_spec('deepspeed') is not None:
        #     import deepspeed
        #     self.net.backward(self.loss)
        #     self.net.step()
        # else:
        #     self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.optimizer.step()          # update weights


    def model_specific_training(self, sample, epoch):

        import SimpleITK as sitk


        img_batch = self.image
        label_batch = self.label
        pred_batch = self.pred
        spine_sample_batch = sample['spine_sample']




        batch_size = img_batch.shape[0]

        for idx in range(batch_size):

            img = img_batch[idx]
            label = label_batch[idx]
            spine_sample = spine_sample_batch[idx]

            pred = pred_batch[idx]

            dsc_value = dsc(pred, label, device=pred.device)

            img_np = img.detach().cpu().numpy()[0]
            label_np = label.detach().cpu().numpy()[0]
            pred_np = pred.detach().cpu().numpy()[0]

            pred_np = np.where(pred_np > 0.5, 1., 0.)

            if os.path.exists(self.params.model_output_directory + '/' + 'epoch_' + str(epoch)) is False:
                os.makedirs(self.params.model_output_directory + '/' + 'epoch_' + str(epoch))

            self.save_df_name = self.params.model_output_directory + '/' 'epoch_' + str(epoch) + '/' + 'df_training_epoch_' + str(epoch)

            sitk.WriteImage(sitk.GetImageFromArray(img_np),
                            self.params.model_output_directory + '/' + 'epoch_' + str(epoch) + '/' + spine_sample + '_sample.nii.gz')


            sitk.WriteImage(sitk.GetImageFromArray(label_np),
                            self.params.model_output_directory + '/' + 'epoch_' + str(epoch) + '/' + spine_sample + '_label.nii.gz')


            sitk.WriteImage(sitk.GetImageFromArray(pred_np),
                            self.params.model_output_directory + '/' + 'epoch_' + str(epoch) + '/' + spine_sample + '_pred.nii.gz')

            train_dict_hold = {'spine_sample': spine_sample,
                               'dsc': dsc_value.item()}

            self.during_training_df = self.during_training_df.append(train_dict_hold, ignore_index=True)


    def reset_model_specific_training(self):

        self.save_in_training_df()
        self.during_training_df = pd.DataFrame(columns=self.df_columns)


    def save_in_training_df(self):
        self.during_training_df.to_csv(self.save_df_name + '.csv', index=False)


    def DSCLoss(self, **fn_params):
        return BaseLossMetric(dsc_loss, **fn_params)

    def BCELoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     self.logger.info('Number of classes set >1, therefore using cross entropy loss instead.')
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(bce_loss, **fn_params)

    def BCEWindowLoss(self, **fn_params):
        return BaseLossMetric(bce_loss_windowed, device=self.device, **fn_params)

    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, **fn_params)

    def CONCURRENCY(self, **fn_params):
        return BaseLossMetric(concurrency, **fn_params)

    def CrossEntropyLoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     self.logger.info('Number of classes set to 1, therefore using binary cross entropy loss instead')
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(ce_loss, **fn_params)

    def L1Loss(self, **fn_params):
        return BaseLossMetric(l1_loss, device=self.device, **fn_params)

    def L2Loss(self, **fn_params):
        return BaseLossMetric(l2_loss, device=self.device, **fn_params)

    def L12Loss(self, **fn_params):
        return BaseLossMetric(l12_loss, device=self.device, **fn_params)

    def SSIMLoss(self, **fn_params):
        return BaseLossMetric(ssim_loss(self.params, device=self.device), device=self.device, **fn_params)

    # def SSIM(self, **fn_params):
    #     return BaseLossMetric(ssim(self.params, device=self.device), device=self.device, **fn_params)


    def SagShapeLoss(self, **fn_params):
        return BaseLossMetric(sag_shape_loss, device=self.device, **fn_params)

    #
    # def CONCURRENCY(self, **fn_params):
    #     def fn(pred, target):
    #         smooth = 1e-7
    #         y_true_f = target.reshape(target.shape[0], -1)
    #         y_pred_f = pred.reshape(pred.shape[0], -1)
    #         intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    #
    #         con = 1 / 2 * (intersection * (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1))) / (
    #                 torch.sum(y_true_f, dim=-1) * torch.sum(y_pred_f, dim=-1) + smooth)
    #         return con.mean()
    #     return fn
    #
    #
    # def DSC(self, **fn_params):
    #     def fn(pred, target, neg=False):
    #         y_true_f = target.reshape(target.shape[0], -1)
    #         y_pred_f = pred.reshape(pred.shape[0], -1)
    #         intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    #         smooth = 1e-7
    #         dsc = (2. * intersection) / (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1) + smooth)
    #         if neg == True:
    #             dsc = 1 - dsc
    #         return dsc.mean()
    #     return fn











