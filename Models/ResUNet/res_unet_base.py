import torch
import torch.nn as nn
import numpy as np
from Models.model_core import *

class ResUNet(nn.Module):
    def __init__(self, params):
        super(ResUNet, self).__init__()

        input_shape = params.input_shape
        num_in_channels = params.num_in_channels
        num_out_channels = params.num_out_channels
        base_filter = params.base_filter
        unet_depth = params.depth

        kernel_size = params.kernel_size
        batch_normalization = params.batch_normalization
        pool_size = params.pool_size
        stride = params.stride
        dilation = params.dilation
        dropout = params.dropout

        dropout_down_conv = params.dropout_down_conv
        dropout_up_conv = params.dropout_up_conv

        normalization_momentum = params.normalization_momentum

        num_conv = params.num_conv



        self.down_layer_list = nn.ModuleList()
        self.up_layer_list = nn.ModuleList()

        input_shape = np.array(input_shape)

        # Down Convolutions
        for layer_depth in range(unet_depth):

            if layer_depth == 0:
                max_pooling = False
                in_channels = num_in_channels
                input_shape = input_shape
            else:
                max_pooling = True
                in_channels = base_filter * (2 ** (layer_depth - 1))
                input_shape = input_shape / 2

            x = UNetConvModule(
                in_channels=in_channels,
                out_channels=base_filter * (2 ** layer_depth),
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout_down_conv,
                batch_normalization=batch_normalization,
                normalization_momentum=normalization_momentum,
                max_pooling=max_pooling,
                activation='relu')

            # hold = nn.ModuleList()
            # hold.append(layer_depth, x)
            self.down_layer_list.append(x)




        # going up
        for layer_depth in range(unet_depth - 2, -1, -1):

            if layer_depth == unet_depth - 2:
                input_shape = input_shape
            else:
                input_shape = input_shape * 2

            x = UNetUpBlock(
                in_channels=base_filter * (2 ** (layer_depth + 1)),
                out_channels=base_filter * (2 ** layer_depth),
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout_up_conv,
                batch_normalization=batch_normalization,
                normalization_momentum=normalization_momentum,
                activation='relu',
                num_conv=num_conv)


            self.up_layer_list.append(x)


        final_conv = nn.Conv3d(in_channels=base_filter,
                               out_channels=num_out_channels,
                               kernel_size=1,
                               )
        if num_out_channels == 1:
            output_activation = ActivationLayer('sigmoid')
        elif num_out_channels > 1:
            # output_activation = ActivationLayer('softmax', dim=1)
            output_activation = ActivationLayer('None', dim=1)
        else:
            output_activation = ActivationLayer('sigmoid')


        self.output_layer = nn.Sequential(final_conv, output_activation)


    def forward(self, x):
        down_blocks = []
        for layer_depth, down_conv in enumerate(self.down_layer_list):
            x = down_conv(x)
            down_blocks.append(x)

        for layer_depth, up_conv in enumerate(self.up_layer_list):
            x = up_conv(x, down_blocks[-layer_depth - 2])

        x = self.output_layer(x)

        return x




class UNetConvModule(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 max_pooling=True,
                 activation=None,
                 num_conv=2):

        super(UNetConvModule, self).__init__()


        # unet_conv_module = []
        self.max_pooling = max_pooling
        if max_pooling:
            self.pooling_layer = nn.MaxPool3d(pool_size)
            # unet_conv_module += [pooling_layer]

        conv1 = ResConvBlock(in_channels=in_channels,
                             out_channels=out_channels,
                             kernel_size=kernel_size,
                             stride=stride,
                             dilation=dilation,
                             batch_normalization=batch_normalization,
                             normalization_momentum=normalization_momentum,
                             activation=activation,
                             padding=padding,
                             input_shape=input_shape,
                             num_conv=num_conv)

        conv2 = ResConvBlock(in_channels=out_channels,
                             out_channels=out_channels,
                             kernel_size=kernel_size,
                             stride=stride,
                             dilation=dilation,
                             batch_normalization=batch_normalization,
                             normalization_momentum=normalization_momentum,
                             activation=activation,
                             padding=padding,
                             input_shape=input_shape,
                             num_conv=num_conv)

        unet_conv_module = [conv1, conv2]

        if dropout > 0:
            dp_layer = nn.Dropout3d(dropout)
            unet_conv_module += [dp_layer]

        self.unet_conv_module = nn.Sequential(*unet_conv_module)


    def forward(self, x):
        input_tensor = x

        if self.max_pooling:
            input_tensor = self.pooling_layer(x)

        x = self.unet_conv_module(input_tensor)

        return x

class UNetUpBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 activation=None,
                 num_conv=2):

        super(UNetUpBlock, self).__init__()

        self.up_conv = UpConv3D(in_channels=in_channels,
                                out_channels=out_channels,
                                kernel_size=2,
                                stride=2,
                                dilation=1,
                                input_shape=input_shape,
                                batch_normalization=False,
                                normalization_momentum=normalization_momentum)

        # print(sum(p.numel() for p in self.up_conv.parameters()))

        self.conv_block = UNetConvModule(
            in_channels=in_channels,
            out_channels=out_channels,
            input_shape=input_shape,
            kernel_size=kernel_size,
            padding=padding,
            pool_size=pool_size,
            stride=stride,
            dilation=dilation,
            dropout=dropout,
            batch_normalization=batch_normalization,
            normalization_momentum=normalization_momentum,
            max_pooling=False,
            activation=activation,
            num_conv=num_conv)

    def forward(self, x1, x2):
        """
        Parameters
        ----------
        x1 : torch.Tensor
            Tensor from up_conv layer.
        x2 : torch.Tensor
            Tensor from down_conv layer that gets concatenated with after up convolution.

        Returns
        -------

        """
        x_up = self.up_conv(x1)
        x = torch.cat([x_up, x2], dim=1)
        x = self.conv_block(x)
        return x

