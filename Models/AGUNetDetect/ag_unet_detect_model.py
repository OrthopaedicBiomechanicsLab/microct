from ..BaseModel import BaseModel
from .ag_unet_detect_base import AGUNetDetect
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *

class AGUNetDetectModel(BaseModel):
    def __init__(self, params, logger=None):
        # super(UNetModel, self).__init__(params)
        BaseModel.__init__(self, params, logger)

        self.model_base_fn = AGUNetDetect
        self.load_model_from_params()
        #
        # self.net = UNet(params)


        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """
        self.image = input['image'].to(self.device)
        self.label = [x.to(self.device) for x in input['label']]

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        self.pred = self.net(self.image)

    def backward(self):
        """Calculate UNet model loss"""
        self.loss = self.calc_loss()
        self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.optimizer.step()          # update weights


    def model_specific_metrics(self, metrics):

        pred = self.pred[1]
        label = self.label[1]

        x0_pred = pred[0] * self.params.sample_size[0]
        y0_pred = pred[1] * self.params.sample_size[1]
        z0_pred = pred[2] * self.params.sample_size[2]

        width_pred = pred[3] * self.params.sample_size[0]
        height_pred = pred[4] * self.params.sample_size[1]
        depth_pred = pred[5] * self.params.sample_size[2]

        x0_gt = label[0] * self.params.sample_size[0]
        y0_gt = label[1] * self.params.sample_size[1]
        z0_gt = label[2] * self.params.sample_size[2]

        width_gt = label[3] * self.params.sample_size[0]
        height_gt = label[4] * self.params.sample_size[1]
        depth_gt = label[5] * self.params.sample_size[2]

        metrics['x0_pred'] = x0_pred
        metrics['y0_pred'] = y0_pred
        metrics['z0_pred'] = z0_pred

        metrics['x0_gt'] = x0_gt
        metrics['y0_gt'] = y0_gt
        metrics['z0_gt'] = z0_gt

        metrics['width_pred'] = width_pred
        metrics['height_pred'] = height_pred
        metrics['depth_pred'] = depth_pred

        metrics['width_gt'] = width_gt
        metrics['height_gt'] = height_gt
        metrics['depth_gt'] = depth_gt

        return metrics

    def SmoothL1Loss(self, **fn_params):
        return BaseLossMetric(smooth_l1_loss, label_index=1, pred_index=1, device=self.device, **fn_params)

    def L1Loss(self, **fn_params):
        return BaseLossMetric(l1_loss, label_index=1, pred_index=1, device=self.device, **fn_params)

    def DSCLoss(self, **fn_params):
        return BaseLossMetric(dsc_loss, label_index=1, pred_index=1, device=self.device, **fn_params)

    def BCELoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     self.logger.info('Number of classes set >1, therefore using cross entropy loss instead.')
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(bce_loss, label_index=1, pred_index=1, device=self.device, **fn_params)

    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, label_index=1, pred_index=1, device=self.device, **fn_params)

    def CONCURRENCY(self, **fn_params):
        return BaseLossMetric(concurrency, device=self.device, **fn_params)

    def CrossEntropyLoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     self.logger.info('Number of classes set to 1, therefore using binary cross entropy loss instead')
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(ce_loss, device=self.device, **fn_params)

    #
    # def CONCURRENCY(self, **fn_params):
    #     def fn(pred, target):
    #         smooth = 1e-7
    #         y_true_f = target.reshape(target.shape[0], -1)
    #         y_pred_f = pred.reshape(pred.shape[0], -1)
    #         intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    #
    #         con = 1 / 2 * (intersection * (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1))) / (
    #                 torch.sum(y_true_f, dim=-1) * torch.sum(y_pred_f, dim=-1) + smooth)
    #         return con.mean()
    #     return fn
    #
    #
    # def DSC(self, **fn_params):
    #     def fn(pred, target, neg=False):
    #         y_true_f = target.reshape(target.shape[0], -1)
    #         y_pred_f = pred.reshape(pred.shape[0], -1)
    #         intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    #         smooth = 1e-7
    #         dsc = (2. * intersection) / (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1) + smooth)
    #         if neg == True:
    #             dsc = 1 - dsc
    #         return dsc.mean()
    #     return fn











