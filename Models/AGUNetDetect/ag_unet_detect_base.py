import torch
import torch.nn as nn
from Models.model_core import *
from Models.weight_initializer import init_weights
from .GridAttentionBlock import GridAttentionBlockND
from .MultiAttentionBlock import MultiAttentionBlock

class AGUNetDetect(nn.Module):
    def __init__(self, params):
        super(AGUNetDetect, self).__init__()

        input_shape = params.input_shape
        num_in_channels = params.num_in_channels
        num_out_channels = params.num_out_channels
        base_filter = params.base_filter
        unet_depth = params.depth

        kernel_size = params.kernel_size
        batch_normalization = params.batch_normalization
        pool_size = params.pool_size
        stride = params.stride
        dilation = params.dilation
        dropout = params.dropout

        residuals = params.residuals
        normalization_momentum = params.normalization_momentum

        non_local_mode = params.non_local_mode

        sub_sample_factor = params.sub_sample_factor

        init_weights_type = params.init_weights_type

        align_corners = params.align_corners

        in_channels_list = [base_filter * (2 ** (layer_depth - 1)) if layer_depth > 0 else num_in_channels for layer_depth in range(unet_depth)]
        out_channels_list = [base_filter * (2 ** (layer_depth)) for layer_depth in range(unet_depth)]

        filters_list = [base_filter * (2 ** (layer_depth)) for layer_depth in range(unet_depth)]

        self.down_layer_list = nn.ModuleList()
        self.up_layer_list = nn.ModuleList()
        self.attention_layer_list = nn.ModuleList()
        self.ds_list = nn.ModuleList()

        input_shape = np.array(input_shape)
        output_shape = np.array(input_shape)

        for layer_depth in range(unet_depth):

            if layer_depth == 0:
                max_pooling = False
                in_channels = num_in_channels
                input_shape = input_shape
            else:
                max_pooling = True
                in_channels = base_filter * (2 ** (layer_depth - 1))
                input_shape = input_shape / 2

            x = UNetConvModule(
                in_channels=in_channels,
                out_channels=base_filter * (2 ** layer_depth),
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout,
                batch_normalization=batch_normalization,
                normalization_momentum=normalization_momentum,
                max_pooling=max_pooling,
                residuals=residuals,
                activation='relu',
                init_weights_type=init_weights_type)

            # hold = nn.ModuleList()
            # hold.append(layer_depth, x)
            self.down_layer_list.append(x)


        self.gating_center = UNetGridGatingSignal3(in_channels=out_channels_list[-1], out_channels=out_channels_list[-1],
                                                   kernel_size=1, input_shape=input_shape,
                                                   batch_normalization=batch_normalization,
                                                   init_weights_type=init_weights_type,
                                                   )
        bottle_neck_shape = input_shape

        # attention blocks
        for layer_depth in range(unet_depth - 1, 1, -1):

            attention = MultiAttentionBlock(in_channels=filters_list[layer_depth - 1],
                                            gate_size=filters_list[layer_depth], inter_size=filters_list[layer_depth - 1],
                                            non_local_mode=non_local_mode, sub_sample_factor=sub_sample_factor,
                                            init_weights_type=init_weights_type,
                                            align_corners=align_corners)

            self.attention_layer_list.append(attention)

        # self.attentionblock2 = MultiAttentionBlock(in_channels=in_channels_list[1],
        #                                            gate_size=filters[2], inter_size=filters[1],
        #                                            non_local_mode=non_local_mode, sub_sample_factor=sub_sample_factor)
        #
        # self.attentionblock3 = MultiAttentionBlock(in_channels=in_channels_list[2],
        #                                            gate_size=filters[3], inter_size=filters[2],
        #                                            nonlocal_mode=non_local_mode, sub_sample_factor=sub_sample_factor)
        #
        # self.attentionblock4 = MultiAttentionBlock(in_channels=in_channels_list[3],
        #                                            gate_size=filters[4], inter_size=filters[3],
        #                                            nonlocal_mode=non_local_mode, sub_sample_factor=sub_sample_factor)

        # going up
        for layer_depth in range(unet_depth - 2, -1, -1):

            if layer_depth == unet_depth - 2:
                input_shape = input_shape
            else:
                input_shape = input_shape * 2

            x = UNetUpBlock(
                in_channels=base_filter * (2 ** (layer_depth + 1)),
                out_channels=base_filter * (2 ** layer_depth),
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout,
                batch_normalization=batch_normalization,
                normalization_momentum=normalization_momentum,
                residuals=residuals,
                activation='relu',
                init_weights_type=init_weights_type)


            self.up_layer_list.append(x)


        # deep supervision

        scale_factor_list = [1, 2, 4, 8]

        for layer_depth in range(unet_depth - 1):

            if layer_depth == 0:
                deep_surpervision_block = Conv3D(in_channels=filters_list[layer_depth],
                                                 out_channels=num_out_channels,
                                                 kernel_size=1,
                                                 input_shape=input_shape,
                                                 batch_normalization=False,
                                                 activation=None)
            else:
                deep_surpervision_block = UNetDeepSuperVision(in_channels=filters_list[layer_depth],
                                                              out_channels=num_out_channels,
                                                              kernel_size=1,
                                                              input_shape=input_shape,
                                                              scale_factor=scale_factor_list[layer_depth],
                                                              align_corners=align_corners)

            self.ds_list.append(deep_surpervision_block)

        final_conv = nn.Conv3d(in_channels=num_out_channels*4,
                               out_channels=num_out_channels,
                               kernel_size=1)

        if num_out_channels == 1:
            output_activation = ActivationLayer('sigmoid')
        elif num_out_channels > 1:
            output_activation = ActivationLayer('softmax', dim=1)
        else:
            output_activation = ActivationLayer('sigmoid')


        self.output_layer = nn.Sequential(final_conv, output_activation)


        # Bottle Neck Conv

        self.bottle_neck_conv_1 = UNetConvModule(
            input_shape=bottle_neck_shape/2,
            in_channels=filters_list[-1],
            out_channels=128,
            kernel_size=3,
            activation='relu',
            batch_normalization=True,
            padding='same',
            max_pooling=True,
        )

        self.bottle_neck_conv_2 = UNetConvModule(
            input_shape=bottle_neck_shape/4,
            in_channels=128,
            out_channels=128,
            kernel_size=1,
            activation='relu',
            batch_normalization=True,
            padding='same',
            max_pooling=True,
        )

        final_size = bottle_neck_shape / 4

        self.flatten_input_size = int(128 * np.prod(final_size))


        rpn_bbox = nn.Linear(in_features=self.flatten_input_size, out_features=6)

        rpn_act = ActivationLayer('sigmoid')

        self.rpn_detect_bbox = nn.Sequential(rpn_bbox, rpn_act)



    def forward(self, x):
        down_blocks = []
        for layer_depth, down_conv in enumerate(self.down_layer_list):
            x = down_conv(x)
            down_blocks.append(x)

        gating = self.gating_center(down_blocks[-1])

        up_blocks = []

        for layer_depth, (attention, up_conv) in enumerate(zip(self.attention_layer_list, self.up_layer_list)):
            if layer_depth == 0:
                g_conv, att = attention(down_blocks[-layer_depth - 2], gating)
            else:
                g_conv, att = attention(down_blocks[-layer_depth - 2], x)
            x = up_conv(x, g_conv)

            up_blocks.append(x)




        if layer_depth < len(self.up_layer_list) - 1:
            remaining_up = len(self.up_layer_list) - 1 - layer_depth
            for layer_depth_2, up_conv2 in enumerate(self.up_layer_list[-remaining_up:]):
                x = up_conv2(x, down_blocks[layer_depth_2])

                up_blocks.append(x)

        deep_supervision_block = []
        for layer_depth, ds_block in enumerate(self.ds_list):
            x = ds_block(up_blocks[-layer_depth - 1])

            deep_supervision_block.append(x)

        deep_supervision_concat = torch.cat(deep_supervision_block, dim=1)

        output_seg = self.output_layer(deep_supervision_concat)


        x = self.bottle_neck_conv_1(down_blocks[-1])

        x = self.bottle_neck_conv_2(x)

        x = x.view((x.size(0), -1))

        rpn_coords = self.rpn_detect_bbox(x)


        return output_seg, rpn_coords


class UNetConvModule(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 max_pooling=True,
                 activation=None,
                 residuals=False,
                 init_weights_type='kaiming'):

        super(UNetConvModule, self).__init__()

        self.residuals = residuals

        # unet_conv_module = []
        self.max_pooling = max_pooling
        if max_pooling:
            self.pooling_layer = nn.MaxPool3d(pool_size)
            # unet_conv_module += [pooling_layer]

        conv1 = Conv3D(in_channels=in_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       normalization_momentum=normalization_momentum,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape,
                       init_weights_type=init_weights_type)

        conv2 = Conv3D(in_channels=out_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       normalization_momentum=normalization_momentum,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape,
                       init_weights_type=init_weights_type)

        unet_conv_module = [conv1, conv2]

        if dropout > 0:
            dp_layer = nn.Dropout3d(dropout)
            unet_conv_module += [dp_layer]

        self.unet_conv_module = nn.Sequential(*unet_conv_module)

        if residuals:
            self.short_cut = Conv3D(in_channels=in_channels,
                                    out_channels=out_channels,
                                    kernel_size=1,
                                    stride=stride,
                                    dilation=dilation,
                                    batch_normalization=batch_normalization,
                                    normalization_momentum=normalization_momentum,
                                    activation=None,
                                    padding=padding,
                                    input_shape=input_shape,
                                    init_weights_type=init_weights_type)

    def forward(self, x):
        input_tensor = x

        if self.max_pooling:
            input_tensor = self.pooling_layer(x)
        #
        # x = self.conv1(x)
        # x = self.conv2(x)

        x = self.unet_conv_module(input_tensor)
        if self.residuals:
            short_cut = self.short_cut(input_tensor)
            x = short_cut + x
            x = F.relu(x)
        return x

class UNetUpBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 activation=None,
                 residuals=False,
                 init_weights_type='kaiming'):

        super(UNetUpBlock, self).__init__()

        self.up_conv = UpConv3D(in_channels=in_channels,
                                out_channels=out_channels,
                                kernel_size=2,
                                stride=2,
                                dilation=1,
                                input_shape=input_shape,
                                batch_normalization=False,
                                normalization_momentum=normalization_momentum,
                                init_weights_type=init_weights_type)

        # print(sum(p.numel() for p in self.up_conv.parameters()))

        self.conv_block = UNetConvModule(
            in_channels=in_channels,
            out_channels=out_channels,
            input_shape=input_shape,
            kernel_size=kernel_size,
            padding=padding,
            pool_size=pool_size,
            stride=stride,
            dilation=dilation,
            dropout=dropout,
            batch_normalization=batch_normalization,
            normalization_momentum=normalization_momentum,
            max_pooling=False,
            residuals=residuals,
            activation=activation,
            init_weights_type=init_weights_type)

    def forward(self, x1, x2):
        """
        Parameters
        ----------
        x1 : torch.Tensor
            Tensor from up_conv layer.
        x2 : torch.Tensor
            Tensor from down_conv layer that gets concatenated with after up convolution.

        Returns
        -------

        """
        x_up = self.up_conv(x1)
        x = torch.cat([x_up, x2], dim=1)
        x = self.conv_block(x)
        return x


class UNetGridGatingSignal3(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=1,
                 padding='same',
                 stride=1,
                 dilation=1,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 activation='relu',
                 init_weights_type='kaiming'):

        super(UNetGridGatingSignal3, self).__init__()


        self.conv = Conv3D(in_channels=in_channels,
                           out_channels=out_channels,
                           kernel_size=kernel_size,
                           stride=stride,
                           dilation=dilation,
                           batch_normalization=batch_normalization,
                           normalization_momentum=normalization_momentum,
                           activation=activation,
                           padding=padding,
                           input_shape=input_shape,
                           init_weights_type=init_weights_type)


        # # initialise the blocks
        # for m in self.children():
        #     init_weights(m, init_type='kaiming')

    def forward(self, inputs):
        outputs = self.conv(inputs)
        return outputs

class UNetDeepSuperVision(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 scale_factor,
                 kernel_size=1,
                 padding='same',
                 stride=1,
                 dilation=1,
                 batch_normalization=False,
                 normalization_momentum=0.1,
                 activation=None,
                 init_weights_type='kaiming',
                 align_corners=True,
                 ):

        super(UNetDeepSuperVision, self).__init__()

        conv = Conv3D(in_channels=in_channels,
                      out_channels=out_channels,
                      kernel_size=kernel_size,
                      stride=stride,
                      dilation=dilation,
                      batch_normalization=batch_normalization,
                      normalization_momentum=normalization_momentum,
                      activation=activation,
                      padding=padding,
                      input_shape=input_shape,
                      init_weights_type=init_weights_type)


        up_conv = nn.Upsample(scale_factor=scale_factor,
                              mode='trilinear',
                              align_corners=align_corners)

        self.deep_supervision = nn.Sequential(conv, up_conv)

    def forward(self, x):
        return self.deep_supervision(x)
