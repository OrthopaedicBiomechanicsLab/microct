import torch
import torchvision
from torchvision.transforms import functional
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Models.model_core import *
import Models.utils as utils

class PyramidROIAlign(nn.Module):
    """Implements ROI Pooling on multiple levels of the feature pyramid.
    Params:
    - pool_shape: [height, width] of the output pooled regions. Usually [7, 7]
    Inputs:
    - boxes: [batch, num_boxes, (y1, x1, y2, x2)] in normalized
             coordinates. Possibly padded with zeros if not enough
             boxes to fill the array.
    - image_meta: [batch, (meta data)] Image details. See compose_image_meta()
    - Feature maps: List of feature maps from different levels of the pyramid.
                    Each is [batch, height, width, channels]
    Output:
    Pooled regions in the shape: [batch, num_boxes, height, width, channels].
    The width and height are those specific in the pool_shape in the layer
    constructor.
    """

    def __init__(self, params):
        """

         Parameters
         ----------
         params : Parameters.ParameterBuilder.Parameters
             Parameters for model creating and training purposes.
         """

        super(PyramidROIAlign, self).__init__()
        self.params = params

    def forward(self, boxes, feature_maps):

        # Boxes are cropped boxes [batch, num_boxes, (x1, y1, x2, y2)] in normalized coords
        # Feature Maps. List of feature maps from different level of the
        # feature pyramid. Each is [batch, height, width, channels]

        # Assign each ROI to a level in the pyramid based on the ROI area.
        x1, y1, x2, y2 = torch.chunk(boxes, 4, dim=-1)
        w = (x2 - x1).squeeze()
        h = (y2 - y1).squeeze()


        # Image shape. Based on all images being the same size.
        image_shape = self.params.sample_size

        # Equation 1 in the Feature Pyramid Networks paper. Account for
        # the fact that our coordinates are normalized here.
        # e.g. a 224x224 ROI (in pixels) maps to P4

        image_area = torch.mul(*image_shape).to(boxes)

        # The additional tf.sqrt(image_area) accounts for h and w to be in normalized coordinates.
        # The 224.0 is based on the original ImageNet pre-training used in the original paper.

        # roi_level = torch.log2(torch.sqrt(h * w) / (
        #         224.0 / torch.sqrt(image_area)))

        # roi_level = torch.log2(
        #     torch.sqrt(h * w) / (torch.sqrt(image_area) / torch.sqrt(image_area))
        # )

        roi_level = torch.log2(
            torch.sqrt(h * w) / (torch.sqrt(image_area))
        )


        # roi_level = ( tf.log(tf.sqrt(h * w) / (tf.sqrt(image_area) / tf.sqrt(image_area))) ) / tf.log(2.0)

        two_tensor = torch.tensor(2, device=boxes.device, dtype=torch.float32)
        five_tensor = torch.tensor(5, device=boxes.device, dtype=torch.float32)

        # Ensures the roi_level is between 2 and 5
        roi_level = torch.round(4 + roi_level)
        roi_level = torch.where(roi_level > two_tensor, roi_level, two_tensor)
        roi_level = torch.where(roi_level < five_tensor, roi_level, five_tensor)
        # roi_level = torch.min(5, torch.max(4 + torch.round(roi_level)[0], 2))
        # roi_level = torch.squeeze(roi_level, 2)


        # Loop through levels and apply ROI pooling to each. P2 to P5.
        pooled = []
        box_to_level = []

        # if self.params.batch_size == 1:
        #     boxes = boxes[0]

        for i, level in enumerate(range(2, 6)):
            # Find the ROI indices where the roi_level matches the looped level

            # For ix, first axis is the amount of matched roi_level and level
            # and the second is the coordinates for each in roi_level
            # ie, ix[:, 0] is which level (2, 3, 4, or 5) and the second is the box_index
            ix = torch.where(roi_level == level)

            if self.params.batch_size == 1:
                ix = (torch.zeros(ix[0].shape[0], device=ix[0].device, dtype=torch.int64), ix[0])

            level_boxes = boxes[ix]

            ix = torch.stack(ix, dim=-1)

            level_boxes = torch.cat([ix[:, 0:1].to(level_boxes), level_boxes], dim=-1)

            # if self.params.batch_size > 1:
            #     level_boxes = torch.cat([ix[:, 0:1].to(level_boxes), level_boxes], dim=-1)
            # else:
            #     level_boxes = torch.cat([torch.zeros((level_boxes.shape[0], 1)).to(level_boxes), level_boxes], dim=-1)


            # Stop gradient propagation to ROI proposals
            # Unclear why this needs to be here
            level_boxes = level_boxes.detach().contiguous()
            ix = ix.detach().contiguous()


            # Crop and Resize
            # From Mask R-CNN paper: "We sample four regular locations, so
            # that we can evaluate either max or average pooling. In fact,
            # interpolating only a single value at each bin center (without
            # pooling) is nearly as effective."
            #
            # Here we use the simplified approach of a single value per bin,
            # which is how it's done in tf.crop_and_resize()
            # Result: [batch * num_boxes, pool_height, pool_width, channels]


            # level_boxes = torch.split(level_boxes, level_boxes.shape[0], dim=0)
            # level_boxes = [boxes.squeeze(0) for boxes in level_boxes]

            if ix.shape[1] > 0:

                aligned_feature_map = torchvision.ops.roi_align(feature_maps[i], level_boxes, self.params.pool_size)
                pooled.append(aligned_feature_map)

                # Keep track of which box is mapped to which level
                box_to_level.append(ix)

            # pooled.append(tf.image.crop_and_resize(
            #     feature_maps[i], level_boxes, box_indices, self.pool_shape,
            #     method="bilinear"))

        # Pack pooled features into one tensor
        pooled = torch.cat(pooled, dim=0)

        """
        This next part is designed to organize the bounding boxes and feature maps accordingly. 
        """

        # Pack box_to_level mapping into one array and add another column representing the order of pooled boxes

        # Stack box_to_level into a single Tensor object
        box_to_level = torch.cat(box_to_level, dim=0)
        # Create a Tensor for the indices of the stacked box_to_level and expand the second dimension.
        box_range = torch.unsqueeze(torch.arange(box_to_level.shape[0]), 1).to(box_to_level)

        # Concatenate the stacked box_to_level the corresponding index number for each.
        box_to_level = torch.cat([box_to_level, box_range], dim=1)

        # Rearrange pooled features to match the order of the original boxes
        # Sort box_to_level by batch then box index
        # TF doesn't have a way to sort by two columns, so merge them and sort.

        # Create sorting tensor based on the coordinates of each matched roi_level to feature level and box index.
        # Since want each feature level separated, multiply the feature level index by a large number to
        # ensure feature levels are separate all levels
        sorting_tensor = box_to_level[:, 0] * 100000 + box_to_level[:, 1]

        # Apply sorting. This sorting is why box_range was created, to kep track of the original indices.
        ix = torch.topk(sorting_tensor, k=box_to_level.shape[0])[1]

        ix_index = np.arange(ix.shape[0] - 1, -1, -1)
        ix = ix[ix_index]

        # Get the original indices from box_range after sorting them
        ix = torch.gather(box_to_level[:, 2], dim=0, index=ix)
        # Use sorted indices to get sorted pooled
        pooled = pooled[ix]


        shape = boxes.shape[:2] + pooled.shape[1:]

        # Re-add the batch dimension
        # Required as output of tf.image.crop_and_resize loses batch dimension and is replaced by num_boxes.
        # Also the reason why the FPN requires TimeDistributed layers rather than normal convoluiton,
        # due to loss of batch information.
        pooled = pooled.contiguous().reshape(shape)
        return pooled