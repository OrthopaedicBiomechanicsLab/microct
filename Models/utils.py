import torch
import numpy as np
import math

def batch_slice(inputs, graph_fn, batch_size, names=None):
    """
    Splits inputs into slices and feeds each slice to a copy of the given
    computation graph and then combines the results. It allows you to run a
    graph on a batch of inputs even if the graph is written to support one
    instance only.

    :param inputs: list of tensors. All must have the same first dimension length
    :param graph_fn: A function that returns a TF tensor that's part of a graph.
    :param batch_size: number of slices to divide the data into.
    :param names: If provided, assigns names to the resulting tensors.
    """

    if not isinstance(inputs, list):
        inputs = [inputs]

    outputs = []

    for i in range(batch_size):

        inputs_slice = [x[i] for x in inputs]
        output_slice = graph_fn(*inputs_slice)

        if not isinstance(output_slice, (tuple, list)):
            output_slice = [output_slice]

        outputs.append(output_slice)

    # Change outputs from a list of slices where each is a list of outputs
    # to a list of outputs and each has a list of slices

    outputs = list(zip(*outputs))

    if names is None:
        names = [None] * len(outputs)

    result = [tf.stack(o, axis=0, name=n)
              for o, n in zip(outputs, names)]

    if len(result) == 1:
        result = result[0]

    return result



def batch_slice_modified(inputs, graph_fn_key, batch_size, names=None):
    """
    Splits inputs into slices and feeds each slice to a copy of the given
    computation graph and then combines the results. It allows you to run a
    graph on a batch of inputs even if the graph is written to support one
    instance only.

    :param inputs: list of tensors. All must have the same first dimension length
    :param graph_fn_key: A function that returns a TF tensor that's part of a graph.
    :param batch_size: number of slices to divide the data into.
    :param names: If provided, assigns names to the resulting tensors.
    """

    if not isinstance(inputs, list):
        inputs = [inputs]

    outputs = []

    for i in range(batch_size):

        inputs_slice = [x[i] for x in inputs]


        output_slice = graph_fn(*inputs_slice)

        if not isinstance(output_slice, (tuple, list)):
            output_slice = [output_slice]

        outputs.append(output_slice)

    result = tf.concat(outputs, axis=0)

    return result




def box_refinement_torch(box, gt_box):
    """
    Compute refinement needed to transform box to gt_box.
    box and gt_box are [N, (x1, y1, x2, y2)]
    """

    width = box[:, 2] - box[:, 0]
    height = box[:, 3] - box[:, 1]
    center_x = box[:, 0] + 0.5 * width
    center_y = box[:, 1] + 0.5 * height

    gt_width = gt_box[:, 2] - gt_box[:, 0]
    gt_height = gt_box[:, 3] - gt_box[:, 1]
    gt_center_x = gt_box[:, 0] + 0.5 * gt_width
    gt_center_y = gt_box[:, 1] + 0.5 * gt_height

    dx = (gt_center_x - center_x) / width
    dy = (gt_center_y - center_y) / height
    dw = torch.log(gt_width / width)
    dh = torch.log(gt_height / height)


    result = torch.stack([dx, dy, dw, dh], dim=-1)
    return result


# def apply_box_deltas(boxes, deltas):
#     """Applies the given deltas to the given boxes.
#     boxes: [N, (y1, x1, y2, x2)]. Note that (y2, x2) is outside the box.
#     deltas: [N, (dy, dx, log(dh), log(dw))]
#     """
#
#     boxes = boxes.astype(np.float32)
#
#     # Convert boxes to (y, x, h, w)
#     box_height = boxes[:, 2] - boxes[:, 0]
#     box_width = boxes[:, 3] - boxes[:, 1]
#
#     box_center_y = boxes[:, 0] + 0.5 * box_height
#     box_center_x = boxes[:, 1] + 0.5 * box_width
#
#     # Apply deltas to (y, x, h, w) from boxes
#
#     center_y = deltas[:, 0] * box_height + box_center_y
#     center_x = deltas[:, 1] * box_width + box_center_x
#     height = np.exp(deltas[:, 2]) * box_height
#     width = np.exp(deltas[:, 3]) * box_width
#
#     # Convert back to (y1, x1, y2, x2) after applying deltas
#
#     y1 = center_y - 0.5 * height
#     x1 = center_x - 0.5 * width
#     y2 = y1 + height
#     x2 = x1 + width
#
#     return np.stack([y1, x1, y2, x2], axis=1)


def apply_box_deltas_torch(boxes, deltas):
    """
    Applies the given deltas to the given boxes.
    :param boxes: [N, (x1, y1, x2, y2)] boxes to update
    :param deltas: [N, (dx, dy, log(dw), log(dh))] refinements to apply
    """
    # Convert to x, y, w, h
    b_w = boxes[:, :, 2] - boxes[:, :, 0]
    b_h = boxes[:, :, 3] - boxes[:, :, 1]
    b_center_x = boxes[:, :, 0] + 0.5 * b_w
    b_center_y = boxes[:, :, 1] + 0.5 * b_h

    # Apply deltas
    center_x = b_center_x + deltas[:, :, 0] * b_w
    center_y = b_center_y + deltas[:, :, 1] * b_h
    width = b_w * torch.exp(deltas[:, :, 2])
    height = b_h * torch.exp(deltas[:, :, 3])

    # Convert back to x1, y1, x2, y2
    x1 = center_x - 0.5 * width
    y1 = center_y - 0.5 * height
    x2 = x1 + width
    y2 = y1 + height


    result = torch.stack([x1, y1, x2, y2], dim=2)

    return result


def apply_box_deltas_center_coord_graph(boxes, deltas):
    """
     Applies the given deltas to the given boxes.
     :param boxes: [N, (y1, x1, y2, x2)] boxes to update
     :param deltas: [N, (dy, dx, log(dh), log(dw))] refinements to apply
     """
    # Convert to y, x, h, w
    height = boxes[:, 2] - boxes[:, 0]
    width = boxes[:, 3] - boxes[:, 1]
    center_y = boxes[:, 0] + 0.5 * height
    center_x = boxes[:, 1] + 0.5 * width

    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width


    result = tf.stack([center_y, center_x], axis=1, name='apply_box_deltas_center_coord_out')

    return result


def apply_box_deltas_graph_by_class(boxes, deltas):
    """
    Applies the given deltas to the given boxes.
    :param boxes: [N, num_class, (y1, x1, y2, x2)] boxes to update
    :param deltas: [N, num_class, (dy, dx, log(dh), log(dw))] refinements to apply
    """
    # Convert to y, x, h, w
    height = boxes[:, :, 2] - boxes[:, :, 0]
    width = boxes[:, :, 3] - boxes[:, :, 1]
    center_y = boxes[:, :, 0] + 0.5 * height
    center_x = boxes[:, :, 1] + 0.5 * width

    # Apply deltas
    center_y += deltas[:, :, 0] * height
    center_x += deltas[:, :, 1] * width
    height *= tf.exp(deltas[:, :, 2])
    width *= tf.exp(deltas[:, :, 3])

    # Convert back to y1, x1, y2, x2
    y1 = center_y - 0.5 * height
    x1 = center_x - 0.5 * width
    y2 = y1 + height
    x2 = x1 + width

    result = tf.stack([y1, x1, y2, x2], axis=2, name='apply_box_deltas_out')

    return result


def apply_box_deltas_graph_individual(boxes, deltas):
    """
    Applies the given deltas to the given boxes.
    :param boxes: [N, (y1, x1, y2, x2)] boxes to update
    :param deltas: [N, (dy, dx, log(dh), log(dw))] refinements to apply
    """
    # Convert to y, x, h, w
    height = boxes[:, 2] - boxes[:, 0]
    width = boxes[:, 3] - boxes[:, 1]
    center_y = boxes[:, 0] + 0.5 * height
    center_x = boxes[:, 1] + 0.5 * width

    # Apply deltas
    center_y += deltas[:, 0] * height
    center_x += deltas[:, 1] * width
    height *= tf.exp(deltas[:, 2])
    width *= tf.exp(deltas[:, 3])

    # Convert back to y1, x1, y2, x2
    y1 = center_y - 0.5 * height
    x1 = center_x - 0.5 * width
    y2 = y1 + height
    x2 = x1 + width

    result = tf.stack([y1, x1, y2, x2], axis=1, name='apply_box_deltas_out')

    return result



def box_refinement(box, gt_box):
    """Compute refinement needed to transform box to gt_box.
    box and gt_box are [N, (x1, y1, x2, y2)]. (x2, y2) is
    assumed to be outside the box.
    """
    box = box.astype(np.float32)
    gt_box = gt_box.astype(np.float32)

    width = box[:, 2] - box[:, 0]
    height = box[:, 3] - box[:, 1]
    center_x = box[:, 0] + 0.5 * width
    center_y = box[:, 1] + 0.5 * height

    gt_width = gt_box[:, 2] - gt_box[:, 0]
    gt_height = gt_box[:, 3] - gt_box[:, 1]
    gt_center_x = gt_box[:, 0] + 0.5 * gt_width
    gt_center_y = gt_box[:, 1] + 0.5 * gt_height

    dx = (gt_center_x - center_x) / width
    dy = (gt_center_y - center_y) / height
    dw = np.log(gt_width / width)
    dh = np.log(gt_height / height)


    return np.stack([dx, dy, dw, dh], axis=1)



def compute_backbone_shapes(params, image_shape):
    """Computes the width and height of each stage of the backbone network.

    Returns:
        [N, (height, width)]. Where N is the number of stages
    """

    # Currently supports ResNet only
    assert params.conv_backbone in ['resnet34', 'resnet50', 'resnet101']
    return np.array(
        [[int(math.ceil(image_shape[0] / stride)),
          int(math.ceil(image_shape[1] / stride))]
         for stride in params.backbone_strides])



def normalize_coordinates2D(bbox, params):
    # Ensure that bbox is a float
    bbox = bbox.astype(dtype=np.float32)
    y_size, x_size = params.input_shape
    scale = np.array([y_size - 1, x_size - 1, y_size - 1, x_size - 1], dtype=np.float32)
    shift = np.array([0, 0, 1, 1], dtype=np.float32)
    result = (bbox - shift) / scale
    return result

def normalize_coordinates2D_torch(bbox, params):

    # Ensure that bbox is a float
    # bbox = bbox.astype(dtype=torch.float32)
    x_size, y_size = params.input_shape
    scale = torch.tensor([x_size - 1, y_size - 1, x_size - 1, y_size - 1], dtype=torch.float32, device=bbox.device)
    shift = torch.tensor([0, 0, 1, 1], dtype=torch.float32, device=bbox.device)
    result = (bbox - shift) / scale
    return result

def norm_boxes_graph(boxes, shape):
    """Converts boxes from pixel coordinates to normalized coordinates.
    boxes: [..., (y1, x1, y2, x2)] in pixel coordinates
    shape: [..., (height, width)] in pixels
    Note: In pixel coordinates (y2, x2) is outside the box. But in normalized
    coordinates it's inside the box.
    Returns:
        [..., (y1, x1, y2, x2)] in normalized coordinates
    """
    boxes = tf.cast(boxes, dtype=tf.float32)
    h, w = tf.split(tf.cast(shape, tf.float32), 2)
    scale = tf.concat([h, w, h, w], axis=-1) - tf.constant(1.0)
    shift = tf.constant([0., 0., 1., 1.])
    return tf.divide(boxes - shift, scale)


def denormalize_coordinates(bbox, config):

    z_size, y_size, _ = config.IMAGE_SHAPE

    scale = tf.constant([z_size - 1, y_size - 1, z_size - 1, y_size - 1], dtype=tf.float32)

    shift = tf.constant([0, 0, 1, 1], dtype=tf.float32)

    result = tf.cast(tf.round(tf.multiply(bbox, scale) + shift), tf.int32)

    return result


def normalize_coordinates_np(bbox, config=None, shape=None):


    assert bool(config is None) != bool(shape is None)

    if config is not None:
        z_size, y_size, _ = config.IMAGE_SHAPE

    elif shape is not None:
        z_size, y_size = shape

    scale = np.array([z_size - 1, y_size - 1, z_size - 1, y_size - 1])

    shift = np.array([0, 0, 1, 1])

    result = (bbox - shift) / scale

    return result


def denormalize_coordinates_np(bbox, config=None, shape=None):

    assert bool(config is None) != bool(shape is None)

    if config is not None:
        z_size, y_size, _ = config.IMAGE_SHAPE

    elif shape is not None:
        z_size, y_size = shape

    scale = np.array([z_size - 1, y_size - 1, z_size - 1, y_size - 1])
    shift = np.array([0, 0, 1, 1])
    result = (bbox * scale) + shift

    result = np.around(result).astype(np.int64)

    return result



def compute_iou(box, boxes, box_area, boxes_area):
    """Calculates IoU of the given box with the array of the given boxes.
    box: 1D vector [y1, x1, y2, x2]
    boxes: [boxes_count, (y1, x1, y2, x2)]
    box_area: float. the area of 'box'
    boxes_area: array of length boxes_count.
    Note: the areas are passed in rather than calculated here for
          efficency. Calculate once in the caller to avoid duplicate work.
    """
    # Calculate intersection areas
    y1 = np.maximum(box[0], boxes[:, 0])
    y2 = np.minimum(box[2], boxes[:, 2])
    x1 = np.maximum(box[1], boxes[:, 1])
    x2 = np.minimum(box[3], boxes[:, 3])

    intersection = np.maximum(x2 - x1, 0) * np.maximum(y2 - y1, 0)

    # intersection = np.maximum((x2 - x1)*(y2 - y1), 0)


    union = box_area + boxes_area[:] - intersection[:]
    iou = intersection / union
    return iou



def compute_overlaps(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (y1, x1, y2, x2)].
    For better performance, pass the largest set first and the smaller second.
    """
    # Areas of anchors and GT boxes
    area1 = (boxes1[:, 2] - boxes1[:, 0]) * (boxes1[:, 3] - boxes1[:, 1])
    area2 = (boxes2[:, 2] - boxes2[:, 0]) * (boxes2[:, 3] - boxes2[:, 1])

    # Compute overlaps to generate matrix [boxes1 count, boxes2 count]
    # Each cell contains the IoU value.
    overlaps = np.zeros((boxes1.shape[0], boxes2.shape[0]))

    for i in range(overlaps.shape[1]):
        box2 = boxes2[i]
        overlaps[:, i] = compute_iou(box2, boxes1, area2[i], area1)

    return overlaps


def clip_boxes_graph(boxes, window):
    """
    :param boxes: [N, (y1, x1, y2, x2)]
    :param window: [4] in the form y1, x1, y2, x2
    """
    # Split
    wy1, wx1, wy2, wx2 = tf.split(window, 4)
    y1, x1, y2, x2 = tf.split(boxes, 4, axis=1)

    # Clip
    y1 = tf.maximum(tf.minimum(y1, wy2), wy1)
    x1 = tf.maximum(tf.minimum(x1, wx2), wx1)
    y2 = tf.maximum(tf.minimum(y2, wy2), wy1)
    x2 = tf.maximum(tf.minimum(x2, wx2), wx1)

    clipped = tf.concat([y1, x1, y2, x2], axis=1, name='clipped_boxes')
    clipped.set_shape((clipped.shape[0], 4))

    return clipped
