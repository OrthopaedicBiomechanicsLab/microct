from ..BaseModel import BaseModel
from .ag_unet_self_supervised_base import AGUNetSelfSupervised
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *

class AGUNetSelfSupervisedModel(BaseModel):
    def __init__(self, params, logger=None):
        # super(UNetModel, self).__init__(params)
        BaseModel.__init__(self, params, logger)

        self.model_base_fn = AGUNetSelfSupervised
        self.load_model_from_params()
        #
        # self.net = UNet(params)


        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """
        self.image = input['image'].to(self.device)
        self.label = input['label'].to(self.device)

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        self.pred = self.net(self.image)

    def backward(self):
        """Calculate UNet model loss"""
        self.loss = self.calc_loss()
        self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.optimizer.step()          # update weights


    def DSCLoss(self, **fn_params):
        return BaseLossMetric(dsc_loss, device=self.device, **fn_params)

    def BCELoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     self.logger.info('Number of classes set >1, therefore using cross entropy loss instead.')
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(bce_loss, device=self.device, **fn_params)

    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, device=self.device, **fn_params)

    def CONCURRENCY(self, **fn_params):
        return BaseLossMetric(concurrency, device=self.device, **fn_params)

    def CrossEntropyLoss(self, **fn_params):
        # if self.params.num_classes > 1:
        #     return BaseLossMetric(ce_loss, **fn_params)
        # else:
        #     self.logger.info('Number of classes set to 1, therefore using binary cross entropy loss instead')
        #     return BaseLossMetric(bce_loss, **fn_params)

        return BaseLossMetric(ce_loss, device=self.device, **fn_params)

    #
    # def CONCURRENCY(self, **fn_params):
    #     def fn(pred, target):
    #         smooth = 1e-7
    #         y_true_f = target.reshape(target.shape[0], -1)
    #         y_pred_f = pred.reshape(pred.shape[0], -1)
    #         intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    #
    #         con = 1 / 2 * (intersection * (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1))) / (
    #                 torch.sum(y_true_f, dim=-1) * torch.sum(y_pred_f, dim=-1) + smooth)
    #         return con.mean()
    #     return fn
    #
    #
    # def DSC(self, **fn_params):
    #     def fn(pred, target, neg=False):
    #         y_true_f = target.reshape(target.shape[0], -1)
    #         y_pred_f = pred.reshape(pred.shape[0], -1)
    #         intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    #         smooth = 1e-7
    #         dsc = (2. * intersection) / (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1) + smooth)
    #         if neg == True:
    #             dsc = 1 - dsc
    #         return dsc.mean()
    #     return fn











