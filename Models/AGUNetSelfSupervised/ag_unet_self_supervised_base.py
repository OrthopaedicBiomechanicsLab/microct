import torch
import torch.nn as nn
from Models.model_core import *
from Models.weight_initializer import init_weights
from Models.AGUNet import GridAttentionBlockND
from Models.AGUNet import MultiAttentionBlock

class AGUNetSelfSupervised(nn.Module):
    def __init__(self, params):
        super(AGUNetSelfSupervised, self).__init__()

        input_shape = params.input_shape
        num_in_channels = params.num_in_channels
        num_classes = params.num_classes
        base_filter = params.base_filter
        unet_depth = params.depth

        kernel_size = params.kernel_size
        batch_normalization = params.batch_normalization
        pool_size = params.pool_size
        stride = params.stride
        dilation = params.dilation
        dropout = params.dropout

        residuals = params.residuals
        normalization_momentum = params.normalization_momentum

        init_weights_type = params.init_weights_type


        in_channels_list = [base_filter * (2 ** (layer_depth - 1)) if layer_depth > 0 else num_in_channels for layer_depth in range(unet_depth)]
        out_channels_list = [base_filter * (2 ** (layer_depth)) for layer_depth in range(unet_depth)]

        filters_list = [base_filter * (2 ** (layer_depth)) for layer_depth in range(unet_depth)]

        self.down_layer_list = nn.ModuleList()
        self.up_layer_list = nn.ModuleList()
        self.attention_layer_list = nn.ModuleList()
        self.ds_list = nn.ModuleList()

        input_shape = np.array(input_shape)

        for layer_depth in range(unet_depth):

            if layer_depth == 0:
                max_pooling = False
                in_channels = num_in_channels
                input_shape = input_shape
            else:
                max_pooling = True
                in_channels = base_filter * (2 ** (layer_depth - 1))
                input_shape = input_shape / 2

            x = UNetConvModule(
                in_channels=in_channels,
                out_channels=base_filter * (2 ** layer_depth),
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout,
                batch_normalization=batch_normalization,
                normalization_momentum=normalization_momentum,
                max_pooling=max_pooling,
                residuals=residuals,
                activation='relu',
                init_weights_type=init_weights_type)


            self.down_layer_list.append(x)


        self.gating_center = UnetGridGatingSignal3(in_channels=out_channels_list[-1], out_channels=out_channels_list[-1],
                                                   kernel_size=1, input_shape=input_shape,
                                                   batch_normalization=batch_normalization,
                                                   init_weights_type=init_weights_type,
                                                   )


        a = 1

        self.max_pool = nn.MaxPool3d((1, 1, 2))

        input_shape = input_shape / (1, 1, 2)

        base_filter_class = 256
        class_depth = 3

        self.class_depth_list = nn.ModuleList()
        for layer_depth in range(class_depth):

            if layer_depth == 0:
                in_channels = base_filter_class
            else:
                in_channels = base_filter_class * (2 ** (layer_depth - 1))


            if all(input_shape >= 3):
                down_kernel_size = 3
            elif all(input_shape > 2):
                down_kernel_size = 2
            else:
                down_kernel_size = 1

            # x = UNetConvModule(
            #     in_channels=in_channels,
            #     out_channels=base_filter_class * (2 ** layer_depth),
            #     input_shape=input_shape,
            #     kernel_size=down_kernel_size,
            #     padding='same',
            #     pool_size=pool_size,
            #     stride=stride,
            #     dilation=dilation,
            #     dropout=dropout,
            #     batch_normalization=batch_normalization,
            #     normalization_momentum=normalization_momentum,
            #     max_pooling=max_pooling,
            #     residuals=False,
            #     activation='relu',
            #     init_weights_type=init_weights_type)


            x = Conv3D(in_channels=in_channels,
                       out_channels=base_filter_class * (2 ** layer_depth),
                       kernel_size=down_kernel_size,
                       input_shape=input_shape,
                       activation='relu',
                       batch_normalization=True,
                       stride=stride,
                       dilation=dilation,
                       normalization_momentum=normalization_momentum,
                       padding='same',
                       init_weights_type=init_weights_type)


            max_pooling_block = nn.MaxPool3d((2, 2, 2))



            conv_pool = nn.Sequential(x, max_pooling_block)

            self.class_depth_list.append(conv_pool)

            input_shape = input_shape / 2

        self.final_conv = nn.Conv3d(in_channels=base_filter_class * (2 ** layer_depth),
                                    out_channels=num_classes,
                                    kernel_size=1)

        self.final_activation = ActivationLayer('softmax', dim=1)


    def forward(self, x):
        down_blocks = []
        for layer_depth, down_conv in enumerate(self.down_layer_list):
            x = down_conv(x)
            down_blocks.append(x)

        gating = self.gating_center(down_blocks[-1])

        x = self.max_pool(gating)

        class_blocks = []
        for layer_depth, class_conv in enumerate(self.class_depth_list):
            x = class_conv(x)
            class_blocks.append(x)

        x = x.view((x.size(0), -1))

        x = self.final_conv(x)

        return x




class UNetConvModule(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.99,
                 max_pooling=True,
                 activation=None,
                 residuals=False,
                 init_weights_type='kaiming'):

        super(UNetConvModule, self).__init__()

        self.residuals = residuals

        # unet_conv_module = []
        self.max_pooling = max_pooling
        if max_pooling:
            self.pooling_layer = nn.MaxPool3d(pool_size)
            # unet_conv_module += [pooling_layer]

        conv1 = Conv3D(in_channels=in_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       normalization_momentum=normalization_momentum,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape,
                       init_weights_type=init_weights_type)

        conv2 = Conv3D(in_channels=out_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       normalization_momentum=normalization_momentum,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape,
                       init_weights_type=init_weights_type)

        unet_conv_module = [conv1, conv2]

        if dropout > 0:
            dp_layer = nn.Dropout3d(dropout)
            unet_conv_module += [dp_layer]

        self.unet_conv_module = nn.Sequential(*unet_conv_module)

        if residuals:
            self.short_cut = Conv3D(in_channels=in_channels,
                                    out_channels=out_channels,
                                    kernel_size=1,
                                    stride=stride,
                                    dilation=dilation,
                                    batch_normalization=batch_normalization,
                                    normalization_momentum=normalization_momentum,
                                    activation=None,
                                    padding=padding,
                                    input_shape=input_shape,
                                    init_weights_type=init_weights_type)

    def forward(self, x):
        input_tensor = x

        if self.max_pooling:
            input_tensor = self.pooling_layer(x)
        #
        # x = self.conv1(x)
        # x = self.conv2(x)

        x = self.unet_conv_module(input_tensor)
        if self.residuals:
            short_cut = self.short_cut(input_tensor)
            x = short_cut + x
            x = F.relu(x)
        return x


class UnetGridGatingSignal3(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=1,
                 padding='same',
                 stride=1,
                 dilation=1,
                 batch_normalization=True,
                 normalization_momentum=0.99,
                 activation='relu',
                 init_weights_type='kaiming'):

        super(UnetGridGatingSignal3, self).__init__()


        self.conv = Conv3D(in_channels=in_channels,
                           out_channels=out_channels,
                           kernel_size=kernel_size,
                           stride=stride,
                           dilation=dilation,
                           batch_normalization=batch_normalization,
                           normalization_momentum=normalization_momentum,
                           activation=activation,
                           padding=padding,
                           input_shape=input_shape,
                           init_weights_type=init_weights_type)



    def forward(self, inputs):
        outputs = self.conv(inputs)
        return outputs

if __name__ == '__main__':

    class params_hold(object):
        model = 'AGUNet',
        input_shape = (128, 128, 256)
        epochs = 100
        depth = 5
        kernel_size = 3
        base_filter = 16
        num_in_channels = 1
        num_out_channels = 1
        num_classes = 24
        batch_size = 4
        load_all_data = True
        lr = 1e-3
        batch_normalization = True
        normalization_momentum = 0.1
        instance_normalization = False
        norm_track_running_states = False
        augmentation = True
        residuals = False
        dropout = 0
        num_class = 25

        ase_filter = 16

        pool_size = 2
        stride = 1
        dilation = 1
        init_weights_type = 'kaiming'

    params = params_hold()
    AGUNetSelfSupervised(params=params)


    params = params_hold()