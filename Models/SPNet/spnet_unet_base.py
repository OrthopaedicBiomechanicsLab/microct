import torch
import torch.nn as nn
import numpy as np
from Models.model_core import *

class SPUNetBase(nn.Module):
    def __init__(self, params):
        super(SPUNetBase, self).__init__()

        input_shape = params.input_shape
        num_in_channels = params.num_in_channels
        num_out_channels = params.num_out_channels
        base_filter = params.base_filter
        unet_depth = params.depth

        kernel_size = params.kernel_size
        batch_normalization = params.batch_normalization
        pool_size = params.pool_size
        stride = params.stride
        dilation = params.dilation
        dropout = params.dropout

        dropout_down_conv = params.dropout_down_conv
        dropout_up_conv = params.dropout_up_conv

        residuals = params.residuals
        normalization_momentum = params.normalization_momentum

        activation = params.activation

        self.down_layer_list = nn.ModuleList()
        self.up_layer_list = nn.ModuleList()

        input_shape = np.array(input_shape)

        # Down Convolutions
        for layer_depth in range(unet_depth):

            if layer_depth == 0:
                max_pooling = False
                in_channels = base_filter
                input_shape = input_shape
            else:
                max_pooling = True
                in_channels = base_filter
                input_shape = input_shape / 2

            x = UNetConvModule(
                in_channels=in_channels,
                out_channels=base_filter,
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout_down_conv,
                batch_normalization=batch_normalization,
                normalization_momentum=normalization_momentum,
                max_pooling=max_pooling,
                residuals=residuals,
                activation=activation)


            if layer_depth == (unet_depth - 1):

                input_shape = input_shape / 2

                x_lower = UNetConvModule(
                    in_channels=base_filter,
                    out_channels=base_filter,
                    input_shape=input_shape,
                    kernel_size=kernel_size,
                    padding='same',
                    pool_size=pool_size,
                    stride=stride,
                    dilation=dilation,
                    dropout=dropout_down_conv,
                    batch_normalization=batch_normalization,
                    normalization_momentum=normalization_momentum,
                    max_pooling=False,
                    residuals=residuals,
                    activation=activation)


                x = nn.Sequential(x, x_lower)

            self.down_layer_list.append(x)




        # going up
        for layer_depth in range(unet_depth - 2, -1, -1):

            if layer_depth == unet_depth - 2:
                input_shape = input_shape
            else:
                input_shape = input_shape * 2


            x = UNetUpBlock(
                in_channels=base_filter,
                out_channels=base_filter,
                input_shape=input_shape,
                kernel_size=kernel_size,
                padding='same',
                pool_size=pool_size,
                stride=stride,
                dilation=dilation,
                dropout=dropout_up_conv,
                batch_normalization=batch_normalization,
                normalization_momentum=normalization_momentum,
                residuals=residuals,
                activation=activation)


            self.up_layer_list.append(x)




    def forward(self, x):
        down_blocks = []
        for layer_depth, down_conv in enumerate(self.down_layer_list):
            x = down_conv(x)
            down_blocks.append(x)

        for layer_depth, up_conv in enumerate(self.up_layer_list):
            x = up_conv(x, down_blocks[-layer_depth - 2])

        return x




class UNetConvModule(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 max_pooling=True,
                 activation=None,
                 residuals=False):

        super(UNetConvModule, self).__init__()

        self.residuals = residuals

        # unet_conv_module = []
        self.max_pooling = max_pooling
        if max_pooling:
            self.pooling_layer = nn.AvgPool3d(pool_size)
            # unet_conv_module += [pooling_layer]

        conv1 = Conv3D(in_channels=in_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       normalization_momentum=normalization_momentum,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape)

        conv2 = Conv3D(in_channels=out_channels,
                       out_channels=out_channels,
                       kernel_size=kernel_size,
                       stride=stride,
                       dilation=dilation,
                       batch_normalization=batch_normalization,
                       normalization_momentum=normalization_momentum,
                       activation=activation,
                       padding=padding,
                       input_shape=input_shape)

        unet_conv_module = [conv1, conv2]

        if dropout > 0:
            dp_layer = nn.Dropout3d(dropout)
            unet_conv_module += [dp_layer]

        self.unet_conv_module = nn.Sequential(*unet_conv_module)

        if residuals:
            self.short_cut = Conv3D(in_channels=in_channels,
                                    out_channels=out_channels,
                                    kernel_size=1,
                                    stride=stride,
                                    dilation=dilation,
                                    batch_normalization=batch_normalization,
                                    normalization_momentum=normalization_momentum,
                                    activation=None,
                                    padding=padding,
                                    input_shape=input_shape)

    def forward(self, x):
        input_tensor = x

        if self.max_pooling:
            input_tensor = self.pooling_layer(x)
        #
        # x = self.conv1(x)
        # x = self.conv2(x)

        x = self.unet_conv_module(input_tensor)
        if self.residuals:
            short_cut = self.short_cut(input_tensor)
            x = short_cut + x
            x = F.relu(x)
        return x

class UNetUpBlock(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 input_shape,
                 kernel_size=3,
                 padding='same',
                 pool_size=2,
                 stride=1,
                 dilation=1,
                 dropout=0,
                 batch_normalization=True,
                 normalization_momentum=0.1,
                 activation=None,
                 residuals=False):

        super(UNetUpBlock, self).__init__()

        self.up_conv = UpConv3D(in_channels=in_channels,
                                out_channels=out_channels,
                                kernel_size=2,
                                stride=2,
                                dilation=1,
                                input_shape=input_shape,
                                batch_normalization=False,
                                normalization_momentum=normalization_momentum)

        # print(sum(p.numel() for p in self.up_conv.parameters()))

        self.conv_block = UNetConvModule(
            in_channels=in_channels * 2,
            out_channels=out_channels,
            input_shape=input_shape,
            kernel_size=kernel_size,
            padding=padding,
            pool_size=pool_size,
            stride=stride,
            dilation=dilation,
            dropout=dropout,
            batch_normalization=batch_normalization,
            normalization_momentum=normalization_momentum,
            max_pooling=False,
            residuals=residuals,
            activation=activation)

    def forward(self, x1, x2):
        """
        Parameters
        ----------
        x1 : torch.Tensor
            Tensor from up_conv layer.
        x2 : torch.Tensor
            Tensor from down_conv layer that gets concatenated with after up convolution.

        Returns
        -------

        """
        x_up = self.up_conv(x1)
        x = torch.cat([x_up, x2], dim=1)
        x = self.conv_block(x)
        return x

