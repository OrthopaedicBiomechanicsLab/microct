import os
import numpy as np
import torch

# np.random.seed(1)
# torch.manual_seed(0)

import pandas as pd

from torch.utils.data import DataLoader
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.patches as mpatches
import matplotlib.cm as cm
from matplotlib import colors

import SimpleITK as sitk


from Models import create_model
from Parameters import construct_params

from Dataset import select_dataset
from Dataset import CustomDataLoader
from torch.cuda.amp import GradScaler
from torch.cuda.amp import autocast

from Models.SPNet.spnet_model import get_target_heatmap

if __name__ == '__main__':


    df_main = pd.read_pickle('../../Dataframes/verse_self_resampled_coord.pickle')
    df_main['spine_filename'] = df_main['sample_name']
    df_main['seg_filename'] = df_main['seg_name']

    # Original model had a transpose to shift stuff. Duplicating to ensure it works in first iteration
    # sample_size = (384, 128, 128)
    sample_size = (128, 128, 384)




    spine_main_path = '/home/klein/GeoffData/VerseResampled_rev2/numpy'
    seg_main_path = '/home/klein/GeoffData/VerseResampled_rev2/numpy'


    df_train = df_main.loc[(df_main['cross_fold_number'] == 0) |
                           (df_main['cross_fold_number'] == 1) |
                           (df_main['cross_fold_number'] == 2) |
                           (df_main['cross_fold_number'] == 3) ]


    # df_train = df_main.loc[(df_main['cross_fold_number'] == 4)]


    params_json = '/home/klein/SpineMets/SpineSegmentation/spnet/SPNet/training_params_spnet.json'
    model_weights = '/home/klein/SpineMets/SpineSegmentation/spnet/SPNet/spnet_100_weights.pt'

    params = construct_params(json_file=params_json)
    params.gpu_ids = '0'
    params.pre_trained_model = False
    params.batch_size = 1
    model = create_model(params)
    model.load_weights(model_weights)

    dataset_fn = select_dataset(params)

    train_dataset = dataset_fn(params, df_train, spine_main_path, seg_main_path,
                               isTrain=False)

    train_generator = CustomDataLoader(params, train_dataset,
                                       num_threads=params.num_threads,
                                       shuffle=False)()

    batch = next(train_generator.__iter__())


    img = batch['image'].to('cuda:0')

    target_landmarks = batch['centroid_coord'].to('cuda:0')

    with autocast():
        heatmap_pred, heatmap_target, _ = model.net.forward(img, target_landmarks)

    heatmap_sigma = torch.full((params.num_classes - 1,), 4.0)

    sigma_scale = 1000.0
    normalize = False
    sigma_regularization = 0.1

    heatmap_sigma = heatmap_sigma.to(heatmap_pred.device)

    # target_heatmap = get_target_heatmap(params.sample_size, target_landmarks,
    #                                     sigmas=heatmap_sigma,
    #                                     scale=sigma_scale, normalize=normalize)

    img_data = img.detach().cpu().numpy()
    img_data = img_data[0, 0, :, :, :]

    heatmap_channel = 18

    gt_heatmap = heatmap_target.detach().cpu().numpy()[0, heatmap_channel, :, :, :]

    pred_heatmap_data = heatmap_pred.detach().cpu().numpy()[0, heatmap_channel, :, :, :]

    sitk.WriteImage(sitk.GetImageFromArray(img_data.astype(np.float)), 'hold.nii.gz')
    sitk.WriteImage(sitk.GetImageFromArray(gt_heatmap.astype(np.float)), 'hold_heat_gt.nii.gz')
    sitk.WriteImage(sitk.GetImageFromArray(pred_heatmap_data.astype(np.float)), 'hold_heat_pred.nii.gz')


    a = np.mean((gt_heatmap - pred_heatmap_data) ** 2)