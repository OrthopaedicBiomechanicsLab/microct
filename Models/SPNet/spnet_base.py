import torch
import torch.nn as nn
from Models.model_core import *

from Models.SPNet.spnet_unet_base import SPUNetBase
from torch.cuda.amp import autocast

def get_target_heatmap(heatmap_size, landmarks, sigmas, scale=1.0, normalize=False):
    device = landmarks.device

    landmarks_shape = list(landmarks.shape)
    sigmas_shape = list(sigmas.shape)
    batch_size = landmarks_shape[0]
    num_landmarks = landmarks_shape[1]
    dim = landmarks_shape[2] - 1
    assert len(heatmap_size) == dim, 'Dimensions do not match.'
    # assert sigmas_shape[0] == num_landmarks, 'Number of sigmas does not match.'

    heatmap_axis = 1
    landmarks_reshaped = torch.reshape(landmarks[..., 1:], [batch_size, num_landmarks] + [1] * dim + [dim])
    is_valid_reshaped = torch.reshape(landmarks[..., 0], [batch_size, num_landmarks] + [1] * dim)
    sigmas_reshaped = torch.reshape(sigmas, [1, num_landmarks] + [1] * dim)

    aranges = [torch.arange(s, dtype=torch.int16, device=device) for s in heatmap_size]
    grid = torch.meshgrid(*aranges)

    grid_stacked = torch.stack(grid, dim=dim)
    grid_stacked = torch.stack([grid_stacked] * batch_size, dim=0)
    grid_stacked = torch.stack([grid_stacked] * num_landmarks, dim=heatmap_axis)

    if normalize:
        # scale /= np.power(np.sqrt(2 * np.pi) * sigmas_reshaped, dim)
        pi = (torch.acos(torch.zeros(1)) * 2).to(landmarks)
        scale /= (torch.sqrt(2 * pi) * sigmas_reshaped) ** dim

    square_diff = torch.square(grid_stacked - landmarks_reshaped)

    squared_distances = torch.sum(square_diff, dim=-1)

    # heatmap = scale * torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
    heatmap = torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
    heatmap_or_zeros = torch.where((is_valid_reshaped + torch.zeros_like(heatmap)) > 0,
                                   heatmap,
                                   torch.zeros_like(heatmap))

    # squared_distances = tf.reduce_sum(tf.pow(grid_stacked - landmarks_reshaped, 2.0), axis=-1)
    # heatmap = scale * tf.exp(-squared_distances / (2 * tf.pow(sigmas_reshaped, 2)))
    # heatmap_or_zeros = tf.where((is_valid_reshaped + tf.zeros_like(heatmap)) > 0, heatmap, tf.zeros_like(heatmap))

    # delta = heatmap_or_zeros[0, 23]
    # sitk.WriteImage(sitk.GetImageFromArray(delta), 'hold_map.nii.gz')

    # return heatmap_or_zeros
    return heatmap_or_zeros.sum(dim=1).unsqueeze(0)


class SPNet(nn.Module):
    def __init__(self, params, logger, device):
        """
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """
        super(SPNet, self).__init__()

        self.logger = logger
        self.params = params

        self.device = device

        input_shape = params.input_shape
        input_shape = np.array(input_shape)

        heatmap_sigma = torch.full((self.params.num_classes - 1,), 2.0)
        # self.heatmap_sigma = nn.Parameter(heatmap_sigma)

        self.heatmap_sigma = heatmap_sigma

        # self.sigma_regularization = 100


        self.sigma_scale = 1000.0
        self.normalize = False
        self.sigma_regularization = 0.1



        self.conv0 = Conv3D(in_channels=params.num_in_channels,
                            out_channels=params.base_filter,
                            kernel_size=3,
                            activation='leakyrelu',
                            batch_normalization=params.batch_normalization,
                            normalization_momentum=params.normalization_momentum,
                            input_shape=input_shape,
                            padding='same')

        self.unet = SPUNetBase(params)


        self.local_heatmap_conv = Conv3D(in_channels=params.base_filter,
                                         # out_channels=params.num_classes-1,
                                         out_channels=1,
                                         kernel_size=3,
                                         activation=None,
                                         batch_normalization=params.batch_normalization,
                                         normalization_momentum=params.normalization_momentum,
                                         input_shape=input_shape,
                                         padding='same')

        self.avg_pool = torch.nn.AvgPool3d(kernel_size=4)

        pool_shape = input_shape // 4

        self.conv1 = Conv3D(
            # in_channels=params.num_classes-1,
            in_channels=1,
                            out_channels=params.base_filter,
                            kernel_size=7,
                            activation=params.activation,
                            batch_normalization=params.batch_normalization,
                            normalization_momentum=params.normalization_momentum,
                            input_shape=pool_shape,
                            padding='same',
                            )

        self.conv2 = Conv3D(in_channels=params.base_filter,
                            out_channels=params.base_filter,
                            kernel_size=7,
                            activation=params.activation,
                            batch_normalization=params.batch_normalization,
                            normalization_momentum=params.normalization_momentum,
                            input_shape=pool_shape,
                            padding='same',
                            )

        self.conv3 = Conv3D(in_channels=params.base_filter,
                            out_channels=params.base_filter,
                            kernel_size=7,
                            activation=params.activation,
                            batch_normalization=params.batch_normalization,
                            normalization_momentum=params.normalization_momentum,
                            input_shape=pool_shape,
                            padding='same',
                            )

        self.conv4 = Conv3D(in_channels=params.base_filter,
                            # out_channels=params.num_classes-1,
                            out_channels=1,
                            kernel_size=7,
                            activation=None,
                            batch_normalization=params.batch_normalization,
                            normalization_momentum=params.normalization_momentum,
                            input_shape=pool_shape,
                            padding='same',
                            )



        self.up_sample = nn.Upsample(scale_factor=4, mode='trilinear', align_corners=True)

        self.final_act = torch.nn.Sigmoid()

    def forward(self, x, target_landmarks):

        with autocast():

            x = self.conv0(x)

            unet_out = self.unet(x)

            local_heatmaps = self.local_heatmap_conv(unet_out)

            downsampled = self.avg_pool(local_heatmaps)

            conv = self.conv1(downsampled)
            conv = self.conv2(conv)
            conv = self.conv3(conv)
            conv = self.conv4(conv)

            spatial_heatmaps = self.up_sample(conv)

            heatmap_pred = self.final_act(local_heatmaps * spatial_heatmaps)


            heatmap_sigma = self.heatmap_sigma.to(x.device)

            heatmap_target = get_target_heatmap(self.params.sample_size, target_landmarks,
                                                sigmas=heatmap_sigma,
                                                scale=self.sigma_scale, normalize=self.normalize)


            heatmap_loss = heatmap_l2_loss(heatmap_pred, heatmap_target, target_landmarks)

            # heatmaps = torch.sigmoid(heatmaps)

            # heatmaps = torch.clamp(heatmaps.to(torch.float32), min=1e-4, max=1 - 1e-4)

            # Cant use 1e-4 for the max due to rounding issues caused by float16
            # heatmaps = torch.clamp(heatmaps, min=1e-4, max=1 - 3e-4)

            return heatmap_pred, heatmap_target, heatmap_loss.unsqueeze(0)


def heatmap_l2_loss(pred, target, landmarks):
    # target = target[:, landmarks[0, :, 0].to(torch.bool)]
    # pred = pred[:, landmarks[0, :, 0].to(torch.bool)]

    heatmap_loss = F.mse_loss(pred, target, reduction='mean')
    return heatmap_loss