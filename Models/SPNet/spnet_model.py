from ..BaseModel import BaseModel
from .spnet_base import SPNet
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *
import importlib
import os
import pandas as pd


import Models.RetinaUNet.model_utils as mutils

import SimpleITK as sitk

from time import time

from torch.cuda.amp import GradScaler
from torch.cuda.amp import autocast



class HeatmapFocalLoss(nn.Module):
    def __init__(self):
        super(HeatmapFocalLoss, self).__init__()

    def forward(self, preds, targets, landmarks):
        with autocast():

            targets = targets[:, landmarks[0, :, 0].to(torch.bool)]
            preds = preds[:, landmarks[0, :, 0].to(torch.bool)]

            pos_inds = targets.eq(1)
            neg_inds = targets.lt(1)


            # neg_weights = torch.pow(1 - targets[neg_inds], 4)

            loss = 0

            pos_preds = preds[pos_inds]
            neg_preds = preds[neg_inds]

            # pos_loss = (torch.log(pos_preds) * torch.pow(1 - pos_preds, 2)).sum()
            # # neg_loss = torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * neg_weights
            # neg_loss = (torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * (torch.pow(1 - targets[neg_inds], 4))).sum()

            pos_loss = (torch.log(pos_preds) * ((1 - pos_preds) ** 2)).sum()
            # neg_loss = torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * neg_weights
            neg_loss = (torch.log(1 - neg_preds) * (neg_preds ** 2) * ((1 - targets[neg_inds]) ** 4)).sum()

            num_pos = pos_inds.float().sum()
            # pos_loss = pos_loss.sum()
            # neg_loss = neg_loss.sum()

            if pos_preds.nelement() == 0:
                loss = loss - neg_loss
            else:
                loss = loss - (pos_loss + neg_loss) / num_pos

            # for pred in preds:
            #     # pos_pred = pred[pos_inds]
            #     # neg_pred = pred[neg_inds]
            #
            #     pos_loss = torch.log(pos_pred) * torch.pow(1 - pos_pred, 2)
            #     neg_loss = torch.log(1 - neg_pred) * torch.pow(neg_pred, 2) * neg_weights
            #
            #     num_pos = pos_inds.float().sum()
            #     pos_loss = pos_loss.sum()
            #     neg_loss = neg_loss.sum()
            #
            #     if pos_pred.nelement() == 0:
            #         loss = loss - neg_loss
            #     else:
            #         loss = loss - (pos_loss + neg_loss) / num_pos

            # del neg_loss
            # del neg_weights
            # del pos_loss
            # del pos_preds
            # del neg_preds

            return loss.unsqueeze(0)



class GetTargetHeatmap(nn.Module):
    def __init__(self):
        super(GetTargetHeatmap, self).__init__()

    def forward(self, heatmap_size, landmarks, sigmas, scale=1.0, normalize=False):
        device = landmarks.device

        landmarks_shape = list(landmarks.shape)
        sigmas_shape = list(sigmas.shape)
        batch_size = landmarks_shape[0]
        num_landmarks = landmarks_shape[1]
        dim = landmarks_shape[2] - 1
        assert len(heatmap_size) == dim, 'Dimensions do not match.'
        # assert sigmas_shape[0] == num_landmarks, 'Number of sigmas does not match.'

        heatmap_axis = 1
        landmarks_reshaped = torch.reshape(landmarks[..., 1:], [batch_size, num_landmarks] + [1] * dim + [dim])
        is_valid_reshaped = torch.reshape(landmarks[..., 0], [batch_size, num_landmarks] + [1] * dim)
        sigmas_reshaped = torch.reshape(sigmas, [1, num_landmarks] + [1] * dim)

        aranges = [torch.arange(s, dtype=torch.int16, device=device) for s in heatmap_size]
        grid = torch.meshgrid(*aranges)

        grid_stacked = torch.stack(grid, dim=dim)
        grid_stacked = torch.stack([grid_stacked] * batch_size, dim=0)
        grid_stacked = torch.stack([grid_stacked] * num_landmarks, dim=heatmap_axis)

        if normalize:
            # scale /= np.power(np.sqrt(2 * np.pi) * sigmas_reshaped, dim)
            pi = (torch.acos(torch.zeros(1)) * 2).to(landmarks)
            scale /= (torch.sqrt(2 * pi) * sigmas_reshaped) ** dim

        square_diff = torch.square(grid_stacked - landmarks_reshaped)

        squared_distances = torch.sum(square_diff, dim=-1)

        # heatmap = scale * torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
        heatmap = torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
        heatmap_or_zeros = torch.where((is_valid_reshaped + torch.zeros_like(heatmap)) > 0,
                                       heatmap,
                                       torch.zeros_like(heatmap))

        # squared_distances = tf.reduce_sum(tf.pow(grid_stacked - landmarks_reshaped, 2.0), axis=-1)
        # heatmap = scale * tf.exp(-squared_distances / (2 * tf.pow(sigmas_reshaped, 2)))
        # heatmap_or_zeros = tf.where((is_valid_reshaped + tf.zeros_like(heatmap)) > 0, heatmap, tf.zeros_like(heatmap))

        # delta = heatmap_or_zeros[0, 23]
        # sitk.WriteImage(sitk.GetImageFromArray(delta), 'hold_map.nii.gz')

        return heatmap_or_zeros
        # return heatmap_or_zeros.sum(dim=1).unsqueeze(0)


def get_target_heatmap(heatmap_size, landmarks, sigmas, scale=1.0, normalize=False):

    device = landmarks.device

    landmarks_shape = list(landmarks.shape)
    sigmas_shape = list(sigmas.shape)
    batch_size = landmarks_shape[0]
    num_landmarks = landmarks_shape[1]
    dim = landmarks_shape[2] - 1
    assert len(heatmap_size) == dim, 'Dimensions do not match.'
    # assert sigmas_shape[0] == num_landmarks, 'Number of sigmas does not match.'

    heatmap_axis = 1
    landmarks_reshaped = torch.reshape(landmarks[..., 1:], [batch_size, num_landmarks] + [1] * dim + [dim])
    is_valid_reshaped = torch.reshape(landmarks[..., 0], [batch_size, num_landmarks] + [1] * dim)
    sigmas_reshaped = torch.reshape(sigmas, [1, num_landmarks] + [1] * dim)

    aranges = [torch.arange(s, dtype=torch.int16, device=device) for s in heatmap_size]
    grid = torch.meshgrid(*aranges)

    grid_stacked = torch.stack(grid, dim=dim)
    grid_stacked = torch.stack([grid_stacked] * batch_size, dim=0)
    grid_stacked = torch.stack([grid_stacked] * num_landmarks, dim=heatmap_axis)

    if normalize:
        # scale /= np.power(np.sqrt(2 * np.pi) * sigmas_reshaped, dim)
        pi = (torch.acos(torch.zeros(1)) * 2).to(landmarks)
        scale /= (torch.sqrt(2 * pi) * sigmas_reshaped) ** dim

    square_diff = torch.square(grid_stacked - landmarks_reshaped)

    squared_distances = torch.sum(square_diff, dim=-1)

    # heatmap = scale * torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
    heatmap = torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
    heatmap_or_zeros = torch.where((is_valid_reshaped + torch.zeros_like(heatmap)) > 0,
                                   heatmap,
                                   torch.zeros_like(heatmap))

    # squared_distances = tf.reduce_sum(tf.pow(grid_stacked - landmarks_reshaped, 2.0), axis=-1)
    # heatmap = scale * tf.exp(-squared_distances / (2 * tf.pow(sigmas_reshaped, 2)))
    # heatmap_or_zeros = tf.where((is_valid_reshaped + tf.zeros_like(heatmap)) > 0, heatmap, tf.zeros_like(heatmap))

    # delta = heatmap_or_zeros[0, 23]
    # sitk.WriteImage(sitk.GetImageFromArray(delta), 'hold_map.nii.gz')

    return heatmap_or_zeros
    # return heatmap_or_zeros.sum(dim=1).unsqueeze(0)


class SPNetModel(BaseModel):
    def __init__(self, params, logger=None):
        super(SPNetModel, self).__init__(params, logger)


        # BaseModel.__init__(self, params, logger)

        self.model_base_fn = SPNet
        self.load_model_from_params(logger, self.device)



        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)
            if self.optimizer_dict is not None:
                self.optimizer.load_state_dict(self.optimizer_dict)
            self.setup_lr_scheduler(params)

            self.scaler = GradScaler()

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

        self.sigma_scale = 1000.0
        self.normalize = False
        self.sigma_regularization = 0.1


    def set_input(self, input, gpu=None):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """

        self.batch_input = {}

        if gpu is None:

            if len(input.items()) == 1:
                self.image = input['image'].to(self.device)
            else:
                self.image = input['image'].to(self.device)
                self.label = input['label'].to(self.device)


        else:
            if isinstance(gpu, float) or isinstance(gpu, int):
                gpu = 'cuda:{}'.format(gpu)

            self.image = input['image'].to(gpu)
            self.target_landmarks = input['centroid_coord'].to(gpu)


    def forward_old(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""

        # self.loss, self.pred = self.net(self.batch_input)
        with autocast():
            heatmap_pred = self.net(self.image)

        target_heatmap_fn = GetTargetHeatmap()

        # heatmap_loss_fn = HeatmapFocalLoss()
        heatmap_loss_fn = HeatmapL2Loss()

        if self.params.num_gpus > 1:
            # heatmap_sigma = self.net.module.heatmap_sigma

            target_heatmap_fn = torch.nn.DataParallel(target_heatmap_fn)
            target_heatmap_fn.to(self.device)

            heatmap_loss_fn = torch.nn.DataParallel(heatmap_loss_fn)
            heatmap_loss_fn.to(self.device)

        else:
            # heatmap_sigma = self.net.heatmap_sigma
            target_heatmap_fn.to(self.device)
            heatmap_loss_fn.to(self.device)

        heatmap_sigma = torch.full((self.params.batch_size, self.params.num_classes - 1), 4.0).to(self.target_landmarks.device)


        heatmap_target = target_heatmap_fn(self.params.sample_size, self.target_landmarks,
                                           sigmas=heatmap_sigma,
                                           scale=self.sigma_scale, normalize=self.normalize)


        # heatmap_target = get_target_heatmap(self.params.sample_size, self.target_landmarks,
        #                                     sigmas=heatmap_sigma,
        #                                     scale=self.sigma_scale, normalize=self.normalize)


        with autocast():
            heatmap_loss = heatmap_loss_fn(heatmap_pred, heatmap_target, self.target_landmarks).mean()
            # heatmap_loss = heatmap_focal_loss(heatmap_pred, heatmap_target, self.target_landmarks)

            # sigma_loss = self.sigma_loss(heatmap_sigma, self.target_landmarks)

            # self.loss = heatmap_loss + (self.sigma_regularization * sigma_loss)

            # self.loss = heatmap_loss.mean()
            self.loss = heatmap_loss


        self.pred = heatmap_pred
        self.label = heatmap_target



    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""

        # self.loss, self.pred = self.net(self.batch_input)
        with autocast():
            heatmap_pred, heatmap_target, loss = self.net(self.image, self.target_landmarks)


        self.pred = heatmap_pred
        self.label = heatmap_target

        self.loss = loss.mean()



    def backward(self):
        """Calculate UNet model loss"""
        # self.loss = self.calc_loss()


        self.scaler.scale(self.loss).backward()


        # if importlib.util.find_spec('deepspeed') is not None:
        #     import deepspeed
        #     self.net.backward(self.loss)
        #     self.net.step()
        # else:
        #     self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        # self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        # self.optimizer.step()          # update weights

        self.scaler.step(self.optimizer)
        self.scaler.update()



    def reset_model_specific_training(self):

        self.save_in_training_df()
        self.during_training_df = pd.DataFrame(columns=self.df_columns)


    def save_in_training_df(self):
        self.during_training_df.to_csv(self.save_df_name + '.csv', index=False)


    # def SagShapeLoss(self, **fn_params):
    #     return BaseLossMetric(sag_shape_loss, device=self.device, **fn_params)


    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, pred_index=0, label_index=0, **fn_params)

    def L2Loss(self, **fn_params):
        return BaseLossMetric(l2_loss, pred_index=1, label_index=0, device=self.device, **fn_params)




    def heatmap_l2_loss(self, pred, target, landmarks):

        target = target[:, landmarks[0, :, 0].to(torch.bool)]
        pred = pred[:, landmarks[0, :, 0].to(torch.bool)]

        heatmap_loss = F.mse_loss(pred, target, reduction='mean')
        return heatmap_loss



    def sigma_loss(self, heatmap_sigma, target_landmarks):
        sigma_valid = torch.mean((heatmap_sigma * target_landmarks[0, :, 0]) ** 2)
        return sigma_valid


def heatmap_focal_loss(preds, targets, landmarks):

    # float_type = preds.dtype

    targets = targets[:, landmarks[0, :, 0].to(torch.bool)]
    preds = preds[:, landmarks[0, :, 0].to(torch.bool)]

    pos_inds = targets.eq(1)
    neg_inds = targets.lt(1)


    # neg_weights = torch.pow(1 - targets[neg_inds], 4)

    loss = 0

    pos_preds = preds[pos_inds]
    neg_preds = preds[neg_inds]

    # pos_loss = (torch.log(pos_preds) * torch.pow(1 - pos_preds, 2)).sum()
    # # neg_loss = torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * neg_weights
    # neg_loss = (torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * (torch.pow(1 - targets[neg_inds], 4))).sum()


    pos_loss = (torch.log(pos_preds) * ((1 - pos_preds) ** 2)).sum()
    # neg_loss = torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * neg_weights
    neg_loss = (torch.log(1 - neg_preds) * (neg_preds ** 2) * ((1 - targets[neg_inds]) ** 4)).sum()

    num_pos = pos_inds.float().sum()
    # pos_loss = pos_loss.sum()
    # neg_loss = neg_loss.sum()

    if pos_preds.nelement() == 0:
        loss = loss - neg_loss
    else:
        loss = loss - (pos_loss + neg_loss) / num_pos

    # for pred in preds:
    #     # pos_pred = pred[pos_inds]
    #     # neg_pred = pred[neg_inds]
    #
    #     pos_loss = torch.log(pos_pred) * torch.pow(1 - pos_pred, 2)
    #     neg_loss = torch.log(1 - neg_pred) * torch.pow(neg_pred, 2) * neg_weights
    #
    #     num_pos = pos_inds.float().sum()
    #     pos_loss = pos_loss.sum()
    #     neg_loss = neg_loss.sum()
    #
    #     if pos_pred.nelement() == 0:
    #         loss = loss - neg_loss
    #     else:
    #         loss = loss - (pos_loss + neg_loss) / num_pos

    # del neg_loss
    # del neg_weights
    # del pos_loss
    # del pos_preds
    # del neg_preds



    return loss



class HeatmapL2Loss(nn.Module):
    def __init__(self):
        super(HeatmapL2Loss, self).__init__()

    def forward(self, pred, target, landmarks):
        with autocast():
            target = target[:, landmarks[0, :, 0].to(torch.bool)]
            pred = pred[:, landmarks[0, :, 0].to(torch.bool)]

            heatmap_loss = F.mse_loss(pred, target, reduction='mean')
            return heatmap_loss.unsqueeze(0)
