from ..BaseModel import BaseModel
from .retina_unet_heatmap_base import RetinaUNetHeatmap
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *
import importlib
import os
import pandas as pd

from .loss_fns import compute_class_loss, compute_bbox_loss
import Models.RetinaUNet.model_utils as mutils

from .retina_unet_base import get_results

import SimpleITK as sitk

from time import time

from torch.cuda.amp import GradScaler
from torch.cuda.amp import autocast

class RetinaUNetHeatmapModel(BaseModel):
    def __init__(self, params, logger=None):
        super(RetinaUNetHeatmapModel, self).__init__(params, logger)


        # BaseModel.__init__(self, params, logger)

        self.model_base_fn = RetinaUNetHeatmap
        self.load_model_from_params(logger, self.device)



        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)
            if self.optimizer_dict is not None:
                self.optimizer.load_state_dict(self.optimizer_dict)
            self.setup_lr_scheduler(params)

            self.scaler = GradScaler()

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

        # self.sigma_scale = 1000.0
        # self.normalize = False
        # self.sigma_regularization = 0.1



    def set_input(self, input, gpu=None):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """

        self.batch_input = {}
        # self.label = []
        if gpu is None:

            if len(input.items()) == 1:
                self.image = input['image'].to(self.device)
            else:
                self.image = input['image'].to(self.device)
                self.label = input['label'].to(self.device)


        else:
            if isinstance(gpu, float) or isinstance(gpu, int):
                gpu = 'cuda:{}'.format(gpu)

            for key in input.keys():
                if key == 'pid':
                    self.batch_input[key] = input[key]
                else:
                    self.batch_input[key] = input[key].to(gpu)

            # self.label.append(self.batch_input['seg'])

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""

        # self.loss, self.pred = self.net(self.batch_input)

        with autocast():
            # loss, seg_pred, heatmap_pred, heatmap_target = self.net(self.batch_input)
            loss, heatmap_pred, heatmap_target = self.net(self.batch_input)


        # loss, seg_pred, heatmap_pred, heatmap_target = self.net(self.batch_input)

        # self.pred = [heatmap_pred, heatmap_pred]
        # self.label.append(heatmap_target)

        self.pred = heatmap_pred
        self.label = heatmap_target

        self.loss = loss.mean()


    def backward(self):
        """Calculate UNet model loss"""
        # self.loss = self.calc_loss()

        self.scaler.scale(self.loss).backward()

        # self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        # self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        # self.optimizer.step()          # update weights

        self.scaler.step(self.optimizer)
        self.scaler.update()



    def reset_model_specific_training(self):

        self.save_in_training_df()
        self.during_training_df = pd.DataFrame(columns=self.df_columns)


    def save_in_training_df(self):
        self.during_training_df.to_csv(self.save_df_name + '.csv', index=False)


    # def SagShapeLoss(self, **fn_params):
    #     return BaseLossMetric(sag_shape_loss, device=self.device, **fn_params)


    # def DSC(self, **fn_params):
    #     return BaseLossMetric(dsc, pred_index=0, label_index=0, **fn_params)

    def L2Loss(self, **fn_params):
        return BaseLossMetric(l2_loss, pred_index=1, label_index=0, device=self.device, **fn_params)






