import torch
import torch.nn as nn
import torch.nn.functional as F
from .model_utils import ConvBase
from Models.model_core import *


class SCNet(nn.Module):

    def __init__(self, params):
        """
        Builds the classifier sub-network.
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """

        super(SCNet, self).__init__()

        self.dim = 3
        # self.n_classes = params.head_classes
        self.n_classes = 2
