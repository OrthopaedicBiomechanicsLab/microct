import torch
import torch.nn as nn
import torch.nn.functional as F
from .model_utils import ConvBase
from Models.model_core import *
from torch.cuda.amp import autocast


import SimpleITK as sitk
class LANet_currently_working(nn.Module):

    def __init__(self, params):
        """
        Builds the classifier sub-network.
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """

        super(LANet, self).__init__()

        self.dim = 3
        # self.n_classes = params.head_classes
        self.n_classes = 2

        input_shape = np.array(params.input_shape)

        # # self.pool3d_0 = nn.AvgPool3d(2)
        # self.pool3d_0 = nn.MaxPool3d(2)
        #
        # # self.pool_conv_0 = Conv3D(in_channels=params.end_filts,
        # #                           out_channels=params.end_filts,
        # #                           kernel_size=3,
        # #                           stride=2,
        # #                           dilation=1,
        # #                           batch_normalization=params.batch_normalization,
        # #                           normalization_momentum=params.normalization_momentum,
        # #                           padding='same',
        # #                           activation='relu',
        # #                           input_shape=input_shape,
        # #                           output_shape=input_shape // 2
        # #                           )
        #
        #
        #
        # self.pool_conv_0 = Conv3D(in_channels=params.end_filts,
        #                         out_channels=params.end_filts * 2,
        #                         kernel_size=3,
        #                         stride=1,
        #                         dilation=1,
        #                         batch_normalization=params.batch_normalization,
        #                         normalization_momentum=params.normalization_momentum,
        #                         padding='same',
        #                         activation='relu',
        #                         input_shape=input_shape // 2,
        #                         )
        #
        #
        # self.pool_conv_1 = Conv3D(in_channels=params.end_filts * 2,
        #                         out_channels=params.end_filts * 2,
        #                         kernel_size=3,
        #                         stride=1,
        #                         dilation=1,
        #                         batch_normalization=params.batch_normalization,
        #                         normalization_momentum=params.normalization_momentum,
        #                         padding='same',
        #                         activation='relu',
        #                         input_shape=input_shape // 2,
        #                         )
        #
        #
        #
        # # self.conv_pool_0 = Conv3D(in_channels=params.end_filts,
        # #                          out_channels=64,
        # #                          kernel_size=7,
        # #                          stride=1,
        # #                          dilation=1,
        # #                          batch_normalization=params.batch_normalization,
        # #                          normalization_momentum=params.normalization_momentum,
        # #                          padding='same',
        # #                          activation='relu',
        # #                          input_shape=input_shape,
        # #                          )
        # #
        # # self.conv_pool_1 = Conv3D(in_channels=64,
        # #                          out_channels=64,
        # #                          kernel_size=7,
        # #                          stride=1,
        # #                          dilation=1,
        # #                          batch_normalization=params.batch_normalization,
        # #                          normalization_momentum=params.normalization_momentum,
        # #                          padding='same',
        # #                          activation='relu',
        # #                          input_shape=input_shape,
        # #                          )
        #
        #
        # # self.pool3d_1 = nn.AvgPool3d(2)
        # self.pool3d_1 = nn.MaxPool3d(2)
        #
        # input_shape = input_shape // 4
        #
        # self.conv1 = Conv3D(in_channels=params.end_filts * 2,
        #                     out_channels=params.end_filts * 4,
        #                     kernel_size=7,
        #                     stride=1,
        #                     dilation=1,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        #
        # self.conv2 = Conv3D(in_channels=params.end_filts * 4,
        #                     out_channels=params.end_filts * 4,
        #                     kernel_size=7,
        #                     stride=1,
        #                     dilation=1,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        #
        # self.conv3 = Conv3D(in_channels=params.end_filts * 4,
        #                     out_channels=params.end_filts * 4,
        #                     kernel_size=3,
        #                     stride=1,
        #                     dilation=1,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        # self.conv4 = Conv3D(in_channels=params.end_filts * 4,
        #                     out_channels=params.end_filts * 4,
        #                     kernel_size=3,
        #                     stride=1,
        #                     dilation=1,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        # self.up_conv_0 = nn.ConvTranspose3d(in_channels=params.end_filts * 4,
        #                                     out_channels=params.end_filts * 2,
        #                                     kernel_size=2,
        #                                     stride=2)
        #
        # input_shape = input_shape * 2
        #
        # self.conv_after_up_0 = Conv3D(in_channels=params.end_filts * 2,
        #                               out_channels=params.end_filts * 2,
        #                               kernel_size=3,
        #                               stride=1,
        #                               dilation=1,
        #                               batch_normalization=params.batch_normalization,
        #                               normalization_momentum=params.normalization_momentum,
        #                               padding='same',
        #                               activation='relu',
        #                               input_shape=input_shape,
        #                               )
        #
        # self.conv_after_up_1 = Conv3D(in_channels=params.end_filts * 2,
        #                               out_channels=params.end_filts * 2,
        #                               kernel_size=3,
        #                               stride=1,
        #                               dilation=1,
        #                               batch_normalization=params.batch_normalization,
        #                               normalization_momentum=params.normalization_momentum,
        #                               padding='same',
        #                               activation='relu',
        #                               input_shape=input_shape,
        #                               )
        #
        # self.up_conv_1 = nn.ConvTranspose3d(in_channels=params.end_filts * 2,
        #                                     out_channels=params.end_filts,
        #                                     kernel_size=2,
        #                                     stride=2)
        #
        #
        #
        # input_shape = input_shape * 2


        # self.pool_conv = Conv3D(in_channels=params.end_filts,
        #                         out_channels=params.end_filts * 4,
        #                         kernel_size=3,
        #                         stride=4,
        #                         dilation=1,
        #                         batch_normalization=None,
        #                         normalization_momentum=params.normalization_momentum,
        #                         padding='same',
        #                         activation=None,
        #                         input_shape=input_shape,
        #                         )


        # self.conv1 = Conv3D(in_channels=params.end_filts,
        #                            out_channels=params.end_filts * 2,
        #                            kernel_size=3,
        #                            stride=1,
        #                            dilation=1,
        #                            batch_normalization=params.batch_normalization,
        #                            normalization_momentum=params.normalization_momentum,
        #                            padding='same',
        #                            activation='relu',
        #                            input_shape=input_shape,
        #                            )
        #
        #
        # self.conv2 = Conv3D(in_channels=params.end_filts * 2,
        #                            out_channels=params.end_filts * 2,
        #                            kernel_size=3,
        #                            stride=1,
        #                            dilation=1,
        #                            batch_normalization=params.batch_normalization,
        #                            normalization_momentum=params.normalization_momentum,
        #                            padding='same',
        #                            activation='relu',
        #                            input_shape=input_shape,
        #                            )
        #
        #
        # self.conv_shortcut = Conv3D(in_channels=params.end_filts,
        #                            out_channels=params.end_filts * 2,
        #                            kernel_size=1,
        #                            stride=1,
        #                            dilation=1,
        #                            batch_normalization=params.batch_normalization,
        #                            normalization_momentum=params.normalization_momentum,
        #                            padding='same',
        #                            activation=None,
        #                            input_shape=input_shape,
        #                            )
        #
        # self.short_cut_activation = torch.nn.ReLU(inplace=True)
        #
        # self.final_conv_1 = Conv3D(in_channels=params.end_filts * 2,
        #                            out_channels=params.end_filts,
        #                            kernel_size=3,
        #                            stride=1,
        #                            dilation=1,
        #                            batch_normalization=params.batch_normalization,
        #                            normalization_momentum=params.normalization_momentum,
        #                            padding='same',
        #                            activation='relu',
        #                            input_shape=input_shape,
        #                            )
        #
        # # self.final_conv_2 = Conv3D(in_channels=params.end_filts * 2,
        # #                            out_channels=params.end_filts * 2,
        # #                            kernel_size=3,
        # #                            stride=1,
        # #                            dilation=1,
        # #                            batch_normalization=params.batch_normalization,
        # #                            normalization_momentum=params.normalization_momentum,
        # #                            padding='same',
        # #                            activation='relu',
        # #                            input_shape=input_shape,
        # #                            )
        #
        #
        #
        # # self.final_conv_3 = Conv3D(in_channels=params.end_filts * 4,
        # #                            out_channels=params.end_filts * 2,
        # #                            kernel_size=3,
        # #                            stride=1,
        # #                            dilation=1,
        # #                            batch_normalization=False,
        # #                            normalization_momentum=params.normalization_momentum,
        # #                            padding='same',
        # #                            activation='relu',
        # #                            input_shape=input_shape,
        # #                            )



        self.final_conv_4 = nn.Conv3d(in_channels=params.end_filts,
                                      out_channels=1,
                                      kernel_size=1,
                                      )

        self.final_act = nn.Sigmoid()


    def forward(self, x_in_1):
        with autocast():
            # x_pool = self.pool3d_0(x_in_1)
            # x_pool_0 = self.pool_conv_0(x_pool)
            # x_pool_1 = self.pool_conv_1(x_pool_0)
            # x_pool_2 = self.pool3d_1(x_pool_1)
            #
            #
            # x_out1 = self.conv1(x_pool_2)
            # x_out2 = self.conv2(x_out1)
            # x_out3 = self.conv3(x_out2)
            # x_out4 = self.conv4(x_out3)
            #
            #
            # x_out_up1 = self.up_conv_0(x_out4)
            # x_out_up2 = self.conv_after_up_0(x_out_up1)
            # x_out_up3 = self.conv_after_up_1(x_out_up2)
            #
            # x_out_up4 = self.up_conv_1(x_out_up3)


            # x_in_1_1 = self.x_in_1_conv_1(x_in_1)
            # x_in_1_2 = self.x_in_1_conv_2(x_in_1_1)

            # x0 = torch.cat([x_out_up3,  x_in_1_2], dim=1)
            # x0 = torch.cat([x_out_up3,  x_in_1], dim=1)

            # x0 = x_out_up3 * x_in_1_2

            # x1 = self.final_conv_1(x0)
            # x1 = self.final_conv_2(x1)
            # # x = self.final_conv_3(x)


            # x1 = self.conv1(x_in_1)
            # x2 = self.conv2(x1)
            #
            # x_shortcut = self.conv_shortcut(x_in_1)
            #
            #
            # x3 = x2 + x_shortcut
            # x3_act = self.short_cut_activation(x3)
            #
            # x1 = self.final_conv_1(x3_act)
            # # x2 = self.final_conv_2(x1)
            # # x3 = self.final_conv_3(x2)
            # x4 = self.final_conv_4(x1)
            #
            # # x4 = self.final_conv_4(x_in_1)
            #
            # # x3 = torch.where(torch.isnan(x3), torch.tensor(-float('inf'), dtype=x3.dtype, device=x3.device), x3)
            #
            # x = self.final_act(x4)
            #
            # # # Cant use 1e-4 for the max due to rounding issues caused by float16
            # # x = torch.clamp(x, min=3e-4, max=1 - 3e-4)


            x4 = self.final_conv_4(x_in_1)

            # x3 = torch.where(torch.isnan(x3), torch.tensor(-float('inf'), dtype=x3.dtype, device=x3.device), x3)


            x = self.final_act(x4)

            # Cant use 1e-4 for the max due to rounding issues caused by float16
            x = torch.clamp(x, min=3e-4, max=1 - 3e-4)

            return x


class LANet(nn.Module):

    def __init__(self, params):
        """
        Builds the classifier sub-network.
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """

        super(LANet, self).__init__()

        self.dim = 3
        # self.n_classes = params.head_classes
        self.n_classes = 2

        input_shape = np.array(params.input_shape)


        # self.pool3d_0 = nn.MaxPool3d(4)
        #
        # input_shape = input_shape // 4
        #
        # self.conv1 = Conv3D(in_channels=params.end_filts,
        #                     out_channels=params.end_filts * 4,
        #                     kernel_size=3,
        #                     stride=1,
        #                     dilation=1,
        #                     # batch_normalization=False, #params.batch_normalization,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        #
        # self.conv2 = Conv3D(in_channels=params.end_filts * 4,
        #                     out_channels=params.end_filts * 8,
        #                     kernel_size=3,
        #                     stride=1,
        #                     dilation=1,
        #                     # batch_normalization=False, #params.batch_normalization,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        #
        # self.conv3 = Conv3D(in_channels=params.end_filts * 8,
        #                     out_channels=params.end_filts * 8,
        #                     kernel_size=3,
        #                     stride=1,
        #                     dilation=1,
        #                     # batch_normalization=False,  #params.batch_normalization,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        # self.conv4 = Conv3D(in_channels=params.end_filts * 8,
        #                     out_channels=params.end_filts * 8,
        #                     kernel_size=3,
        #                     stride=1,
        #                     dilation=1,
        #                     # batch_normalization=False,  #params.batch_normalization,
        #                     batch_normalization=params.batch_normalization,
        #                     normalization_momentum=params.normalization_momentum,
        #                     padding='same',
        #                     activation='relu',
        #                     input_shape=input_shape,
        #                     )
        #
        # self.up_conv_0 = nn.ConvTranspose3d(in_channels=params.end_filts * 8,
        #                                     out_channels=params.end_filts * 4,
        #                                     kernel_size=2,
        #                                     stride=2)
        #
        # input_shape = input_shape * 2
        #
        # self.conv_after_up_0 = Conv3D(in_channels=params.end_filts * 4,
        #                               out_channels=params.end_filts * 4,
        #                               kernel_size=3,
        #                               stride=1,
        #                               dilation=1,
        #                               batch_normalization=False,  #params.batch_normalization,
        #                               # batch_normalization=params.batch_normalization,
        #                               normalization_momentum=params.normalization_momentum,
        #                               padding='same',
        #                               activation='relu',
        #                               input_shape=input_shape,
        #                               )
        #
        # self.conv_after_up_1 = Conv3D(in_channels=params.end_filts * 4,
        #                               out_channels=params.end_filts * 4,
        #                               kernel_size=3,
        #                               stride=1,
        #                               dilation=1,
        #                               batch_normalization=False,  #params.batch_normalization,
        #                               # batch_normalization=params.batch_normalization,
        #                               normalization_momentum=params.normalization_momentum,
        #                               padding='same',
        #                               activation='relu',
        #                               input_shape=input_shape,
        #                               )
        #
        # self.up_conv_1 = nn.ConvTranspose3d(in_channels=params.end_filts * 4,
        #                                     out_channels=params.end_filts * 2,
        #                                     kernel_size=2,
        #                                     stride=2)
        #
        #
        #
        # input_shape = input_shape * 2
        #
        #
        # self.final_conv_1 = Conv3D(in_channels=params.end_filts * 2,
        #                            out_channels=params.end_filts * 2,
        #                            kernel_size=3,
        #                            stride=1,
        #                            dilation=1,
        #                            # batch_normalization=False, #params.batch_normalization,
        #                            batch_normalization=False,  # params.batch_normalization,
        #                            normalization_momentum=params.normalization_momentum,
        #                            padding='same',
        #                            activation='relu',
        #                            input_shape=input_shape,
        #                            )

        self.final_conv_2 = nn.Conv3d(in_channels=params.end_filts,
                                      out_channels=25,
                                      kernel_size=1,
                                      )

        self.final_act = nn.Sigmoid()

    def forward(self, x_in_1):
        with autocast():
            # x_pool = self.pool3d_0(x_in_1)
            #
            #
            # x_out1 = self.conv1(x_pool)
            # x_out2 = self.conv2(x_out1)
            # x_out3 = self.conv3(x_out2)
            # x_out4 = self.conv4(x_out3)
            #
            #
            # x_out_up1 = self.up_conv_0(x_out4)
            # x_out_up2 = self.conv_after_up_0(x_out_up1)
            # x_out_up3 = self.conv_after_up_1(x_out_up2)
            #
            # x_out_up4 = self.up_conv_1(x_out_up3)
            #
            #
            # x1 = self.final_conv_1(x_out_up4)
            x2 = self.final_conv_2(x_in_1)



            x = self.final_act(x2)

            # Cant use 1e-4 for the max due to rounding issues caused by float16
            x = torch.clamp(x, min=3e-4, max=1 - 3e-4)

            return x

