import torch
import torch.nn as nn
import numpy as np
from Models.model_core import *


from Models.RetinaUNet.backbone import FPNBackBone


from Models.RetinaUNet.local_appearance_net import LANet
from Models.RetinaUNet.spatial_config_net import SCNet

from torch.cuda.amp import autocast


from .model_utils import ConvBase
import Models.RetinaUNet.model_utils as mutils


def get_target_heatmap(heatmap_size, landmarks, sigmas, scale=1.0, normalize=False, semantic=True):
    device = landmarks.device

    landmarks_shape = list(landmarks.shape)
    sigmas_shape = list(sigmas.shape)
    batch_size = landmarks_shape[0]
    num_landmarks = landmarks_shape[1]
    dim = landmarks_shape[2] - 1
    assert len(heatmap_size) == dim, 'Dimensions do not match.'
    # assert sigmas_shape[0] == num_landmarks, 'Number of sigmas does not match.'

    heatmap_axis = 1
    landmarks_reshaped = torch.reshape(landmarks[..., 1:], [batch_size, num_landmarks] + [1] * dim + [dim])
    is_valid_reshaped = torch.reshape(landmarks[..., 0], [batch_size, num_landmarks] + [1] * dim)
    sigmas_reshaped = torch.reshape(sigmas, [1, num_landmarks] + [1] * dim)

    aranges = [torch.arange(s, dtype=torch.int16, device=device) for s in heatmap_size]
    grid = torch.meshgrid(*aranges)

    grid_stacked = torch.stack(grid, dim=dim)
    grid_stacked = torch.stack([grid_stacked] * batch_size, dim=0)
    grid_stacked = torch.stack([grid_stacked] * num_landmarks, dim=heatmap_axis)

    if normalize:
        # scale /= np.power(np.sqrt(2 * np.pi) * sigmas_reshaped, dim)
        pi = (torch.acos(torch.zeros(1)) * 2).to(landmarks)
        scale /= (torch.sqrt(2 * pi) * sigmas_reshaped) ** dim

    square_diff = torch.square(grid_stacked - landmarks_reshaped)

    squared_distances = torch.sum(square_diff, dim=-1)

    # heatmap = scale * torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
    heatmap = torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
    heatmap_or_zeros = torch.where((is_valid_reshaped + torch.zeros_like(heatmap)) > 0,
                                   heatmap,
                                   torch.zeros_like(heatmap))

    # squared_distances = tf.reduce_sum(tf.pow(grid_stacked - landmarks_reshaped, 2.0), axis=-1)
    # heatmap = scale * tf.exp(-squared_distances / (2 * tf.pow(sigmas_reshaped, 2)))
    # heatmap_or_zeros = tf.where((is_valid_reshaped + tf.zeros_like(heatmap)) > 0, heatmap, tf.zeros_like(heatmap))

    # delta = heatmap_or_zeros[0, 23]
    # sitk.WriteImage(sitk.GetImageFromArray(delta), 'hold_map.nii.gz')

    if semantic:
        return heatmap_or_zeros.sum(dim=1).unsqueeze(1)
    else:
        return heatmap_or_zeros
    # return heatmap_or_zeros.sum(dim=1).unsqueeze(1)


def heatmap_focal_loss(preds, targets, landmarks):

    # targets = targets[:, landmarks[0, :, 0].to(torch.bool)]
    # preds = preds[:, landmarks[0, :, 0].to(torch.bool)]

    pos_inds = targets.eq(1)
    neg_inds = targets.lt(1)

    alpha = 2
    beta = 4

    # neg_weights = torch.pow(1 - targets[neg_inds], 4)

    pos_preds = preds[pos_inds]
    neg_preds = preds[neg_inds]

    # pos_loss = (torch.log(pos_preds) * torch.pow(1 - pos_preds, 2)).sum()
    # # neg_loss = torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * neg_weights
    # neg_loss = (torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * (torch.pow(1 - targets[neg_inds], 4))).sum()

    pos_loss = (torch.log(pos_preds) * ((1 - pos_preds) ** alpha)).sum()
    # neg_loss = torch.log(1 - neg_preds) * torch.pow(neg_preds, 2) * neg_weights
    neg_loss = (torch.log(1 - neg_preds) * (neg_preds ** alpha) * ((1 - targets[neg_inds]) ** beta)).sum()

    num_pos = pos_inds.float().sum()
    # pos_loss = pos_loss.sum()
    # neg_loss = neg_loss.sum()

    if pos_preds.nelement() == 0:
        loss = -neg_loss
    else:
        loss = -(pos_loss + neg_loss) / num_pos

    # loss = loss / torch.abs(neg_loss)

    # for pred in preds:
    #     # pos_pred = pred[pos_inds]
    #     # neg_pred = pred[neg_inds]
    #
    #     pos_loss = torch.log(pos_pred) * torch.pow(1 - pos_pred, 2)
    #     neg_loss = torch.log(1 - neg_pred) * torch.pow(neg_pred, 2) * neg_weights
    #
    #     num_pos = pos_inds.float().sum()
    #     pos_loss = pos_loss.sum()
    #     neg_loss = neg_loss.sum()
    #
    #     if pos_pred.nelement() == 0:
    #         loss = loss - neg_loss
    #     else:
    #         loss = loss - (pos_loss + neg_loss) / num_pos

    # img_size = torch.pow(torch.prod(targets.shape), 1/3)
    #
    # loss = loss / img_size

    return loss.unsqueeze(0)


class RetinaUNetHeatmap(nn.Module):
    def __init__(self, params, logger, device):
        """
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """
        super(RetinaUNetHeatmap, self).__init__()

        self.logger = logger
        self.params = params

        self.device = device

        input_shape = params.input_shape
        input_shape = np.array(input_shape)


        conv = mutils.NDConvGenerator(3)
        self.Fpn = FPNBackBone(self.params, operate_stride1=self.params.operate_stride1, conv_2=conv)


        # final_conv_filter = self.params.num_seg_classes if self.params.num_seg_classes > 2 else 1
        #
        # self.final_conv = ConvBase()(self.params.end_filts, final_conv_filter, ks=1, pad=0, norm=None,
        #                              relu=None)

        self.LANet = LANet(self.params)
        # self.SCNet = SCNet(self.params)

        # heatmap_sigma = torch.full((self.params.num_classes - 1,), 4.0)
        # self.heatmap_sigma = nn.Parameter(heatmap_sigma)


        self.sigma_regularization = 100
        self.cross_entropy_regularization = 100


    def forward(self, batch, **kwargs):
        """
        train method (also used for validation monitoring). wrapper around forward pass of network. prepares input data
        for processing, computes losses, and stores outputs in a dictionary.
        :param batch: dictionary containing 'data', 'seg', etc.
        :return: results_dict: dictionary with keys:
                'boxes': list over batch elements. each batch element is a list of boxes. each box is a dictionary:
                        [[{box_0}, ... {box_n}], [{box_0}, ... {box_n}], ...]
                'seg_preds': pixelwise segmentation output (b, c, y, x, (z)) with values [0, .., n_classes].
                'monitor_values': dict of values to be monitored.
        """
        with autocast():

            img = batch['image']

            seg_true = batch['seg']

            target_landmarks = batch['centroid_coord']

            sigma_scale = 1000.0
            heatmap_sigma = 2.0
            # heatmap_sigma = 6.0
            normalize = False


            # sigma_scale = 1.0
            # heatmap_sigma = 6.0
            num_landmarks = self.params.num_classes - 1

            sigmas = torch.full((num_landmarks,), fill_value=heatmap_sigma).to(target_landmarks)





            fpn_outs = self.Fpn(img)

            # seg_logits = self.final_conv(fpn_outs[0])

            # local_appearance_out = self.LANet(fpn_outs[0])
            # pred_heatmap = self.SCNet(local_appearance_out)

            heatmap_pred = self.LANet(fpn_outs[0])

            if heatmap_pred.shape[1] > 1:
                semantic = False
            else:
                semantic = True

            heatmap_target = get_target_heatmap(self.params.sample_size, target_landmarks,
                                                sigmas=sigmas, scale=sigma_scale, normalize=normalize, semantic=semantic)

            # print()
            # print(heatmap_target.shape)
            # print(heatmap_pred.shape)

            # seg_logits = heatmap_pred
            #
            # binary = False
            # if seg_logits.shape[1] > 1:
            #     seg_preds = F.softmax(seg_logits, dim=1)[:, 1:]
            # else:
            #     seg_preds = torch.sigmoid(seg_logits)
            #     binary = True


            # if binary == True:
            #     seg_loss_ce = F.binary_cross_entropy_with_logits(seg_logits, seg_true)
            # else:
            #     seg_loss_ce = F.cross_entropy(seg_logits, seg_true[:, 0])

            # heatmap_loss = F.mse_loss(heatmap_pred, heatmap_target)
            # heatmap_loss = F.l1_loss(heatmap_pred, heatmap_target)
            heatmap_loss = heatmap_focal_loss(heatmap_pred, heatmap_target, target_landmarks)

            # heatmap_loss = heatmap_focal_loss(heatmap_pred, heatmap_target, target_landmarks) + (1e4 * F.mse_loss(heatmap_pred, heatmap_target))


            # a = 1

            # if epoch < 20:
            #     heatmap_loss = F.mse_loss(heatmap_pred, heatmap_target)
            # else:
            #     heatmap_loss = heatmap_focal_loss(heatmap_pred, heatmap_target, target_landmarks)

            #
            # sigma_valid = torch.mean((self.heatmap_sigma * target_landmarks[0, :, 0]) ** 2)
            #
            # sigma_loss = self.sigma_regularization * sigma_valid

            # if var_seg.shape[1] > 1:
            #     seg_loss_ce = F.cross_entropy(seg_logits, var_seg[:, 0])
            # else:
            #     seg_loss_ce = F.binary_cross_entropy_with_logits(seg_logits[:, 1], var_seg[:, 0].to(torch.float))


            # loss = batch_class_loss + batch_bbox_loss + (seg_loss_dice + seg_loss_ce) / 2

            # loss = batch_class_loss + batch_bbox_loss + seg_loss_ce

            # non_zero_channels

            heatmap_loss = torch.where(torch.isnan(heatmap_loss),
                                       torch.tensor(0, dtype=heatmap_loss.dtype, device=heatmap_loss.device),
                                       heatmap_loss)

            # loss = seg_loss_ce + heatmap_loss
            # loss = self.cross_entropy_regularization * seg_loss_ce + heatmap_loss
            loss = heatmap_loss

            # loss = seg_loss_ce + heatmap_loss + sigma_loss
            # loss = heatmap_loss + sigma_loss

            # loss = seg_loss_ce


            # return loss.unsqueeze(0), seg_preds, heatmap_pred, heatmap_target
            return loss.unsqueeze(0), heatmap_pred, heatmap_target

            # return loss.unsqueeze(0)


