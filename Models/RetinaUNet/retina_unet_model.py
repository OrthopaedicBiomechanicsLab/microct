from ..BaseModel import BaseModel
from .retina_unet_base import RetinaUNet
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *
import importlib
import os
import pandas as pd

from .loss_fns import compute_class_loss, compute_bbox_loss
import Models.RetinaUNet.model_utils as mutils

from .retina_unet_base import get_results

import SimpleITK as sitk

from time import time

from torch.cuda.amp import GradScaler
from torch.cuda.amp import autocast

class RetinaUNetModel(BaseModel):
    def __init__(self, params, logger=None):
        super(RetinaUNetModel, self).__init__(params, logger)


        # BaseModel.__init__(self, params, logger)

        self.model_base_fn = RetinaUNet
        self.load_model_from_params(logger, self.device)
        #
        # self.net = UNet(params)


        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)
            if self.optimizer_dict is not None:
                self.optimizer.load_state_dict(self.optimizer_dict)
            self.setup_lr_scheduler(params)

            self.scaler = GradScaler()

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)


    def set_input(self, input, gpu=None):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """

        self.batch_input = {}

        self.label = []

        if gpu is None:

            if len(input.items()) == 1:
                self.image = input['image'].to(self.device)
            else:
                self.image = input['image'].to(self.device)
                self.label = input['label'].to(self.device)


        else:
            if isinstance(gpu, float) or isinstance(gpu, int):
                gpu = 'cuda:{}'.format(gpu)

            for key in input.keys():
                if key == 'pid':
                    self.batch_input[key] = input[key]
                else:
                    self.batch_input[key] = input[key].to(gpu)


            # for key in input.keys():
            #     self.batch_input[key] = input[key].to(gpu)

            # self.label = self.batch_input['seg']
            self.label.append(self.batch_input['seg'])


            # if len(input.items()) == 1:
            #     self.image = input['image'].to(gpu)
            # else:
            #     self.image = input['image'].to(gpu)
            #     self.label = input['label'].to(gpu)


        # if self.net.training is True:
        #     self.image = input['image'].to(self.device)
        #     self.label = input['label'].to(self.device)
        #
        # if self.net.training is False:
        #     if len(input.items()) == 1:
        #         self.image = input['image'].to(self.device)
        #     else:
        #         self.image = input['image'].to(self.device)
        #         self.label = input['label'].to(self.device)

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""

        # self.loss, self.pred = self.net(self.batch_input)

        with autocast():

            self.loss, seg_pred, heatmap_pred, heatmap_target = self.net(self.batch_input)


        self.pred = [seg_pred, heatmap_pred]

        self.label.append(heatmap_target)

        self.loss = self.loss.mean()

        # results_dict = self.after_forward(*result_tuple)

        # self.pred = torch.from_numpy(results_dict.pop('seg_preds').astype(np.float32)).to(self.device)
        #
        # self.loss = results_dict.pop('torch_loss')

        # if self.net.training is False:
        #     if self.pred.shape[1] > 1:
        #         self.pred = torch.nn.functional.softmax(self.pred, dim=1)
        #         self.pred = torch.argmax(self.pred, dim=1, keepdim=True)

        # del results_dict
        # del result_tuple

    def after_forward(self,
                      anchor_class_match_torch, anchor_target_deltas_torch,
                      # anchor_class_match,
                      img, class_logits, pred_deltas,
                      detections, seg_logits, var_seg_ohe, var_seg,
                      anchors,
                      pos_anchors,
                      # box_results_list,
                      *args, **kwargs):

        batch_class_loss = torch.tensor([0], dtype=torch.float32).to(self.device)
        batch_bbox_loss = torch.tensor([0], dtype=torch.float32).to(self.device)

        anchor_class_match = anchor_class_match_torch.cpu().numpy()

        np_anchors = anchors.cpu().numpy()[0]

        detections = mutils.reshape_detections(detections, self.params.batch_size, len(self.params.gpu_ids))

        pos_anchors = pos_anchors.cpu().numpy()

        box_results_list = [[] for _ in range(img.shape[0])]


        # roi_labels_full = self.batch_input['roi_labels'].clone().cpu().numpy()
        # bb_target_full = self.batch_input['bb_target'].clone().cpu().numpy()
        # roi_masks_full = self.batch_input['roi_masks'].clone().cpu().numpy()

        roi_labels_copy = self.batch_input['roi_labels'].clone()
        bb_target_copy = self.batch_input['bb_target'].clone()
        roi_masks_copy = self.batch_input['roi_masks'].clone()

        roi_labels_full = roi_labels_copy.detach().cpu().numpy()
        bb_target_full = bb_target_copy.detach().cpu().numpy()
        roi_masks_full = roi_masks_copy.detach().cpu().numpy()

        for b in range(self.params.batch_size):

            # roi_labels, bb_target, roi_masks = mutils.remove_padding(self.batch_input['roi_labels'][b].detach().cpu().numpy(),
            #                                                          self.batch_input['bb_target'][b].detach().cpu().numpy(),
            #                                                          self.batch_input['roi_masks'][b].detach().cpu().numpy())


            roi_labels, bb_target, roi_masks = mutils.remove_padding(roi_labels_full[b],
                                                                     bb_target_full[b],
                                                                     roi_masks_full[b])


            gt_boxes = bb_target
            gt_class_ids = roi_labels


            if len(gt_boxes) > 0:

                for ix in range(len(gt_boxes)):
                    box_results_list[b].append({'box_coords': bb_target[ix],
                                                'box_label': roi_labels[ix], 'box_type': 'gt'})

                for p in pos_anchors[b]:
                    box_results_list[b].append({'box_coords': p, 'box_type': 'pos_anchor'})

            # class_loss, neg_anchor_ix = compute_class_loss(anchor_class_match_torch, class_logits[b], self.device)
            # bbox_loss = compute_bbox_loss(anchor_target_deltas_torch, pred_deltas[b], anchor_class_match_torch, self.device)

            class_loss, neg_anchor_ix = compute_class_loss(anchor_class_match_torch[b], class_logits[b], self.device)
            bbox_loss = compute_bbox_loss(anchor_target_deltas_torch[b], pred_deltas[b], anchor_class_match_torch[b], self.device)

            # add negative anchors used for loss to results_dict for monitoring.
            neg_anchors = mutils.clip_boxes_numpy(
                np_anchors[np.argwhere(anchor_class_match[b] == -1).flatten()][neg_anchor_ix], img.shape[2:])

            for n in neg_anchors:
                box_results_list[b].append({'box_coords': n, 'box_type': 'neg_anchor'})

            batch_class_loss += class_loss / img.shape[0]
            batch_bbox_loss += bbox_loss / img.shape[0]

            del bbox_loss
            del class_loss

        results_dict = get_results(self.params, img.shape, detections, seg_logits, box_results_list)
        seg_loss_dice = 1 - mutils.batch_dice(F.softmax(seg_logits, dim=1), var_seg_ohe)
        # if (seg_loss_dice > 0.80):
        #
        #     for batch_idx in range(self.params.batch_size):
        #         if results_dict['seg_preds'][batch_idx].max() > 0:
        #
        #         # sitk.WriteImage(sitk.GetImageFromArray(results_dict['seg_preds'].detach().cpu().numpy()[0, 0].astype(np.float)),
        #         #                 self.params.model_output_directory + '/hold_seg_pred.nii.gz')
        #
        #             sitk.WriteImage(sitk.GetImageFromArray(results_dict['seg_preds'][batch_idx, 0].astype(np.float)),
        #                             self.params.model_output_directory + f'/hold_seg_pred_{batch_idx}.nii.gz')
        #
        #             sitk.WriteImage(sitk.GetImageFromArray(self.batch_input['data'].detach().cpu().numpy()[batch_idx, 0].astype(np.float)),
        #                             self.params.model_output_directory + f'/hold_{batch_idx}.nii.gz')
        #             sitk.WriteImage(sitk.GetImageFromArray(self.batch_input['seg'].detach().cpu().numpy()[batch_idx, 0].astype(np.float)),
        #                             self.params.model_output_directory + f'/hold_seg_{batch_idx}.nii.gz')

        seg_loss_ce = F.cross_entropy(seg_logits, var_seg[:, 0])
        loss = batch_class_loss + batch_bbox_loss + (seg_loss_dice + seg_loss_ce) / 2
        results_dict['torch_loss'] = loss
        results_dict['monitor_values'] = {'loss': loss.item(), 'class_loss': batch_class_loss.item()}
        results_dict['logger_string'] = \
            "loss: {0:.2f}, class: {1:.2f}, bbox: {2:.2f}, seg dice: {3:.3f}, seg ce: {4:.3f}, mean pix. pr.: {5:.5f}" \
                .format(loss.item(), batch_class_loss.item(), batch_bbox_loss.item(), seg_loss_dice.item(),
                        seg_loss_ce.item(), np.mean(results_dict['seg_preds']))


        del batch_class_loss
        del batch_bbox_loss

        del roi_labels_copy
        del bb_target_copy
        del roi_masks_copy

        del roi_labels_full
        del bb_target_full
        del roi_masks_full

        del anchors

        return results_dict

    def backward(self):
        """Calculate UNet model loss"""
        # self.loss = self.calc_loss()


        # if importlib.util.find_spec('apex') is not None:
        #     from apex import amp
        #     with amp.scale_loss(self.loss, self.optimizer) as scaled_loss:
        #         scaled_loss.backward()
        #
        # else:
        #     self.loss.backward()


        self.scaler.scale(self.loss).backward()


        # if importlib.util.find_spec('deepspeed') is not None:
        #     import deepspeed
        #     self.net.backward(self.loss)
        #     self.net.step()
        # else:
        #     self.loss.backward()

    def optimize_model(self):
        self.forward()                   # compute prediction
        # self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        # self.optimizer.step()          # update weights

        self.scaler.step(self.optimizer)
        self.scaler.update()



    def reset_model_specific_training(self):

        self.save_in_training_df()
        self.during_training_df = pd.DataFrame(columns=self.df_columns)


    def save_in_training_df(self):
        self.during_training_df.to_csv(self.save_df_name + '.csv', index=False)


    # def SagShapeLoss(self, **fn_params):
    #     return BaseLossMetric(sag_shape_loss, device=self.device, **fn_params)


    def DSC(self, **fn_params):
        return BaseLossMetric(dsc, pred_index=0, label_index=0, **fn_params)

    def L2Loss(self, **fn_params):
        return BaseLossMetric(l2_loss, pred_index=1, label_index=0, device=self.device, **fn_params)






