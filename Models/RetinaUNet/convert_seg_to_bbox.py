# import torch
# import torch.nn as nn
# import torch.nn.functional as F
# from Models.model_core import *
# from torch.autograd import Variable


from scipy.ndimage.measurements import label as lb

# import copy
# from cuda_functions import nms_2D, nms_3D

# class SegToBBox(nn.Module):


import numpy as np

class SegToBBox:
    def __init__(self,
                 get_rois_from_seg_flag=False,
                 class_specific_seg_flag=False):

        # super(SegToBBox, self).__init__()

        self.get_rois_from_seg_flag = get_rois_from_seg_flag
        self.class_specific_seg_flag = class_specific_seg_flag

    # def __call__(self, data_dict):
    #     dim = data_dict['data'].ndim - 1
    #
    #     bb_target = []
    #     roi_masks = []
    #     roi_labels = []
    #     out_seg = np.copy(data_dict['seg'])
    #
    #     p_coords_list = []
    #     p_roi_masks_list = []
    #     p_roi_labels_list = []
    #
    #     if np.sum(data_dict['seg'] != 0) > 0:
    #         if self.get_rois_from_seg_flag:
    #             clusters, n_cands = lb(data_dict['seg'].numpy())
    #             data_dict['class_target'] = [data_dict['class_target']] * n_cands
    #         else:
    #             n_cands = int(np.max(data_dict['seg']))
    #             clusters = data_dict['seg']
    #
    #         rois = np.array([(clusters == ii) * 1 for ii in range(1, n_cands + 1)])  # separate clusters and concat
    #         for rix, r in enumerate(rois):
    #             if np.sum(r != 0) > 0:  # check if the lesion survived data augmentation
    #                 seg_ixs = np.argwhere(r != 0)
    #                 coord_list = [np.min(seg_ixs[:, 1]) - 1, np.min(seg_ixs[:, 2]) - 1, np.max(seg_ixs[:, 1]) + 1,
    #                               np.max(seg_ixs[:, 2]) + 1]
    #
    #                 coord_list.extend([np.min(seg_ixs[:, 3]) - 1, np.max(seg_ixs[:, 3]) + 1])
    #
    #                 p_coords_list.append(coord_list)
    #                 p_roi_masks_list.append(r)
    #                 # add background class = 0. rix is a patient wide index of lesions. since 'class_target' is
    #                 # also patient wide, this assignment is not dependent on patch occurrances.
    #                 p_roi_labels_list.append(data_dict['class_target'][rix + 1])
    #
    #             if self.class_specific_seg_flag:
    #                 out_seg[data_dict['seg'] == rix + 1] = data_dict['class_target'][rix + 1]
    #
    #         if not self.class_specific_seg_flag:
    #             out_seg[data_dict['seg'] > 0] = 1
    #
    #         # bb_target.append(np.array(p_coords_list))
    #         # roi_masks.append(np.array(p_roi_masks_list).astype('uint8'))
    #         # roi_labels.append(np.array(p_roi_labels_list))
    #
    #         bb_target = np.array(p_coords_list)
    #         roi_masks = np.array(p_roi_masks_list).astype('uint8')
    #         roi_labels = np.array(p_roi_labels_list)
    #
    #     # else:
    #     #     bb_target.append([])
    #     #     roi_masks.append(np.zeros_like(data_dict['seg'])[None])
    #     #     roi_labels.append(np.array([-1]))
    #
    #     if self.get_rois_from_seg_flag:
    #         data_dict.pop('class_target', None)
    #
    #     data_dict['bb_target'] = np.array(bb_target)
    #     data_dict['roi_masks'] = np.array(roi_masks)
    #     data_dict['roi_labels'] = np.array(roi_labels)
    #     data_dict['seg'] = out_seg
    #
    #     return data_dict


