import numpy as np
import torch

def compute_iou_3D(box, boxes, box_volume, boxes_volume):
    """Calculates IoU of the given box with the array of the given boxes.
    box: 1D vector [y1, x1, y2, x2, z1, z2] (typically gt box)
    boxes: [boxes_count, (y1, x1, y2, x2, z1, z2)]
    box_area: float. the area of 'box'
    boxes_area: array of length boxes_count.

    Note: the areas are passed in rather than calculated here for
          efficency. Calculate once in the caller to avoid duplicate work.
    """
    # Calculate intersection areas
    y1 = torch.max(box[0], boxes[:, 0])
    y2 = torch.min(box[2], boxes[:, 2])
    x1 = torch.max(box[1], boxes[:, 1])
    x2 = torch.min(box[3], boxes[:, 3])
    z1 = torch.max(box[4], boxes[:, 4])
    z2 = torch.min(box[5], boxes[:, 5])

    zero_hold = torch.zeros(x1.shape, dtype=torch.float32, device=x1.device)

    intersection = torch.max(x2 - x1, zero_hold) * torch.max(y2 - y1, zero_hold) * torch.max(z2 - z1, zero_hold)
    union = box_volume + boxes_volume[:] - intersection[:]
    iou = intersection / union

    return iou


def compute_overlaps(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (y1, x1, y2, x2)]. / 3D: (z1, z2))
    For better performance, pass the largest set first and the smaller second.
    """
    # Areas of anchors and GT boxes

    # Areas of anchors and GT boxes
    volume1 = (boxes1[:, 2] - boxes1[:, 0]) * (boxes1[:, 3] - boxes1[:, 1]) * (boxes1[:, 5] - boxes1[:, 4])
    volume2 = (boxes2[:, 2] - boxes2[:, 0]) * (boxes2[:, 3] - boxes2[:, 1]) * (boxes2[:, 5] - boxes2[:, 4])
    # Compute overlaps to generate matrix [boxes1 count, boxes2 count]
    # Each cell contains the IoU value.
    overlaps = torch.zeros((boxes1.shape[0], boxes2.shape[0])).to(boxes1)
    for i in range(overlaps.shape[1]):
        box2 = boxes2[i]  # this is the gt box
        overlaps[:, i] = compute_iou_3D(box2, boxes1, volume2[i], volume1)
    return overlaps


def gt_anchor_matching(params, anchors, gt_boxes, gt_class_ids=None):
    """

    Parameters
    ----------
    params : Parameters.ParameterBuilder.Parameters
    anchors
    gt_boxes
    gt_class_ids

    Returns
    -------

    """
    # """Given the anchors and GT boxes, compute overlaps and identify positive
    # anchors and deltas to refine them to match their corresponding GT boxes.
    #
    # anchors: [num_anchors, (y1, x1, y2, x2, (z1), (z2))]
    # gt_boxes: [num_gt_boxes, (y1, x1, y2, x2, (z1), (z2))]
    # gt_class_ids (optional): [num_gt_boxes] Integer class IDs for one stage detectors. in RPN case of Mask R-CNN,
    # set all positive matches to 1 (foreground)
    #
    # Returns:
    # anchor_class_matches: [N] (int32) matches between anchors and GT boxes.
    #            1 = positive anchor, -1 = negative anchor, 0 = neutral.
    #            In case of one stage detectors like RetinaNet/RetinaUNet this flag takes
    #            class_ids as positive anchor values, i.e. values >= 1!
    # anchor_delta_targets: [N, (dy, dx, (dz), log(dh), log(dw), (log(dd)))] Anchor bbox deltas.
    # """

    # anchor_class_matches = np.zeros([anchors.shape[0]], dtype=np.int32)
    # anchor_delta_targets = np.zeros((params.rpn_train_anchors_per_image, 2*params.dim))
    # anchor_matching_iou = params.anchor_matching_iou


    anchor_class_matches = torch.zeros([anchors.shape[0]], dtype=torch.float32, device=anchors.device, requires_grad=False)
    anchor_delta_targets = torch.zeros((params.rpn_train_anchors_per_image, 2*params.dim),
                                       dtype=torch.float32, device=anchors.device, requires_grad=False)
    anchor_matching_iou = params.anchor_matching_iou

    if gt_boxes is None:
        # anchor_class_matches = np.full(anchor_class_matches.shape, fill_value=-1)
        anchor_class_matches = torch.full(size=anchor_class_matches.shape, fill_value=-1,
                                          dtype=torch.float32, device=anchors.device, requires_grad=False)

        return anchor_class_matches, anchor_delta_targets

    # for mrcnn: anchor matching is done for RPN loss, so positive labels are all 1 (foreground)
    if gt_class_ids is None:
        # gt_class_ids = np.array([1] * len(gt_boxes))
        gt_class_ids = torch.tensor([1] * len(gt_boxes), dtype=torch.float32, device=anchors.device, requires_grad=False)

    # Compute overlaps [num_anchors, num_gt_boxes]
    overlaps = compute_overlaps(anchors, gt_boxes)

    # Match anchors to GT Boxes
    # If an anchor overlaps a GT box with IoU >= anchor_matching_iou then it's positive.
    # If an anchor overlaps a GT box with IoU < 0.1 then it's negative.
    # Neutral anchors are those that don't match the conditions above,
    # and they don't influence the loss function.
    # However, don't keep any GT box unmatched (rare, but happens). Instead,
    # match it to the closest anchor (even if its max IoU is < 0.1).

    # 1. Set negative anchors first. They get overwritten below if a GT box is
    # matched to them. Skip boxes in crowd areas.
    anchor_iou_argmax = torch.argmax(overlaps, dim=1)

    anchor_iou_max = overlaps[torch.arange(overlaps.shape[0]), anchor_iou_argmax]

    neg_one = torch.tensor(-1, dtype=torch.float32, device=anchor_iou_max.device)
    if anchors.shape[1] == 4:
        # anchor_class_matches[(anchor_iou_max < 0.1)] = -1
        anchor_class_matches = torch.where(anchor_iou_max < 0.1, neg_one, anchor_class_matches)
    elif anchors.shape[1] == 6:
        # anchor_class_matches[(anchor_iou_max < 0.01)] = -1
        anchor_class_matches = torch.where(anchor_iou_max < 0.01, neg_one, anchor_class_matches)

    else:
        raise ValueError('anchor shape wrong {}'.format(anchors.shape))

    # 2. Set an anchor for each GT box (regardless of IoU value).
    # gt_iou_argmax = np.argmax(overlaps, axis=0)
    gt_iou_argmax = torch.argmax(overlaps, dim=0)

    for ix, ii in enumerate(gt_iou_argmax):
        anchor_class_matches[ii] = gt_class_ids[ix]

    # 3. Set anchors with high overlap as positive.
    # above_trhesh_ixs = np.argwhere(anchor_iou_max >= anchor_matching_iou)

    above_thresh_ixs = torch.where(anchor_iou_max >= anchor_matching_iou)
    # above_trhesh_ixs = torch.where(anchor_iou_max >= 0.4)

    anchor_class_matches[above_thresh_ixs] = gt_class_ids[anchor_iou_argmax[above_thresh_ixs]]

    # Subsample to balance positive anchors.
    # ids = np.where(anchor_class_matches > 0)[0]
    ids = torch.where(anchor_class_matches > 0)[0]

    extra = len(ids) - (params.rpn_train_anchors_per_image // 2)
    if extra > 0:
        # Reset the extra ones to neutral
        # ids = np.random.choice(ids, extra, replace=False)

        ids_index = torch.randperm(len(ids))
        ids = ids[ids_index[:7]]
        anchor_class_matches[ids] = 0

    # Leave all negative proposals negative now and sample from them in online hard example mining.
    # For positive anchors, compute shift and scale needed to transform them to match the corresponding GT boxes.

    # ids = np.where(anchor_class_matches > 0)[0]
    ids = torch.where(anchor_class_matches > 0)[0]
    ix = 0  # index into anchor_delta_targets

    rpn_bbox_std_dev = torch.tensor(params.rpn_bbox_std_dev, dtype=torch.float32, device=anchors.device)

    for i, a in zip(ids, anchors[ids]):
        # closest gt box (it might have IoU < anchor_matching_iou)
        gt = gt_boxes[anchor_iou_argmax[i]]

        # convert coordinates to center plus width/height.
        gt_h = gt[2] - gt[0]
        gt_w = gt[3] - gt[1]
        gt_center_y = gt[0] + 0.5 * gt_h
        gt_center_x = gt[1] + 0.5 * gt_w
        # Anchor
        a_h = a[2] - a[0]
        a_w = a[3] - a[1]
        a_center_y = a[0] + 0.5 * a_h
        a_center_x = a[1] + 0.5 * a_w

        gt_d = gt[5] - gt[4]
        gt_center_z = gt[4] + 0.5 * gt_d
        a_d = a[5] - a[4]
        a_center_z = a[4] + 0.5 * a_d

        anchor_delta_targets[ix] = torch.tensor([(gt_center_y - a_center_y) / a_h,
                                                 (gt_center_x - a_center_x) / a_w,
                                                 (gt_center_z - a_center_z) / a_d,
                                                 torch.log(gt_h / a_h),
                                                 torch.log(gt_w / a_w),
                                                 torch.log(gt_d / a_d)],
                                                dtype=torch.float32, device=anchors.device, requires_grad=True)

        # normalize.
        anchor_delta_targets[ix] /= rpn_bbox_std_dev
        ix += 1

    return anchor_class_matches, anchor_delta_targets



def remove_padding(roi_labels, bb_targets, roi_masks):


    idx = torch.nonzero(roi_labels, as_tuple=True)

    roi_labels = roi_labels[idx]
    bb_targets = bb_targets[idx]
    roi_masks = roi_masks[idx]


    return roi_labels, bb_targets, roi_masks