import torch
import torch.nn as nn
import torch.nn.functional as F
from .model_utils import ConvBase


class Classifier(nn.Module):
    def __init__(self, params):
        """
        Builds the classifier sub-network.
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """

        super(Classifier, self).__init__()
        self.dim = 3
        # self.n_classes = params.head_classes
        self.n_classes = 2

        conv = ConvBase()
        n_input_channels = params.end_filts
        n_features = params.n_rpn_features
        # n_output_channels = params.n_anchors_per_pos * params.head_classes
        # n_output_channels = params.n_anchors_per_pos * 2
        n_output_channels = 18
        # n_output_channels = 234
        # n_output_channels = 18

        anchor_stride = params.rpn_anchor_stride

        # self.n_features = n_features
        # self.n_output_channels = n_output_channels

        self.conv_1 = conv(n_input_channels, n_features, ks=3, stride=anchor_stride, pad=1, relu=params.relu)
        self.conv_2 = conv(n_features, n_features, ks=3, stride=anchor_stride, pad=1, relu=params.relu)
        self.conv_3 = conv(n_features, n_features, ks=3, stride=anchor_stride, pad=1, relu=params.relu)
        self.conv_4 = conv(n_features, n_features, ks=3, stride=anchor_stride, pad=1, relu=params.relu)
        self.conv_final = conv(n_features, n_output_channels, ks=3, stride=anchor_stride, pad=1, relu=None)


    def forward(self, x):
        """
        :param x: input feature map (b, in_c, y, x, (z))
        :return: class_logits (b, n_anchors, n_classes)
        """
        x = self.conv_1(x)
        x = self.conv_2(x)
        x = self.conv_3(x)
        x = self.conv_4(x)
        class_logits = self.conv_final(x)


        axes = (0, 2, 3, 1) if self.dim == 2 else (0, 2, 3, 4, 1)
        class_logits = class_logits.permute(*axes)
        class_logits = class_logits.contiguous()
        class_logits = class_logits.view(x.size()[0], -1, self.n_classes)

        return [class_logits]

