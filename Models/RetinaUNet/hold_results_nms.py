import numpy as np
import SimpleITK as sitk
import torch

if __name__ == '__main__':

    pred_img = sitk.ReadImage('D:/retina_unet_semantic_heatmap_single_output_focal_norm_off/hold_raw_heat_pred_3.nii.gz')

    pred = sitk.GetArrayFromImage(pred_img)

    pred_torch = torch.tensor(pred)

    pred_torch = pred_torch.unsqueeze(0).unsqueeze(0)

    hmax = torch.nn.functional.max_pool3d(pred_torch, (3, 3, 3), stride=1, padding=1)

    keep = (hmax == pred_torch).float()

    final = pred_torch * keep

    batch, cat, height, width, depth = final.size()

    topk_scores, topk_inds = torch.topk(final.view(1, -1), 20)

    hold = torch.arange(0, (height * width * depth))

    hold2 = torch.reshape(hold, (height, width, depth))

    coords = [torch.where(topk == hold2) for topk in topk_inds[0]]

    # topk_inds = topk_inds % (height * width * depth)
    topk_ys   = (topk_inds / width).float()
    topk_xs   = (topk_inds % width).float()
    sitk.WriteImage(sitk.GetImageFromArray(final.cpu().numpy()[0, 0]), 'hold_heat_nms.nii.gz')