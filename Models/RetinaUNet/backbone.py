import torch
import torch.nn as nn
import torch.nn.functional as F
from Models.model_core import *
from .model_utils import ConvBase

class FPNBackBone(nn.Module):
    def __init__(self, params, operate_stride1=False, conv_2=None):
        """

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        operate_stride1
        """


        # """
        # from params:
        # :param input_channels: number of channel dimensions in input data.
        # :param start_filts:  number of feature_maps in first layer. rest is scaled accordingly.
        # :param out_channels: number of feature_maps for output_layers of all levels in decoder.
        # :param conv: instance of custom conv class containing the dimension info.
        # :param res_architecture: string deciding whether to use "resnet50" or "resnet101".
        # :param operate_stride1: boolean flag. enables adding of Pyramid levels P1 (output stride 2) and P0 (output stride 1).
        # :param norm: string specifying type of feature map normalization. If None, no normalization is applied.
        # :param relu: string specifying type of nonlinearity. If None, no nonlinearity is applied.
        # :param sixth_pooling: boolean flag. enables adding of Pyramid level P6.
        # """

        super(FPNBackBone, self).__init__()

        self.start_filts = params.base_filter
        start_filts = self.start_filts
        self.n_blocks = [3, 4, {"resnet50": 6, "resnet101": 23}[params.conv_backbone], 3]
        conv = ConvBaseFn(dim=3)
        self.block = ResBlock
        # self.block_2 = ResBlock_2
        self.block_expansion = 4
        self.operate_stride1 = operate_stride1
        self.sixth_pooling = params.sixth_pooling
        # self.dim = conv.dim

        if operate_stride1:

            C0_0 = Conv3D(in_channels=params.num_in_channels,
                          out_channels=start_filts,
                          kernel_size=3,
                          padding=1,
                          activation='relu',
                          batch_normalization=params.batch_normalization)

            C0_1 = Conv3D(in_channels=start_filts,
                          out_channels=start_filts,
                          kernel_size=3,
                          padding=1,
                          activation='relu',
                          batch_normalization=params.batch_normalization)

            self.C0 = nn.Sequential(C0_0, C0_1)

            self.C1 = Conv3D(in_channels=start_filts,
                             out_channels=start_filts,
                             kernel_size=7,
                             padding=3,
                             stride=(2, 2, 1),
                             activation='relu',
                             batch_normalization=params.batch_normalization,
                             )

            # self.C1 = Conv3D(in_channels=start_filts,
            #                  out_channels=start_filts,
            #                  kernel_size=7,
            #                  padding=3,
            #                  stride=(2, 2, 2),
            #                  activation='relu',
            #                  batch_normalization=params.batch_normalization,
            #                  )


        else:
            self.C1 = Conv3D(in_channels=params.num_in_channels,
                             out_channels=start_filts,
                             kernel_size=3,
                             padding=3,
                             stride=(2, 2, 1),
                             activation='relu',
                             batch_normalization=params.batch_normalization,
                             )

            # self.C1 = Conv3D(in_channels=params.num_in_channels,
            #                  out_channels=start_filts,
            #                  kernel_size=3,
            #                  padding=3,
            #                  stride=(2, 2, 2),
            #                  activation='relu',
            #                  batch_normalization=params.batch_normalization,
            #                  )


        start_filts_exp = start_filts * self.block_expansion

        C2_layers = []
        C2_layers.append(nn.MaxPool3d(kernel_size=3, stride=(2, 2, 1), padding=1))
        # C2_layers.append(nn.MaxPool3d(kernel_size=3, stride=(2, 2, 2), padding=1))


        # C2_layers.append(Conv3D(in_channels=start_filts,
        #                         out_channels=start_filts,
        #                         kernel_size=3,
        #                         stride=(2, 2, 2),
        #                         padding=1))

        C2_layers.append(self.block(start_filts, start_filts, stride=1, norm=params.norm, relu=params.relu,
                                    downsample=(start_filts, self.block_expansion, 1)))
        for i in range(1, self.n_blocks[0]):
            C2_layers.append(self.block(start_filts_exp, start_filts, norm=params.norm, relu=params.relu))
        self.C2 = nn.Sequential(*C2_layers)

        #
        # self.C2_0 = nn.MaxPool3d(kernel_size=3, stride=(2, 2, 1), padding=1)
        # self.C2_1 = self.block(start_filts, start_filts, stride=1, norm=params.norm, relu=params.relu,
        #                          downsample=(start_filts, self.block_expansion, 1))
        #
        # self.C2_2 = self.block(start_filts_exp, start_filts, norm=params.norm, relu=params.relu)
        # self.C2_3 = self.block(start_filts_exp, start_filts, norm=params.norm, relu=params.relu)
        #
        # self.C2 = nn.Sequential(self.C2_0, self.C2_1, self.C2_2, self.C2_3)



        C3_layers = []
        C3_layers.append(self.block(start_filts_exp, start_filts * 2, stride=2, norm=params.norm, relu=params.relu,
                                    downsample=(start_filts_exp, 2, 2)))
        for i in range(1, self.n_blocks[1]):
            C3_layers.append(self.block(start_filts_exp * 2, start_filts * 2, norm=params.norm, relu=params.relu))
        self.C3 = nn.Sequential(*C3_layers)

        C4_layers = []
        C4_layers.append(self.block(
            start_filts_exp * 2, start_filts * 4, stride=2, norm=params.norm, relu=params.relu,
            downsample=(start_filts_exp * 2, 2, 2)))
        for i in range(1, self.n_blocks[2]):
            C4_layers.append(self.block(start_filts_exp * 4, start_filts * 4, norm=params.norm, relu=params.relu))
        self.C4 = nn.Sequential(*C4_layers)

        C5_layers = []
        C5_layers.append(self.block(
            start_filts_exp * 4, start_filts * 8, stride=2, norm=params.norm, relu=params.relu,
            downsample=(start_filts_exp * 4, 2, 2)))
        for i in range(1, self.n_blocks[3]):
            C5_layers.append(self.block(start_filts_exp * 8, start_filts * 8, norm=params.norm, relu=params.relu))
        self.C5 = nn.Sequential(*C5_layers)

        if self.sixth_pooling:
            C6_layers = []
            C6_layers.append(self.block(
                start_filts_exp * 8, start_filts * 16, stride=2, norm=params.norm, relu=params.relu,
                downsample=(start_filts_exp * 8, 2, 2)))
            for i in range(1, self.n_blocks[3]):
                C6_layers.append(
                    self.block(start_filts_exp * 16, start_filts * 16, norm=params.norm, relu=params.relu))
            self.C6 = nn.Sequential(*C6_layers)


        self.out_channels = params.end_filts

        self.P1_upsample = Interpolate(scale_factor=(2, 2, 1), mode='trilinear', in_channels=self.out_channels, out_channels=self.out_channels)
        self.P2_upsample = Interpolate(scale_factor=(2, 2, 1), mode='trilinear', in_channels=self.out_channels, out_channels=self.out_channels)

        # self.P1_upsample = Interpolate(scale_factor=(2, 2, 2), mode='trilinear', in_channels=self.out_channels, out_channels=self.out_channels)
        # self.P2_upsample = Interpolate(scale_factor=(2, 2, 2), mode='trilinear', in_channels=self.out_channels, out_channels=self.out_channels)


        # self.P1_upsample = Interpolate(scale_factor=(2, 2, 2), mode='trilinear')
        # self.P2_upsample = Interpolate(scale_factor=(2, 2, 2), mode='trilinear')


        self.P5_conv1 = conv(start_filts * 32 + params.n_latent_dims, self.out_channels, ks=1, stride=1, relu=None)  #

        self.P4_conv1 = conv(start_filts * 16, self.out_channels, ks=1, stride=1, relu=None)
        self.P3_conv1 = conv(start_filts * 8, self.out_channels, ks=1, stride=1, relu=None)
        self.P2_conv1 = conv(start_filts * 4, self.out_channels, ks=1, stride=1, relu=None)
        self.P1_conv1 = conv(start_filts, self.out_channels, ks=1, stride=1, relu=None)



        # self.P4_conv1 = conv(start_filts * 16, self.out_channels, ks=1, stride=1, relu=None, norm=params.norm)
        # self.P3_conv1 = conv(start_filts * 8, self.out_channels, ks=1, stride=1, relu=None, norm=params.norm)
        # self.P2_conv1 = conv(start_filts * 4, self.out_channels, ks=1, stride=1, relu=None, norm=params.norm)
        # self.P1_conv1 = conv(start_filts, self.out_channels, ks=1, stride=1, relu=None, norm=params.norm)


        if operate_stride1:
            self.P0_conv1 = conv(start_filts, self.out_channels, ks=1, stride=1, relu=None)
            self.P0_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None)

        self.P1_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None)
        self.P2_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None)
        self.P3_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None)
        self.P4_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None)
        self.P5_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None)




        # self.P1_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None, norm=params.norm)
        # self.P2_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None, norm=params.norm)
        # self.P3_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None, norm=params.norm)
        # self.P4_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None, norm=params.norm)
        # self.P5_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None, norm=params.norm)

        if self.sixth_pooling:
            self.P6_conv1 = conv(start_filts * 64, self.out_channels, ks=1, stride=1, relu=None)
            self.P6_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None)

            # self.P6_conv1 = conv(start_filts * 64, self.out_channels, ks=1, stride=1, relu=None, norm=params.norm)
            # self.P6_conv2 = conv(self.out_channels, self.out_channels, ks=3, stride=1, pad=1, relu=None, norm=params.norm)

            self.p6_upsample = nn.ConvTranspose3d(in_channels=self.out_channels,
                                                  out_channels=self.out_channels,
                                                  kernel_size=2,
                                                  stride=2)

        self.p5_upsample = nn.ConvTranspose3d(in_channels=self.out_channels,
                                              out_channels=self.out_channels,
                                              kernel_size=2,
                                              stride=2)

        self.p4_upsample = nn.ConvTranspose3d(in_channels=self.out_channels,
                                              out_channels=self.out_channels,
                                              kernel_size=2,
                                              stride=2)

        self.p3_upsample = nn.ConvTranspose3d(in_channels=self.out_channels,
                                              out_channels=self.out_channels,
                                              kernel_size=2,
                                              stride=2)

    def forward(self, x):
        """

        Parameters
        ----------
        x : input image of shape (b, c, y, x, (z))

        Returns
        -------

        """

        if self.operate_stride1:
            c0_out = self.C0(x)
        else:
            c0_out = x

        c1_out = self.C1(c0_out)
        c2_out = self.C2(c1_out)
        c3_out = self.C3(c2_out)
        c4_out = self.C4(c3_out)
        c5_out = self.C5(c4_out)
        if self.sixth_pooling:
            c6_out = self.C6(c5_out)
            p6_pre_out = self.P6_conv1(c6_out)
            # p5_pre_out = self.P5_conv1(c5_out) + F.interpolate(p6_pre_out, scale_factor=2)
            p5_pre_out = self.P5_conv1(c5_out) + self.p6_upsample(p6_pre_out)
        else:
            p5_pre_out = self.P5_conv1(c5_out)

        # p4_pre_out = self.P4_conv1(c4_out) + F.interpolate(p5_pre_out, scale_factor=2)
        # p3_pre_out = self.P3_conv1(c3_out) + F.interpolate(p4_pre_out, scale_factor=2)
        # p2_pre_out = self.P2_conv1(c2_out) + F.interpolate(p3_pre_out, scale_factor=2)


        p4_pre_out = self.P4_conv1(c4_out) + self.p5_upsample(p5_pre_out)
        p3_pre_out = self.P3_conv1(c3_out) + self.p4_upsample(p4_pre_out)
        p2_pre_out = self.P2_conv1(c2_out) + self.p3_upsample(p3_pre_out)

        # plot feature map shapes for debugging.
        # for ii in [c0_out, c1_out, c2_out, c3_out, c4_out, c5_out, c6_out]:
        #     print ("encoder shapes:", ii.shape)
        #
        # for ii in [p6_out, p5_out, p4_out, p3_out, p2_out, p1_out]:
        #     print("decoder shapes:", ii.shape)

        p2_out = self.P2_conv2(p2_pre_out)
        p3_out = self.P3_conv2(p3_pre_out)
        p4_out = self.P4_conv2(p4_pre_out)
        p5_out = self.P5_conv2(p5_pre_out)
        out_list = [p2_out, p3_out, p4_out, p5_out]

        if self.sixth_pooling:
            p6_out = self.P6_conv2(p6_pre_out)
            out_list.append(p6_out)

        if self.operate_stride1:
            p1_pre_out = self.P1_conv1(c1_out) + self.P2_upsample(p2_pre_out)
            p0_pre_out = self.P0_conv1(c0_out) + self.P1_upsample(p1_pre_out)
            # p1_out = self.P1_conv2(p1_pre_out) # usually not needed.
            p0_out = self.P0_conv2(p0_pre_out)
            out_list = [p0_out] + out_list

        return out_list




class ResBlock(nn.Module):

    def __init__(self, start_filts, planes, stride=1, downsample=None, norm=None, relu='relu'):
        super(ResBlock, self).__init__()
        # self.conv1 = conv(start_filts, planes, ks=1, stride=stride, norm=norm, relu=relu)
        # self.conv2 = conv(planes, planes, ks=3, pad=1, norm=norm, relu=relu)
        # self.conv3 = conv(planes, planes * 4, ks=1, norm=norm, relu=None)
        
        self.conv1 = Conv3D(in_channels=start_filts,
                            out_channels=planes,
                            kernel_size=1,
                            stride=stride,
                            batch_normalization=norm,
                            activation=relu)

        self.conv2 = Conv3D(in_channels=planes,
                            out_channels=planes,
                            kernel_size=3,
                            padding=1,
                            batch_normalization=norm,
                            activation=relu)

        self.conv3 = Conv3D(in_channels=planes,
                            out_channels=planes * 4,
                            kernel_size=1,
                            batch_normalization=norm,
                            activation=None)
        
        
        self.relu = nn.ReLU(inplace=True) if relu == 'relu' else nn.LeakyReLU(inplace=True)


        if downsample is not None:
            # self.downsample = conv(downsample[0], downsample[0] * downsample[1], ks=1, stride=downsample[2], norm=norm, relu=None)

            self.downsample = Conv3D(in_channels=downsample[0],
                                     out_channels=downsample[0] * downsample[1],
                                     kernel_size=1,
                                     stride=downsample[2],
                                     batch_normalization=norm,
                                     activation=None)

        else:
            self.downsample = None
        self.stride = stride

    def forward(self, x):
        residual = x
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        if self.downsample:
            residual = self.downsample(x)
        out += residual
        out = self.relu(out)
        return out


class Interpolate(nn.Module):
    def __init__(self, scale_factor, mode, in_channels, out_channels):
        super(Interpolate, self).__init__()
        # self.interp = nn.functional.interpolate
        self.scale_factor = scale_factor
        self.mode = mode

        self.interp = nn.ConvTranspose3d(in_channels=in_channels,
                                         out_channels=out_channels,
                                         kernel_size=self.scale_factor,
                                         stride=self.scale_factor)

    def forward(self, x):
        # x0 = self.interp(x, scale_factor=self.scale_factor, mode=self.mode, align_corners=False)

        x0 = self.interp(x)
        return x0





class ConvBaseFn(object):
    """
    generic wrapper around conv-layers to avoid 2D vs. 3D distinguishing in code.
    """
    def __init__(self, dim=3):
        self.dim = dim

    def __call__(self, c_in, c_out, ks, pad=0, stride=1, norm=None, relu='relu'):
        """
        :param c_in: number of in_channels.
        :param c_out: number of out_channels.
        :param ks: kernel size.
        :param pad: pad size.
        :param stride: kernel stride.
        :param norm: string specifying type of feature map normalization. If None, no normalization is applied.
        :param relu: string specifying type of nonlinearity. If None, no nonlinearity is applied.
        :return: convolved feature_map.
        """

        conv = nn.Conv3d(c_in, c_out, kernel_size=ks, padding=pad, stride=stride)
        if norm is not None:
            if norm == 'instance_norm':
                norm_layer = nn.InstanceNorm3d(c_out)
            elif norm == 'batch_norm':
                norm_layer = nn.BatchNorm3d(c_out)
            elif norm == True:
                norm_layer = nn.BatchNorm3d(c_out)
            else:
                raise ValueError('norm type as specified in configs is not implemented... {}'.format(norm))
            conv = nn.Sequential(conv, norm_layer)

        if relu is not None:
            if relu == 'relu':
                relu_layer = nn.ReLU(inplace=True)
            elif relu == 'leaky_relu':
                relu_layer = nn.LeakyReLU(inplace=True)
            elif relu == False:
                pass
            else:
                raise ValueError('relu type as specified in configs is not implemented...')
            conv = nn.Sequential(conv, relu_layer)

        return conv





class ResBlock_2(nn.Module):

    def __init__(self, start_filts, planes, conv, stride=1, downsample=None, norm=None, relu='relu'):
        super(ResBlock_2, self).__init__()
        self.conv1 = conv(start_filts, planes, ks=1, stride=stride, norm=norm, relu=relu)
        self.conv2 = conv(planes, planes, ks=3, pad=1, norm=norm, relu=relu)
        self.conv3 = conv(planes, planes * 4, ks=1, norm=norm, relu=None)
        self.relu = nn.ReLU(inplace=True) if relu == 'relu' else nn.LeakyReLU(inplace=True)
        if downsample is not None:
            self.downsample = conv(downsample[0], downsample[0] * downsample[1], ks=1, stride=downsample[2], norm=norm, relu=None)
        else:
            self.downsample = None
        self.stride = stride

    def forward(self, x):
        residual = x
        out = self.conv1(x)
        out = self.conv2(out)
        out = self.conv3(out)
        if self.downsample:
            residual = self.downsample(x)
        out += residual
        out = self.relu(out)
        return out

