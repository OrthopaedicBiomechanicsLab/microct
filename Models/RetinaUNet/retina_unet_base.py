import torch
import torch.nn as nn
import numpy as np
from Models.model_core import *

from Models.RegionProposal import RPNBase
from Models.RegionProposal import GenerateRPNProposals
from Models.RetinaUNet.backbone import FPNBackBone
from Models.RetinaUNet.bb_regressor import BBRegressor
from Models.RetinaUNet.classifier import Classifier

from Models.RetinaUNet.local_appearance_net import LANet
from Models.RetinaUNet.spatial_config_net import SCNet




from .model_utils import ConvBase
import Models.RetinaUNet.model_utils as mutils

import Models.RetinaUNet.model_utils_torch as mutils_torch

from .loss_fns import compute_class_loss, compute_bbox_loss

import SimpleITK as sitk

from Models.ProposalLayer import PropsalLayer
from Models.DetectionTargetLayer import DetectionTargetLayer
from Models.DetectionLayer import DetectionLayer
from Models.FeaturePyramidNetwork import FPNClassifier

import Models.utils as utils

from Models.RetinaUNet.convert_seg_to_bbox import SegToBBox


def get_results(params, img_shape, detections, seg_logits, box_results_list=None):
    """

    Parameters
    ----------
    params : Parameters.ParameterBuilder.Parameters
    img_shape :
        (n_final_detections, (y1, x1, y2, x2, (z1), (z2), batch_ix, pred_class_id, pred_score)
    detections
    seg_logits
    box_results_list

    Returns
    -------

    """

    # """
    # Restores batch dimension of merged detections, unmolds detections, creates and fills results dict.
    # :param img_shape:
    # :param detections: (n_final_detections, (y1, x1, y2, x2, (z1), (z2), batch_ix, pred_class_id, pred_score)
    # :param box_results_list: None or list of output boxes for monitoring/plotting.
    # each element is a list of boxes per batch element.
    # :return: results_dict: dictionary with keys:
    #          'boxes': list over batch elements. each batch element is a list of boxes. each box is a dictionary:
    #                   [[{box_0}, ... {box_n}], [{box_0}, ... {box_n}], ...]
    #          'seg_preds': pixel-wise class predictions (b, 1, y, x, (z)) with values [0, ..., n_classes] for
    #                       retina_unet and dummy array for retina_net.
    # """
    detections = detections.cpu().data.numpy() if isinstance(detections, torch.Tensor) else detections
    batch_ixs = detections[:, params.dim*2]
    detections = [detections[batch_ixs == ix] for ix in range(img_shape[0])]

    # for test_forward, where no previous list exists.
    if box_results_list is None:
        box_results_list = [[] for _ in range(img_shape[0])]

    for ix in range(img_shape[0]):

        if 0 not in detections[ix].shape:

            boxes = detections[ix][:, :2 * params.dim].astype(np.int32)
            class_ids = detections[ix][:, 2 * params.dim + 1].astype(np.int32)
            scores = detections[ix][:, 2 * params.dim + 2]

            # Filter out detections with zero area. Often only happens in early
            # stages of training when the network weights are still a bit random.
            if params.dim == 2:
                exclude_ix = np.where((boxes[:, 2] - boxes[:, 0]) * (boxes[:, 3] - boxes[:, 1]) <= 0)[0]
            else:
                exclude_ix = np.where(
                    (boxes[:, 2] - boxes[:, 0]) * (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 5] - boxes[:, 4]) <= 0)[0]

            if exclude_ix.shape[0] > 0:
                boxes = np.delete(boxes, exclude_ix, axis=0)
                class_ids = np.delete(class_ids, exclude_ix, axis=0)
                scores = np.delete(scores, exclude_ix, axis=0)

            if 0 not in boxes.shape:
                for ix2, score in enumerate(scores):
                    if score >= params.model_min_confidence:
                        box_results_list[ix].append({'box_coords': boxes[ix2],
                                                     'box_score': score,
                                                     'box_type': 'det',
                                                     'box_pred_class_id': class_ids[ix2]})

    results_dict = {'boxes': box_results_list}
    if seg_logits is None:
        # output dummy segmentation for retina_net.
        results_dict['seg_preds'] = np.zeros(img_shape)[:, 0][:, np.newaxis]
    else:
        # output label maps for retina_unet.
        results_dict['seg_preds'] = F.softmax(seg_logits, 1).argmax(1).cpu().data.numpy()[:, np.newaxis].astype('uint8')
        # results_dict['seg_preds'] = F.softmax(seg_logits, dim=1).argmax(1).unsqueeze(1).type(torch.float32)

    return results_dict


def get_target_heatmap(heatmap_size, landmarks, sigmas, scale=1.0, normalize=False):

    device = landmarks.device

    landmarks_shape = list(landmarks.shape)
    sigmas_shape = list(sigmas.shape)
    batch_size = landmarks_shape[0]
    num_landmarks = landmarks_shape[1]
    dim = landmarks_shape[2] - 1
    assert len(heatmap_size) == dim, 'Dimensions do not match.'
    assert sigmas_shape[0] == num_landmarks, 'Number of sigmas does not match.'

    heatmap_axis = 1
    landmarks_reshaped = torch.reshape(landmarks[..., 1:], [batch_size, num_landmarks] + [1] * dim + [dim])
    is_valid_reshaped = torch.reshape(landmarks[..., 0], [batch_size, num_landmarks] + [1] * dim)
    sigmas_reshaped = torch.reshape(sigmas, [1, num_landmarks] + [1] * dim)

    aranges = [torch.arange(s, dtype=torch.int16, device=device) for s in heatmap_size]
    grid = torch.meshgrid(*aranges)

    grid_stacked = torch.stack(grid, dim=dim)
    grid_stacked = torch.stack([grid_stacked] * batch_size, dim=0)
    grid_stacked = torch.stack([grid_stacked] * num_landmarks, dim=heatmap_axis)

    if normalize:
        # scale /= np.power(np.sqrt(2 * np.pi) * sigmas_reshaped, dim)
        pi = (torch.acos(torch.zeros(1)) * 2).to(landmarks)
        scale /= (torch.sqrt(2 * pi) * sigmas_reshaped) ** dim

    square_diff = torch.square(grid_stacked - landmarks_reshaped)

    squared_distances = torch.sum(square_diff, dim=-1)

    heatmap = scale * torch.exp(-squared_distances / (2 * torch.square(sigmas_reshaped)))
    heatmap_or_zeros = torch.where((is_valid_reshaped + torch.zeros_like(heatmap)) > 0,
                                   heatmap,
                                   torch.zeros_like(heatmap))

    # squared_distances = tf.reduce_sum(tf.pow(grid_stacked - landmarks_reshaped, 2.0), axis=-1)
    # heatmap = scale * tf.exp(-squared_distances / (2 * tf.pow(sigmas_reshaped, 2)))
    # heatmap_or_zeros = tf.where((is_valid_reshaped + tf.zeros_like(heatmap)) > 0, heatmap, tf.zeros_like(heatmap))

    # delta = heatmap_or_zeros[0, 23]
    # sitk.WriteImage(sitk.GetImageFromArray(delta), 'hold_map.nii.gz')

    return heatmap_or_zeros
    # return heatmap_or_zeros.sum(dim=1).unsqueeze(0)


class RetinaUNet(nn.Module):
    def __init__(self, params, logger, device):
        """
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """
        super(RetinaUNet, self).__init__()

        self.logger = logger
        self.params = params

        self.device = device

        input_shape = params.input_shape
        input_shape = np.array(input_shape)

        # self.np_anchors = mutils.generate_pyramid_anchors(self.logger, self.params)
        # self.anchors = torch.from_numpy(self.np_anchors).float()


        conv = mutils.NDConvGenerator(3)
        self.Fpn = FPNBackBone(self.params, operate_stride1=self.params.operate_stride1, conv_2=conv)
        # self.Classifier = Classifier(self.params)
        # self.BBRegressor = BBRegressor(self.params)
        #
        # self.seg_to_bbox = SegToBBox(self.params.get_rois_from_seg_flag,
        #                              self.params.class_specific_seg_flag)

        final_conv_filter = self.params.num_seg_classes if self.params.num_seg_classes > 2 else 1

        self.final_conv = ConvBase()(self.params.end_filts, final_conv_filter, ks=1, pad=0, norm=None,
                                     relu=None)

        self.LANet = LANet(self.params)
        # self.SCNet = SCNet(self.params)

        heatmap_sigma = torch.full((self.params.num_classes - 1,), 4.0)
        self.heatmap_sigma = nn.Parameter(heatmap_sigma)


        self.sigma_regularization = 100



    def forward(self, batch, train_test='train', **kwargs):
        if train_test == 'train':
            # forward_fn = self.train_forward
            forward_fn = self.train_forward_rev2
        else:
            forward_fn = self.test_forward

        # if torch.cuda.is_available():
        #     self.anchors = self.anchors.cuda()
        #     # self.anchors = self.anchors.to(self.device)
        #     # self.anchors = self.anchors.to(batch['data'].device)
        # else:
        #     self.anchors = self.np_anchors



        results_dict = forward_fn(batch, **kwargs)

        return results_dict


    def train_forward(self, batch, **kwargs):
        """
        train method (also used for validation monitoring). wrapper around forward pass of network. prepares input data
        for processing, computes losses, and stores outputs in a dictionary.
        :param batch: dictionary containing 'data', 'seg', etc.
        :return: results_dict: dictionary with keys:
                'boxes': list over batch elements. each batch element is a list of boxes. each box is a dictionary:
                        [[{box_0}, ... {box_n}], [{box_0}, ... {box_n}], ...]
                'seg_preds': pixelwise segmentation output (b, c, y, x, (z)) with values [0, .., n_classes].
                'monitor_values': dict of values to be monitored.
        """

        # batch = self.seg_to_bbox(batch)

        img = batch['data']
        # gt_class_ids = batch['roi_labels']
        # gt_boxes = batch['bb_target']

        var_seg_ohe = mutils.get_one_hot_encoding(batch['seg'].cpu().numpy(), self.params.num_seg_classes)

        if isinstance(var_seg_ohe, np.ndarray):
            # var_seg_ohe = torch.FloatTensor(var_seg_ohe).cuda()
            var_seg_ohe = torch.from_numpy(var_seg_ohe.astype(np.float32)).to(img.device)
        else:
            # var_seg_ohe = var_seg_ohe.cuda()
            # var_seg_ohe = var_seg_ohe.to(self.device)
            var_seg_ohe = var_seg_ohe.to(img.device)


        # var_seg_ohe = torch.FloatTensor(mutils.get_one_hot_encoding(batch['seg'], self.cf.num_seg_classes)).cuda()


        if isinstance(batch['seg'], np.ndarray):
            # var_seg = torch.from_numpy(batch['seg']).type(torch.long).cuda()
            var_seg = torch.from_numpy(batch['seg']).type(torch.long).to(img.device)
        else:
            # var_seg = batch['seg'].type(torch.long).cuda()
            var_seg = batch['seg'].type(torch.long).to(img.device)

        if isinstance(img, np.ndarray):
            # img = torch.from_numpy(img).float().cuda()
            img = torch.from_numpy(img).float().cuda()
        else:
            img = img.cuda()
            # img = img.to(self.device)


        # batch_class_loss = torch.tensor([0], dtype=torch.float32).to(img.device)
        # batch_bbox_loss = torch.tensor([0], dtype=torch.float32).to(img.device)


        # # list of output boxes for monitoring/plotting. each element is a list of boxes per batch element.
        # box_results_list = [[] for _ in range(img.shape[0])]
        detections, class_logits, pred_deltas, seg_logits = self.forward_fn(img)

        anchor_class_match_torch = []
        anchor_target_deltas_torch = []
        # anchor_class_match_np = []

        # loop over batch
        for b in range(img.shape[0]):

            roi_labels, bb_target, roi_masks = mutils.remove_padding(batch['roi_labels'][b].cpu().numpy(),
                                                                     batch['bb_target'][b].cpu().numpy(),
                                                                     batch['roi_masks'][b].cpu().numpy())

            gt_boxes = bb_target
            gt_class_ids = roi_labels

            # add gt boxes to results dict for monitoring.
            # if len(gt_boxes[b]) > 0:

            if len(gt_boxes) > 0:
                # for ix in range(len(gt_boxes[b])):

                # for ix in range(len(gt_boxes)):
                #     # box_results_list[b].append({'box_coords': batch['bb_target'][b][ix],
                #     #                             'box_label': batch['roi_labels'][b][ix], 'box_type': 'gt'})
                #
                #     box_results_list[b].append({'box_coords': bb_target[ix],
                #                                 'box_label': roi_labels[ix], 'box_type': 'gt'})

                # match gt boxes with anchors to generate targets.

                # anchor_class_match, anchor_target_deltas = mutils.gt_anchor_matching(
                #     self.params,
                #     self.np_anchors,
                #     gt_boxes[b].numpy() if isinstance(gt_boxes[b], torch.Tensor) else gt_boxes[b],
                #     gt_class_ids[b].numpy() if isinstance(gt_class_ids[b], torch.Tensor) else gt_class_ids[b])


                anchor_class_match, anchor_target_deltas = mutils.gt_anchor_matching(
                    self.params,
                    self.np_anchors,
                    gt_boxes.numpy() if isinstance(gt_boxes, torch.Tensor) else gt_boxes,
                    gt_class_ids.numpy() if isinstance(gt_class_ids, torch.Tensor) else gt_class_ids)


                # add positive anchors used for loss to results_dict for monitoring.
                pos_anchors = mutils.clip_boxes_numpy(
                    self.np_anchors[np.argwhere(anchor_class_match > 0)][:, 0], img.shape[2:])

                # for p in pos_anchors:
                #     box_results_list[b].append({'box_coords': p, 'box_type': 'pos_anchor'})
                #     # box_results_list.append({'box_coords': p, 'box_type': 'pos_anchor'})

            else:
                anchor_class_match = np.array([-1] * self.np_anchors.shape[0])
                anchor_target_deltas = np.array([0])

            # anchor_class_match_torch = torch.from_numpy(anchor_class_match).to(self.device)
            # anchor_target_deltas_torch = torch.from_numpy(anchor_target_deltas).float().to(self.device)

            anchor_class_match_torch.append(torch.from_numpy(anchor_class_match).to(img.device))
            anchor_target_deltas_torch.append(torch.from_numpy(anchor_target_deltas).float().to(img.device))

            # anchor_class_match_np.append(anchor_class_match)
            # compute losses.
            # class_loss, neg_anchor_ix = compute_class_loss(anchor_class_match_torch, class_logits[b], self.device)
            # bbox_loss = compute_bbox_loss(anchor_target_deltas_torch, pred_deltas[b], anchor_class_match_torch, self.device)

        anchor_class_match_torch = torch.stack(anchor_class_match_torch, dim=0)
        anchor_target_deltas_torch = torch.stack(anchor_target_deltas_torch, dim=0)

        # anchor_class_match_np = np.stack(anchor_class_match_np, axis=0)

        return (anchor_class_match_torch, anchor_target_deltas_torch,
                # anchor_class_match_np,
                img, class_logits, pred_deltas,
                detections.unsqueeze(0),
                seg_logits, var_seg_ohe, var_seg,
                self.anchors.unsqueeze(0),
                torch.tensor(pos_anchors).unsqueeze(-1).to(img.device),
                # box_results_list,
                )



        #     class_loss, neg_anchor_ix = compute_class_loss(anchor_class_match_torch, class_logits, self.device)
        #     bbox_loss = compute_bbox_loss(anchor_target_deltas_torch, pred_deltas, anchor_class_match_torch, self.device)
        #
        #     # add negative anchors used for loss to results_dict for monitoring.
        #     neg_anchors = mutils.clip_boxes_numpy(self.np_anchors[np.argwhere(anchor_class_match == -1).flatten()][neg_anchor_ix], img.shape[2:])
        #
        #     for n in neg_anchors:
        #         box_results_list[b].append({'box_coords': n, 'box_type': 'neg_anchor'})
        #         # box_results_list.append({'box_coords': n, 'box_type': 'neg_anchor'})
        #
        #     batch_class_loss += class_loss / img.shape[0]
        #     batch_bbox_loss += bbox_loss / img.shape[0]
        #
        # results_dict = get_results(self.params, img.shape, detections, seg_logits, box_results_list)
        # seg_loss_dice = 1 - mutils.batch_dice(F.softmax(seg_logits, dim=1), var_seg_ohe)
        # if (seg_loss_dice > 0.80) and (results_dict['seg_preds'].max() > 0):
        #     sitk.WriteImage(sitk.GetImageFromArray(results_dict['seg_preds'][0, 0].astype(np.float)),
        #                     self.params.model_output_directory + '/hold_seg_pred.nii.gz')
        #
        #     sitk.WriteImage(sitk.GetImageFromArray(batch['data'][0, 0].astype(np.float)),
        #                     self.params.model_output_directory + '/hold.nii.gz')
        #     sitk.WriteImage(sitk.GetImageFromArray(batch['seg'][0, 0].astype(np.float)),
        #                     self.params.model_output_directory + '/hold_seg.nii.gz')
        #
        # seg_loss_ce = F.cross_entropy(seg_logits, var_seg[:, 0])
        # loss = batch_class_loss + batch_bbox_loss + (seg_loss_dice + seg_loss_ce) / 2
        # results_dict['torch_loss'] = loss
        # results_dict['monitor_values'] = {'loss': loss.item(), 'class_loss': batch_class_loss.item()}
        # results_dict['logger_string'] = \
        #     "loss: {0:.2f}, class: {1:.2f}, bbox: {2:.2f}, seg dice: {3:.3f}, seg ce: {4:.3f}, mean pix. pr.: {5:.5f}"\
        #     .format(loss.item(), batch_class_loss.item(), batch_bbox_loss.item(), seg_loss_dice.item(),
        #             seg_loss_ce.item(), np.mean(results_dict['seg_preds']))
        #
        # return results_dict



    def forward_fn(self, img):
        """
        forward pass of the model.
        :param img: input img (b, c, y, x, (z)).
        :return: rpn_pred_logits: (b, n_anchors, 2)
        :return: rpn_pred_deltas: (b, n_anchors, (y, x, (z), log(h), log(w), (log(d))))
        :return: batch_proposal_boxes: (b, n_proposals, (y1, x1, y2, x2, (z1), (z2), batch_ix)) only for monitoring/plotting.
        :return: detections: (n_final_detections, (y1, x1, y2, x2, (z1), (z2), batch_ix, pred_class_id, pred_score)
        :return: detection_masks: (n_final_detections, n_classes, y, x, (z)) raw molded masks as returned by mask-head.
        """
        # Feature extraction
        fpn_outs = self.Fpn(img)
        seg_logits = self.final_conv(fpn_outs[0])
        selected_fmaps = [fpn_outs[i + 1] for i in self.params.pyramid_levels]

        # Loop through pyramid layers
        class_layer_outputs, bb_reg_layer_outputs = [], []  # list of lists
        for p in selected_fmaps:
            class_layer_outputs.append(self.Classifier(p))
            bb_reg_layer_outputs.append(self.BBRegressor(p))

        # Concatenate layer outputs
        # Convert from list of lists of level outputs to list of lists
        # of outputs across levels.
        # e.g. [[a1, b1, c1], [a2, b2, c2]] => [[a1, a2], [b1, b2], [c1, c2]]
        class_logits = list(zip(*class_layer_outputs))
        class_logits = [torch.cat(list(o), dim=1) for o in class_logits][0]
        bb_outputs = list(zip(*bb_reg_layer_outputs))
        bb_outputs = [torch.cat(list(o), dim=1) for o in bb_outputs][0]

        # merge batch_dimension and store info in batch_ixs for re-allocation.
        # batch_ixs = torch.arange(class_logits.shape[0]).unsqueeze(1).repeat(1, class_logits.shape[1]).view(-1).cuda()
        # batch_ixs = torch.arange(class_logits.shape[0]).unsqueeze(1).repeat(1, class_logits.shape[1]).view(-1).to(self.device)
        batch_ixs = torch.arange(class_logits.shape[0]).unsqueeze(1).repeat(1, class_logits.shape[1]).view(-1).to(class_logits.device)
        flat_class_softmax = F.softmax(class_logits.view(-1, class_logits.shape[-1]), 1)
        flat_bb_outputs = bb_outputs.view(-1, bb_outputs.shape[-1])
        detections = mutils.refine_detections(self.anchors, flat_class_softmax, flat_bb_outputs, batch_ixs, self.params)

        return detections, class_logits, bb_outputs, seg_logits


    def forward_fn_rev2(self, img):
        """
        forward pass of the model.
        :param img: input img (b, c, y, x, (z)).
        :return: rpn_pred_logits: (b, n_anchors, 2)
        :return: rpn_pred_deltas: (b, n_anchors, (y, x, (z), log(h), log(w), (log(d))))
        :return: batch_proposal_boxes: (b, n_proposals, (y1, x1, y2, x2, (z1), (z2), batch_ix)) only for monitoring/plotting.
        :return: detections: (n_final_detections, (y1, x1, y2, x2, (z1), (z2), batch_ix, pred_class_id, pred_score)
        :return: detection_masks: (n_final_detections, n_classes, y, x, (z)) raw molded masks as returned by mask-head.
        """
        # Feature extraction
        fpn_outs = self.Fpn(img)
        seg_logits = self.final_conv(fpn_outs[0])


        # local_appearance_out = self.LANet(fpn_outs[0])
        # pred_heatmap = self.SCNet(local_appearance_out)

        pred_heatmap = self.LANet(fpn_outs[0], fpn_outs[1])

        # selected_fmaps = [fpn_outs[i + 1] for i in self.params.pyramid_levels]
        #
        # # Loop through pyramid layers
        # class_layer_outputs, bb_reg_layer_outputs = [], []  # list of lists
        # for p in selected_fmaps:
        #     class_layer_outputs.append(self.Classifier(p))
        #     bb_reg_layer_outputs.append(self.BBRegressor(p))
        #
        # # Concatenate layer outputs
        # # Convert from list of lists of level outputs to list of lists
        # # of outputs across levels.
        # # e.g. [[a1, b1, c1], [a2, b2, c2]] => [[a1, a2], [b1, b2], [c1, c2]]
        # class_logits = list(zip(*class_layer_outputs))
        # class_logits = [torch.cat(list(o), dim=1) for o in class_logits][0]
        # bb_outputs = list(zip(*bb_reg_layer_outputs))
        # bb_outputs = [torch.cat(list(o), dim=1) for o in bb_outputs][0]
        #
        # # merge batch_dimension and store info in batch_ixs for re-allocation.
        # # batch_ixs = torch.arange(class_logits.shape[0]).unsqueeze(1).repeat(1, class_logits.shape[1]).view(-1).cuda()
        # # batch_ixs = torch.arange(class_logits.shape[0]).unsqueeze(1).repeat(1, class_logits.shape[1]).view(-1).to(self.device)
        # batch_ixs = torch.arange(class_logits.shape[0]).unsqueeze(1).repeat(1, class_logits.shape[1]).view(-1).to(class_logits.device)
        # flat_class_softmax = F.softmax(class_logits.view(-1, class_logits.shape[-1]), 1)
        # flat_bb_outputs = bb_outputs.view(-1, bb_outputs.shape[-1])
        # detections = mutils.refine_detections(self.anchors, flat_class_softmax, flat_bb_outputs, batch_ixs, self.params)

        # return detections, class_logits, bb_outputs, seg_logits
        return seg_logits, pred_heatmap


    def train_forward_rev2(self, batch, **kwargs):
        """
        train method (also used for validation monitoring). wrapper around forward pass of network. prepares input data
        for processing, computes losses, and stores outputs in a dictionary.
        :param batch: dictionary containing 'data', 'seg', etc.
        :return: results_dict: dictionary with keys:
                'boxes': list over batch elements. each batch element is a list of boxes. each box is a dictionary:
                        [[{box_0}, ... {box_n}], [{box_0}, ... {box_n}], ...]
                'seg_preds': pixelwise segmentation output (b, c, y, x, (z)) with values [0, .., n_classes].
                'monitor_values': dict of values to be monitored.
        """

        # batch = self.seg_to_bbox(batch)

        img = batch['data']
        # gt_class_ids = batch['roi_labels']
        # gt_boxes = batch['bb_target']

        target_landmarks = batch['centroid_coord']

        sigma_scale = 1000.0
        # heatmap_sigma = 4.0
        normalize = True


        # sigma_scale = 1.0
        # heatmap_sigma = 6.0
        num_landmarks = self.params.num_classes - 1

        # sigmas = torch.full((num_landmarks,), fill_value=heatmap_sigma).to(target_landmarks)

        heatmap_target = get_target_heatmap(self.params.sample_size, target_landmarks,
                                            sigmas=self.heatmap_sigma, scale=sigma_scale, normalize=normalize)


        # img_hold = img.detach().cpu().numpy()[0, 0]
        # heatmap_hold = heatmap_target.detach().cpu().numpy()
        # sitk.WriteImage(sitk.GetImageFromArray(img_hold), 'hold_img.nii.gz')
        # sitk.WriteImage(sitk.GetImageFromArray(heatmap_hold[0, -3]), 'hold_heat.nii.gz')

        # var_seg_ohe = mutils.get_one_hot_encoding(batch['seg'].cpu().numpy(), self.params.num_seg_classes)
        #
        # if isinstance(var_seg_ohe, np.ndarray):
        #     # var_seg_ohe = torch.FloatTensor(var_seg_ohe).cuda()
        #     var_seg_ohe = torch.from_numpy(var_seg_ohe.astype(np.float32)).to(img.device)
        # else:
        #     # var_seg_ohe = var_seg_ohe.cuda()
        #     # var_seg_ohe = var_seg_ohe.to(self.device)
        #     var_seg_ohe = var_seg_ohe.to(img.device)



        # var_seg_ohe = torch.FloatTensor(mutils.get_one_hot_encoding(batch['seg'], self.cf.num_seg_classes)).cuda()


        if isinstance(batch['seg'], np.ndarray):
            # var_seg = torch.from_numpy(batch['seg']).type(torch.long).cuda()
            var_seg = torch.from_numpy(batch['seg']).type(torch.float).to(img.device)
        else:
            # var_seg = batch['seg'].type(torch.long).cuda()
            var_seg = batch['seg'].to(img.device)

        if isinstance(img, np.ndarray):
            # img = torch.from_numpy(img).float().cuda()
            img = torch.from_numpy(img).float().cuda()
        else:
            img = img.cuda()
            # img = img.to(self.device)


        # batch_class_loss = torch.tensor([0], dtype=torch.float32).to(img.device)
        # batch_bbox_loss = torch.tensor([0], dtype=torch.float32).to(img.device)


        # # list of output boxes for monitoring/plotting. each element is a list of boxes per batch element.
        # box_results_list = [[] for _ in range(img.shape[0])]
        # detections, class_logits, pred_deltas, seg_logits = self.forward_fn(img)
        seg_logits, heatmap_pred = self.forward_fn_rev2(img)

        anchor_class_match_torch = []
        anchor_target_deltas_torch = []

        # loop over batch
        # for b in range(img.shape[0]):
        #
        #     # roi_labels_hold, bb_target_hold, roi_masks_hold = mutils.remove_padding(batch['roi_labels'][b].cpu().numpy(),
        #     #                                                                         batch['bb_target'][b].cpu().numpy(),
        #     #                                                                         batch['roi_masks'][b].cpu().numpy())
        #
        #     # roi_labels, bb_target, roi_masks = mutils_torch.remove_padding(batch['roi_labels'][b],
        #     #                                                                batch['bb_target'][b],
        #     #                                                                batch['roi_masks'][b])
        #
        #     gt_boxes = batch['bb_target'][b]
        #     gt_class_ids = batch['roi_labels'][b]
        #
        #     # gt_boxes_hold = bb_target_hold
        #     # gt_class_ids_hold = roi_labels_hold
        #
        #     if len(gt_boxes) > 0:
        #         # match gt boxes with anchors to generate targets.
        #
        #         anchor_class_match, anchor_target_deltas = mutils_torch.gt_anchor_matching(self.params, self.anchors, gt_boxes, gt_class_ids)
        #
        #
        #         # anchor_class_match, anchor_target_deltas = mutils.gt_anchor_matching(self.params, self.np_anchors,
        #         #                                                                      gt_boxes_hold, gt_class_ids_hold)
        #
        #     else:
        #         anchor_class_match = np.array([-1] * self.np_anchors.shape[0])
        #         anchor_target_deltas = np.array([0])
        #
        #     # anchor_class_match_torch.append(torch.from_numpy(anchor_class_match).to(img.device))
        #     # anchor_target_deltas_torch.append(torch.from_numpy(anchor_target_deltas).float().to(img.device))
        #
        #     # anchor_class_match_np.append(anchor_class_match)
        #     # compute losses.
        #     # class_loss, neg_anchor_ix = compute_class_loss(anchor_class_match, class_logits[b])
        #     class_loss = compute_class_loss(anchor_class_match, class_logits[b])
        #     bbox_loss = compute_bbox_loss(anchor_target_deltas, pred_deltas[b], anchor_class_match)
        #
        #
        #     batch_class_loss += class_loss / img.shape[0]
        #     batch_bbox_loss += bbox_loss / img.shape[0]


        # seg_preds = F.softmax(seg_logits, 1).argmax(1)[:, np.newaxis]

        binary = False
        if seg_logits.shape[1] > 1:
            seg_preds = F.softmax(seg_logits, dim=1)[:, 1:]
        else:
            # seg_pred = seg_logits
            seg_preds = torch.sigmoid(seg_logits)

            binary = True


        # seg_loss_dice = 1 - mutils.batch_dice(F.softmax(seg_logits, dim=1), var_seg_ohe)

        if binary == True:
            # seg_loss_ce = F.binary_cross_entropy(seg_logits, var_seg)
            seg_loss_ce = F.binary_cross_entropy_with_logits(seg_logits, var_seg)
        else:
            seg_loss_ce = F.cross_entropy(seg_logits, var_seg[:, 0])

        heatmap_loss = F.mse_loss(heatmap_pred, heatmap_target)  # * torch.prod(torch.tensor(heatmap_target.shape, device=heatmap_target.device))


        sigma_valid = torch.mean((self.heatmap_sigma * target_landmarks[0, :, 0]) ** 2)

        sigma_loss = self.sigma_regularization * sigma_valid

        # if var_seg.shape[1] > 1:
        #     seg_loss_ce = F.cross_entropy(seg_logits, var_seg[:, 0])
        # else:
        #     seg_loss_ce = F.binary_cross_entropy_with_logits(seg_logits[:, 1], var_seg[:, 0].to(torch.float))


        # loss = batch_class_loss + batch_bbox_loss + (seg_loss_dice + seg_loss_ce) / 2

        # loss = batch_class_loss + batch_bbox_loss + seg_loss_ce

        # non_zero_channels

        # loss = seg_loss_ce + heatmap_loss
        # loss = seg_loss_ce + heatmap_loss + sigma_loss
        loss = heatmap_loss + sigma_loss

        # loss = seg_loss_ce

        # if (seg_loss_dice > 0.80):
        #
        #     for batch_idx in range(self.params.batch_size):
        #         if seg_preds[batch_idx].max() > 0:
        #
        #             sitk.WriteImage(sitk.GetImageFromArray(seg_preds.detach().cpu().numpy()[batch_idx, 0].astype(np.float)),
        #                             self.params.model_output_directory + '/hold_seg_pred.nii.gz')
        #
        #             sitk.WriteImage(sitk.GetImageFromArray(batch['data'].detach().cpu().numpy()[batch_idx, 0].astype(np.float)),
        #                             self.params.model_output_directory + f'/hold_{batch_idx}.nii.gz')
        #             sitk.WriteImage(sitk.GetImageFromArray(batch['seg'].detach().cpu().numpy()[batch_idx, 0].astype(np.float)),
        #                             self.params.model_output_directory + f'/hold_seg_{batch_idx}.nii.gz')


        return loss, seg_preds, heatmap_pred, heatmap_target
        # return loss.unsqueeze(0)



    def test_forward(self, batch, **kwargs):
        """
        test method. wrapper around forward pass of network without usage of any ground truth information.
        prepares input data for processing and stores outputs in a dictionary.
        :param batch: dictionary containing 'data'
        :return: results_dict: dictionary with keys:
               'boxes': list over batch elements. each batch element is a list of boxes. each box is a dictionary:
                       [[{box_0}, ... {box_n}], [{box_0}, ... {box_n}], ...]
               'seg_preds': pixel-wise class predictions (b, 1, y, x, (z)) with values [0, ..., n_classes] for
                            retina_unet and dummy array for retina_net.
        """
        img = batch['data']
        if isinstance(img, np.ndarray):
            img = torch.from_numpy(img).float().cuda()
        if isinstance(img, torch.Tensor):
            img = img.cuda()
        detections, _, _, seg_logits = self.forward_fn(img)
        results_dict = get_results(self.params, img.shape, detections, seg_logits)
        return results_dict
