from Models.weight_initializer import init_weights

from Models.model_utils import *
import numpy as np

class ConvND(nn.Module):
    def __init__(self,
                 dim,
                 in_channels,
                 out_channels,
                 kernel_size=3,
                 stride=1,
                 dilation=1,
                 normalization=False,
                 batch_normalization = True,
                 norm_type='batch',
                 normalization_momentum=0.1,
                 norm_track_running_states=True,
                 norm_affine=True,
                 norm_eps=1e-5,
                 activation=None,
                 padding=None,
                 input_shape=None,
                 output_shape=None,
                 init_weights_type='kaiming'):

        super(ConvND, self).__init__()

        conv_fn = getattr(nn, f'Conv{int(dim)}d')

        self.in_channels = in_channels
        self.out_channels = out_channels

        self.input_shape = input_shape

        if output_shape is None:
            if padding == 'same':
                output_shape = input_shape / stride


        if isinstance(padding, int):
            conv = conv_fn(in_channels=in_channels, out_channels=out_channels,
                           padding=padding, kernel_size=kernel_size, stride=stride, dilation=dilation)
            padding = None

        else:
            conv = conv_fn(in_channels=in_channels, out_channels=out_channels,
                             kernel_size=kernel_size, stride=stride, dilation=dilation)

        if padding is not None:
            pad_layer = PadTensor(padding, dim=3, input_shape=input_shape, operation_type='down_conv',
                                  kernel_size=kernel_size, stride=stride, dilation=dilation,
                                  output_shape=output_shape)

            # Should be pad then conv. Differences are only for compatibility with older models.
            conv_block = [pad_layer, conv]

        else:
            conv_block = [conv]


        if normalization:
            bn = NormalizationLayer(dim=dim, num_features=out_channels, norm_type=norm_type,
                                    momentum=normalization_momentum,
                                    track_running_stats=norm_track_running_states,
                                    affine=norm_affine,
                                    eps=norm_eps,
                                    )

            conv_block = conv_block + [bn]

        if activation:
            act = ActivationLayer(activation_type=activation)
            conv_block = conv_block + [act]

        self.conv_block = nn.Sequential(*conv_block)

    def forward(self, x):
        x = self.conv_block(x)
        return x



class Conv2D(nn.Module):
    def __init__(self, **kwargs):
        super(Conv2D, self).__init__()
        dim = 2
        self.conv_fn = ConvND(dim, **kwargs)
    def forward(self, x):
        return self.conv_fn(x)


class Conv3D(nn.Module):
    def __init__(self, **kwargs):
        super(Conv3D, self).__init__()
        dim = 3
        self.conv_fn = ConvND(dim, **kwargs)
    def forward(self, x):
        return self.conv_fn(x)



# class Conv2D(nn.Module):
#     def __init__(self,
#                  in_channels,
#                  out_channels,
#                  kernel_size=3,
#                  stride=1,
#                  dilation=1,
#                  normalization=False,
#                  norm_type='batch',
#                  normalization_momentum=0.1,
#                  norm_track_running_states=True,
#                  norm_affine=True,
#                  norm_eps=1e-5,
#                  activation=None,
#                  padding=None,
#                  input_shape=None,
#                  output_shape=None,
#                  init_weights_type='kaiming'):
#         """
#         This is a simple conv3D layer. No normalization or activation.
#         :param in_channels: tuple; Number of channels for input feature map or image.
#         :param out_channels: tuple; Number of channels for output feature map or image.
#         :param kernel_size: int or tuple; Size of the convolution kernel.
#                             If int, all dimensions of the kernel will be the same.
#         """
#         super(Conv2D, self).__init__()
#         self.in_channels = in_channels
#         self.out_channels = out_channels
#
#         self.input_shape = input_shape
#
#         if output_shape is None:
#             if padding == 'same':
#                 output_shape = input_shape / stride
#
#         # self.in_channels = in_channels
#         # self.out_channels = out_channels
#         # self.kernel_size = kernel_size
#         # self.stride = stride
#         # self.dilation = dilation
#         # self.batch_normalization = batch_normalization
#         # self.instance_normalization = instance_normalization
#         # self.normalization_momentum = normalization_momentum
#         # self.norm_track_running_states = norm_track_running_states
#         # self.activation = activation
#         # self.padding = padding
#         # self.input_shape = input_shape,
#         # self.output_shape = output_shape
#         # self.init_weights_type = init_weights_type
#
#         # pad_amount = PadTensorFunctional(padding=padding, output_shape=input_shape,
#         #                                  conv_type='down_conv', kernel_size=kernel_size,
#         #                                  stride=stride, dilation=dilation)
#
#         conv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels,
#                          kernel_size=kernel_size, stride=stride, dilation=dilation)
#
#         self.conv_hold = nn.Conv2d(in_channels=in_channels, out_channels=out_channels,
#                                    kernel_size=kernel_size, stride=stride, dilation=dilation)
#
#         if padding is not None:
#         # if (input_shape is not None) and (padding is not None):
#
#             pad_layer = PadTensor(padding, dim=2, operation_type='down_conv',
#                                   kernel_size=kernel_size, stride=stride, dilation=dilation,
#                                   input_shape=input_shape,
#                                   output_shape=output_shape)
#
#             # Should be pad then conv. Differences are only for compatibility with older models.
#             conv_block = [pad_layer, conv]
#         else:
#             conv_block = [conv]
#
#
#         if normalization:
#             bn = NormalizationLayer(dim=2, num_features=out_channels, norm_type=norm_type,
#                                     momentum=normalization_momentum,
#                                     track_running_stats=norm_track_running_states,
#                                     affine=norm_affine,
#                                     eps=norm_eps,
#                                     )
#
#             conv_block = conv_block + [bn]
#
#
#         if activation:
#             act = ActivationLayer(activation_type=activation)
#             conv_block = conv_block + [act]
#
#         self.conv_block = nn.Sequential(*conv_block)
#
#         # # initialise the blocks
#         # for m in self.children():
#         #     init_weights(m, init_type=init_weights_type)
#
#     def forward(self, x):
#         x = self.conv_block(x)
#         return x
#
#
# class Conv3D(nn.Module):
#     def __init__(self,
#                  in_channels,
#                  out_channels,
#                  kernel_size=3,
#                  stride=1,
#                  dilation=1,
#                  normalization=False,
#                  norm_type='batch',
#                  normalization_momentum=0.1,
#                  norm_track_running_states=True,
#                  norm_affine=True,
#                  norm_eps=1e-5,
#                  activation=None,
#                  padding=None,
#                  input_shape=None,
#                  output_shape=None,
#                  init_weights_type='kaiming'):
#         """
#         This is a simple conv3D layer. No normalization or activation.
#         :param in_channels: tuple; Number of channels for input feature map or image.
#         :param out_channels: tuple; Number of channels for output feature map or image.
#         :param kernel_size: int or tuple; Size of the convolution kernel.
#                             If int, all dimensions of the kernel will be the same.
#         """
#         super(Conv3D, self).__init__()
#         self.in_channels = in_channels
#         self.out_channels = out_channels
#
#         self.input_shape = input_shape
#
#         if output_shape is None:
#             if padding == 'same':
#                 output_shape = input_shape / stride
#         # pad_amount = PadTensorFunctional(padding=padding, output_shape=input_shape,
#         #                                  conv_type='down_conv', kernel_size=kernel_size,
#         #                                  stride=stride, dilation=dilation)
#
#
#         if isinstance(padding, int):
#             conv = nn.Conv3d(in_channels=in_channels, out_channels=out_channels,
#                              padding=padding, kernel_size=kernel_size, stride=stride, dilation=dilation)
#             padding = None
#
#         else:
#             conv = nn.Conv3d(in_channels=in_channels, out_channels=out_channels,
#                              kernel_size=kernel_size, stride=stride, dilation=dilation)
#
#         if padding is not None:
#         # if (input_shape is not None) and (padding is not None):
#             pad_layer = PadTensor(padding, dim=3, input_shape=input_shape, operation_type='down_conv',
#                                   kernel_size=kernel_size, stride=stride, dilation=dilation,
#                                   output_shape=output_shape)
#
#             # Should be pad then conv. Differences are only for compatibility with older models.
#             conv_block = [pad_layer, conv]
#
#         else:
#             conv_block = [conv]
#
#
#         if normalization:
#             bn = NormalizationLayer(dim=3, num_features=out_channels, norm_type=norm_type,
#                                     momentum=normalization_momentum,
#                                     track_running_stats=norm_track_running_states,
#                                     affine=norm_affine,
#                                     eps=norm_eps,
#                                     )
#
#             conv_block = conv_block + [bn]
#
#
#
#         # if (batch_normalization is True) and (instance_normalization is True):
#         #     Warning('Both batch normalization and instance normalization are set to True. As both cannot be used, '
#         #             'will result in default configuration and use batch normalization')
#         #     instance_normalization = False
#         #
#         # if batch_normalization:
#         #     bn = batch_normalization_layer(out_channels,
#         #                                    dim=3,
#         #                                    momentum=normalization_momentum,
#         #                                    track_running_stats=norm_track_running_states,
#         #                                    affine=norm_affine,
#         #                                    eps=norm_eps,
#         #                                    )
#         #     conv_block = conv_block + [bn]
#         #
#         # if instance_normalization:
#         #     instnorm = instance_normalization_layer(out_channels,
#         #                                             dim=3,
#         #                                             momentum=normalization_momentum,
#         #                                             track_running_stats=norm_track_running_states,
#         #                                             affine=norm_affine,
#         #                                             eps=norm_eps,
#         #                                             )
#         #     conv_block = conv_block + [instnorm]
#
#         if activation:
#             act = ActivationLayer(activation_type=activation)
#             conv_block = conv_block + [act]
#
#         self.conv_block = nn.Sequential(*conv_block)
#
#
#         # # initialise the blocks
#         # for m in self.children():
#         #     init_weights(m, init_type=init_weights_type)
#
#     def forward(self, x):
#         x = self.conv_block(x)
#         return x


class UpConv3D(nn.Module):
    def __init__(self,
                 in_channels,
                 out_channels,
                 kernel_size=2,
                 stride=2,
                 dilation=1,
                 normalization=False,
                 batch_normalization=True,
                 norm_type='batch',
                 normalization_momentum=0.1,
                 norm_track_running_states=True,
                 norm_affine=True,
                 norm_eps=1e-5,
                 activation=None,
                 padding=None,
                 input_shape=None,
                 output_shape=None,
                 init_weights_type='kaiming'):
    # def __init__(self, input_shape, in_channels, out_channels, kernel_size=2, stride=2, dilation=1, padding=0):
        """
        This is a simple up-conv3D layer. No normalization or activation.
        :param in_channels: tuple; Number of channels for input feature map or image.
        :param out_channels: tuple; Number of channels for output feature map or image.
        :param kernel_size: int or tuple; Size of the convolutional kernel.
                            If int, all dimensions of the kernel will be the same.
        """
        super(UpConv3D, self).__init__()

        up_conv = nn.ConvTranspose3d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                                     stride=stride, dilation=dilation)

        if (input_shape is not None) and (padding is not None):
            pad_layer = PadTensor(padding, input_shape=input_shape, operation_type='up_conv',
                                  kernel_size=kernel_size, stride=stride, dilation=dilation,
                                  output_shape=output_shape)

            # Should be pad then conv. Differences are only for compatibility with older models.
            up_conv = [pad_layer, up_conv]
        else:
            up_conv = [up_conv]


        if normalization:
            bn = NormalizationLayer(dim=3, num_features=out_channels, norm_type=norm_type,
                                    momentum=normalization_momentum,
                                    track_running_stats=norm_track_running_states,
                                    affine=norm_affine,
                                    eps=norm_eps,
                                    )

            conv_block = up_conv + [bn]




        # if (batch_normalization is True) and (instance_normalization is True):
        #     Warning('Both batch normalization and instance normalization are set to True. As both cannot be used, '
        #             'will result in default configuration and use batch normalization')
        #     instance_normalization = False
        #
        # if batch_normalization:
        #     bn = batch_normalization_layer(out_channels,
        #                                    dim=3,
        #                                    momentum=normalization_momentum,
        #                                    track_running_stats=norm_track_running_states,
        #                                    affine=norm_affine,
        #                                    eps=norm_eps,
        #                                    )
        #     up_conv = up_conv + [bn]
        #
        # if instance_normalization:
        #     instnorm = instance_normalization_layer(out_channels,
        #                                             dim=3,
        #                                             momentum=normalization_momentum,
        #                                             track_running_stats=norm_track_running_states,
        #                                             affine=norm_affine,
        #                                             eps=norm_eps,
        #                                             )
        #     up_conv = up_conv + [instnorm]

        if activation:
            act = ActivationLayer(activation_type=activation)
            up_conv = up_conv + [act]

        self.up_conv = nn.Sequential(*up_conv)

        # # initialise the blocks
        # for m in self.children():
        #     init_weights(m, init_type=init_weights_type)

    def forward(self, x):
        x = self.up_conv(x)
        return x


class NormalizationLayer(nn.Module):
    def __init__(self, num_features, dim, norm_type='batch', momentum=0.1,
                 track_running_stats=None,
                 affine=None,
                 eps=1e-5):
        """
        Parameters
        ----------
        num_features : int
            Number of channels for the input for normalization.
        dim : int
            The dimension of the tensor to be normalization.
        norm_type : str
            The type of normalization to perform.
        momentum : float = 0.1
            Momentum of the normalization parameters.
        track_running_stats: bool = True
            Booling to determine if the std and mean should be tracked
        affine : bool = True
            If normalization is learnable
        eps : float = 1e-5
            Value added to the denominator for numerical stability
        """

        super(NormalizationLayer, self).__init__()

        if track_running_stats is None:
            track_running_stats = dict(batch=True, instance=False)

        if norm_type == 'batch':
            norm_fn = getattr(nn, f'BatchNorm{int(dim)}d')

        elif norm_type == 'instance':
            norm_fn = getattr(nn, f'InstanceNorm{int(dim)}d')

        else:
            raise ValueError('Proper norm_type argument required. Options are "batch" or "instance".')


        if track_running_stats is None:
            track_running_stats = True if norm_type == 'batch' else False

        if affine is None:
            affine = True if norm_type == 'batch' else False


        self.norm = norm_fn(num_features=num_features,
                            momentum=momentum,
                            track_running_stats=track_running_stats, affine=affine, eps=eps)

    def forward(self, x):
        x = self.norm(x)
        return x

class batch_normalization_layer(nn.Module):
    def __init__(self, num_features, dim, momentum=0.1, track_running_stats=True, affine=True, eps=1e-5):
        """
        Batch normalization layer.
        :param num_features: int or tuple; Number of channels for the input for normalization.
                            If tuple, the assumed shape will be (N, C, D, H, W) and C will be extracted.
        :param momentum: int; Momentum used for batch normalization calculation.
                        Default is 0.99 as it is the default from Keras
        """
        super(batch_normalization_layer, self).__init__()

        # Check to ensure that num_features is not a tuple. If it is a tuple, extract C from it.
        # Tuple would be of shape (N, C, D, H, W).

        if isinstance(num_features, int):
            num_features = num_features
        elif len(num_features) == 5:
            num_features = num_features[1]

        if dim == 3:
            self.bn = nn.BatchNorm3d(num_features=num_features,
                                     momentum=momentum,
                                     track_running_stats=track_running_stats, affine=affine, eps=eps)
        elif dim == 2:
            self.bn = nn.BatchNorm2d(num_features=num_features,
                                     momentum=momentum,
                                     track_running_stats=track_running_stats, affine=affine, eps=eps)
        elif dim == 1:
            self.bn = nn.BatchNorm1d(num_features=num_features,
                                     momentum=momentum,
                                     track_running_stats=track_running_stats, affine=affine, eps=eps)
        else:
            raise ValueError('Dimension required for batch normalization.')

    def forward(self, x):
        x = self.bn(x)
        return x


class instance_normalization_layer(nn.Module):
    def __init__(self, num_features, dim, momentum=0.1, track_running_stats=False, affine=False, eps=1e-5):
        """
        Instance normalization layer.
        :param num_features: int or tuple; Number of channels for the input for normalization.
                            If tuple, the assumed shape will be (N, C, D, H, W) and C will be extracted.
        :param momentum: int; Momentum used for batch normalization calculation.
                        Default is 0.99 as it is the default from Keras
        """
        super(instance_normalization_layer, self).__init__()

        # Check to ensure that num_features is not a tuple. If it is a tuple, extract C from it.
        # Tuple would be of shape (N, C, D, H, W).

        if isinstance(num_features, int):
            num_features = num_features
        elif len(num_features) == 5:
            num_features = num_features[1]


        if dim == 3:
            self.instnorm = nn.InstanceNorm3d(num_features=num_features,
                                              momentum=momentum,
                                              track_running_stats=track_running_stats, affine=affine, eps=eps)
        elif dim == 2:
            self.instnorm = nn.InstanceNorm2d(num_features=num_features,
                                              momentum=momentum,
                                              track_running_stats=track_running_stats, affine=affine, eps=eps)
        elif dim == 1:
            self.instnorm = nn.InstanceNorm1d(num_features=num_features,
                                              momentum=momentum,
                                              track_running_stats=track_running_stats, affine=affine, eps=eps)
        else:
            raise ValueError('Dimension required for batch normalization.')


    def forward(self, x):
        x = self.instnorm(x)
        return x


class ActivationLayer(nn.Module):
    def __init__(self, activation_type, **kwargs):
        """
        Layer used for activations
        :param activation_type: string; Name of activation for to be selected.
                                        Potential options are "relu", "sigmoid", and "softmax", with more to be added.
                                        Selection is not case sensitive.
        :param kwargs: dict; Additional parameters, if any, for selected activation.
        """
        super(ActivationLayer, self).__init__()

        assert isinstance(activation_type, str), 'Activation must be of type string.'

        activation_type = activation_type.lower()
        #
        # self.hold = None
        # if activation_type == 'softmax':
        #     self.hold = 1

        # For linear activation, the output is the identity as the previous layer performs the linear function.
        # Linear activation is the same as no activation.
        activation_dict = {'relu': nn.ReLU(inplace=True),
                           'leakyrelu': nn.LeakyReLU(negative_slope=0.1, inplace=True),
                           'sigmoid': nn.Sigmoid(),
                           'softmax': nn.Softmax(**kwargs),
                           'logsoftmax': nn.LogSoftmax(**kwargs),
                           'tanh': nn.Tanh(),
                           'none': nn.Identity()}


        self.activation = activation_dict[activation_type]
    def forward(self, x):
        x = self.activation(x)
        return x


