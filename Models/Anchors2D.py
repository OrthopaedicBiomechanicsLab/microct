import numpy as np
import math
import Models.utils as utils

def __generate_anchors_per_scale(scales, ratios, shape, feature_stride, anchor_stride):
    """
    scales: 1D array of anchor sizes in pixels. Example: [32, 64, 128]
    ratios: 1D array of anchor ratios of width/height. Example: [0.5, 1, 2]
    shape: [height, width] spatial shape of the feature map over which
            to generate anchors.
    feature_stride: Stride of the feature map relative to the image in pixels.
    anchor_stride: Stride of anchors on the feature map. For example, if the
        value is 2 then generate anchors for every other feature map pixel.
    """
    # Get all combinations of scales and ratios
    scales, ratios = np.meshgrid(np.array(scales), np.array(ratios))
    scales = scales.flatten()
    ratios = ratios.flatten()


    # Enumerate heights and widths from scales and ratios
    heights = scales * np.sqrt(ratios)
    widths = scales / np.sqrt(ratios)


    # Enumerate shifts in feature space
    shifts_x = np.arange(0, shape[0], anchor_stride) * feature_stride
    shifts_y = np.arange(0, shape[1], anchor_stride) * feature_stride
    shifts_x, shifts_y = np.meshgrid(shifts_x, shifts_y)

    # Enumerate combinations of shifts, widths, and heights
    box_widths, box_centers_x = np.meshgrid(widths, shifts_x)
    box_heights, box_centers_y = np.meshgrid(heights, shifts_y)

    # Reshape to get a list of (x, y) and a list of (w, h)
    box_centers = np.stack([box_centers_x, box_centers_y], axis=2).reshape([-1, 2])

    box_sizes = np.stack([box_widths, box_heights], axis=2).reshape([-1, 2])

    # Convert to corner coordinates (x1, y1, x2, y2)
    boxes = np.concatenate([box_centers - 0.5 * box_sizes,
                            box_centers + 0.5 * box_sizes], axis=1)
    return boxes



def __generate_pyramid_anchors(scales, ratios, feature_shapes, feature_strides,
                               anchor_stride):
    """Generate anchors at different levels of a feature pyramid. Each scale
    is associated with a level of the pyramid, but each ratio is used in
    all levels of the pyramid.
    Returns:
    anchors: [N, (x1, y1, x2, y2)]. All generated anchors in one array. Sorted
        with the same order of the given scales. So, anchors of scale[0] come
        first, then anchors of scale[1], and so on.
    """
    # Anchors
    # [anchor_count, (x1, y1, x2, y2)]
    anchors = []
    for i in range(len(scales)):

        anchors_hold = __generate_anchors_per_scale(scales[i], ratios, feature_shapes[i],
                                                    feature_strides[i], anchor_stride)
        anchors.append(anchors_hold)
    return np.concatenate(anchors, axis=0)

def get_anchors(params, normalize=True):

    """Returns anchor pyramid for the given image size."""

    image_shape = params.sample_size
    backbone_shapes = utils.compute_backbone_shapes(params, image_shape)

    _anchor_cache = {}

    _anchor_hold = {}

    if not tuple(image_shape) in _anchor_cache:

        anchors = __generate_pyramid_anchors(
            params.anchor_scales,
            params.anchor_ratios,
            backbone_shapes,
            params.backbone_strides,
            params.anchor_strides)

        _anchor_cache[tuple(image_shape)] = anchors

    anchors = _anchor_cache[tuple(image_shape)]

    if normalize:
        anchors = utils.normalize_coordinates2D(anchors, params)

    return anchors


if __name__ == '__main__':

    image_shape = np.array([512, 512, 3])

    BACKBONE_STRIDES = [4, 8, 16, 32, 64]

    backbone_shapes = np.array([
        [int(math.ceil(image_shape[0] / stride)), int(math.ceil(image_shape[1] / stride))]
        for stride in BACKBONE_STRIDES])

    a = generate_pyramid_anchors(
        (32, 64, 128, 256, 512),
        [0.5, 1, 2],
        backbone_shapes,
        BACKBONE_STRIDES,
        1)

    # anchors = np.broadcast_to(a, (2,) + a.shape)