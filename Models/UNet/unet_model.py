from ..BaseModel import BaseModel
from .unet_base import UNet
from LossMetricFunctions import BaseLossMetric
from LossMetricFunctions.loss_metric_utils import *
import importlib
import os
import pandas as pd
from torch.cuda.amp import autocast, GradScaler

class UNetModel(BaseModel):
    def __init__(self, params, logger=None):
        super(UNetModel, self).__init__(params, logger)


        # BaseModel.__init__(self, params, logger)

        self.model_base_fn = UNet
        self.load_model_from_params()
        #
        # self.net = UNet(params)


        # print(sum(p.numel() for p in self.model.parameters()))

        # # Setup loss functions
        # loss_fn_list = []
        # for loss_name, loss_args in self.loss_fn:
        #     loss_fn_list.append(getattr(loss_metric_utils, loss_name)(loss_args))
        # self.loss = loss_fn_list
        #
        # metric_fn_dict = {}
        # if self.metric_dict is not None:
        #     for metric_name, metric_args in self.metric_dict:
        #         metric_fn_dict[metric_name] = getattr(loss_metric_utils, metric_name)(metric_args)

        if self.isTrain:
            optim_fn = self.setup_optim(params.optimizer)
            self.optimizer = optim_fn(self.net.parameters(), **params.opt_args)
            self.setup_lr_scheduler(params)

        # self.loss, self.loss_names = self.loss_metric_name_mapping(params.loss_dict)
        # self.metrics, self.metric_names = self.loss_metric_name_mapping(params.metric_dict)

        self.df_columns = ['spine_sample', 'dsc']

        self.during_training_df = pd.DataFrame(columns=self.df_columns)


        self.scaler = GradScaler()

    def set_input(self, input, gpu=None):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.
        Parameters:
            input (dict): include the data itself and its metadata information.
        The option 'direction' can be used to swap images in domain A and domain B.
        """

        if gpu is None:

            if len(input.items()) == 1:
                self.image = input['image'].to(self.device)
            else:
                self.image = input['image'].to(self.device)
                self.label = input['label'].to(self.device)


        else:
            if isinstance(gpu, float) or isinstance(gpu, int):
                gpu = 'cuda:{}'.format(gpu)

            if len(input.items()) == 1:
                self.image_np = input['image']
                self.image = input['image'].to(gpu)
            else:
                self.image_np = input['image']
                self.image = input['image'].to(gpu)
                self.label = input['label'].to(gpu)


    def forward(self):

        with autocast():
            self.pred = self.net(self.image)

        if self.net.training is False:
            if self.pred.shape[1] > 1:
                self.pred = torch.softmax(self.pred, dim=1)
                self.pred = torch.argmax(self.pred, dim=1, keepdim=True)


    def backward(self):
        """Calculate UNet model loss"""
        with autocast():
            self.loss = self.calc_loss()

        # self.loss.backward()

        self.scaler.scale(self.loss).backward()

    def optimize_model(self):
        # self.forward()                   # compute prediction
        # self.set_requires_grad(self.net, True)  # enable backprop for D
        # self.optimizer.zero_grad()     # set gradients to zero
        # self.backward()                # calculate gradients
        # self.optimizer.step()          # update weights


        self.forward()                   # compute prediction
        self.set_requires_grad(self.net, True)  # enable backprop for D
        self.optimizer.zero_grad()     # set gradients to zero
        self.backward()                # calculate gradients
        self.scaler.step(self.optimizer)          # update weights

        self.scaler.update()

    def model_specific_training(self, sample, epoch):
        pass


    def reset_model_specific_training(self):
        pass

    def save_in_training_df(self):
        self.during_training_df.to_csv(self.save_df_name + '.csv', index=False)


    # def DSCLoss(self, **fn_params):
    #     return BaseLossMetric(dsc_loss, **fn_params)
    #
    # def BCELoss(self, **fn_params):
    #     return BaseLossMetric(bce_loss, **fn_params)
    #
    # def BCEWindowLoss(self, **fn_params):
    #     return BaseLossMetric(bce_loss_windowed, device=self.device, **fn_params)
    #
    # def DSC(self, **fn_params):
    #     return BaseLossMetric(dsc, **fn_params)
    #
    # def CONCURRENCY(self, **fn_params):
    #     return BaseLossMetric(concurrency, **fn_params)
    #
    # def CrossEntropyLoss(self, **fn_params):
    #
    #     return BaseLossMetric(ce_loss, **fn_params)
    #
    # def L1Loss(self, **fn_params):
    #     return BaseLossMetric(l1_loss, device=self.device, **fn_params)
    #
    # def L2Loss(self, **fn_params):
    #     return BaseLossMetric(l2_loss, device=self.device, **fn_params)
    #
    # def L12Loss(self, **fn_params):
    #     return BaseLossMetric(l12_loss, device=self.device, **fn_params)
    #
    # def SSIMLoss(self, **fn_params):
    #     return BaseLossMetric(ssim_loss(self.params, device=self.device), device=self.device, **fn_params)

    # def SSIM(self, **fn_params):
    #     return BaseLossMetric(ssim(self.params, device=self.device), device=self.device, **fn_params)


    def SagShapeLoss(self, **fn_params):
        return BaseLossMetric(sag_shape_loss, device=self.device, **fn_params)









