import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Models.model_core import *


class PropsalLayer(nn.Module):
    def __init__(self, params):

        """

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """

        super(PropsalLayer, self).__init__()

        self.proposal_count = params.post_nms_rois_training
        self.nms_threshold = params.rpn_nms_threshold
        self.params = params

    def forward(self, scores, bbox_delta, anchors):

        # Box Scores. Use the foreground class confidence. [Batch, num_rois, 1]
        # scores = x[0][:, :, 1]
        # scores = scores[:, :, 1]

        # # Bounding boxes [batch, num_rois, 4]
        # bbox = x[1]

        rpn_bbox_std_dev = torch.tensor(self.params.rpn_bbox_std_dev, dtype=torch.float32, device=scores.device)
        rpn_bbox_avg = torch.tensor(self.params.rpn_bbox_avg, dtype=torch.float32, device=scores.device)

        bbox_delta = (bbox_delta * rpn_bbox_std_dev) + rpn_bbox_avg

        # Anchors
        # anchors = x[2]

        # Improve performance by trimming to top anchors by score and doing the rest on the smaller subset.
        pre_nms_limit = min(self.params.max_nms_limit, anchors.shape[1])

        topk_output = torch.topk(scores, pre_nms_limit, dim=1, sorted=True)
        ix = topk_output[1]

        ix_stacked = torch.stack([ix] * bbox_delta.shape[-1], dim=ix.dim())

        # Collect the scores, bbox coordinates, and anchors for the top_k ones
        scores = torch.gather(scores, dim=1, index=ix)

        bbox_delta = torch.gather(bbox_delta, dim=1, index=ix_stacked)
        pre_nms_anchors = torch.gather(anchors, dim=1, index=ix_stacked)

        # Apply deltas to anchors to get refined anchors.
        # [batch, N, (x1, y1, x2, y2)]
        boxes = apply_box_deltas_graph(pre_nms_anchors, bbox_delta)

        # Clip to image boundaries. Since we're in normalized coordinates, clip to 0..1 range.
        # [batch, N, (x1, y1, x2, y2)]
        window = torch.tensor([0, 0, self.params.sample_size[0], self.params.sample_size[0]], dtype=torch.float32, device=boxes.device)
        boxes = clip_boxes_graph(boxes, window)

        # Pytorch NMS input is (x1, y1, x2, y2), whereas tensorflow is (y1, x1, y2, x2).
        # Unsure if the swapping for x's and y's will have an impact. May need to edit code for consistency.
        # Pytorch NMS does not seem as good as tensorflow's. May need to implement my own.


        proposals_list = []
        for batch_idx in range(boxes.shape[0]):

            indices = torchvision.ops.nms(boxes[batch_idx], scores[batch_idx], iou_threshold=self.nms_threshold)
            proposals = boxes[batch_idx, indices[:self.proposal_count]]

            # Pad if needed
            padding = max(self.proposal_count - proposals.shape[0], 0)
            proposals = F.pad(proposals, [0, 0, 0, padding])

            proposals_list.append(proposals)

        proposals = torch.stack(proposals_list, dim=0)

        return proposals


def apply_box_deltas_graph(boxes, deltas):
    """
    Applies the given deltas to the given boxes.
    :param boxes: [B, N, (x1, y1, x2, y2)] boxes to update
    :param deltas: [B, N, (dx, dy, log(dw), log(dh))] refinements to apply
    """
    # Convert to x, y, w, h
    width = boxes[:, :, 2] - boxes[:, :, 0]
    height = boxes[:, :, 3] - boxes[:, :, 1]
    center_x = boxes[:, :, 0] + 0.5 * width
    center_y = boxes[:, :, 1] + 0.5 * height

    # Apply deltas
    center_x = center_x + deltas[:, :, 0] * width
    center_y = center_y + deltas[:, :, 1] * height
    width = width * torch.exp(deltas[:, :, 2])
    height = height * torch.exp(deltas[:, :, 3])


    # Convert back to x1, y1, x2, y2
    x1 = center_x - 0.5 * width
    y1 = center_y - 0.5 * height
    x2 = x1 + width
    y2 = y1 + height


    result = torch.stack([x1, y1, x2, y2], dim=boxes.dim() - 1)

    return result


def clip_boxes_graph(boxes, window):
    """
    :param boxes: [N, (x1, y1, x2, y2)]
    :param window: [4] in the form x1, y1, x2, y2
    """
    # Split


    # wy1, wx1, wy2, wx2 = torch.split(window, 4)
    # y1, x1, y2, x2 = torch.split(boxes, 4, dim=1)

    wx1, wy1, wx2, wy2 = window
    x1, y1, x2, y2 = torch.chunk(boxes, 4, dim=-1)


    # Clip all values outside the window
    x1 = torch.max(torch.min(x1, wx2), wx1)
    y1 = torch.max(torch.min(y1, wy2), wy1)
    x2 = torch.max(torch.min(x2, wx2), wx1)
    y2 = torch.max(torch.min(y2, wy2), wy1)


    clipped = torch.cat([x1, y1, x2, y2], dim=-1)
    # clipped.set_shape((clipped.shape[0], 4))

    return clipped
