import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
from Models.model_core import *
import Models.utils as utils

class DetectionTargetLayer(nn.Module):
    """
      Subsamples proposals and generates target box refinement, class_ids,
        and masks for each.
        Inputs:
        :param proposals: [batch, N, (x1, y1, x2, y2)] in normalized coordinates. Might
                   be zero padded if there are not enough proposals.
        :param gt_class_ids: [batch, MAX_GT_INSTANCES] Integer class IDs.
        :param gt_boxes: [batch, MAX_GT_INSTANCES, (x1, y1, x2, y2)] in normalized coordinates.
        :param gt_masks: [batch, height, width, MAX_GT_INSTANCES] of boolean type

        Returns: Target ROIs and corresponding class IDs, bounding box shifts, and masks.

        :returns rois: [batch, TRAIN_ROIS_PER_IMAGE, (x1, y1, x2, y2)] in normalized coordinates
        :returns target_class_ids: [batch, TRAIN_ROIS_PER_IMAGE]. Integer class IDs.
        :returns target_deltas: [batch, TRAIN_ROIS_PER_IMAGE, NUM_CLASSES, (dx, dy, log(dw), log(dh), class_id)]
                Class-specific bbox refinements.
        :returns target_mask: [batch, TRAIN_ROIS_PER_IMAGE, height, width)
                     Masks cropped to bbox boundaries and resized to neural network output size.

        Note: Returned arrays might be zero padded if not enough target ROIs.
    """
    def __init__(self, params):
        """
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        """
        super(DetectionTargetLayer, self).__init__()

        self.params = params

    def forward(self, proposals, gt_class_ids, gt_boxes):

        num_batches = proposals.shape[0]

        rois_list = []
        roi_gt_class_ids_list = []
        deltas_list = []
        center_roi_gt_box_list = []
        for batch_idx in range(num_batches):
            rois, roi_gt_class_ids, deltas, center_roi_gt_box = self.detection_target(proposals[batch_idx],
                                                                                      gt_class_ids[batch_idx],
                                                                                      gt_boxes[batch_idx])

            rois_list.append(rois)
            roi_gt_class_ids_list.append(roi_gt_class_ids)
            deltas_list.append(deltas)
            center_roi_gt_box_list.append(center_roi_gt_box)

        rois = torch.stack(rois_list, dim=0)
        roi_gt_class_ids = torch.stack(roi_gt_class_ids_list, dim=0)
        deltas = torch.stack(deltas_list, dim=0)
        center_roi_gt_box = torch.stack(center_roi_gt_box_list, dim=0)

        return rois, roi_gt_class_ids, deltas, center_roi_gt_box




    def detection_target(self, proposals, gt_class_ids, gt_boxes):
        """
        Generates detection targets for one image. Subsamples proposals and
        generates target class IDs, bounding box deltas, and masks for each.
        Inputs:
        :param proposals: [N, (x1, y1, x2, y2)] in normalized coordinates. Might
                        be zero padded if there are not enough proposals.
        :param gt_class_ids: [MAX_GT_INSTANCES] int class IDs
        :param gt_boxes: [MAX_GT_INSTANCES, (x1, y1, x2, y2)] in normalized coordinates.
        :param gt_masks: [height, width, MAX_GT_INSTANCES] of boolean type.

        :returns: Target ROIs and corresponding class IDs, bounding box shifts, and masks.
        :returns rois: [TRAIN_ROIS_PER_IMAGE, (x1, y1, x2, y2)] in normalized coordinates
        :returns class_ids: [TRAIN_ROIS_PER_IMAGE]. Integer class IDs. Zero padded.
        :returns deltas: [TRAIN_ROIS_PER_IMAGE, NUM_CLASSES, (dx, dy, log(dw), log(dh))]
                Class-specific bbox refinements.
        :returns masks: [TRAIN_ROIS_PER_IMAGE, height, width). Masks cropped to bbox
               boundaries and resized to neural network output size.
        Note: Returned arrays might be zero padded if not enough target ROIs.
        """



        idx_non_zero_proposals = torch.where(torch.sum(proposals, dim=1) != 0)[0]
        proposals = proposals[idx_non_zero_proposals]

        idx_non_zero_gt_boxes = torch.where(torch.sum(gt_boxes, dim=1) != 0)[0]
        gt_boxes = gt_boxes[idx_non_zero_gt_boxes]
        gt_class_ids = gt_class_ids[idx_non_zero_gt_boxes]


        # Remove crowded boxes. Crowded boxes are situations where bounding
        # boxes surrounding multiple instances. Exclude them from training.
        # Crowded boxes have a negative class id.

        crowd_ix = torch.where(gt_class_ids < 0)[0]
        non_crowd_ix = torch.where(gt_class_ids > 0)[0]

        crowd_gt_boxes = gt_boxes[crowd_ix]

        gt_class_ids = gt_class_ids[non_crowd_ix]
        gt_boxes = gt_boxes[non_crowd_ix]

        # Compute overlaps matrix
        # Overlaps for proposals and non-crowd gt boxes [proposals, gt_boxes]
        overlaps = overlaps_graph(proposals, gt_boxes)

        # Computer overlaps for crowded gt boxes [anchors, crowds]
        crowd_overlaps = overlaps_graph(proposals, crowd_gt_boxes)

        # Check to see if overlaps are significant
        if crowd_overlaps.shape[1] > 0:
            crowd_iou_max = torch.max(crowd_overlaps, dim=-1)
            no_crowd_bool = (crowd_iou_max < 0.001)
        else:
            no_crowd_bool = None

        # Determine positive and negative ROIs

        # Determine the maximum IoU for each ground truth for all proposals.
        # ie, for each proposal, what was the maximum IoU
        roi_iou_max = torch.max(overlaps, dim=-1)

        # Determine the indices for the IoUs above the required threshold.
        positive_indices = torch.where(roi_iou_max[0] > self.params.detect_target_threshold)[0]

        # Determine negative ROIs are those with < 0.5 with every GT box. Skip crowds.
        # Currently not using crowded bounding_boxes, so not implementing with no_crowd_bool. Old code there for ref.
        negative_indices = torch.where(roi_iou_max[0] < self.params.detect_target_threshold)[0]

        # negative_indices = tf.where(tf.logical_and(roi_iou_max < self.params.detect_target_threshold, no_crowd_bool))[:, 0]


        # Initially, there may be cases where there are no positive_indices and it can screw up the algorith.
        # As such, the maximum for each class, regardless of IoU value, will be set as positive. This ensures proper
        # initial training. After enough epochs these values will have been picked-up in the above few lines, making
        # this redundant.

        # Using a for-loop to remove the indices from the negative and positive index lists.
        # For the positive index this, this was done to ensure that the same number does not appear twice, but not
        # the cleanest method.
        roi_iou_max_class_idx = torch.max(overlaps, dim=0)[1]

        # for idx in roi_iou_max_class_idx:
        #     positive_indices = positive_indices[positive_indices != idx]
        #     negative_indices = negative_indices[negative_indices != idx]
        #
        # positive_indices = torch.cat([positive_indices, roi_iou_max_class_idx], dim=0)

        # Subsample ROIs to generate a better mixture or positive and negative ROIs.
        # Aim for 33% positive, or for ROI_POSITIVE_RATIO in config
        # Positive ROIs
        # Amount of positive ROIs
        positive_count = int(self.params.train_rois_per_image * self.params.roi_pos_ratio)

        rand_positive_idx = torch.randperm(positive_indices.shape[0], dtype=positive_indices.dtype)

        positive_indices = positive_indices[rand_positive_idx[:positive_count]]

        # Re-evaluate number of positive samples to ensure correct and cast as a Tensor type
        positive_count = positive_indices.shape[0]

        # Sample negative ROIs. Add enough to maintain positive:negative ratio.
        # Ratio of negative samples required
        r = 1.0 / self.params.roi_pos_ratio

        # Amount of negative samples to maintain ratio
        negative_count = int((r * positive_count) - positive_count)

        rand_negative_idx = torch.randperm(negative_indices.shape[0], dtype=negative_indices.dtype)
        negative_indices = negative_indices[rand_negative_idx[:negative_count]]

        # Obtain the positive and negative ROIs from the sampled positive and negative indices
        positive_rois = proposals[positive_indices]
        negative_rois = proposals[negative_indices]

        # Obtain overlaps corresponding to the randomly sampled positive ROIs
        positive_overlaps = overlaps[positive_indices]

        # Determine the the indices of where there is the largest overlap between a randomly
        # sampled positive ROI proposal and the ground truth bounding box.
        # Based on the condition that positively sampled overlaps exist.
        # Potiential that none exist if no proposal IoU is about the required threshold.


        if positive_overlaps.shape[0] > 0:
            roi_gt_box_assignment = torch.argmax(positive_overlaps, dim=-1)
        else:
            roi_gt_box_assignment = torch.empty(0, dtype=torch.int64, device=positive_overlaps.device)

        # Collect the ground truth boxes and ids based on the maximum IoU for the randomly sampled positive samples.
        roi_gt_boxes = gt_boxes[roi_gt_box_assignment]
        roi_gt_class_ids = gt_class_ids[roi_gt_box_assignment]

        # Compute deltas based on the positive_rois and the gt_boxes
        deltas = utils.box_refinement_torch(positive_rois, roi_gt_boxes)
        # Normalize them based on the standard deviation. No need to subtract mean as mean is zero
        # deltas = deltas / torch.tensor(self.params.bbox_std_dev).to(deltas)
        deltas = (deltas - torch.tensor(self.params.rpn_bbox_avg).to(deltas)) / torch.tensor(self.params.bbox_std_dev).to(deltas)

        # Append negative ROIs and pad bbox deltas that are not used for negative ROIs with zeros.
        rois = torch.cat([positive_rois, negative_rois], dim=0)

        # Amount of negative and positive samples
        N = negative_rois.shape[0]
        P = max(self.params.train_rois_per_image - rois.shape[0], 0)

        rois = F.pad(rois, [0, 0, 0, P])

        roi_gt_boxes = F.pad(roi_gt_boxes, [0, 0, 0, N + P])
        roi_gt_class_ids = F.pad(roi_gt_class_ids, [0, N + P])

        deltas = F.pad(deltas, [0, 0, 0, N + P])

        center_x_gt_box = torch.div(roi_gt_boxes[:, 0] + roi_gt_boxes[:, 2], 2)
        center_y_gt_box = torch.div(roi_gt_boxes[:, 1] + roi_gt_boxes[:, 3], 2)

        center_roi_gt_box = torch.stack([center_x_gt_box, center_y_gt_box], dim=center_x_gt_box.dim())

        return [rois, roi_gt_class_ids, deltas, center_roi_gt_box]


def overlaps_graph(boxes1, boxes2):
    """
    Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (x1, y1, x2, y2)].
    """

    # Reshape and tile so every coordinate pair in box1 can be compared
    # to every coordinate pair in box2 without the need of loops.
    # Use rehape and tile since tf does not have something similar to np.repeat

    # Boxes1 and boxes2 are repeated and reshaped so
    b1 = torch.repeat_interleave(boxes1, boxes2.shape[0], dim=0)
    b2 = boxes2.repeat(boxes1.shape[0], 1)

    # b1 = tf.reshape(tf.tile(tf.expand_dims(boxes1, 1), [1, 1, tf.shape(boxes2)[0]]), [-1, 4])
    # b2 = tf.tile(boxes2, [tf.shape(boxes1)[0], 1])

    # 2. Compute intersection between boxes1 and boxes2
    b1_x1, b1_y1, b1_x2, b1_y2 = torch.chunk(b1, 4, dim=-1)
    b2_x1, b2_y1, b2_x2, b2_y2 = torch.chunk(b2, 4, dim=-1)

    y1 = torch.max(b1_y1, b2_y1)
    x1 = torch.max(b1_x1, b2_x1)

    y2 = torch.min(b1_y2, b2_y2)
    x2 = torch.min(b1_x2, b2_x2)

    zero_tensor = torch.zeros(1).to(y1)

    # The use of zero to make sure negatives are not used
    intersection = torch.max(x2 - x1, zero_tensor) * torch.max(y2 - y1, zero_tensor)

    # 3. Compute union between boxes1 and boxes2
    b1_area = (b1_x2 - b1_x1) * (b1_y2 - b1_y1)
    b2_area = (b2_x2 - b2_x1) * (b2_y2 - b2_y1)

    union = b1_area + b2_area - intersection

    # Compute intersection-over-union (IoU) and reshape to [boxes1, boxes2]
    iou = intersection / union

    # This type of shape allows for each proposals overlap with every gt bounding box
    overlaps = iou.contiguous().view((boxes1.shape[0], boxes2.shape[0]))

    return overlaps
