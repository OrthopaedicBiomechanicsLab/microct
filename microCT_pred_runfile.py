from Predictor import Predictor
import pandas as pd
import torch
import numpy as np

if __name__ == '__main__':
    np.random.seed(1)
    torch.random.manual_seed(1)


    # edit the follow parameters

    model_name = 'UNet'
    df_main = pd.read_csv('/home/hmahdi/Microct/MicroCT/microCT_osteo_data.csv')

    spine_main_path = '/home/hmahdi/Microct/MicroCT/Machine Learning/downsampled_new'
    seg_main_path = '/home/hmahdi/Microct/MicroCT/Machine Learning/downsampled_new'

    df_val = df_main.loc[df_main['cross_fold_number'] != 10 ] # use ==1 for regular validation
    

    # output directory
    pred_output_dir = '/home/hmahdi/Microct/micro_ct/'+model_name
    model_directory = 'micro_ct/'+model_name

    model_weights_filename = pred_output_dir+'/unet_best_weights_tversky_focal_ratio.pt'

    # end of essential editing 




    # no need to edit the json is automatically generated during training
    params_json = model_directory+'/training_params_'+model_name.lower()+'.json'


    predictor = Predictor(df_main=df_val,
                          input_source_dir=spine_main_path,
                          gt_source_dir=seg_main_path,
                          params_json=params_json,
                          save_predictions=True,
                          pred_output_directory=pred_output_dir,
                          gpu_ids='0',
                          cross_fold_identifier='cross_fold_number',
                          model_directory=model_directory,
                          model_weights_filename=model_weights_filename,
                          eval_cross_fold=True,
                          # params_updates=params_updates
                          )

    predictor.predict_and_evaluate()
