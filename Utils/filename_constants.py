def UNDER_SCORE():
    return '_'


def NUMPY_EXTENSION():
    return '.npy'


def TRAIN_DATA():
    return 'data_train'


def TARGET_DATA_TRAIN():
    return 'target_train'


def SAMPLE_DATA():
    return '_sample'


def GROUND_TRUTH():
    return '_gt'


def NIFTI_EXTENSION():
    return '.nii'


def DATA_BATCH_KEYWORD():
    return 'data'


def GROUND_TRUTH_BATHC_KEYWORD():
    return '_target'


def CSV_EXTENSION():
    return '.csv'

def EVALUATION():
    return '_evaluation'

def PREDICTION():
    return '_prediction'

def JSON_EXTENSION():
    return '.json'

def H5_EXTENSION():
    return '.h5'
