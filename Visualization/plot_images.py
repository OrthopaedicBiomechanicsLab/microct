import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import SimpleITK as sitk
import numpy as np
if __name__ == '__main__':

    path = 'D:\SpineFiles_rev2'

    sample_name = '163749_crop'

    spine_img = sitk.ReadImage(path + '/' + sample_name + '.nii.gz')
    seg_img = sitk.ReadImage(path + '/' + sample_name + '_seg.nii.gz')

    spine_data = sitk.GetArrayFromImage(spine_img)
    seg_data = sitk.GetArrayFromImage(seg_img)

    seg_mask = np.ma.masked_where(seg_data < 0.9, seg_data)

    slice_idx = 66
    gs = gridspec.GridSpec(1, 2)
    fig = plt.figure()

    ax1 = fig.add_subplot(gs[0, 0])
    ax1.imshow(spine_data[::-1, :, slice_idx])
    plt.title('Sag Slice')
    plt.axis('off')

    ax2 = fig.add_subplot(gs[0, 1])
    ax2.imshow(spine_data[::-1, :, slice_idx])
    ax2.imshow(seg_mask[::-1, :, slice_idx], interpolation='none', alpha=0.8, cmap='Reds_r')
    plt.axis('off')
    plt.title('Seg Pred Overlay')

    plt.savefig('seg_pred.png', bbox_inches='tight', pad_inches=0.25, dpi=800)

    plt.show()