from tqdm import tqdm
import numpy as np

class VisualizationUtils:

    def __init__(self, num_samples, total_epoch, metric_names):

        self.num_samples = num_samples
        self.total_epoch = total_epoch
        self.metric_names = metric_names


class ProgressBar:
    def __init__(self, batch_size, num_samples, total_epoch, verbose=1):
        """
        Initialize progress bar using tqdm
        Parameters
        ----------
        batch_size : int
            Number of samples per batch
        num_samples : int
            Total number of samples
        total_epoch : int
            Total number of epochs
        current_epoch : int
            The current epoch
        """
        self.batch_size = batch_size
        self.num_samples = num_samples
        self.total_epoch = total_epoch
        self.verbose = verbose
        self.pbar = None
        self.print_dict = None


    def initialize_progbar(self, current_epoch):
        self.current_epoch = current_epoch
        if self.verbose == 1:
            disable = False
        else:
            disable = True

        if self.pbar is None:
            self.pbar = tqdm(total=self.num_samples + 1,
                             desc=f'Epoch {current_epoch + 1}/{self.total_epoch}', unit='img', disable=disable)
        else:
            self.pbar.close()
            self.pbar = tqdm(total=self.num_samples + 1,
                             desc=f'Epoch {current_epoch + 1}/{self.total_epoch}', unit='img', disable=disable)

    def update_values(self, values):
        """
        Print the current loss and metric values
        Parameters
        ----------

        values : dict
            Dict for the loss and metrics
        Returns
        -------

        """
        value_names = values.keys()

        print_dict = {}  # Define a new dict so the original is not overwritten, as dict is mutable

        for name in value_names:

            print_dict[name] = np.mean(values[name]) if len(values[name]) != 0 else 0

        self.print_dict = print_dict


    def print_update(self):
        self.pbar.set_postfix(**self.print_dict)
        self.pbar.update(self.batch_size)

    def return_dict(self):
        return self.print_dict

    def close(self):
        # self.logger.info(str(self.pbar))
        if self.verbose == 1:
            self.pbar.close()

    def to_logger(self, logger):
        if self.verbose == 1:
            logger.info(str(self.pbar))
        else:
            logger.info('Epoch: ' + str(self.current_epoch) + ' | ' + str(self.pbar.postfix))

if __name__ == '__main__':

    test = {'dsc': 0,
            'val_dsc': 0}

    pbar = ProgressBar(5, 1, 1)
    pbar.initialize_progbar(1)
    pbar.update(1, test, partial_normalize='val')