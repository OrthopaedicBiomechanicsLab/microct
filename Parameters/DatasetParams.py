class DatasetParams(object):
    def __init__(self):
        pass

    def build_dataset_params(self,
                             input_source_dir=None, gt_source_dir=None, load_all_data=False,
                             sample_size=(64, 128, 128), num_classes=1,
                             single_vert_crop=False, translation_augmentation=False, translation_args=None,
                             in_frame_translation=False, num_repeats=1,
                             low_res_augmentation=False, augmentation=False,
                             shuffle=False, affine_augmentation=False, augmentation_params=None, crop_pad=False,
                             uniform_pad_crop=True, directional_pad_crop=False,
                             intensity_augmentation=None, intensity_aug_params={},
                             pre_train_method=None, pre_train_method_args={},
                             transform_aug_on_val_test=False, **kwargs):

        self.input_source_dir = input_source_dir
        self.gt_source_dir = gt_source_dir
        self.load_all_data = load_all_data

        self.sample_size = sample_size

        self.num_classes = num_classes


        self.transform_aug_on_val_test = transform_aug_on_val_test
        self.single_vert_crop = single_vert_crop


        self.augmentation = augmentation

        self.num_repeats = num_repeats

        self.in_frame_translation = in_frame_translation
        self.low_res_augmentation = low_res_augmentation

        self.shuffle = shuffle

        self.crop_pad = crop_pad
        self.uniform_pad_crop = uniform_pad_crop
        self.directional_pad_crop = directional_pad_crop

        self.intensity_augmentation = intensity_augmentation
        self.intensity_aug_params = intensity_aug_params

        self.pre_train_method = pre_train_method
        self.pre_train_method_args = pre_train_method_args

        if augmentation is True:
            self.affine_augmentation = True
        else:
            self.affine_augmentation = affine_augmentation

        self.augmentation_params = {'rot_ang': (30, 30, 30),
                                    'shear': None,
                                    'scale_factor': (0.8, 1.2),
                                    'x_flip': True,
                                    'y_flip': True,
                                    'z_flip': True,
                                    'elastic': True}

        if augmentation_params is not None:
            assert isinstance(augmentation_params, dict), 'Augmentation parameters must be in a dictionary'

            self.augmentation_params.update(augmentation_params)


        self.translation_augmentation = translation_augmentation

        self.translation_args = {'img_in_frame': True,
                                 'out_img_size': None,
                                 'center': False,
                                 'shape_scaling': 2}

        if translation_args is not None:
            assert isinstance(translation_args, dict), 'Translation parameters must be in a dictionary'

            self.translation_args.update(translation_args)



        if self.num_classes == 26:
            self.vert_encode = {
                'C1': 1,
                'C2': 2,
                'C3': 3,
                'C4': 4,
                'C5': 5,
                'C6': 6,
                'C7': 7,
                'T1': 8,
                'T2': 9,
                'T3': 10,
                'T4': 11,
                'T5': 12,
                'T6': 13,
                'T7': 14,
                'T8': 15,
                'T9': 16,
                'T10': 17,
                'T11': 18,
                'T12': 19,
                'L1': 20,
                'L2': 21,
                'L3': 22,
                'L4': 23,
                'L5': 24,
                'L6': 25}

            self.class_dict = {
                1: 'C1',
                2: 'C2',
                3: 'C3',
                4: 'C4',
                5: 'C5',
                6: 'C6',
                7: 'C7',
                8: 'T1',
                9: 'T2',
                10: 'T3',
                11: 'T4',
                12: 'T5',
                13: 'T6',
                14: 'T7',
                15: 'T8',
                16: 'T9',
                17: 'T10',
                18: 'T11',
                19: 'T12',
                20: 'L1',
                21: 'L2',
                22: 'L3',
                23: 'L4',
                24: 'L5',
                25: 'L6',
            }

        elif self.num_classes == 13:

            self.vert_encode = {
                'T6': 1,
                'T7': 2,
                'T8': 3,
                'T9': 4,
                'T10': 5,
                'T11': 6,
                'T12': 7,
                'L1': 8,
                'L2': 9,
                'L3': 10,
                'L4': 11,
                'L5': 12}

            self.class_dict = {
                1: 'T6',
                2: 'T7',
                3: 'T8',
                4: 'T9',
                5: 'T10',
                6: 'T11',
                7: 'T12',
                8: 'L1',
                9: 'L2',
                10: 'L3',
                11: 'L4',
                12: 'L5',
            }
