import numpy as np

class ModelParameters(object):

    def __init_(self):
        pass

    def build_mod_params(self, model, **kwargs):
        """
        Default model parameter tools.

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            The base training parameters to pass the model parameters to.
        model : str
            The name of the model to build. Currently, only "UNet" model is supported.

        """
        default_param_dict = {'UNet': self.__UNetParams,
                              'UNetClass': self.__UNetClassParams,
                              'ResUNet': self.__ResUNet,
                              'UNetClassifier': self.__UNetClassParams,
                              'UNetPatch': self.__UNetClassParamsPatch,
                              'UNetClassifierPatch': self.__UNetClassParamsPatch,
                              'AGUNet': self.__AGUNet,
                              'AGUNetDetect': self.__AGUNetDetect,
                              'FasterRCNN': self.__FasterRCNN,
                              'FasterRCNNTorch': self.__FasterRCNN,
                              'RetinaUNet': self.__RetinaUnet,
                              'SPNet': self.__SPNet,
                              'R2AttUNet': self.__R2AttUNet,}
        default_param_dict[model](**kwargs)

        # TODO change num_classes to be num_out_channels to distinguish output channels for segmentations and use num_classes for classification labels

    def __UNetParams(self,
                     sample_size=(64, 128, 128), depth=5, base_filter=16,
                     kernel_size=3, activation='relu',
                     normalization=False, norm_type='batch',
                     normalization_momentum=0.1, norm_track_running_states=True,
                     norm_affine=True, norm_eps=1e-5,
                     pool_size=2, stride=1, dilation=1,
                     dropout=0, dropout_down_conv=0, dropout_up_conv=0,
                     residuals=False, num_in_channels=1, num_out_channels=1, *args, **kwargs):

        self.input_shape = sample_size
        self.depth = depth
        self.base_filter = base_filter

        self.kernel_size = kernel_size
        self.normalization = normalization
        self.norm_type = norm_type
        self.normalization_momentum = normalization_momentum
        self.norm_track_running_states = norm_track_running_states
        self.norm_affine = norm_affine
        self.norm_eps = norm_eps

        self.pool_size = pool_size
        self.stride = stride
        self.dilation = dilation

        self.dropout = dropout
        self.dropout_down_conv = dropout_down_conv
        self.dropout_up_conv = dropout_up_conv

        self.residuals = residuals

        self.num_in_channels = num_in_channels
        self.num_out_channels = num_out_channels

        self.activation = activation

    def __UNetClassParams(self,
                          num_fcn_blocks=3, dense_layer_size=48, num_classes=1, **kwargs):


        self.__UNetParams(**kwargs)

        self.num_fcn_blocks = num_fcn_blocks
        self.dense_layer_size = dense_layer_size
        self.num_classes = num_classes
        self.multi_class_learning = True
        self.multi_class_loss_order = ['seg', 'class']



    def __UNetClassParamsPatch(self, **kwargs):

        self.__UNetClassParams(**kwargs)
        self.multi_class_loss_order = ['seg', 'class', 'comp']


    def __ResUNet(self,
                  num_conv=2, **kwargs):

        self.__UNetParams(**kwargs)
        self.num_conv = num_conv
        self.residuals = False

    def __AGUNet(self,
                 non_local_mode='concatenation', sub_sample_factor=(2, 2, 2),
                 feature_scale=4, align_corners=True,
                 init_weights_type='kaiming', **kwargs):

        self.__UNetParams(**kwargs)

        self.non_local_mode = non_local_mode
        self.init_weights_type = init_weights_type
        self.sub_sample_factor = sub_sample_factor
        self.feature_scale = feature_scale
        self.align_corners = align_corners



    def __AGUNetDetect(self, **kwargs):

        self.__AGUNet(**kwargs)

        self.multi_class_loss_order = ['seg', 'detect']




    def __ResNet(self,
                 sample_size=None,
                 input_shape=None,
                 kernel_size=3,
                 architecture='resnet50',
                 base_filter=64,
                 normalization=True,
                 norm_type='batch',
                 normalization_momentum=0.1,
                 norm_track_running_states=True,
                 norm_affine=True,
                 norm_eps=1e-5,
                 general_stride=1,
                 conv_block_stride=2,
                 dilation=1,
                 dropout=0,
                 num_fully_connected_features=1000,
                 classify=True,
                 return_output_layer_blocks=False, **kwargs):

        if (input_shape is None) and (sample_size is not None):
            self.input_shape = sample_size
            self.sample_size = sample_size
        elif (input_shape is not None) and (sample_size is None):
            self.input_shape = input_shape
            self.sample_size = input_shape
        elif (input_shape is not None) and (sample_size is not None):
            self.input_shape = sample_size
            self.sample_size = sample_size
        else:
            self.input_shape = input_shape
            self.sample_size = sample_size

        self.kernel_size = kernel_size

        self.normalization = normalization
        self.norm_type = norm_type

        self.norm_track_running_states = norm_track_running_states
        self.normalization_momentum = normalization_momentum
        self.norm_affine = norm_affine
        self.norm_eps = norm_eps

        self.dropout = dropout

        self.dilation = dilation

        self.general_stride = general_stride
        self.conv_block_stride = conv_block_stride

        self.num_fully_connected_features = num_fully_connected_features

        self.architecture = architecture
        self.base_filter = base_filter
        self.classify = classify
        self.return_output_layer_blocks = return_output_layer_blocks


    def __FasterRCNN(self,
                     sample_size=None,
                     input_shape=None,
                     normalization=True,
                     norm_type='batch',
                     dropout=0,
                     conv_backbone='resnet50', compute_backbone_shape=None,
                     backbone_strides=(4, 8, 16, 32, 64),
                     fpn_class_fc_layer=1024, top_down_pyramid_size=256, num_classes=13,
                     num_fpn_input_feature_maps=4,
                     anchor_scales=(32, 64, 128, 256, 512), anchor_ratios=(1 / 2, 1, 2),
                     anchor_strides=1, rpn_shared_channels=512,
                     rpn_nms_threshold=0.7, pos_iou_threshold=0.7, neg_iou_threshold=0.3, max_nms_limit=6000,
                     rpn_train_anchors_per_image=256,
                     post_nms_rois_training=2000, post_nms_rois_inference=1000,
                     train_rois_per_image=200,
                     roi_pos_ratio=0.33,

                     input_scale_factor_for_fpn=(4, 4, 8, 16, 32),

                     use_mini_mask=True, mini_mask_shape=56,
                     pool_size=7, mask_pool_size=14, mask_shape=28, max_gt_instances=100,

                     rpn_bbox_avg=None, rpn_bbox_std_dev=None, bbox_std_dev=None,
                     detect_max_instances=100, detect_min_confidence=0.7, detect_nms_threshold=0.3,
                     detect_target_threshold=0.5,
                     grad_clip_norm=5,
                     **kwargs):



        self.__ResNet(classify=False,
                      return_output_layer_blocks=True)

        if (input_shape is None) and (sample_size is not None):
            self.input_shape = sample_size
            self.sample_size = sample_size
        elif (input_shape is not None) and (sample_size is None):
            self.input_shape = input_shape
            self.sample_size = input_shape
        elif (input_shape is not None) and (sample_size is not None):
            self.input_shape = sample_size
            self.sample_size = sample_size
        else:
            self.input_shape = input_shape
            self.sample_size = sample_size

        self.normalization = normalization
        self.norm_type = norm_type
        self.dropout = dropout



        self.conv_backbone = conv_backbone

        # Only useful if you supply a callable to BACKBONE. Should compute
        # the shape of each layer of the FPN Pyramid.
        # See model.compute_backbone_shapes
        self.compute_backbone_shape = compute_backbone_shape

        # The strides of each layer of the FPN Pyramid. These values
        # are based on a Resnet101 backbone.
        self.backbone_strides = backbone_strides

        # Size of the fully-connected layers in the classification graph
        self.fpn_class_fc_layer = fpn_class_fc_layer

        # Size of the top-down layers used to build the feature pyramid
        self.top_down_pyramid_size = top_down_pyramid_size

        # The number of input feature maps for the forward FPN pass.
        # Generally this is 4, but could change if a different backend is used.
        self.num_fpn_input_feature_maps = num_fpn_input_feature_maps

        # How the input scales downwards for each input into the FPN layer from the ResNet backbone.
        # Necessary to be able to calculate the required padding for the FPN convolution layers.
        self.input_scale_factor_for_fpn = input_scale_factor_for_fpn

        # Number of classification classes (including background)
        self.num_classes = num_classes  # Override in sub-classes

        # The number of feature maps for the shared layer in the RPN
        self.rpn_shared_channels = rpn_shared_channels

        # Length of square anchor side in pixels
        self.anchor_scales = anchor_scales

        # Ratios of anchors at each cell (width/height)
        # A value of 1 represents a square anchor, and 0.5 is a wide anchor
        self.anchor_ratios = anchor_ratios

        # Anchor stride
        # If 1 then anchors are created for each cell in the backbone feature map.
        # If 2, then anchors are created for every other cell, and so on.
        self.anchor_strides = anchor_strides

        # Non-max suppression threshold to filter RPN proposals.
        # You can increase this during training to generate more propsals.
        self.rpn_nms_threshold = rpn_nms_threshold

        # Thresholds for the IoU to determine positive and negative RoIs
        self.pos_iou_threshold = pos_iou_threshold
        self.neg_iou_threshold = neg_iou_threshold

        # How many anchors per image to use for RPN training
        self.rpn_train_anchors_per_image = rpn_train_anchors_per_image

        # ROIs kept after non-maximum suppression (training and inference)
        self.post_nms_rois_training = post_nms_rois_training
        self.post_nms_rois_inference = post_nms_rois_inference

        # If enabled, resizes instance masks to a smaller size to reduce
        # memory load. Recommended when using high-resolution images.
        self.use_mini_mask = use_mini_mask
        self.mini_mask_shape = mini_mask_shape  # (height, width) of the mini-mask


        # Number of ROIs per image to feed to classifier/mask heads
        # The Mask RCNN paper uses 512 but often the RPN doesn't generate
        # enough positive proposals to fill this and keep a positive:negative
        # ratio of 1:3. You can increase the number of proposals by adjusting
        # the RPN NMS threshold.
        self.train_rois_per_image = train_rois_per_image

        # Percent of positive ROIs used to train classifier/mask heads
        self.roi_pos_ratio = roi_pos_ratio

        # Pooled ROIs
        self.pool_size = pool_size
        self.mask_pool_size = mask_pool_size

        # Shape of output mask
        # To change this you also need to change the neural network mask branch
        self.mask_shape = mask_shape

        # Maximum number of ground truth instances to use in one image
        self.max_gt_instances = max_gt_instances

        # Bounding box refinement standard deviation for RPN and final detections.
        self.rpn_bbox_avg = [0, 0, 0, 0] if rpn_bbox_avg is None else rpn_bbox_avg

        self.rpn_bbox_std_dev = [0.1, 0.1, 0.2, 0.2] if rpn_bbox_std_dev is None else rpn_bbox_std_dev
        self.bbox_std_dev = [0.1, 0.1, 0.2, 0.2] if bbox_std_dev is None else bbox_std_dev

        # Maximum number of scores to keep from the top_k in the Proposal Layer
        self.max_nms_limit = max_nms_limit

        # Max number of final detections
        self.detect_max_instances = detect_max_instances

        # Minimum probability value to accept a detected instance
        # ROIs below this threshold are skipped
        self.detect_min_confidence = detect_min_confidence

        # Non-maximum suppression threshold for detection
        self.detect_nms_threshold = detect_nms_threshold

        # Mini-batch IoU threshold to determine positive and negative proposals
        self.detect_target_threshold = detect_target_threshold


        # Gradient norm clipping
        self.grad_clip_norm = grad_clip_norm


    def __SPNet(self,
                *args,
                **kwargs):

        self.__UNetParams(*args, **kwargs)


    def __RetinaUnet(self,
                     sample_size=None,
                     input_shape=None,

                     conv_backbone='resnet50',
                     operate_stride1=True,
                     sixth_pooling=False,

                     base_filter=18,
                     end_filts=None,
                     num_in_channels=1,
                     num_classes=13,

                     normalization=False,
                     norm_type='batch',
                     normalization_momentum=False,
                     norm_track_running_states=True,
                     norm_affine=True,
                     norm_eps=1e-5,
                     relu='relu',

                     class_specific_seg_flag=False,
                     get_rois_from_seg_flag=False,

                     pre_nms_limit=50000,

                     num_seg_classes_full=13,
                     n_rpn_features=64,
                     rpn_anchor_ratios=(1 / 2, 1, 2),
                     rpn_anchor_stride=1,
                     **kwargs):

        self.num_channels = num_in_channels
        self.num_in_channels = num_in_channels

        self.sample_size = sample_size
        self.input_shape = input_shape

        if (self.sample_size is None) and (self.input_shape is not None):
            self.sample_size = self.input_shape
        elif (self.sample_size is not None) and (self.input_shape is None):
            self.input_shape = self.sample_size
        elif (self.sample_size is None) and (self.input_shape is None):
            assert self.sample_size == self.input_shapeend_filts

        self.normalization = normalization
        self.norm_type = norm_type
        self.normalization_momentum = normalization_momentum
        self.norm_track_running_states = norm_track_running_states

        self.norm_affine = norm_affine
        self.norm_eps = norm_eps


        self.conv_backbone = conv_backbone

        self.operate_stride1 = operate_stride1

        self.sixth_pooling = sixth_pooling

        self.base_filter = base_filter

        self.relu = relu

        self.norm = normalization




        self.start_filts = base_filter
        if end_filts is None:
            self.end_filts = self.start_filts * 2
        else:
            self.end_filts = end_filts
        self.n_latent_dims = 0

        self.class_specific_seg_flag = class_specific_seg_flag
        self.get_rois_from_seg_flag = get_rois_from_seg_flag
        self.head_classes = num_seg_classes_full
        self.num_classes = num_classes
        self.num_seg_classes = num_seg_classes_full if self.class_specific_seg_flag else 2



        self.rpn_anchor_ratios = rpn_anchor_ratios
        self.n_anchors_per_pos = len(self.rpn_anchor_ratios) * 3
        self.n_rpn_features = n_rpn_features
        self.rpn_anchor_stride = rpn_anchor_stride

        # pre-selection of detections for NMS-speedup. per entire batch.
        self.pre_nms_limit = pre_nms_limit


        self.dim = 3
        self.pre_crop_size_3D = [128, 128, 384]
        self.patch_size_3D = [128, 128, 384]
        self.patch_size = self.patch_size_3D
        self.pre_crop_size = self.pre_crop_size_3D

        self.rpn_bbox_std_dev = (0.1, 0.1, 0.1, 0.2, 0.2, 0.2)
        self.bbox_std_dev = (0.1, 0.1, 0.1, 0.2, 0.2, 0.2)


        self.window = (0, 0, self.patch_size[0], self.patch_size[1], 0, self.patch_size_3D[2])
        self.scale = (self.patch_size[0], self.patch_size[1],
                      self.patch_size[0], self.patch_size[1],
                      self.patch_size_3D[2], self.patch_size_3D[2])

        # patch_size to be used for training. pre_crop_size is the patch_size before data augmentation.


        self.rpn_anchor_scales = {'xy': [[8], [16], [32], [64]], 'z': [[2], [4], [8], [16]]}

        self.backbone_strides = {'xy': [4, 8, 16, 32], 'z': [1, 2, 4, 8]}

        self.pyramid_levels = [0, 1, 2, 3]

        self.model_max_instances_per_batch_element = 30  # per batch element and class.
        self.detection_nms_threshold = 1e-5  # needs to be > 0, otherwise all predictions are one cluster.
        self.model_min_confidence = 0.1

        # Threshold for first stage (RPN) non-maximum suppression (NMS):  LOWER == HARDER SELECTION
        self.rpn_nms_threshold = 0.7 if self.dim == 2 else 0.7

        # loss sampling settings.
        self.rpn_train_anchors_per_image = 40 # 6  # per batch element
        self.train_rois_per_image = 6 # per batch element
        self.roi_positive_ratio = 0.5
        self.anchor_matching_iou = 0.7

        # self.backbone_shapes = np.array(
        #     [[int(np.ceil(self.patch_size[0] / stride)),
        #       int(np.ceil(self.patch_size[1] / stride)),
        #       int(np.ceil(self.patch_size[2] / stride_z))]
        #      for stride, stride_z in zip(self.backbone_strides['xy'], self.backbone_strides['z']
        #                                  )])
    def __R2AttUNet(self, t = 2,
                 init_weights_type='kaiming', **kwargs):

        self.__UNetParams(**kwargs)

        self.t = t
        self.init_weights_type = init_weights_type