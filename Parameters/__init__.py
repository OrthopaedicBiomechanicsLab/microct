from .ModelParameters import ModelParameters




def construct_params(json_file=None, params_dict=None, params_updates=None,
                     pre_trained_params=None, pre_trained_params_json=None, pre_trained_params_dict=None):

    if (json_file is not None) and (params_dict is not None):
        print('Json file and parameters dictionary being passed, but only Json file will be read.')
        params_dict = None

    if json_file is not None:
        params_dict = params_dict_from_json(json_file)

    if params_updates is not None:
        for param_key in params_updates.keys():
            params_dict[param_key] = params_updates[param_key]

    # Always runs, but only returns an updated dict if one of the pre_train is not None, all are None by default.
    params_dict = update_params(params_dict,
                                pre_trained_params=pre_trained_params,
                                pre_trained_params_json=pre_trained_params_json,
                                pre_trained_params_dict=pre_trained_params_dict)

    from .ParameterBuilder import Parameters
    params = Parameters()
    params.construct_params(params_dict=params_dict)

    return params

def update_params(params_dict, pre_trained_params=None, pre_trained_params_json=None, pre_trained_params_dict=None,
                  ignore_pre_train_params=None):


    if ignore_pre_train_params is None:
        ignore_pre_train_params = ['loss_dict', 'metric_dict', 'loss_weights', 'loss_weights_dict',
                                   'pre_train_method', 'pre_train_method_args']

    # If there is no pre_train, return the dict with nothing changed.
    if (pre_trained_params_json is None) and (pre_trained_params_dict is None) and (pre_trained_params is None):
        return params_dict

    # If there is a pre_trained params (in dict, json, or class form) update it
    if pre_trained_params is not None:
        pre_trained_params_dict = params_to_dict(pre_trained_params)

    elif pre_trained_params_json is not None:
        pre_trained_params_dict = params_dict_from_json(pre_trained_params_json)

    elif (pre_trained_params_dict is not None):
        pre_trained_params_dict = pre_trained_params_dict

    # for updated_param_key in params_dict.keys():
    #     pre_trained_params_dict[updated_param_key] = params_dict[updated_param_key]

    for pre_train_keys in pre_trained_params_dict.keys():
        existing_param_keys = list(params_dict.keys())

        if pre_train_keys not in existing_param_keys:
            if pre_train_keys not in ignore_pre_train_params:
                params_dict[pre_train_keys] = pre_trained_params_dict[pre_train_keys]



    # params_dict.update(pre_trained_params_dict)

    return params_dict



def params_to_dict(params):
    param_dict = dict((key, value) for (key, value) in params.__dict__.items())
    return param_dict

def params_dict_from_json(json_file):
    import json
    params_json = open(json_file, 'r')
    params_dict = json.load(params_json)
    return params_dict

def params_to_json(params, file_name):
    import json
    param_dict = params_to_dict(params)
    params_json_file = json.dumps(param_dict)
    with open(file_name, 'w') as json_file:
        json_file.write(params_json_file)
