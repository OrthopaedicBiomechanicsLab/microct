class OptimizerParameters(object):
    def __init(self):
        pass

    def build_optimizer_params(self, opt_name,
                 lr_scheduler=None, *args, **kwargs):
        """

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.
        opt_name
        lr_scheduler
        kwargs
        """
        default_param_dict = {'Adam': self.__adam_parameters}

        self.lr_scheduler = lr_scheduler

        default_param_dict[opt_name](*args, **kwargs)

    def __adam_parameters(self, lr=0.001, beta=(0.9, 0.999), eps=1e-8, weight_decay=0, amsgrad=False, **kwargs):

        self.opt_args = {}
        self.opt_args['lr'] = lr
        self.opt_args['betas'] = beta
        self.opt_args['eps'] = eps
        self.opt_args['weight_decay'] = weight_decay
        self.opt_args['amsgrad'] = amsgrad







