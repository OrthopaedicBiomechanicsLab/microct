import os
from abc import ABC, abstractmethod

import collections

from Parameters.ModelParameters import ModelParameters
from Parameters.OptimizerParams import OptimizerParameters
from Parameters.DatasetParams import DatasetParams

from . import params_dict_from_json

class Parameters(ModelParameters, OptimizerParameters, DatasetParams):
    """
    Class for the parameter construction for the model. All parameters will initialized and constructed through a class
    structure. The user can pass in custom parameters in the form of a dictionary to the "construct_params" function to
    overwrite default parameters assigned. A string for the model type, under "model" must be always pased for proper
    construction.
    """

    def __init__(self):
        super(Parameters, self).__init__()

    def construct_params(self, json_file=None, params_dict=None):

        if (json_file is not None) and (params_dict is not None):
            raise UserWarning('User passing both a json file and a params dict. Currently, only one type is supported.'
                              'Params dict will be overwritten by the json file.')

        if json_file is not None:
            params_dict = params_dict_from_json(json_file)

        self.initialize_params(**params_dict)
        self.__model_params(**params_dict)
        self.__optimizer_params(**params_dict)
        self.__dataset_params(**params_dict)
        self.__loss_params_dict(**params_dict)


    def initialize_params(self, model,
                          input_source_dir=None, gt_source_dir=None, name=None,
                          gpu_ids='0', output_dir=None, epochs=10, starting_epoch=0, batch_size=1,
                          max_dataset_size=float('inf'),
                          load_from_checkpoint=False, checkpoint_save=False, checkpoint_file=None,
                          optimizer='Adam', opt_args=None, k_fold=None, relative_save_weight_period=None, CV=None,
                          model_name=None, model_output_directory=None, pre_trained_model=False,
                          pre_trained_model_weights=None, apex_opt_level='O0', imgs_during_training=False,
                          loss=None, metrics=None, loss_weights=None,
                          isTrain=False, num_threads=1, verbose=1, save_best_epoch=False, history_while_training=False,
                          dataset=None, final_activation=True, fine_tune_full=False,
                          torch_model=False, skip_load_layers=None, batch_normalization = True,
                          is_distributed=False, default_server_dir=None,
                          **kwargs):

        # TODO fix the CV value and k_fold values defined here with respect to them used in the trainer script.
        #  Probably easiest to remove them here

        # TODO fix the opt_args parameter to ensure it is used properly. Currently, the value passed in the
        #  `initialize_params` is not used and opt_args is properly defined in `__optimizer_params`.

        # TODO fix the fact that pre_trained_model is defined here but never passed here, and only used in the Trainer.
        #  This is similar to pre_trained_model_weights. Furthermore, `pre_trained_params_json` is passed to the Trainer
        #  but not defined here, which is arguable more important (since it will define how the pre-training was done).

        # TODO overhaul loss and metrics and everything with them

        # TODO determine if isTrain parameter is necessary and its utility

        # TODO output_dir and model_output_directory seem to have the same purpose. Determine which is used more and
        #  consolidate functionality

        # TODO is final_activation used? What is it even for anymore?

        # TODO what is the parameter `name` used for? Is it necessary?
        """
        The base parameters for the parameter object that is passed through the worked flow.


        Parameters
        ----------
        model : str
            String specify the model architecture to use. Refer to the documentation to determine what architectures are
            currently available and how to create a custom one. This value will also determine what model architecture
            parameters are available.
        input_source_dir : str, None
            String specifying the path of the input data for training. This value is passed into the parameter object
            through the Trainer class where it is used to construct the data generator. However, if training without the
            Trainer class, this is not necessary.
            Default: input_source_dir = None
        gt_source_dir : str, None
            String specifying the path for the ground truth labels for training. Similar to `input_source_dir` this
            is passed in the Trainer class where it is used to construct the data generator. Also, if training without
            the Trainer class, this is not necessary.
        name : str, None
            String specifying the name of the model. Honestly, I don't think this is used at all.
            Default: name = None
        gpu_ids : str, None
            Strings specifying the gpu_ids to be used for training. If multiple GPUs are to be used, numbers in the
            string must be separated by a comma. ie, if GPUs 0 and 1 are to be used, gpu_ids='0,1' is the proper syntax.
            Currently, multi-gpu functionality is done with DataParallel modelling with PyTorch. However, this should be
            updated to DistributedDataParallel in the future, as it is needed to take full advantage of Nvidia's Apex
            package.
            Default: gpu_ids = '0'
        output_dir : str, None
            Path to where the output files (weights, csv, etc.) of the model should be saved. Currently, this seems to
            have the same functionality as `model_output_directory`. Need to determine which parameter is more widely
            used and consolidate functionality.
            Default: output_dir = None
        epochs : int
            The number of training epochs.
            Default: epochs = 10
        starting_epoch : int
            The starting epoch, useful when loading an existing model.
            Default: starting_epoch = 0
        batch_size : int
            Size of each batch.
            Default: batch_size = 1
        max_dataset_size : int, float
            The max size of the dataset for training.
            Default: max_dataset_size = float('inf')
        load_from_checkpoint : bool
            Allow model to resume training from a checkpointed save file.
            Default: load_from_checkpoint = False
        checkpoint_save : bool
            Allow model to save each epoch to a checkpoint file.
            Default: checkpoint_save = False
        checkpoint_file : str, None
            String specifying the location of the checkpoint file to load.
            Default: checkpoint_file = None
        optimizer : str
            The string name for the optimizer to use.
            Refer to the PyTorch documentation for specifics on each optimizer.
            Default: optimizer = 'Adam'
        opt_args : dict, None
            Custom parameters to pass to the PyTorch optimizer. These will include learning rate, and beta terms.
            Refer to the PyTorch documentation for specifics on each optimizer and their parameters. Passing None
            Default: opt_args = None
            Note: Currently, these opt_args are not used, and are overwritten during the __optimizer_params function.
        k_fold : int, None
            The k_fold split of the data. If None, the data is not split into different k_folds and will follow user
            defined training and validation splits.
            Default: k_fold = None
            Note: Unclear if this parameter is properly utilized, or if another k_fold is used in the Trainer class.
        relative_save_weight_period : int, None
            The relative period with respect to number of epochs when to save weights. Must be an integer value. ie, if
            value is 3 and number_of_epochs is 150, weights will be saved every 150/3 = 50 epochs. If value is 3 and
            number_of_epochs is 200, 200/3 = 66.6667 ~ 66. Therefore, every 66 epochs will be saved. regardless of what
            value is selected, final epoch results will be returned. The value of None will mean no relative saving with
            respect to the epochs will be used.
            Default: relative_save_weight_period = None
        CV : Boolean (True, False)
            Boolean toggle to determine if the model is trained with a cross-fold validation scheme. This will be based
            on if the dataset being used has the data already split into specific cross-folds. This will also depend on
            value set for k_fold above.
            Default: CV = None
            Note: Currently, the value specified here is not used and is reset in the Trainer class. This is similar to
            the k_fold parameter above. This needs to be fixed.
        model_name : str, None
            Model name defined by the user for the model. If this is None, it will be filled in with some default name
            based on the model architecture type in the Trainer class. If a model is trained outside of the Trainer
            class, this is not used. Used naming of output files (weights, csv tables, etc.) during model training.
            Default: model_name = None
        model_output_directory : str, None
            The output direction (either relative or absolute) for where the model output files (weights, csv, etc.)
            should be saved. This is used in the Trainer class. Should be set if using the Trainer class, unclear if it
            will break if a path is not specified. If training outside of the Trainer class, this is not specifically
            necessary.
            Default: model_output_directory = None
        pre_trained_model : Boolean
            Boolean toggle to specify if a model is to use pre-trained model weights. Used with the Trainer class. This
            will tell the Trainer to load the previous weights and either continue training or train for another task.
            Currently, this is used with the self-supervised model a pre-trained model.
            Default: pre_trained_model = False
            Note: This is not currently set in the parameters class, but is passed in through the Trainer class where it
            will override the default value if necessary. This is due to the thought that this is more specific to how a
            model is trained as opposed its structure. However, it is necessary to streamline these parameters.
            Default: pre_trained_model = None
        pre_trained_model_weights : str, None
            The path of the pre-trained model weights to load if pre_trained_model=True. Similar to pre_trained_model,
            this parameter is not currently used here directly, but indirectly as it is passed into the Trainer where
            it overwrites the parameter.
            Default: pre_trained_model_weights = None
        apex_opt_level : str
            String value to specify the Apex precision type used for training. Currently, Apex is not used and this
            parameter does not do anything.
        imgs_during_training : Boolean
            Boolean toggle to determine if training images should be saved during training. Currently only works with
            the AGUNet and the UNet. Further, the `model_specific_training` function must be filled for the specific
            model of interest. Currently only works with (sematic) segmentation tasks.
            Default: imgs_during_training = False
        loss : dict, list, tuple, str, None
            List of strings specifying the specific loss function(s) for training. The current way loss functions are
            set-up and initialized needs a complete overhaul.
            Default: loss = None
            Note: The default value of None will cause the model to not be constructed properly, simply due to how the
            losses initialized and defined.
        metrics : dict, list, tuple, str, None
            List of strings specifying teh specific metric function(s) that will be used during training and validation.
            Similar to how the loss is defined, this needs a complete overhaul, for the same reasons. The main
            difference is that the model will work if metrics=None.
            Default: metrics = None
        loss_weights : dict, list, tuple, int, float, None
            Weights for the loss functions used. Can be stored in a list or tuple, which must match the length of loss
            passed. Can be a dict, where the key for each entry must match the same name passed into loss. Float or int
            can be used when there is only a single loss function. None sets the weights to everything for the default
            of 1.
            Default: loss_weights = None
        isTrain : Boolean
            Boolean toggle to determine if the model is to be trained. This parameter is currently not used and its
            utility needs to be investigated.
            Default: isTrain = False
        num_threads : int
            The number of threads to pass to the data generator to enable multiprocessing.
            Default: num_threads = 1
        verbose : int
            Integer specifying the verbosity for training. This verbosity parameter is currently not altered here but is
            altered in the Trainer class. Toggling may be necessary depending on training environment due to `tqdm`.
            Default: verbose = 1
        save_best_epoch : Boolean
            Toggle to determine if the best epoch should be saved. This is based on the training loss at the end of each
            epoch where lower is considered better.
            Default: save_best_epoch = False
        history_while_training : Boolean
            Toggle to determine if the overall training history should be saved as a csv. This will include the average
            loss and metrics values after each epoch.
        dataset : str, None
            The name of the dataset used for training. Refer to the dataset documentation for more information about the
            different options and how to make a custom. However, this is only necessary with the Trainer class. Refer
            to the documentation about how to Train with and without the Trainer.
            Default: dataset = None
            Note: The default value of None will only allow Training when not using the Trainer class.
        final_activation : Boolean
            Honestly, I forget what this is for. I believe it had something to do with the loading a pre-trained model
            and using its activation as opposed to something else... Honestly, I need to investigate this.
            Default: final_activation = None
        fine_tune_full : Boolean
            Boolean toggle to determine a full fine tune training should be done. This is used in conjunction with the
            pre_trained model and if only some layers or the whole model should be updated.
            Default: fine_tune_full = False
        torch_model : Boolean
            Boolean toggle to specify is the model used is a pre-defined model from PyTorch. This was only ever used
            with testing some models previously and will most likely be removed in the future.
            Default: torch_model = False
        skip_load_layers : list, None
            List of strings specifying the names of layers to skip the loading of weights into. This can be used in
            conjunction with the `fine_tune_full` parameter so that some layers do not have their weights loaded into.
            However, this is only a one-way operation. By that, it only skips layers in the state_dict to avoid loading,
            rather than being able to specify specific layers in the model to avoid. This distinction is important if
            trying to load weights into two potentially different models, like for instant if half or part of a model
            was pre-trained and those weights are to be loaded into the full model. This should be addressed in the,
            but not a major concern at the moment.
            Default: skip_load_layers = None
        is_distributed: Boolean
            Boolean to specify if a multi-gpu code will run with the PyTorch Distributed Data Parallel module or normal
            Data Parallel module. If the number of GPUs found gpu_ids is less than or equal to one (len(gpu_ids) <= 1),
            then is_distributed is automatically forced to False. If is_distributed is False and len(gpu_ids) > 1
            modules relating to Apex for mixed precision will be disabled as Apex relies on the Distributed Data
            Parallel module for multi-gpu support. Default is set to False to ensure user does not accidentally enable
            it.
            Default: is_distributed = False.
        default_server_dir: str, None
            Path for secondary server location on the server. Used if there are issues saving to the normal location.
            Default: default_server_dir = None
        kwargs

        Returns
        -------

        """

        # Directories for model input and output. If a model takes in X to predict y from and compare to ground truth Y,
        # INPUT_SOURCE_DIR will contain X, and GT_SOURCE_DIR will contain Y.
        self.input_source_dir = input_source_dir
        self.gt_source_dir = gt_source_dir

        # Base parameters, to be overwritten
        self.name = name  # Name of experiment
        self.gpu_ids = gpu_ids  # The gpu_ids for training and testing purposes
        self.output_dir = output_dir  # The directory for saved weights, parameters, json files, csv, etc. to be saved.

        self.epochs = epochs  # Number of epochs for training. Default is 10.
        self.starting_epoch = starting_epoch
        self.batch_size = batch_size  # The size of each batch
        self.max_dataset_size = max_dataset_size  # The maximum amount of samples allowed for the dataset

        self.load_from_checkpoint = load_from_checkpoint
        self.checkpoint_save = checkpoint_save

        self.checkpoint_file = checkpoint_file

        self.model = model  # Name of the model to be used.

        self.optimizer = optimizer
        self.opt_args = opt_args

        self.pre_trained_model = pre_trained_model
        self.pre_trained_model_weights = pre_trained_model_weights

        self.num_threads = num_threads  # Number of threads for the generator to utilize during data loading

        self.multi_class_learning = False
        self.multi_class_loss_order = None
        self.verbose = verbose

        self.k_fold = k_fold
        self.relative_save_weight_period = relative_save_weight_period
        self.CV = CV

        self.apex_opt_level = apex_opt_level

        self.model_name = model_name
        self.model_output_directory = model_output_directory
        self.save_best_epoch = save_best_epoch

        self.history_while_training = history_while_training

        self.dataset = dataset

        self.imgs_during_training = imgs_during_training

        self.loss = loss
        self.loss_weights = loss_weights
        self.metrics = metrics


        self.final_activation = final_activation
        self.fine_tune_full = fine_tune_full

        self.is_distributed = is_distributed
        self.default_server_dir = default_server_dir



        self.torch_model = torch_model

        self.isTrain = isTrain

        self.skip_load_layers = skip_load_layers

        #self.batch_normalization = batch_normalization

        if (self.gpu_ids is not None) and (isinstance(self.gpu_ids, str)):
            self.gpu_ids = self.gpu_ids.replace(' ', '')
            self.gpu_ids = self.gpu_ids.split(',')


    def __loss_params_dict(self, **kwargs):
        # TODO Add loss weights for multi-class loss models

        self.loss_dict = None
        self.loss_weights_dict = dict()

        if self.loss is not None:
            self.loss_dict = dict()
            if isinstance(self.loss, dict):
                self.loss_dict = self.loss

            elif isinstance(self.loss, tuple) or isinstance(self.loss, list):
                for loss_idx, loss_name in enumerate(self.loss):
                    self.loss_dict[loss_name] = {}


            elif isinstance(self.loss, str):
                self.loss_dict[self.loss] = {}


            if self.loss_weights is not None:
                if isinstance(self.loss_weights, dict):
                    self.loss_weights_dict = self.loss_weights
                elif isinstance(self.loss_weights, tuple) or isinstance(self.loss_weights, list):
                    for loss_idx, loss_name in enumerate(self.loss_dict.keys()):
                        self.loss_weights_dict[loss_name] = self.loss_weights[loss_idx]
                elif isinstance(self.loss_weights, int) or isinstance(self.loss_weights, float):
                    loss_name = self.loss_dict.keys()[0]
                    self.loss_weights_dict[loss_name] = self.loss_weights
            else:
                for loss_idx, loss_name in enumerate(self.loss_dict.keys()):
                    self.loss_weights_dict[loss_name] = 1


        self.metrics_dict = None
        if self.metrics is not None:
            self.metrics_dict = dict()

            if isinstance(self.metrics, dict):
                self.metrics_dict = self.metrics

            elif isinstance(self.metrics, tuple) or isinstance(self.metrics, list):
                for metric_name in self.metrics:
                    self.metrics_dict[metric_name] = {}
            elif isinstance(self.metrics, str):
                self.metrics_dict[self.metrics] = {}

        # if self.multi_class_learning is True:
        #     self.loss_dict = {}
        #     self.loss_weights_dict = {}
        #     if isinstance(self.loss, tuple) or isinstance(self.loss, list) or isinstance(self.loss, set):
        #         assert len(self.loss) == 2, 'If passing loss functions as a list, must only pass the same number as' \
        #                                     ' the number of labels. For more options, pass as a dict.'
        #         for loss_type, loss_name in zip(self.multi_class_loss_order, self.loss):
        #             single_class_loss = {}
        #             single_class_loss[loss_name] = {}
        #             self.loss_dict[loss_type] = single_class_loss
        #     elif isinstance(self.loss, dict):
        #         import collections
        #         compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
        #         keys_list = list(self.loss.keys())
        #
        #         if compare(self.multi_class_loss_order, keys_list):
        #             for loss_type in self.multi_class_loss_order:
        #                 if isinstance(self.loss[loss_type], dict):
        #                     self.loss_dict[loss_type] = self.loss[loss_type]
        #                 elif isinstance(self.loss[loss_type], str):
        #                     dict_hold = {self.loss[loss_type]: {}}
        #                     self.loss_dict[loss_type] = dict_hold
        #                 elif isinstance(self.loss[loss_type], tuple) or isinstance(self.loss[loss_type], list) or isinstance(self.loss[loss_type], set):
        #                     dict_hold = {}
        #                     for loss_hold in self.loss[loss_type]:
        #                         dict_hold[loss_hold] = {}
        #                     self.loss_dict[loss_type] = dict_hold
        #         else:
        #             for loss_type, loss_name in zip(self.multi_class_loss_order, self.loss):
        #                 single_class_loss = {}
        #                 single_class_loss[loss_name] = self.loss[loss_name]
        #                 self.loss_dict[loss_type] = single_class_loss
        #
        #         # for loss_name in self.loss.keys():
        #         #     self.loss_dict[loss_name] = self.loss[loss_name]
        #
        # else:
        #     if self.loss_dict is not None:
        #         self.loss_dict = self.loss_dict
        #         self.loss_weights_dict = self.loss_weights_dict if self.loss_weights_dict is not None else\
        #             dict.fromkeys(self.loss_dict.keys(), 1)
        #     elif self.loss is not None:
        #         self.loss_dict = {}
        #         self.loss_weights_dict = {}
        #         if isinstance(self.loss, tuple) or isinstance(self.loss, list) or isinstance(self.loss, set):
        #             for loss_idx, loss_name in enumerate(self.loss):
        #                 self.loss_dict[loss_name] = {}
        #                 self.loss_weights_dict[loss_name] = self.loss_weights[loss_idx] if self.loss_weights is not None else 1
        #                 # self.loss.append(loss_name)
        #         elif isinstance(self.loss, dict):
        #             for loss_idx, loss_name in enumerate(self.loss.keys()):
        #                 self.loss_dict[loss_name] = self.loss[loss_name]
        #                 self.loss_weights_dict[loss_name] = self.loss_weights[loss_idx] if self.loss_weights is not None else 1
        #                 # self.loss.append(loss_name)
        #         elif isinstance(self.loss, str):
        #             self.loss_dict[self.loss] = {}
        #             self.loss_weights_dict[self.loss] = self.loss_weights[0] if self.loss_weights is not None else 1
        #             # self.loss.append(loss)
        #
        # if self.metric_dict is not None:
        #     self.metric_dict = self.metric_dict
        #
        # elif self.metrics is not None:
        #     self.metric_dict = {}
        #     if isinstance(self.metrics, tuple) or isinstance(self.metrics, list) or isinstance(self.metrics, set):
        #         for metric_name in self.metrics:
        #             self.metric_dict[metric_name] = {}
        #             # self.metric_names.append(metric_name)
        #     elif isinstance(self.metrics, dict):
        #         for metric_name in self.metrics.keys():
        #             self.metric_dict[metric_name] = self.metrics[metric_name]
        #             # self.metric_names.append(metric_name)
        #     elif isinstance(self.metrics, str):
        #         self.metric_dict[self.metrics] = {}
        #         # self.metric_names.append(metrics)

    def __model_params(self, **kwargs):
        self.build_mod_params(**kwargs)

    def __optimizer_params(self, *args, **kwargs):

        self.build_optimizer_params(self.optimizer, *args, **kwargs)

        # assert isinstance(self.optimizer, str), 'Optimizer should be a string for the optimizer name.'
        # if self.opt_args is None:
        #     OptimizerParameters(self, self.optimizer, **kwargs)
        # elif isinstance(self.opt_args, dict):
        #     OptimizerParameters(self, self.optimizer, **self.opt_args)
        # else:
        #     assert isinstance(self.opt_args, dict), 'Custom optimizer parameters should be passed as a dict.'


    def __dataset_params(self, **kwargs):
        self.build_dataset_params(**kwargs)


    # def save_params_to_json(self, file_name):
    #     import json
    #     param_dict = self.params_to_dict()
    #     params_json_file = json.dumps(param_dict)
    #     with open(file_name, 'w') as json_file:
    #         json_file.write(params_json_file)
    #
    #
    # def build_params_from_json(self, json_file):
    #     import json
    #     params_json = open(json_file, 'r')
    #     params_dict = json.load(params_json)
    #     return params_dict
    #
    # def params_to_dict(self):
    #     param_dict = dict((key, value) for (key, value) in self.__dict__.items())
    #     return param_dict


if __name__ == '__main__':

    input_source_dir = ''
    gt_source_dir = ''

    training_params = dict(model='UNet',
                           sample_size=(64, 128, 128),
                           epochs=150,
                           depth=5,
                           base_filter=32,
                           batch_size=6,
                           load_all_data=True,
                           lr=1e-4,
                           batch_normalization=True,
                           num_labels=1,
                           residuals=False,
                           dropout=0)

    params = Parameters()
    params.construct_params(params_dict=training_params)
