import json
import logging
import os
import time
from logging.config import dictConfig as logging_dictConfig
from pprint import pformat
from Dataset import CustomDataLoader
import torch
import numpy as np
import pandas as pd

import Trainer.FileNamesUtil as FileNamesUtil
import Trainer.GridSearch_Consts as GS_Util
from Trainer.GridSearchParams import GridSearchParams
from log import logging_dict_config

from Parameters import construct_params, params_to_dict, update_params
from Models import create_model
from Dataset import select_dataset


class Trainer:
    def __init__(self,
                 training_params,
                 output_directory,
                 df_training,
                 input_source_dir,
                 gt_source_dir,
                 df_validation=None,
                 relative_save_weight_period=None,
                 training_log_name=None,
                 verbose=1,
                 run_validation=True,
                 grid_search_params=None,
                 k_fold=None,
                 use_time_stamp=False,
                 user_key_word=None,
                 params=None,
                 model_name=None,
                 CV=False,
                 cross_fold_identifier='cross_fold_number',
                 pre_trained_model=False,
                 pre_trained_model_weights=None,
                 pre_trained_params=None,
                 pre_trained_params_json=None,
                 pre_trained_params_dict=None):
        """
        This is the constructor of the Trainer class. It performs initialization
        of the class variables and auto-generates the filenames if they are
        not been provided by the user

        :param relative_save_weight_period: Default of None. The relative period with respect to number of epochs when
                to save weights. Must be an integer value. ie, if value is 3 and number_of_epochs is 150, weights will
                be saved every 150/3 = 50 epochs. If value is 3 and number_of_epochs is 200, 200/3 = 66.6667 ~ 66.
                Therefore, every 66 epochs will be saved. regardless of what value is selected, final epoch results
                will be returned.

        """
        self.user_model_name = model_name
        self.params = params

        self.grid_search_params = grid_search_params
        self.training_params = training_params

        self.df_training = df_training
        self.df_validation = df_validation
        self.input_source_dir = input_source_dir
        self.gt_source_dir = gt_source_dir

        self.top_level_output_dir = output_directory

        self.verbose = verbose
        self.CV = CV
        self.cross_fold_identifier = cross_fold_identifier
        self.run_validation = run_validation
        self.user_key_word = user_key_word

        self.training_history_filename = None
        self.model_json_filename = None
        self.output_directory = None

        self.pre_trained_model = pre_trained_model
        self.pre_trained_model_weights = pre_trained_model_weights
        self.pre_trained_params = pre_trained_params
        self.pre_trained_params_json = pre_trained_params_json
        self.pre_trained_params_dict = pre_trained_params_dict

        self.training_log_name = training_log_name

        self.relative_save_weight_period = relative_save_weight_period

        self.k_fold = k_fold
        self.k_fold_list = None
        if self.CV is True:
            self.k_fold = None

        self.use_time_stamp = use_time_stamp
        self.time_stamp = FileNamesUtil.get_time()

        self.measures_all_folds = None

    def create_main_model_folder(self):
        # TODO change so when grid_search is only one parameter set (like if running multiple in parallel) saves better
        # TODO change naming in case of nested dictionaries like passed argmuments for internal functions
        self.main_model_dir = FileNamesUtil.create_model_grid_search_directory(self.training_params,
                                                                               self.grid_search_params,
                                                                               model_name=self.user_model_name)
        if self.use_time_stamp is False:
            self.main_output_dir = self.top_level_output_dir + '/' + self.main_model_dir
        else:
            self.main_output_dir = self.top_level_output_dir + '/' + self.time_stamp + '/' + self.main_model_dir

        # Necessary when running multiple k_folds simultaneously and having them results go into their proper locations
        if isinstance(self.k_fold, int) or isinstance(self.k_fold, float):
            time.sleep(self.k_fold)

        if os.path.exists(self.main_output_dir) is False:
            os.makedirs(self.main_output_dir)

    def initialize_log_and_dir(self):
        if self.training_log_name is None:
            self.training_log_name = self.main_output_dir + '/train.log'
        else:
            self.training_log_name = self.main_output_dir + '/' + self.training_log_name
        self.logging_dict = logging_dict_config(self.training_log_name)
        logging_dictConfig(self.logging_dict)
        self.logger = logging.getLogger(__name__)
        self.logger_format = logging.Formatter(fmt=self.logging_dict['formatters']['f']['format'],
                                               datefmt=self.logging_dict['formatters']['f']['datefmt'])
        self.logger.info('Training data can be found in: ' + self.input_source_dir)

    def build_model_name(self, model_type, grid_search, num_grid_search_params):

        if num_grid_search_params > 1:

            self.model_name = FileNamesUtil.build_model_name(model_type, k_fold=self.k_fold,
                                                             model_param=grid_search,
                                                             model_name=self.user_model_name)

            self.model_output_dir = FileNamesUtil.create_output_directory(self.main_output_dir,
                                                                          os.path.join(self.model_name))

        else:
            if self.user_model_name is not None:
                self.model_name = self.user_model_name
            elif 'model' in self.training_params.keys():
                self.model_name = self.training_params['model'].lower()
            elif 'model' in self.grid_search_params.keys():
                self.model_name = self.grid_search_params['model'].values[0].lower()
            else:
                raise KeyError('Must provide a model for naming; either a user defined one or pass model through the'
                               ' training or grid_search parameter dictionaries.')

            self.model_output_dir = self.main_output_dir


    def build_model_k_fold_output_dir(self, k_fold):
        self.model_name_k_fold = self.model_name + '_k_fold_' + str(k_fold)
        self.model_output_dir_k_fold = FileNamesUtil.create_output_directory(self.model_output_dir,
                                                                             self.model_name_k_fold)

    def build_model_weight_filename(self, model_dir, k_fold=None):
        self.model_weights_filename = FileNamesUtil.create_model_weights_filename(
            model_dir,
            self.model_name,
            k_fold=k_fold)

        self.logger.info('model weights file is ' + str(self.model_weights_filename))


    def build_training_history_filename(self, model_dir, k_fold=None):
        self.training_history_filename = FileNamesUtil.create_training_history_filename(
            model_dir,
            self.model_name,
            k_fold=k_fold)
        self.logger.info('training history file is ' + str(self.training_history_filename))

    def save_total_grid_search_params(self):
        self.model_training_params_filename = FileNamesUtil.create_training_params_filename(
            self.model_output_dir,
            self.model_name)
        params_json_file = json.dumps(self.params_dict)
        with open(self.model_training_params_filename, 'w') as json_file:
            json_file.write(params_json_file)

    def save_evaluation_filename(self, k_fold=None, all_fold=False):
        """
        this method creates a filename for the evaluation results.
        the file is located in the self.output_directory and it is
        built based on the name of the self.model_weights_filename
        """

        if k_fold is None:
            evaluation_filename = os.path.join(self.model_output_dir,
                                               'evaluation_' + self.model_name)

            self.measures_df.to_csv(evaluation_filename + '.csv', index=False)
            self.measures_df.to_pickle(evaluation_filename + '.pickle')

            self.logger.info(
                'evaluation measures are saved to:' + evaluation_filename)

        else:
            self.evaluation_filename_k_fold = os.path.join(self.model_output_dir_k_fold,
                                                           'evaluation_' + self.model_name_k_fold)

            self.measures_df.to_csv(self.evaluation_filename_k_fold + '.csv', index=False)
            self.measures_df.to_pickle(self.evaluation_filename_k_fold + '.pickle')

            self.logger.info(
                'evaluation measures are saved to:' + self.evaluation_filename_k_fold)


        if all_fold is True:
            evaluation_filename = os.path.join(self.model_output_dir,
                                               'evaluation_' + self.model_name)

            self.measures_all_folds.to_csv(evaluation_filename + '.csv', index=False)
            self.measures_all_folds.to_pickle(evaluation_filename + '.pickle')

            self.logger.info(
                'evaluation measures are saved to:' + evaluation_filename)

    def update_params_with_user_input(self):

        if self.params.input_source_dir is None:
            self.params.input_source_dir = self.input_source_dir
        if self.params.gt_source_dir is None:
            self.params.gt_source_dir = self.gt_source_dir
        if self.params.relative_save_weight_period is None:
            self.params.relative_save_weight_period = self.relative_save_weight_period
        if self.params.verbose is None:
            self.params.verbose = self.verbose


        if self.params.model_name is None:
            self.params.model_name = self.model_name

        if (self.params.CV is not None) and (self.CV is None):
            self.CV = self.params.CV
        elif (self.params.CV is None) and (self.CV is not None):
            self.params.CV = self.CV

        if self.k_fold_list is not None:
            self.params.k_fold = str(len(self.k_fold_list))

        if (self.k_fold is not None) and (self.CV == False):
            self.params.k_fold = str(self.k_fold)

    def train(self):
        """
        this method trains the neural network
        """

        np.random.seed(1)
        torch.random.manual_seed(1)

        self.create_main_model_folder()
        self.initialize_log_and_dir()

        # Initialize log handler
        handler = None

        if self.grid_search_params is None:
            # Use a dummy grid_search_params for the for loop in the train function.
            self.grid_search_params = pd.DataFrame(data={'hold': [1]})
            self.logger.info('No grid search running.')

        else:
            grid_utility = GridSearchParams(self.grid_search_params)
            self.grid_search_params = grid_utility.get_params_dataframe()
            grid_utility.save_grid_search(output_location=self.main_output_dir)
            self.logger.info('Model grid search for: ')
            self.logger.info(self.grid_search_params)

        self.logger.info('Beginning to train model.')

        # Setup k_folds for cross-validation
        if self.CV is True:
            self.auto_split = None
            # Copy dataframe as to not overwrite the df_training dataframe later on
            self.df_samples = self.df_training.copy()

            # Check to see if the dataframe has a k-fold column.
            df_columns = self.df_training.columns
            self.auto_split = False
            self.k_fold_list = None
            if self.cross_fold_identifier in df_columns:
                # Organize k-folds for cross-validation
                # Remove any string values from the list
                unique_fold_values = self.df_training[self.cross_fold_identifier].unique()
                check_unique = [isinstance(value, str) for value in unique_fold_values]
                if True in check_unique:
                    self.non_int_index = self.df_training.loc[
                        (self.df_training[self.cross_fold_identifier].str.isdigit() == False)].index
                    self.df_training = self.df_training.drop(index=self.non_int_index)

                self.k_fold_list = np.sort(self.df_training[self.cross_fold_identifier].unique().astype('int'))

                if len(self.k_fold_list) == 1:
                    self.logger.warning('Only one k_fold value was found in the dataframe. Please ensure this is'
                                        ' correct setting or use the non cross-validation trainer.')

                self.k_fold = None

            elif isinstance(self.k_fold, int) or isinstance(self.k_fold, float):
                self.k_fold = int(self.k_fold)
                self.auto_split = True

            else:
                raise KeyError('Cross_fold_identifier name for dataframe or k_fold number need to be passed for '
                               'cross validation training.')

        num_grid_search_params = self.grid_search_params.shape[0]
        for index, _ in self.grid_search_params.iterrows():
            grid_search_model_params = self.grid_search_params.loc[index]

            # Convert numpy dtypes type (np.bool_, np.int_, np.float_) to Python bool type (bool, int, fload)
            # so it can work with the json file.

            grid_search_model_params = grid_search_model_params.apply(lambda x:
                                                                      bool(x) if isinstance(x, np.bool_) else x)
            grid_search_model_params = grid_search_model_params.apply(lambda x:
                                                                      int(x) if isinstance(x, np.int_) else x)
            grid_search_model_params = grid_search_model_params.apply(lambda x:
                                                                      float(x) if isinstance(x, np.float_) else x)

            # Initialize folder for specific grid search value
            self.build_model_name(model_type=self.training_params['model'],
                                  grid_search=grid_search_model_params,
                                  num_grid_search_params=num_grid_search_params)

            # Add grid search params into existing user parameters
            self.training_params.update(grid_search_model_params.to_dict())

            if self.params is None:
                self.params = construct_params(params_dict=self.training_params,
                                               pre_trained_params=self.pre_trained_params,
                                               pre_trained_params_json=self.pre_trained_params_json,
                                               pre_trained_params_dict=self.pre_trained_params_dict)

            self.update_params_with_user_input()

            self.params.model_name = self.model_name
            self.params.model_output_directory = self.model_output_dir
            self.params.pre_trained_model = self.pre_trained_model
            self.params.pre_trained_model_weights = self.pre_trained_model_weights
            self.params.root_dir = self.model_output_dir


            # Only add a change the logger if there are multiple grid search parameters.
            if num_grid_search_params > 1:
                if index > 0:
                    self.logger.removeHandler(handler)
                handler = logging.FileHandler(self.model_output_dir + '/' + 'train.log')
                handler.setFormatter(self.logger_format)
                self.logger.addHandler(handler)

            self.params_dict = params_to_dict(self.params)

            self.logger.info('Paramters are: ')
            self.logger.info(pformat(self.params_dict))


            self.logger.info(('model parameters number ' + str(index) + ' of ' + str(self.grid_search_params.index)))

            self.save_total_grid_search_params()


            self.model = create_model(self.params, self.logger)

            self.logger.info('model has been built successfully')

            # Determine if training normally or with cross-validation

            if self.CV is False:
                self._train_normal()

            elif self.CV is True:

                self._train_CV()




    def _train_normal(self):

        batch_size = self.params.batch_size
        epoch_size = self.params.epochs

        dataset_fn = select_dataset(self.params)

        dataset_train = dataset_fn(self.params, self.df_training, self.input_source_dir, self.gt_source_dir,
                                   isTrain=self.params.isTrain)

        if self.df_validation is not None:
            dataset_val = dataset_fn(self.params, self.df_validation, self.input_source_dir, self.gt_source_dir,
                                       isTrain=False)
        else:
            dataset_val = None

        self.logger.info(
            'saving the json file and the history file...')

        self.logger.info('training started')

        self.model.fit(dataset_train, dataset_val, verbose=self.verbose)


        self.build_model_weight_filename(model_dir=self.model_output_dir, k_fold=self.k_fold)
        self.model_weight_filename = self.params.model_name + '_' + str(epoch_size) + '_weights.pt'
        self.model.save_weights(self.model_weights_filename + '_' + str(epoch_size) + '_weights.pt')

        self.logger.info('training completed for this set of parameters.')

        self.build_training_history_filename(model_dir=self.model_output_dir, k_fold=self.k_fold)
        self.model.save_training_history(self.training_history_filename)

        self.logger.info(self.training_history_filename + ' is successfully saved.')

        if (self.df_validation is not None) and (self.run_validation is True):
            self.post_training_evaluation(df_validation=self.df_validation, k_fold=self.k_fold)

    def _train_CV(self):

        batch_size = self.params.batch_size
        epoch_size = self.params.epochs

        # If k_fold is an int, split data into equal parts.
        if self.auto_split is True:
            self.k_fold_list = np.arange(self.k_fold)

        handler = None

        self.measures_all_folds = pd.DataFrame(
            columns=['test_file'] + ['ground_truth'] + ['dataframe_idx'] + ['k_fold'] + self.model.metric_names)

        for k_fold_idx, validation_fold in enumerate(self.k_fold_list):
            np.random.seed(1)
            torch.random.manual_seed(1)

            self.build_model_k_fold_output_dir(validation_fold)

            if k_fold_idx > 0:
                self.logger.removeHandler(handler)
            handler = logging.FileHandler(self.model_output_dir_k_fold + '/' + 'train.log')
            handler.setFormatter(self.logger_format)
            self.logger.addHandler(handler)

            if k_fold_idx > 0:
                torch.cuda.empty_cache()
                self.model.reinitialize_model()

            # self.model.net.train()
            training_folds = np.delete(self.k_fold_list, validation_fold)

            if self.auto_split is False:
                self.df_training = self.df_samples.loc[self.df_samples[self.cross_fold_identifier].isin(training_folds)]
                self.df_validation = self.df_samples.loc[self.df_samples[self.cross_fold_identifier].isin([validation_fold])]
            else:
                self.df_training = np.array_split(self.df_samples, training_folds)
                self.df_validation = np.array_split(self.df_samples, validation_fold)


            dataset_fn = select_dataset(self.params)

            dataset_train = dataset_fn(self.params, self.df_training, self.input_source_dir, self.gt_source_dir,
                                         isTrain=self.params.isTrain)

            dataset_val = dataset_fn(self.params, self.df_validation, self.input_source_dir, self.gt_source_dir,
                                       isTrain=False)

            self.logger.info('saving the json file and the history file...')
            self.logger.info('training started')

            self.model.fit(dataset_train, dataset_val, verbose=self.verbose)

            if self.params.save_best_epoch is True:
                self.logger.info('The best epoch for training based on the minimum loss was: {}'.format(
                    self.model.best_epoch))
            else:
                self.logger.info('The best epoch for training was not recorded.')

            self.build_model_weight_filename(model_dir=self.model_output_dir_k_fold, k_fold=validation_fold)
            self.model.save_weights(self.model_weights_filename + '_' + str(epoch_size) + '_weights.pt')

            self.logger.info('training completed for this set of parameters.')

            self.build_training_history_filename(model_dir=self.model_output_dir_k_fold, k_fold=validation_fold)
            self.model.save_training_history(self.training_history_filename)

            self.logger.info(self.training_history_filename + ' is successfully saved.')


            if self.run_validation is True:
                self.post_training_evaluation(df_validation=self.df_validation, k_fold=validation_fold)

        if self.run_validation is True:
            self.save_evaluation_filename(all_fold=True)


    def post_training_evaluation(self, df_validation, k_fold=None):


        dataset_fn = select_dataset(self.params)
        val_generator = dataset_fn(self.params, df_validation, self.input_source_dir, self.gt_source_dir,
                                   isTrain=False)


        self.measures_df = pd.DataFrame(
            columns=['test_file'] + ['ground_truth'] + ['dataframe_idx'] + ['k_fold'] + self.model.metric_names)


        idx = 0
        prediction = None

        val_dataloader = CustomDataLoader(self.params, val_generator, num_threads=0, shuffle=False, batch_size=1)
        for idx, batch in enumerate(val_dataloader):


            print(('loading batch ' + str(idx + 1) + ' of ' + str(df_validation.shape[0])))

            self.logger.info('Prediction and evaluating batch ' + str(idx + 1))

            torch.cuda.empty_cache()

            # batch = val_generator.get_batch(idx, batch_size=1)

            sample_name, label_name, additional = val_generator.get_sample_names(idx)

            self.model.set_input(batch)
            self.model.forward()
            batch_metric = self.model.calc_metrics()
            batch_metric = self.model.model_specific_metrics(batch_metric)


            dict_hold = {'test_file': sample_name,
                         'ground_truth': label_name,
                         'k_fold': k_fold}

            for name in batch_metric.keys():
                dict_hold[name] = batch_metric[name]

            self.measures_df = self.measures_df.append(dict_hold, ignore_index=True)

            if self.measures_all_folds is not None:
                self.measures_all_folds = self.measures_all_folds.append(dict_hold, ignore_index=True)

        self.save_evaluation_filename(k_fold=k_fold)

