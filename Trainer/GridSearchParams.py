import pandas as pd
import Trainer.GridSearch_Consts as Util
from itertools import product

class GridSearchParams:
    """this class builds the hyper-parameter search space"""
    def __init__(self, params_dictionary: dict):
        """
        constructor
        :param params_dictionary : this is not None, if the user wants to
        train the network based on one set of arguments.
        """
        self.build_grid_search_params_from_dict(params_dictionary)

    def save_grid_search(self, output_filename=None, output_location=None):
        """
        saves the data-frame to a csv file
        :param output_filename: the output filename
        :return:
        """
        if (output_location is not None) and (output_filename is None):
            self.params_dataframe.to_csv(output_location + '/grid_search.csv')
        elif (output_location is None) and (output_filename is None):
            self.params_dataframe.to_csv('grid_search.csv')
        elif (output_location is None) and (output_filename is not None):
            self.params_dataframe.to_csv(output_filename)
        elif (output_location is not None) and (output_filename is not None):
            self.params_dataframe.to_csv(output_location + '/' + output_filename)

    def build_grid_search_params_from_dict(self, params_dictionary):
        """
        this method builds a data frame with one row, based on the
        user input parameters
        :param params_dictionary: the input parameters
        :return:
        """
        permuted_dictionary = [dict(zip(params_dictionary, v)) for v in
                               product(*params_dictionary.values())]

        self.params_dataframe = pd.DataFrame(permuted_dictionary)

    def get_params_dataframe(self):
        """
        :return: returns the hyper parameters data frame
        """
        return self.params_dataframe

    @staticmethod
    def __get_architecture():
        """
        Get the correct architecture to use to build the model
        :return: list of available architectures
        """
        arch = ['unet']
        return arch

    @staticmethod
    def __get_optimizers():
        """
        different values for the optimizer
        :return: list of available optimizers
        """
        optimizers = [Util.ADAM()]
        return optimizers

    @staticmethod
    def __get_kernel_1d_size():
        """
        different sizes for the kernel
        :return: list of kernel sizes
        """
        kernel_1d_size = [3]
        return kernel_1d_size

    @staticmethod
    def __get_kernel_initializers():
        """
        different kernel initialization techniques
        :return: list of initializers
        """
        kernel_initiliazier_values = ['glorot_uniform']
        return kernel_initiliazier_values

    @staticmethod
    def __get_Epochs():
        """
        number of epochs
        :return: list of epochs
        """
        epochs = [100]
        return epochs

    @staticmethod
    def __get_dilation_rate():
        """
        list of dilation rates
        Notes: tried values larger than 1 and the out of memory exception thrown

        of params should be the same
        :return: list of different dilation values
        """
        dilation_rate_values = [1]
        return dilation_rate_values

    @staticmethod
    def __get_batch_sizes():
        batch_sizes = [5]
        return batch_sizes

    @staticmethod
    def __get_depthes():
        """
        depth of the network
        :return:
        """
        depthes = [5]
        return depthes

    @staticmethod
    def __get_dropouts():
        """
        different dropout rates
        :return:
        """
        dropouts = [0]
        return dropouts

    @staticmethod
    def __get_pool_size():
        """
        different pooling sizes
        :return:
        """
        pool_size = [(2, 2, 2)]
        return pool_size

    @staticmethod
    def __get_augment_training():
        """
        whether training data is augmented
        :return:
        """
        augment_training = [True]
        return augment_training


    @staticmethod
    def __get_crop_size():
        """
        If data is cropped or not
        :return:
        """
        crop_size = [None]
        return crop_size

    @staticmethod
    def __get_base_filters():
        """
        If data is cropped or not
        :return:
        """
        base_filters = [16]
        return base_filters
