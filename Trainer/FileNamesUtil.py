"""
(C) Martel Lab, 2018

@author: homa
"""
import os
import time

def get_time():
    """
    get the current time , formats it and returns it
    :return: formatted time
    """
    time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S-%MS")
    return time_stamp


def create_model_weights_filename(output_directory, model_name, k_fold=None):
    """
    this method auto-generates a filename for the weights of the
    trained model , based on the number of epochs, number of epochs per
    batch and time provided by input parameter time_stamp

    :param output_directory: output directory for the weights filename
    :param keyword:the keyword to be used in the filename
    :param time_stamp: this is an string in the format of HH-MM
    """
    if k_fold is None:
        weights_filename = os.path.join(output_directory, model_name)
    else:
        weights_filename = os.path.join(output_directory, model_name + '_k_fold_' + str(k_fold))
    return weights_filename

def create_training_params_filename(output_directory, model_name, k_fold=None):

    if k_fold is None:
        training_params = os.path.join(output_directory,
                                             ('training_params_' + model_name + '.json'))
    else:
        training_params = os.path.join(output_directory,
                                             ('training_params_' + model_name + '_k_fold_' + str(k_fold) + '.json'))
    return training_params


def create_training_history_filename(output_directory, model_name, k_fold=None):
    """
    this method auto-generates a filename for the training history file. The
    filename is based on the number of epochs and number of epochs per batch
    and the time provided by the input parameter, time_stamp

    :param output_directory: the output directory for the json file
    :param model_name: The name of the model
    :param time_stamp: a string in the format of HH-MM
    :return:
    """
    import os
    if k_fold is None:
        training_history_filename = os.path.join(output_directory,
                                                 ('training_history_' + model_name + '.csv'))
    else:
        training_history_filename = os.path.join(output_directory,
                                                 ('training_history_' + model_name + '_k_fold_' + str(k_fold) + '.csv'))

    return training_history_filename

def build_model_name(model_type, model_param=None, k_fold=None, model_name=None):
    # Keyword naming goes: epochs_optimizer_dilation-rate_depth_base-filters_dropout_augment-training_crop-size

    # If the user already provides a model name, use it
    if model_name is not None:
        return model_name

    if model_param is not None:
        model_params_key_list = [str(v).replace('_', '') for v in model_param.values]
        model_params_key = ('_').join(model_params_key_list)
        model_name = (model_type + '_' + model_params_key)
    else:
        model_name = model_type
    if k_fold is not None:
        model_name = model_name + '_k_fold_' + str(k_fold)

    return model_name


def create_output_directory(directory, model_name=None):
    """
    creats a subdirectory in the directory for all the model's related files
    to be saved in
    :param directory: the top level output directory
    :param model_name: the sub folder for the model name
    :return:
    """
    if model_name is not None:
        sub_directory = os.path.join(directory, model_name)
    else:
        sub_directory = directory

    if not os.path.exists(sub_directory):
        os.makedirs(sub_directory)
        return sub_directory
    else:
        # raise ValueError(sub_directory + ' exists')
        return sub_directory


def create_model_grid_search_directory(training_params, grid_search_params=None, model_name=None):
    if model_name is not None:
        return model_name

    if grid_search_params is None:
        filename = training_params['model']
        return filename


    keys_list = list(grid_search_params.keys())
    filename = ''
    for idx, key in enumerate(keys_list):

        if key.endswith('args'):
            key_name = 'args'
        else:
            key_name = key[:3] if len(key) > 4 else key
        key_name = key_name.replace('_', '')
        if idx > 0:
            filename = ('_').join([filename, key_name])
        else:
            filename = key_name

        if (isinstance(grid_search_params[key], list) is False) and (isinstance(grid_search_params[key], tuple) is False):
            grid_search_params[key] = [grid_search_params[key]]
        for value in grid_search_params[key]:

            if isinstance(value, dict):

                dict_key = list(value.keys())[0]
                dict_value = str(value[dict_key])
                filename = ('_').join([filename, dict_key, dict_value])

            else:
                value = str(value)
                value = value.replace('_', '')
                filename = ('_').join([filename, value])
    return filename


