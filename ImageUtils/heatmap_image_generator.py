import math
import numpy as np

import SimpleITK as sitk


class HeatmapImageGenerator(object):
    """
    Generates numpy arrays of Gaussian landmark images for the given parameters.
    :param image_size: Output image size
    :param sigma: Sigma of Gaussian
    :param scale_factor: Every value of the landmark is multiplied with this value
    :param normalize_center: if true, the value on the center is set to scale_factor
                             otherwise, the default gaussian normalization factor is used
    :param size_sigma_factor: the region size for which values are being calculated
    """

    def __init__(self,
                 image_size,
                 sigma,
                 scale_factor,
                 normalize_center=True,
                 size_sigma_factor=10):
        self.image_size = image_size
        self.sigma = sigma
        self.scale_factor = scale_factor
        self.dim = len(image_size)
        self.normalize_center = normalize_center
        self.size_sigma_factor = size_sigma_factor

    def generate_heatmap(self, coords, sigma_scale_factor, sigma=None, size_sigma_factor=None):
        """
        Generates a numpy array of the landmark image for the specified point and parameters.
        :param coords: numpy coordinates ([x], [x, y] or [x, y, z]) of the point.
        :param sigma_scale_factor: Every value of the gaussian is multiplied by this value.
        :return: numpy array of the landmark image.
        """
        # landmark holds the image

        if sigma:
            self.sigma = sigma
        if size_sigma_factor:
            self.size_sigma_factor = size_sigma_factor


        if isinstance(self.sigma, float) or isinstance(self.sigma, int):
            self.sigma = np.array([self.sigma, self.sigma, self.sigma])
        else:
            self.sigma = np.array(self.sigma)

        heatmap = np.zeros(self.image_size, dtype=np.float32)

        # window_size = int(self.sigma * self.size_sigma_factor)

        window_size = self.sigma * self.size_sigma_factor

        flipped_coords = coords[::-1].astype(int)

        start_coord = np.array(flipped_coords) - window_size / 2
        end_coord = np.array(flipped_coords) + window_size / 2

        start_coord = np.maximum(0, start_coord).astype(int)
        end_coord = np.minimum(self.image_size, end_coord).astype(int)

        # return zero landmark, if region is invalid, i.e., landmark is outside of image
        if np.any(start_coord >= end_coord):
            return heatmap

        adjusted_window = (end_coord - start_coord).astype(int)

        sigma = self.sigma * sigma_scale_factor
        scale = self.scale_factor

        if not self.normalize_center:
            scale /= math.pow(math.sqrt(2 * math.pi) * sigma, self.dim)

        if self.dim == 1:
            dx = np.meshgrid(range(adjusted_window[0]))
            x_diff = dx + start_coord[0] - flipped_coords[0]

            squared_distances = x_diff * x_diff

            cropped_heatmap = scale * np.exp(-squared_distances / (2 * math.pow(sigma, 2)))

            heatmap[start_coord[0]:end_coord[0]] = cropped_heatmap[:]

        if self.dim == 2:
            dy, dx = np.meshgrid(range(adjusted_window[1]), range(adjusted_window[0]))
            x_diff = dx + start_coord[0] - flipped_coords[0]
            y_diff = dy + start_coord[1] - flipped_coords[1]

            squared_distances = x_diff * x_diff + y_diff * y_diff

            cropped_heatmap = scale * np.exp(-squared_distances / (2 * math.pow(sigma, 2)))

            heatmap[start_coord[0]:end_coord[0],
            start_coord[1]:end_coord[1]] = cropped_heatmap[:, :]

        elif self.dim == 3:
            dy, dx, dz = np.meshgrid(range(adjusted_window[1]), range(adjusted_window[0]), range(adjusted_window[2]))
            # These are matrices for the (x - x_mean) values in the gaussian,
            # where (dx + region_start[0]) is the changing x value.
            # This also assumes that each step is a single index.

            x_diff = dx + start_coord[0] - flipped_coords[0]
            y_diff = dy + start_coord[1] - flipped_coords[1]
            z_diff = dz + start_coord[2] - flipped_coords[2]

            squared_distances = x_diff * x_diff + y_diff * y_diff + z_diff * z_diff

            # cropped_heatmap = scale * np.exp(-squared_distances / (2 * math.pow(sigma, 2)))

            sigma_x = sigma[0]
            sigma_y = sigma[1]
            sigma_z = sigma[2]

            cropped_heatmap = scale * np.exp(-(x_diff ** 2) / (2 * (sigma_x ** 2))) * np.exp(-(y_diff ** 2) / (2 * (sigma_y ** 2))) * np.exp(-(z_diff ** 2) / (2 * (sigma_z ** 2)))

            heatmap[start_coord[0]:end_coord[0], start_coord[1]:end_coord[1], start_coord[2]:end_coord[2]] = cropped_heatmap[:, :, :]

        return heatmap

    def generate_heatmaps(self, landmarks, landmarks_scale, stack_axis, sigma=None, size_sigma_factor=None):
        """
        Generates a numpy array landmark images for the specified points and parameters.
        :param landmarks: List of points. A point is a dictionary with the following entries:
            'is_valid': bool, determines whether the coordinate is valid or not
            'coords': numpy coordinates ([x], [x, y] or [x, y, z]) of the point.
            'scale': scale factor of the point.
        :param stack_axis: The axis where to stack the np arrays.
        :return: numpy array of the landmark images.
        """
        heatmap_list = []

        for landmark in landmarks:
            heatmap_list.append(self.generate_heatmap(landmark, landmarks_scale,
                                                      sigma=sigma, size_sigma_factor=size_sigma_factor))

        heatmaps = np.stack(heatmap_list, axis=stack_axis)
        return heatmaps


if __name__ == '__main__':
    heat_map_gen = HeatmapImageGenerator(image_size=(128, 64, 64),
                                         sigma=3.0,
                                         scale_factor=1.0,
                                         normalize_center=True,
                                         size_sigma_factor=10
                                         )




