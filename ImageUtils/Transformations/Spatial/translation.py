from abc import ABC

import SimpleITK as sitk
import numpy as np

from ImageUtils.Transformations.Spatial import utils
from ImageUtils.Transformations.Spatial.utils import SpatialTransformBase
# from VerseData.VerSeGaussHeat.utils.random import float_uniform





class TranslateTransformBase(SpatialTransformBase):
    """
    Translation transformation base class.
    """
    def __init__(self, dim, used_dimensions=None, **kwargs):
        """
        Initializer
        :param dim: The dimension.
        :param used_dimensions: Boolean list of which dimension indizes to use for the transformation.
        :param args: Arguments passed to super init.
        :param kwargs: Keyword arguments passed to super init.
        """
        self.used_dimensions = used_dimensions or [True] * dim
        self.dim = dim

        self.size = None
        self.spacing = None
        self.direction = None
        self.origin = None

        assert len(self.used_dimensions) == dim, 'Length of used_dimensions must be equal to dim.'

    def get_translate_transform(self, dim, translation, direction):
        """
        Returns the sitk transform based on the given parameters.
        :param dim: The dimension.
        :param offset: List of offsets for each dimension.
        :return: The sitk.AffineTransform().
        """
        assert len(translation) == dim, 'Length of offset must be equal to dim.'

        t = sitk.AffineTransform(dim)
        offset_with_used_dimensions_only = [o if used else 0 for used, o in zip(self.used_dimensions, translation)]
        t.Translate(offset_with_used_dimensions_only)
        aff_matrix = np.array(direction).reshape([dim, dim]).flatten()
        t.SetMatrix(aff_matrix)
        return t



class ResampleInputTransform:

    def get_input_params(self, image=None,
                         input_size=None, input_spacing=None,
                         input_direction=None, input_origin=None, **kwargs):

        input_size, input_spacing, input_origin, input_direction = self.get_image_parameters(
            image=image,
            input_size=input_size,
            input_spacing=input_spacing,
            input_direction=input_direction,
            input_origin=input_origin
        )

        return input_size, input_spacing, input_origin, input_direction

    def get_output_params(self, input_image=None,
                          output_size=None, output_spacing=None,
                          output_direction=None, output_origin=None, **kwargs):


        assert (output_size is not None) or (output_spacing is not None) or (input_image is not None), 'One must exist.'

        if output_spacing is not None:
            dim = len(output_spacing)
        if output_size is not None:
            dim = len(output_size)

        if input_image is not None:
            input_size, input_spacing, input_origin, input_direction = self.get_image_parameters(image=input_image)
        else:
            input_size = None
            input_spacing = None
            input_origin = [0] * dim
            input_direction = np.eye(dim).flatten().tolist()

        if output_origin is None:
            output_origin = input_origin


        if output_direction is None:
            output_direction = input_direction


        if (output_spacing is not None) and (output_size is None):
            output_size = [int(input_size[idx] * input_spacing[idx] / output_spacing[idx]) for idx in range(dim)]
        elif (output_spacing is None) and (output_size is not None):
            output_spacing = [int(input_size[idx] * input_spacing[idx] / output_size[idx]) for idx in range(dim)]
        elif (output_spacing is None) and (output_size is None):
            output_spacing = input_spacing
            output_size = input_size


        return output_size, output_spacing, output_origin, output_direction

    @staticmethod
    def get_image_parameters(image=None,
                             input_size=None,
                             input_spacing=None,
                             input_direction=None,
                             input_origin=None):

        if image:
            input_size = image.GetSize()
            input_spacing = image.GetSpacing()
            input_origin = image.GetOrigin()
            input_direction = image.GetDirection()

        else:
            input_size = input_size
            input_spacing = input_spacing
            input_origin = input_origin
            input_direction = input_direction

        return input_size, input_spacing, input_origin, input_direction




class InputCenterToOrigin_rev2(ResampleInputTransform):
    """
    A translation transformation which transforms the input image center to the origin.
    """

    def __init__(self, image, **kwargs):
        super(InputCenterToOrigin_rev2, self).__init__(image, **kwargs)

    def __call__(self, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: Must contain either 'image', or 'input_size' and 'input_spacing', which define the input image physical space.
        :return: The sitk.AffineTransform().
        """

        self.size, self.spacing, self.direction, self.origin = utils.image_params(**kwargs)

        # input_center = self.get_input_center(**kwargs)
        center = utils.input_image_center(**kwargs)

        return self.get_translate_transform(self.dim, center)



class Fixed(TranslateTransformBase):
    """
    A translation transformation with a fixed offset.
    """
    def __init__(self, dim, offset, **kwargs):
        """
        Initializer.
        :param dim: The dimension.
        :param offset: List of offsets for each dimension.
        :param args: Arguments passed to super init.
        :param kwargs: Keyword arguments passed to super init.
        """
        super(Fixed, self).__init__(dim, **kwargs)
        self.current_offset = offset

    def get(self, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: Not used.
        :return: The sitk.AffineTransform().
        """
        return self.get_translate_transform(self.dim, self.current_offset)


class Random(TranslateTransformBase):
    """
    A translation transformation with a random offset.
    """
    def __init__(self, dim, random_offset, **kwargs):
        """
        Initializer.
        :param dim: The dimension.
        :param random_offset: List of random offsets per dimension. Random offset is calculated uniformly within [-random_offset[i], random_offset[i]]
        :param args: Arguments passed to super init.
        :param kwargs: Keyword arguments passed to super init.
        """
        super(Random, self).__init__(dim, **kwargs)
        self.random_offset = random_offset

    def __call__(self, transformation_dict, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: Not used.
        :return: The sitk.AffineTransform().
        """


        input_size, input_spacing, input_origin, input_direction = self.get_input_params(**transformation_dict)

        current_offset = [np.random.uniform(low=float(-self.random_offset[i]), high=float(self.random_offset[i]))
                          for i in range(len(self.random_offset))]

        return self.get_translate_transform(self.dim, current_offset, input_direction)


class InputCenterTransformBase(TranslateTransformBase):
    """
    A translation transformation which uses the center of the input image
    """
    def input_center(self, **kwargs):
        """
        Returns the input center based on either the parameters defined by the initializer or by **kwargs.
        The function uses the result of self.get_image_size_spacing_direction_origin(**kwargs) to define the output_center for each entry of output_size and output_spacing that is None.
        :param kwargs: Must contain either 'image', or 'input_size' and 'input_spacing', which define the input image physical space.
        :return: The sitk.AffineTransform().
        """
        input_size, input_spacing, input_direction, input_origin = utils.image_params(**kwargs)
        # -1 is important, as it is always the center pixel.
        input_size_half = [(input_size[i] - 1) * 0.5 for i in range(self.dim)]
        return utils.index_to_physical_point(input_size_half, input_origin, input_spacing, input_direction)


class InputCenterToOrigin(TranslateTransformBase):
    """
    A translation transformation which transforms the input image center to the origin.
    """

    def __init__(self, dim, used_dimensions=None, **kwargs):
        super(InputCenterToOrigin, self).__init__(dim, used_dimensions, **kwargs)

    def __call__(self, transformation_dict, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: Must contain either 'image', or 'input_size' and 'input_spacing', which define the input image physical space.
        :return: The sitk.AffineTransform().
        """

        size, spacing, origin, direction = self.get_input_params(**transformation_dict)
        center = utils.image_center(size)
        trans = utils.index_to_physical_point(center, origin, spacing, direction)
        return self.get_translate_transform(self.dim, trans, direction)


class OriginToInputCenter(TranslateTransformBase):
    """
    A translation transformation which transforms the origin to the the input image center.
    """

    def __init__(self, dim, used_dimensions=None, **kwargs):
        super(OriginToInputCenter, self).__init__(dim, used_dimensions, **kwargs)

    def __call__(self, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: Must contain either 'image', or 'input_size' and 'input_spacing', which define the input image physical space.
        :return: The sitk.AffineTransform().
        """
        input_center = utils.input_image_center(**kwargs)
        negative_input_center = [-i for i in input_center]
        return self.get_translate_transform(self.dim, negative_input_center)


class OutputCenterToOrigin(TranslateTransformBase):
    """
    A translation transformation which transforms the output image center to the origin.
    """

    def __init__(self, dim, used_dimensions=None, **kwargs):
        super(OutputCenterToOrigin, self).__init__(dim, used_dimensions, **kwargs)

    def __call__(self, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: These parameters are given to self.get_output_center().
        :return: The sitk.AffineTransform().
        """
        output_center = utils.output_image_center(**kwargs)
        return self.get_translate_transform(self.dim, output_center)


class OriginToOutputCenter(TranslateTransformBase):
    """
    A translation transformation which transforms origin to the the output image center.
    """

    def __init__(self, dim, used_dimensions=None, **kwargs):
        super(OriginToOutputCenter, self).__init__(dim, used_dimensions, **kwargs)


    def __call__(self, transformation_dict, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: These parameters are given to self.get_output_center().
        :return: The sitk.AffineTransform().
        """

        size, spacing, origin, direction = self.get_output_params(**transformation_dict)

        center = utils.image_center(size, **kwargs)
        trans = utils.index_to_physical_point(center, origin, spacing, direction)
        negative_output_center = [-o for o in trans]
        return self.get_translate_transform(self.dim, negative_output_center, direction)



class RandomFactorInput(TranslateTransformBase):
    """
    A translation transform that translates the input image by a random factor, such that it will be cropped.
    The input center should usually be at the origin before this transformation.
    The actual translation value per dimension will be calculated as follows:
    (input_size[i] * input_spacing[i] - self.remove_border[i]) * float_uniform(-self.random_factor[i], self.random_factor[i]) for each dimension.
    """
    def __init__(self, dim, random_factor, remove_border=None, **kwargs):
        """
        Initializer.
        :param dim: The dimension.
        :param random_factor: List of random factors per dimension.
        :param remove_border: List of values that will be subtracted from the input size before calculating the translation value.
        :param args: Arguments passed to super init.
        :param kwargs: Keyword arguments passed to super init.
        """
        super(RandomFactorInput, self).__init__(dim, **kwargs)
        self.random_factor = random_factor
        self.remove_border = remove_border or [0] * self.dim

    def __call__(self, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: Must contain either 'image', or 'input_size' and 'input_spacing', which define the input image physical space.
        :return: The sitk.AffineTransform().
        """
        # TODO check, if direction or origin are really needed
        input_size, input_spacing, input_direction, input_origin = utils.image_params(**kwargs)
        assert np.allclose(input_direction, np.eye(self.dim).flatten()), 'this transformation only works for eye direction, is: ' + input_direction
        assert np.allclose(input_origin, np.zeros(self.dim)), 'this transformation only works for zeros origin, is: ' + input_origin
        current_offset = [(input_size[i] * input_spacing[i] - self.remove_border[i]) * np.random.uniform(low=float(-self.random_factor[i]), high=float(self.random_factor[i]))
                          for i in range(len(self.random_factor))]
        return self.get_translate_transform(self.dim, current_offset)
