from abc import ABC

import SimpleITK as sitk
import numpy as np

from ImageUtils.Transformations.Spatial import utils
from ImageUtils.Transformations.Spatial.utils import SpatialTransformBase


class RotationTransformBase(SpatialTransformBase):
    """
    Rotation transformation base class.
    """

    def __init__(self, dim, used_dimensions=None, **kwargs):
        """
        Initializer
        :param dim: The dimension.
        :param used_dimensions: Boolean list of which dimension indizes to use for the transformation.
        :param args: Arguments passed to super init.
        :param kwargs: Keyword arguments passed to super init.
        """
        self.used_dimensions = used_dimensions or [True] * dim
        self.dim = dim

        assert len(self.used_dimensions) == dim, 'Length of used_dimensions must be equal to dim.'


    def get_rotation_transform(self, angles, direction):
        """
        Returns the sitk transform based on the given parameters.
        :param dim: The dimension.
        :param angles: List of angles for each dimension (in radians).
        :return: The sitk.AffineTransform().
        """
        if not isinstance(angles, list):
            angles = [angles]
        assert isinstance(angles, list), 'Angles parameter must be a list of floats, one for each dimension.'
        assert len(angles) in [1, 3], 'Angles must be a list of length 1 for 2D, or 3 for 3D.'

        t = sitk.AffineTransform(self.dim)

        if len(angles) == 1:
            # 2D
            t.Rotate(0, 1, angle=angles[0])
        elif len(angles) > 1:
            # 3D
            # rotate about x axis
            t.Rotate(1, 2, angle=angles[0])
            # rotate about y axis
            t.Rotate(0, 2, angle=angles[1])
            # rotate about z axis
            t.Rotate(0, 1, angle=angles[2])

        rot_matrix = np.array(t.GetMatrix()).reshape([self.dim, self.dim])

        aff_matrix = np.matmul(np.array(direction).reshape([self.dim, self.dim]), rot_matrix)
        t.SetMatrix(aff_matrix.flatten())
        return t


class Random(RotationTransformBase):
    """
    A rotation transformation with random angles (in radian).
    """
    def __init__(self, dim, random_angles, **kwargs):
        """
        Initializer.
        :param dim: The dimension.
        :param random_angles: List of random angles per dimension. Random angle is calculated uniformly within [-random_angles[i], random_angles[i]]
        :param args: Arguments passed to super init.
        :param kwargs: Keyword arguments passed to super init.
        """
        super(Random, self).__init__(dim, **kwargs)

        self.random_angles = random_angles

    def __call__(self, transformation_dict, **kwargs):
        """
        Returns the sitk transform based on the given parameters.
        :param kwargs: Not used.
        :return: The sitk.AffineTransform().
        """

        size, spacing, origin, direction = self.get_input_params(**transformation_dict)

        current_angles = [0]

        if isinstance(self.random_angles, int) or isinstance(self.random_angles, float):
            self.random_angles = [self.random_angles]

        if self.dim == 2:
            if len(self.random_angles) == 1:
                angle = np.random.uniform(low=float(-self.random_angles[0]), high=float(self.random_angles[0]))
                current_angles = [angle] * self.dim
            else:
                current_angles = [np.random.uniform(low=float(-self.random_angles[i]),
                                                    high=float(self.random_angles[i]))
                                  for i in range(self.dim)]

        elif self.dim == 3:
            # rotate by same random angle in each dimension
            if len(self.random_angles) == 1:
                angle = np.random.uniform(low=float(-self.random_angles[0]), high=float(self.random_angles[0]))
                current_angles = [angle] * self.dim
            else:
                # rotate by individual angle in each dimension
                current_angles = [np.random.uniform(low=float(-self.random_angles[i]),
                                                    high=float(self.random_angles[i]))
                                  for i in range(self.dim)]

        return self.get_rotation_transform(current_angles, direction)