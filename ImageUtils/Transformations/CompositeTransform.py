import SimpleITK as sitk
import numpy as np

class CompositeTransform:
    """
    A composite transformation consisting of multiple other consecutive transformations.
    """
    def __init__(self, dim, transformations, **kwargs):
        """
        Initializer.
        :param dim: The dimension of the transform.
        :param transformations: List of other transformations.
        :param args: Arguments passed to super init.
        :param kwargs: Keyword arguments passed to super init.
        """
        self.dim = dim
        self.transformations = transformations


    def __call__(self):
        """
        Returns the composite sitk transform.
        :param kwargs: Optional parameters sent to the other transformations.
        :return: The composite sitk transform.
        """

        self.transformations_list = [transform for transform in self.transformations]
        comp = self.create_composite()

        return comp

    def create_composite(self, merge_affine=False):
        """
        Creates a composite sitk transform based on a list of sitk transforms.
        :param dim: The dimension of the transformation.
        :param transformations: A list of sitk transforms.
        :param merge_affine: If true, merge affine transformations before calculating the composite transformation.
        :return: The composite sitk transform.
        """
        # if merge_affine:
        #     merged_transformations = []
        #     combined_matrix = None
        #     for transformation in transformations:
        #         if isinstance(transformation, sitk.AffineTransform):
        #             if combined_matrix is None:
        #                 combined_matrix = np.eye(dim + 1)
        #             current_matrix = get_affine_homogeneous_matrix(dim, transformation)
        #             combined_matrix = current_matrix @ combined_matrix
        #         else:
        #             if combined_matrix is not None:
        #                 matrix, translation = get_affine_matrix_and_translation(dim, combined_matrix)
        #                 combined_affine_transform = sitk.AffineTransform(dim)
        #                 combined_affine_transform.SetMatrix(matrix)
        #                 combined_affine_transform.SetTranslation(translation)
        #                 merged_transformations.append(combined_affine_transform)
        #             merged_transformations.append(transformation)
        #             combined_matrix = None
        #     if combined_matrix is not None:
        #         matrix, translation = get_affine_matrix_and_translation(dim, combined_matrix)
        #         combined_affine_transform = sitk.AffineTransform(dim)
        #         combined_affine_transform.SetMatrix(matrix)
        #         combined_affine_transform.SetTranslation(translation)
        #         merged_transformations.append(combined_affine_transform)
        #     transformations = merged_transformations

        compos = sitk.Transform(self.dim, sitk.sitkIdentity)
        for transform in self.transformations_list:
            compos.AddTransform(transform)
        return compos



def get_affine_homogeneous_matrix(dim, transformation):
    """
    Returns a homogeneous matrix for an affine transformation.
    :param dim: The dimension of the transformation.
    :param transformation: The sitk transformation.
    :return: A homogeneous (dim+1)x(dim+1) matrix as an np.array.
    """
    matrix = np.eye(dim + 1)
    matrix[:dim, :dim] = np.array(transformation.GetMatrix()).reshape([dim, dim]).T
    matrix[dim, :dim] = np.array(transformation.GetTranslation())
    return matrix


def get_affine_matrix_and_translation(dim, homogeneous_matrix):
    """
    Returns an affine transformation parameters for a homogeneous matrix.
    :param dim: The dimension of the transformation.
    :param homogeneous_matrix: The homogeneous (dim+1)x(dim+1) matrix as an np.array.
    :return: A tuple of the homogeneous matrix as a list, and the translation parameters as a list.
    """
    matrix = homogeneous_matrix[:dim, :dim].T.reshape(-1).tolist()
    translation = homogeneous_matrix[dim, :dim].tolist()
    return matrix, translation