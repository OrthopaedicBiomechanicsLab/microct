import numpy as np
import skimage
import os


def out_frame_translation_augmentation(sample_size, img_list, out_img_size=None, shape_scaling=2):

    img = img_list[0]

    # img_hold = np.zeros(sample_size)
    # seg_hold = np.zeros(sample_size) if seg is not None else None

    img_size = np.array(img.shape)

    # Coordinates of the frame where the image is centered

    # buffer_z = 0
    # buffer_y = 0
    # buffer_x = 0

    # buffer_z = 25
    # buffer_y = 30
    # buffer_x = 30
    #
    # min_z = -img_size[0] // 2 + buffer_z
    # min_y = -img_size[1] // 2 + buffer_y
    # min_x = -img_size[2] // 2 + buffer_x
    #
    # max_z = sample_size[0] - (img_size[0] // 2) - buffer_z
    # max_y = sample_size[1] - (img_size[1] // 2) - buffer_y
    # max_x = sample_size[2] - (img_size[2] // 2) - buffer_x



    # buffer_z = 30
    # buffer_y = 25
    # buffer_x = 30
    #
    # min_z = -img_size[0] // 2 + buffer_z
    # min_y = -img_size[1] // 2 + (buffer_y + 20)
    # min_x = -img_size[2] // 2 + buffer_x
    #
    # max_z = sample_size[0] - (img_size[0] // 2) - buffer_z
    # max_y = sample_size[1] - (img_size[1] // 2) - buffer_y
    # max_x = sample_size[2] - (img_size[2] // 2) - buffer_x

    # buffer_z = 40
    # buffer_y = 40
    # buffer_x = 40
    #
    # min_z = -img_size[0] // 2 + buffer_z
    # min_y = -img_size[1] // 2 + buffer_y
    # min_x = -img_size[2] // 2 + buffer_x
    #
    # max_z = sample_size[0] - (img_size[0] // 2) - buffer_z
    # max_y = sample_size[1] - (img_size[1] // 2) - buffer_y
    # max_x = sample_size[2] - (img_size[2] // 2) - buffer_x
    #
    # z0 = np.random.randint(min_z, max_z)
    # y0 = np.random.randint(min_y, max_y)
    # x0 = np.random.randint(min_x, max_x)


    # buffer_z = 40
    # buffer_y = 40
    # buffer_x = 40
    #
    # min_z = -img_size[0] // 4
    # min_y = -img_size[1] // 4
    # min_x = -img_size[2] // 4
    #
    # max_z = sample_size[0] - (img_size[0] // 4)
    # max_y = sample_size[1] - (img_size[1] // 4)
    # max_x = sample_size[2] - (img_size[2] // 4)
    #
    # z0 = np.random.randint(min_z, max_z)
    # y0 = np.random.randint(min_y, max_y)
    # x0 = np.random.randint(min_x, max_x)

    min_max_offset = (img_size * 0.2).astype(np.int)


    min_z = np.maximum(-15, -min_max_offset[0])
    min_y = np.maximum(-15, -min_max_offset[1])
    min_x = np.maximum(-15, -min_max_offset[2])

    # min_z = -15 # -40
    # min_y = -15 # -20
    # min_x = -15 # -20

    # max_z = sample_size[0] - img_size[0] + 15 # 40
    # max_y = sample_size[1] - img_size[1] + 15 # 20
    # max_x = sample_size[2] - img_size[2] + 15 # 20


    max_z = sample_size[0] - img_size[0] + np.minimum(15, min_max_offset[0])
    max_y = sample_size[1] - img_size[1] + np.minimum(15, min_max_offset[1])
    max_x = sample_size[2] - img_size[2] + np.minimum(15, min_max_offset[2])

    z0 = np.random.randint(min_z, max_z)
    y0 = np.random.randint(min_y, max_y)
    x0 = np.random.randint(min_x, max_x)

    # z0 = max_z
    # y0 = max_y
    # x0 = max_x

    # z0 = 267
    # y0 = -49
    # x0 = 52


    z1 = z0 + img_size[0]
    y1 = y0 + img_size[1]
    x1 = x0 + img_size[2]

    if z0 < 0:
        z0_crop = -1 * z0
        z1_crop = img_size[0]
        z0_loc = 0
        z1_loc = z1
    elif z1 > sample_size[0]:
        z0_crop = 0
        z1_crop = sample_size[0] - z0
        z0_loc = z0
        # z0_loc = z0
        z1_loc = sample_size[0]
    else:
        z0_crop = 0
        z1_crop = img_size[0]
        z0_loc = z0
        z1_loc = z1

    if y0 < 0:
        y0_crop = -1 * y0
        y1_crop = img_size[1]
        y0_loc = 0
        y1_loc = y1
    elif y1 > sample_size[1]:
        y0_crop = 0
        y1_crop = sample_size[1] - y0
        y0_loc = y0
        y1_loc = sample_size[1]
    else:
        y0_crop = 0
        y1_crop = img_size[1]
        y0_loc = y0
        y1_loc = y1

    if x0 < 0:
        x0_crop = -1 * x0
        x1_crop = img_size[2]
        x0_loc = 0
        x1_loc = x1
    elif x1 > sample_size[2]:
        x0_crop = 0
        x1_crop = sample_size[2] - x0
        x0_loc = x0
        x1_loc = sample_size[2]
    else:
        x0_crop = 0
        x1_crop = img_size[2]
        x0_loc = x0
        x1_loc = x1

    # img_hold[z0_loc:z1_loc, y0_loc:y1_loc, x0_loc:x1_loc] = img[z0_crop:z1_crop, y0_crop:y1_crop, x0_crop:x1_crop]
    # seg_hold[z0_loc:z1_loc, y0_loc:y1_loc, x0_loc:x1_loc] = seg[z0_crop:z1_crop, y0_crop:y1_crop, x0_crop:x1_crop]


    return_img_list = []
    for img in img_list:
        img_hold = np.zeros(sample_size)
        img_hold[z0_loc:z1_loc, y0_loc:y1_loc, x0_loc:x1_loc] = img[z0_crop:z1_crop, y0_crop:y1_crop, x0_crop:x1_crop]
        return_img_list.append(img_hold)

    return return_img_list


def in_frame_translation_augmentation(sample_size, img_list):

    img = img_list[0]

    img_hold = np.zeros(sample_size)

    cropped_size = img.shape

    z0 = np.random.randint(0, sample_size[0] - cropped_size[0]) if sample_size[0] - cropped_size[0] > 0 else 0
    y0 = np.random.randint(0, sample_size[1] - cropped_size[1]) if sample_size[1] - cropped_size[1] > 0 else 0
    x0 = np.random.randint(0, sample_size[2] - cropped_size[2]) if sample_size[2] - cropped_size[2] > 0 else 0

    z1 = z0 + cropped_size[0]
    y1 = y0 + cropped_size[1]
    x1 = x0 + cropped_size[2]

    return_img_list = []
    for img in img_list:
        img_hold = np.zeros(sample_size)
        img_hold[z0:z1, y0:y1, x0:x1] = img
        return_img_list.append(img_hold)

    # img_hold[z0:z1, y0:y1, x0:x1] = img
    # seg_hold[z0:z1, y0:y1, x0:x1] = seg if seg is not None else None

    return return_img_list


def center_sample(sample_size, img_list):

    img = img_list[0]

    img_hold = np.zeros(sample_size)

    cropped_size = img.shape

    from ImageUtils.CropPadImg import determine_padding_amount

    x0, x1, y0, y1, z0, z1 = determine_padding_amount(cropped_size, sample_size)


    return_img_list = []
    for img in img_list:
        img_hold = np.pad(img, ((x0, x1),
                                (y0, y1),
                                (z0, z1)),
                          mode='constant', constant_values=img.min())
        return_img_list.append(img_hold)


    # img_hold[z0:z1, y0:y1, x0:x1] = img
    # seg_hold[z0:z1, y0:y1, x0:x1] = seg if seg is not None else None

    return return_img_list


def remove_img_padding(img_list):
    """
    Remove padding from img and determine where is should be
    """

    img = img_list[0]

    img_idx = np.argwhere(img > 0)

    (zmin, ymin, xmin), (zmax, ymax, xmax) = img_idx.min(0), img_idx.max(0) + 1

    return_img_list = []
    for img_hold in img_list:
        return_img_list.append(img_hold[zmin:zmax, ymin:ymax, xmin:xmax])

    return return_img_list


class TranslationAugmentation:

    def __init__(self, sample_size, translation_args, **kwargs):
        # img_in_frame = True, out_img_size = None, center = False, shape_scaling = 2,

        self.sample_size = sample_size

        self.out_img_size = translation_args['out_img_size']
        self.shape_scaling = translation_args['shape_scaling']
        self.img_in_frame = translation_args['img_in_frame']
        self.center = translation_args['center']

    def __call__(self, img_list):

        # img = img_list[0]
        # seg = img_list[1] if len(img_list) > 1 else None

        # img_list = remove_img_padding(img_list)

        if self.center:
            img_list_out = center_sample(self.sample_size, img_list)
        elif self.img_in_frame:
            # img_list_out = in_frame_translation_augmentation(self.sample_size, img_list)
            img_list_out = in_frame_translation_augmentation(self.sample_size, img_list)

        else:
            img_list_out = out_frame_translation_augmentation(self.sample_size, img_list,
                                                              self.out_img_size, self.shape_scaling)

        if len(img_list_out) > 1:
            return img_list_out
        else:
            return img_list_out[0]

if __name__ == '__main__':


    import SimpleITK as sitk

    # path = 'D:/SpineSamplesFullResampled_rev2/images'

    path = 'D:/SpineSamplesBlocked/images'

    spine_filesname = '001_initial_0.nii.gz'
    seg_filename = '001_initial_0_seg.nii.gz'

    img = sitk.ReadImage(path + '/' + spine_filesname)
    seg = sitk.ReadImage(path + '/' + seg_filename)

    img_data = sitk.GetArrayFromImage(img)
    seg_data = sitk.GetArrayFromImage(seg)


    img_data = np.where(img_data < -1024, -1024, img_data)
    # Normalized
    img_data = (img_data - img_data.min()) / (img_data.max() - img_data.min())

    sample_size = (256, 128, 128)

    # for idx in range(1000):
    #     # print(idx)
    #     aug = TranslationAugmentaiton(sample_size=sample_size, img_in_frame=True)
    #     img_out, seg_out = aug([img_data, seg_data])
    # img_in_frame = True, out_img_size = None, center = False, shape_scaling = 2,

    trans_args = {'img_in_frame': False,
                  'center': False,
                  'out_img_size': None,
                  'shape_scaling': 2}


    aug = TranslationAugmentation(sample_size=sample_size, translation_args=trans_args)
    img_out, seg_out = aug([img_data, seg_data])

    img2 = sitk.GetImageFromArray(img_out)
    seg2 = sitk.GetImageFromArray(seg_out)

    sitk.WriteImage(img2, 'hold.nii.gz')
    sitk.WriteImage(seg2, 'hold_seg.nii.gz')

    a=1