import numpy as np
from scipy.ndimage import interpolation
import scipy.ndimage as ndi
from scipy.ndimage.filters import gaussian_filter, gaussian_filter1d
from scipy.ndimage.interpolation import geometric_transform
from scipy.ndimage.interpolation import map_coordinates
from scipy.interpolate import interpn
from skimage.transform import resize, warp, warp_coords, warp_polar
import SimpleITK as sitk
from scipy.interpolate import BSpline

def __random_scaling(scaling):
    scale_factor = np.random.uniform(scaling[0], scaling[1])
    scaling_mat = np.array([[scale_factor, 0, 0, 1],
                            [0, scale_factor, 0, 1],
                            [0, 0, scale_factor, 1],
                            [0, 0, 0, 1]])
    return scaling_mat


def __random_rotation(rotation):
    rotation = np.deg2rad(np.array(rotation))
    if len(rotation) == 1:
        ai, aj = 0, 0
        ak = np.random.uniform(-rotation[0], rotation[0])
    else:
        ai = np.random.uniform(-rotation[0], rotation[0])
        aj = np.random.uniform(-rotation[1], rotation[1])
        ak = np.random.uniform(-rotation[2], rotation[2])

    # print(ai, aj, ak)
    si, sj, sk = np.sin(ai), np.sin(aj), np.sin(ak)
    ci, cj, ck = np.cos(ai), np.cos(aj), np.cos(ak)

    rot = np.array([[cj * ck, -ci * sk + si * sj * ck, si * sk + ci * sj * ck, 0],
                    [cj * sk, ci * ck + si * sj * sk, -si * ck + ci * sj * sk, 0],
                    [-sj, si * cj, ci * cj, 0],
                    [0, 0, 0, 1]])

    return rot


def __random_shear(shearing_ang):
    if len(shearing_ang) == 1:
        shearing_ang = np.deg2rad(shearing_ang)
        hxy = np.tan(np.random.uniform(-shearing_ang, shearing_ang))
        hyx = np.tan(np.random.uniform(-shearing_ang, shearing_ang))

        hxz = 0
        hzx = 0
        hzy = 0
        hyz = 0

    else:
        shearing_ang = np.deg2rad(np.array(shearing_ang))

        hxy = np.tan(np.random.uniform(-shearing_ang[0], shearing_ang[0]))
        hyx = np.tan(np.random.uniform(-shearing_ang[1], shearing_ang[1]))

        hxz = np.tan(np.random.uniform(-shearing_ang[2], shearing_ang[2]))
        hzx = np.tan(np.random.uniform(-shearing_ang[3], shearing_ang[3]))

        hzy = np.tan(np.random.uniform(-shearing_ang[4], shearing_ang[4]))
        hyz = np.tan(np.random.uniform(-shearing_ang[5], shearing_ang[5]))

    shear_mat = np.array([[1, hxy, hxz, 0],
                          [hyx, 1, hyz, 0],
                          [hzx, hzy, 1, 0],
                          [0, 0, 0, 1]])

    return shear_mat


def __elastic_transform(image_list=None, label_list=None, coord_list=None,
                        alpha=720, sigma=10, random_state=None):
    '''
    Found from https://www.kaggle.com/babbler/mnist-data-augmentation-with-elastic-distortion
    Elastic deformation of images as described in [Simard2003]_.
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
       Convolutional Neural Networks applied to Visual Document Analysis", in
       Proc. of the International Conference on Document Analysis and
       Recognition, 2003.
    :param image: Input 2D image
    :param alpha: Scaling factor. Controls intensity of the deformation.
    :param sigma: Standard deviation of gaussian filter. Functionally the elasticity coefficient.
    :return:
    '''

    if image_list is not None:
        shape = image_list[0].shape[-3:]
    else:
        shape = label_list[0].shape[-3:]

    # sigma = 10

    sigma = np.random.randint(10, 21)

    shape_hold = np.array(shape) * 2

    dx = gaussian_filter((np.random.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dy = gaussian_filter((np.random.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha
    dz = gaussian_filter((np.random.rand(*shape) * 2 - 1), sigma, mode="constant", cval=0) * alpha

    x_range = np.arange(shape[0])
    y_range = np.arange(shape[1])
    z_range = np.arange(shape[2])

    x, y, z = np.meshgrid(np.arange(shape[0]), np.arange(shape[1]), np.arange(shape[2]), indexing='ij')
    XX, YY, ZZ = x + dx, y + dy, z + dz
    indices = (XX, YY, ZZ)

    coord_list_transformed = None
    if coord_list is not None:

        # coord_list_hold = coord_list[coord_list[:, -1] == 1, :-1][:, ::-1].astype(np.int)
        coord_list_hold = coord_list[coord_list[:, -1] == 1, :-1][:, ::-1]


        # shifted_coords_z = coord_list_hold[:, 0] - dx[coord_list_hold[:, 0], coord_list_hold[:, 1], coord_list_hold[:, 2]]
        # shifted_coords_y = coord_list_hold[:, 1] - dy[coord_list_hold[:, 0], coord_list_hold[:, 1], coord_list_hold[:, 2]]
        # shifted_coords_x = coord_list_hold[:, 2] - dz[coord_list_hold[:, 0], coord_list_hold[:, 1], coord_list_hold[:, 2]]

        shifted_coords_z = coord_list_hold[:, 0] - interpn((x_range, y_range, z_range), dx, (coord_list_hold[:, 0], coord_list_hold[:, 1], coord_list_hold[:, 2]), bounds_error=False, fill_value=None)
        shifted_coords_y = coord_list_hold[:, 1] - interpn((x_range, y_range, z_range), dy, (coord_list_hold[:, 0], coord_list_hold[:, 1], coord_list_hold[:, 2]), bounds_error=False, fill_value=None)
        shifted_coords_x = coord_list_hold[:, 2] - interpn((x_range, y_range, z_range), dz, (coord_list_hold[:, 0], coord_list_hold[:, 1], coord_list_hold[:, 2]), bounds_error=False, fill_value=None)

        shifted_coords = np.stack([shifted_coords_x, shifted_coords_y, shifted_coords_z], axis=1)

        coord_list_transformed = np.copy(coord_list).astype(np.float)
        coord_list_transformed[coord_list[:, -1] == 1, :-1] = shifted_coords

    # indices_hold = [x + dx, y + dy, z + dz]




    transformed_img = None
    if image_list is not None:
        transformed_img = []
        for idx, img_hold in enumerate(image_list):
            if img_hold.ndim > len(shape):
                transformed_hold = np.zeros(img_hold.shape)
                num_channels = img_hold.shape[0]
                for idx_channel in range(num_channels):
                    transformed_hold[idx_channel] = map_coordinates(img_hold[idx_channel], indices, order=1, cval=img_hold[idx_channel].min())
            else:
                # transformed_hold = map_coordinates(img_hold, indices, order=1).reshape(shape)
                transformed_hold = map_coordinates(img_hold, indices, order=1, cval=img_hold.min())
                # sitk.WriteImage(sitk.GetImageFromArray(transformed_hold), 'img_hold_5.nii.gz')
                # sitk.WriteImage(sitk.GetImageFromArray(img_hold), 'img_hold_6.nii.gz')
                #
                # a = 1
            transformed_img.append(transformed_hold)



    transformed_label = None
    if label_list is not None:
        transformed_label = []
        for idx, img_hold in enumerate(label_list):
            if img_hold.ndim > len(shape):
                transformed_hold = np.zeros(img_hold.shape)
                num_channels = img_hold.shape[0]
                for idx_channel in range(num_channels):
                    transformed_hold[idx_channel] = map_coordinates(img_hold[idx_channel], indices, order=0).reshape(shape)
            else:
                transformed_hold = map_coordinates(img_hold, indices, order=0).reshape(shape)
            transformed_label.append(transformed_hold)


    return transformed_img, transformed_label, coord_list_transformed


def __determine_operations(scaling, rotation, shearing, elastic,
                           z_flip, x_flip, y_flip):

    if scaling is not None:
        scaling = np.random.random_integers(0, 1)
    else:
        scaling = 0


    if rotation is not None:
        rotation = np.random.random_integers(0, 1)
    else:
        rotation = 0


    if shearing is not None:
        shearing = np.random.random_integers(0, 1)
    else:
        shearing = 0

    if elastic is not False:
        elastic = np.random.random_integers(0, 1)
    else:
        elastic = 0

    if z_flip is not False:
        z_flip = np.random.random_integers(0, 1)
    else:
        z_flip = 0


    if x_flip is not False:
        x_flip = np.random.random_integers(0, 1)
    else:
        x_flip = 0


    if y_flip is not False:
        y_flip = np.random.random_integers(0, 1)
    else:
        y_flip = 0


    return (scaling, rotation, shearing, elastic,
            x_flip, y_flip, z_flip)


def DataAugmentation(spine_img=None, seg_img=None, coord_list=None,
                     sample_size=(64, 128, 128),
                     scaling=None, rotation=None, shearing=None, elastic=False,
                     x_flip=False, y_flip=False, z_flip=False, **kwargs):
    """
    :param spine_img: Input sample CT spine image
    :param seg_img: Input sample segmentation
    :param spacing:
    :param sample_size: Size of the samples in [x, y, z]
    :param univoxal:
    :param scaling: Parameters used to determine the range the sample is scaled for augmentation. Can be either a list
                    of length 2 or length 6. Length 2 specifies the lower and upper bounds for isotropic scaling in
                    x, y and z. Length 6 specifies the low and upper bounds for the specific x, y and z scaling
                    (ie, [xyz_lower, xyz_upper] or [x_lower, x_upper, y_lower, y_upper, z_lower, z_upper])
    :param rotation: Specifies if the image is rotate. Can be either a list of length 2 or length 6. Length 2 specifies
                     the lower and upper bounds for rotation only about the z-axis. Length 6 specifies the low and upper
                     bounds for a complete Euler rotation matrix.
                     (ie, [z_lower, z_upper] or [x_lower, x_upper, y_lower, y_upper, z_lower, z_upper])
    :param shearing: Parameter used to decide if the image is sheared. Can either be a list of length 2 or length 6.
                     Length 2 specifies the lower and upper bounds for shearing along the x-direction only, whereas a
                     length of 6 specifies the lower and upper bounds for shearing along x, y and z.
                     (ie, [x_lower, x_upper] or [x_lower, x_upper, y_lower, y_upper, z_lower, z_upper])
    :param x_flip: Boolean to decide if the axial (transverse) planes of the image should be flipped along the
                          longitudinal direction.
                          (ie, flip each slice along the z-axis in the y-axis, so x indices are reversed)
    :param y_flip: Boolean to decide if the axial (transverse) planes of the image should be flipped along the
                          lateral direction.
                          (ie, flip each slice along the z-axis in the x-axis, so y indices are reversed)
    :param z_flip: Boolean to decide if the z-indices should be flipped.
    :return:
    """



    (scaling_prob, rotation_prob, shearing_prob, elastic_prob, x_flip_prob, y_flip_prop, z_flip_prob) = \
        __determine_operations(scaling, rotation, shearing, elastic, z_flip, x_flip, y_flip)

    transform_matrix = None


    if rotation_prob:
        rot = __random_rotation(rotation)
        transform_matrix = rot

    if scaling_prob:
        scaling_mat = __random_scaling(scaling)
        transform_matrix = scaling_mat if transform_matrix is None else np.dot(transform_matrix, scaling_mat)

    if shearing_prob:
        shear_mat = __random_shear(shearing)
        transform_matrix = shear_mat if transform_matrix is None else np.dot(transform_matrix, shear_mat)

    multi_channel_input = None
    multi_channel_seg = None

    if spine_img is not None:
        if spine_img[0].ndim > len(sample_size):
            multi_channel_input = True
        else:
            multi_channel_input = False


    if seg_img is not None:
        if seg_img[0].ndim > len(sample_size):
            multi_channel_seg = True
        else:
            multi_channel_seg = False

    if coord_list is not None:
        coord_list = np.array(coord_list)[:, ::-1]

        coord_list_hold = np.copy(coord_list)


    if transform_matrix is not None:

        if spine_img is not None:
            for idx, img_hold in enumerate(spine_img):
                if multi_channel_input:
                    img_hold = np.transpose(img_hold, (0,) + tuple(range(img_hold.ndim - 1, 0, -1)))
                else:
                    img_hold = np.transpose(img_hold)
                spine_img[idx] = img_hold

        if seg_img is not None:
            for idx, img_hold in enumerate(seg_img):
                if multi_channel_seg:
                    img_hold = np.transpose(img_hold, (0,) + tuple(range(img_hold.ndim - 1, 0, -1)))
                else:
                    img_hold = np.transpose(img_hold)
                seg_img[idx] = img_hold

        o_x = float(sample_size[0]) / 2 + 0.5
        o_y = float(sample_size[1]) / 2 + 0.5
        o_z = float(sample_size[2]) / 2 + 0.5

        offset_matrix = np.array([[1, 0, 0, o_x], [0, 1, 0, o_y], [0, 0, 1, o_z], [0, 0, 0, 1]])
        reset_matrix = np.array([[1, 0, 0, -o_x], [0, 1, 0, -o_y], [0, 0, 1, -o_z], [0, 0, 0, 1]])


        transform_matrix = np.dot(np.dot(offset_matrix, transform_matrix), reset_matrix)


        if spine_img is not None:
            for idx, img_hold in enumerate(spine_img):
                if multi_channel_input:
                    transform_matrix_multi = np.zeros((img_hold.ndim + 1, img_hold.ndim + 1))
                    transform_matrix_multi[1:, 1:] = transform_matrix
                    transform_matrix_multi[0, 0] = 1
                    img_hold = ndi.interpolation.affine_transform(img_hold, transform_matrix_multi, order=1, cval=img_hold.min())
                else:
                    img_hold = ndi.interpolation.affine_transform(img_hold, transform_matrix, order=1, cval=img_hold.min())
                img_hold = np.transpose(img_hold)
                spine_img[idx] = img_hold


        if seg_img is not None:
            for idx, img_hold in enumerate(seg_img):
                if multi_channel_seg:
                    transform_matrix_multi = np.zeros((img_hold.ndim + 1, img_hold.ndim + 1))
                    transform_matrix_multi[1:, 1:] = transform_matrix
                    transform_matrix_multi[0, 0] = 1
                    img_hold = ndi.interpolation.affine_transform(img_hold, transform_matrix_multi, order=0)
                else:
                    img_hold = ndi.interpolation.affine_transform(img_hold, transform_matrix, order=0)
                img_hold = np.transpose(img_hold)
                seg_img[idx] = img_hold

        if coord_list is not None:
            coord_list_hold = np.copy(coord_list)
            coord_list = np.matmul(np.linalg.inv(transform_matrix), coord_list.transpose()).transpose()
            # coord_list = np.matmul(transform_matrix, coord_list.transpose()).transpose()

            a = 1

    a = 1
    if elastic_prob:
    #if True:
        spine_img, seg_img, coord_list = __elastic_transform(image_list=spine_img, label_list=seg_img,
                                                             coord_list=coord_list,
                                                             alpha=720, sigma=24)


    if x_flip_prob:
        if spine_img is not None:
            spine_img = [img_hold[..., ::-1] for img_hold in spine_img]
        if seg_img is not None:
            seg_img = [img_hold[..., ::-1] for img_hold in seg_img]

        if coord_list is not None:
            x_centroid_flipped = spine_img[0].shape[2] - coord_list[coord_list[:, -1] == 1][:, 0]
            coord_list[coord_list[:, -1] == 1, 0] = x_centroid_flipped


    if y_flip_prop:
        if spine_img is not None:
            if multi_channel_input:
                spine_img = [img_hold[:, :, ::-1, :] for img_hold in spine_img]
            else:
                spine_img = [img_hold[:, ::-1, :] for img_hold in spine_img]

        if seg_img is not None:
            if multi_channel_seg:
                seg_img = [img_hold[:, :, ::-1, :] for img_hold in seg_img]
            else:
                seg_img = [img_hold[:, ::-1, :]for img_hold in seg_img]

        if coord_list is not None:
            y_centroid_flipped = spine_img[0].shape[1] - coord_list[coord_list[:, -1] == 1][:, 1]
            coord_list[coord_list[:, -1] == 1, 1] = y_centroid_flipped

    if z_flip_prob:
        if spine_img is not None:
            if multi_channel_input:
                spine_img = [img_hold[:, ::-1, :, :] for img_hold in spine_img]
            else:
                spine_img = [img_hold[::-1, :, :] for img_hold in spine_img]

        if seg_img is not None:
            if multi_channel_seg:
                seg_img = [img_hold[:, ::-1, :, :] for img_hold in seg_img]
            else:
                seg_img = [img_hold[::-1, :, :] for img_hold in seg_img]

        if coord_list is not None:
            z_centroid_flipped = spine_img[0].shape[0] - coord_list[coord_list[:, -1] == 1][:, 2]
            coord_list[coord_list[:, -1] == 1, 2] = z_centroid_flipped




    return spine_img, seg_img, coord_list



class ShapeAugmentationClass:


    def __init__(self, sample_size=(64, 128, 128),
                 scale_factor=None, rot_ang=None, shear=None, elastic=False,
                 x_flip=False, y_flip=False, z_flip=False, **kwargs):


        self.sample_size = sample_size
        self.elastic = elastic
        self.scale_factor = scale_factor
        self.rot_ang = rot_ang
        self.shear = shear

        self.x_flip = x_flip
        self.y_flip = y_flip
        self.z_flip = z_flip

    def __call__(self, spine_list=None, seg_list=None, coord_list=None):


        spine_img, seg_img, coord_list = DataAugmentation(spine_img=spine_list, seg_img=seg_list, coord_list=coord_list,
                                                          sample_size=self.sample_size,
                                                          scaling=self.scale_factor, rotation=self.rot_ang,
                                                          shearing=self.shear, elastic=self.elastic,
                                                          x_flip=self.x_flip, y_flip=self.y_flip,
                                                          z_flip=self.z_flip)

        if (spine_img is not None) and (len(spine_img) == 1):
            spine_img = spine_img[0]

        if (seg_img is not None) and (len(seg_img) == 1):
            seg_img = seg_img[0]


        return spine_img, seg_img, coord_list

        if (spine_img is not None) and (seg_img is not None) and (coord_list is not None):
            return [spine_img, seg_img, coord_list]
        elif spine_img is not None:
            return spine_img
        elif seg_img is not None:
            return seg_img


if __name__ == '__main__':


    import pandas as pd
    import matplotlib.pyplot as plt

    df = pd.read_pickle('../Util/spine_seg_dataframe_Numpy.pickle')

    df = df.sample(frac=1)
    spine_main_path = 'D:/Geoff_Klein/Segmentation_Data_08_08_08/SpineSamples_Numpy'
    seg_main_path = 'D:/Geoff_Klein/Segmentation_Data_08_08_08/SegSamples_Numpy'

    spine_filename = df['spine_filename'].to_list()[0]
    seg_filename = df['seg_filename'].to_list()[0]


    spine_img = np.load(spine_main_path + '/' + spine_filename)
    seg_img = np.load(seg_main_path + '/' + seg_filename)

    from ModelUtils.DataGeneratorProcessingNumpy import crop_and_pad_img

    sample_size = (64, 128, 128)
    scaling = None
    rotation = None
    shearing = None
    elastic = False
    x_flip = True
    y_flip = True
    z_flip = True

    spine_img[spine_img < -1024] = -1024
    spine_img = (spine_img - spine_img.min()) / (spine_img.max() - spine_img.min()) * 255

    spine_img_cropped, seg_img_cropped = crop_and_pad_img(spine_img, seg_img, sample_size, translation=True)

    # shear_range = [0.5, 2]

    spine_data_trans, seg_data_trans = DataAugmentation(spine_img=spine_img_cropped, seg_img=seg_img_cropped,
                                                        scaling=scaling, rotation=rotation, shearing=shearing,
                                                        elastic=elastic,
                                                        x_flip=False, y_flip=False, z_flip=False)


    # fig = plt.figure(1)
    #
    # plt.imshow(spine_img_cropped[32, :, :], cmap='gray')
    # plt.imshow(seg_img_cropped[32, :, :], alpha=0.6)
    # plt.show()

    import SimpleITK as sitk

    spine_img = sitk.GetImageFromArray(spine_data_trans)
    seg_img = sitk.GetImageFromArray(seg_data_trans)

    sitk.WriteImage(spine_img, 'test.nii')
    sitk.WriteImage(seg_img, 'test_seg.nii')
    print()


