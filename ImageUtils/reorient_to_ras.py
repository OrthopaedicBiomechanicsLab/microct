import os
import numpy as np
import itk
import SimpleITK as sitk



def reorient_to_rai(image):
    """
    Reorient image to RAI orientation.
    :param image: Input itk image.
    :return: Input image reoriented to RAI.
    """
    filter = itk.OrientImageFilter.New(image)
    filter.UseImageDirectionOn()
    filter.SetInput(image)
    m = itk.GetMatrixFromArray(np.array([[1, 0, 0], [0, 1, 0], [0, 0, -1]], np.float64))
    filter.SetDesiredCoordinateDirection(m)
    filter.Update()
    reoriented = filter.GetOutput()


    img_direction = list(itk.GetArrayFromMatrix(reoriented.GetDirection()).flatten())
    img_origin = list(reoriented.GetOrigin())
    img_spacing = list(reoriented.GetSpacing())

    reoriented_data = itk.GetArrayFromImage(reoriented).astype(np.float32)
    reoriented_sitk_img = sitk.GetImageFromArray(reoriented_data)


    reoriented_sitk_img.SetSpacing(img_spacing)
    reoriented_sitk_img.SetOrigin(img_origin)
    reoriented_sitk_img.SetDirection(img_direction)

    return reoriented_sitk_img


if __name__ == '__main__':

    image_name = 'D:/OdetteDICOMS/3092931_orig.nii.gz'

    img = itk.imread(image_name, itk.F)
    reoriented = reorient_to_rai(img)

    # itk.imwrite(reoriented, 'D:/OdetteDICOMS/3092931_reoriented.nii.gz')

    img_direction = list(itk.GetArrayFromMatrix(reoriented.GetDirection()).flatten())
    img_origin = list(reoriented.GetOrigin())
    img_spacing = list(reoriented.GetSpacing())

    reoriented_data = itk.GetArrayFromImage(reoriented).astype(np.float32)

    reoriented_sitk_img = sitk.GetImageFromArray(reoriented_data)

    reoriented_sitk_img.SetSpacing(img_spacing)
    reoriented_sitk_img.SetOrigin(img_origin)
    reoriented_sitk_img.SetDirection(img_direction)

    # sitk.WriteImage(reoriented_sitk_img, 'D:/OdetteDICOMS/3092931_reoriented_sitk.nii.gz')

    sitk_data = sitk.GetArrayFromImage(reoriented_sitk_img)

    diff = sitk_data - reoriented_data