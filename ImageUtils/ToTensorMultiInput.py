import torch
import numpy as np
class ToTensorMultiInput(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, input_list):
        if isinstance(input_list, list) or isinstance(input_list, tuple):
            to_tensor_list = []
            for input in input_list:
                if input is not None:
                    to_tensor_list.append(torch.from_numpy(input.astype(np.float32)))
                else:
                    to_tensor_list.append(None)

            if len(to_tensor_list) == 1:
                return to_tensor_list[0]
            else:
                return to_tensor_list

        if isinstance(input_list, dict):
            to_tensor_dict = dict()

            for key in input_list.keys():
                if input_list[key] is not None:
                    to_tensor_dict[key] = torch.from_numpy(input_list[key].astype(np.float32))
                else:
                    to_tensor_dict[key] = None

            if len(to_tensor_dict) == 1:
                return to_tensor_dict(to_tensor_dict.keys[0])
            else:
                return to_tensor_dict

        if isinstance(input_list, np.ndarray):
            return torch.from_numpy(input_list.astype(np.float32))

        if isinstance(input_list, float) or isinstance(input_list, int):
            return torch.tensor(input_list, dtype=torch.float32)
