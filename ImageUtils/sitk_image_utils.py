import SimpleITK as sitk

def img_min_max(img):
    """
    Computes the min and max intensity of an sitk image.
    :param input_image: The sitk image.
    :return: The minimum and maximum as a list
    """
    min_max_filter = sitk.MinimumMaximumImageFilter()
    min_max_filter.Execute(img)
    return [min_max_filter.GetMinimum(), min_max_filter.GetMaximum()]


def clamp_img(img, clamp_min=None, clamp_max=None):
    """
    Clamp the intensities at a minimum and/or maximum value.
    :param input_image: The sitk image.
    :param clamp_min: The minimum value to clamp.
    :param clamp_max: The maximum value to clamp.
    :return: The clamped sitk image.
    """
    if clamp_min is not None or clamp_max is not None:
        clamp_filter = sitk.ClampImageFilter()
        if clamp_min and clamp_max:
            clamp_filter.SetLowerBound(float(clamp_min))
            clamp_filter.SetUpperBound(float(clamp_max))
        elif clamp_min and not clamp_max:
            clamp_max = img_min_max(img)[1]
            clamp_filter.SetLowerBound(float(clamp_min))
            clamp_filter.SetUpperBound(float(clamp_max))
        elif not clamp_min and clamp_max:
            clamp_min = img_min_max(img)[0]
            clamp_filter.SetLowerBound(float(clamp_min))
            clamp_filter.SetUpperBound(float(clamp_max))
        output_image = clamp_filter.Execute(img)
        return output_image
    else:
        return img