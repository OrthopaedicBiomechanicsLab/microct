import numpy as np
import SimpleITK as sitk
import pandas as pd
import os


from ImageUtils.TranslationAugmentation import TranslationAugmentation
from Dataset.SpineSelfSupervised import remove_slice_blocks
from ImageUtils.IntensityAugmentation import IntensityAugmentation
from ImageUtils.ShapeAugmentation import ShapeAugmentationClass


def crop_image(img, seg):

    pad_index = np.where(np.amax(img, axis=(1, 2)) > 0)[0]

    z0 = pad_index[0]
    z1 = pad_index[-1] + 1

    main_img = img[z0:z1, :, :]
    main_seg = seg[z0:z1, :, :]

    return main_img, main_seg



if __name__ == '__main__':

    # np.random.seed(1)

    path = 'D:/SpineSamplesBlocked/images'

    spine_filesname = '001_initial_1.nii.gz'
    seg_filename = '001_initial_1_seg.nii.gz'

    img = sitk.ReadImage(path + '/' + spine_filesname)
    seg = sitk.ReadImage(path + '/' + seg_filename)

    img_data = sitk.GetArrayFromImage(img)
    seg_data = sitk.GetArrayFromImage(seg)




    img_data = np.where(img_data < -1024, -1024, img_data)
    img_data = (img_data - img_data.min()) / (img_data.max() - img_data.min())


    img_data, seg_data = crop_image(img_data, seg_data)
    sample_size = img_data.shape

    intensity_augmentation = 'gaussian'

    img_data_orig = img_data.copy()




    # Intensity Augmentation
    intensity_aug = IntensityAugmentation(intensity_augmentations=intensity_augmentation)

    # Translation Augmentation
    trans_args = {'img_in_frame': False,
                  'center': False,
                  'out_img_size': None,
                  'shape_scaling': 2}


    trans_aug = TranslationAugmentation(sample_size=sample_size, translation_args=trans_args)



    # Affine Augmentation
    # shape_aug_params = {'rot_ang': (30, 30, 30),
    #                     'shear': None,
    #                     'scale_factor': (0.8, 1.2),
    #                     'x_flip': True,
    #                     'y_flip': True,
    #                     'z_flip': True,
    #                     'elastic': True}


    shape_aug_params = {'rot_ang': None,
                        'shear': None,
                        'scale_factor': None,
                        'x_flip': False,
                        'y_flip': False,
                        'z_flip': False,
                        'elastic': False}

    shape_aug = ShapeAugmentationClass(sample_size=sample_size, **shape_aug_params)


    # img_data = intensity_aug([img_data])
    # img_data = remove_slice_blocks(img_data, num_blocks=4, num_slices=5)   # Self-supervised slice removal
    img_data, seg_data = trans_aug([img_data, seg_data])
    # img_data, seg_data = shape_aug([img_data, seg_data])



    img2 = sitk.GetImageFromArray(img_data)
    seg2 = sitk.GetImageFromArray(seg_data)
    img_orig = sitk.GetImageFromArray(img_data_orig)

    sitk.WriteImage(img2, 'hold_trans.nii.gz')
    sitk.WriteImage(img_orig, 'hold_orig.nii.gz')
    sitk.WriteImage(seg2, 'hold_trans_seg.nii.gz')