import numpy as np


def find_maximum_coord_in_image(image):
    # calculate maximum
    max_index = np.argmax(image)
    coord = np.array(np.unravel_index(max_index, image.shape), np.int32)
    return coord

def find_maximum_in_image(image):
    # calculate maximum
    coord = find_maximum_coord_in_image(image)
    max_value = image[tuple(coord)]
    # flip indizes from [y,x] to [x,y]
    return max_value, coord


def find_quadratic_subpixel_maximum_in_image(image):
    # Solution of a quadratic estimator based on "Bounds on (Deterministic) Correlation Functions with Application to Registration"
    # Unclear how to derive this quadratic estimator
    coord = find_maximum_coord_in_image(image)
    max_value = image[tuple(coord)]
    refined_coord = coord.astype(np.float32)
    dim = coord.size
    for i in range(dim):
        if int(coord[i]) - 1 < 0 or int(coord[i]) + 1 >= image.shape[i]:
            continue
        before_coord = coord.copy()
        before_coord[i] -= 1
        after_coord = coord.copy()
        after_coord[i] += 1
        pa = image[tuple(before_coord)]
        pb = image[tuple(coord)]
        pc = image[tuple(after_coord)]
        diff = 0.5 * (pa - pc) / (pa - 2 * pb + pc)
        refined_coord[i] += diff
    return max_value, refined_coord