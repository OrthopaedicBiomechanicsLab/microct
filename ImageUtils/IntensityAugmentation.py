import numpy as np
import skimage
from skimage import exposure
from skimage.transform import resize
from skimage import filters


def _contrast_stretching(img, **kwargs):

    plow, phigh = np.percentile(img, (2, 98))
    img_rescale = exposure.rescale_intensity(img, in_range=(plow, phigh))
    return img_rescale


def _histogram_equalizer(img, **kwargs):

    img_eq = exposure.equalize_hist(img, nbins=20)
    return img_eq


def _adaptive_equalization(img, clip_limit=0.03, **kwargs):
    img_adapteq = exposure.equalize_adapthist(img, clip_limit=clip_limit)
    return img_adapteq


def _gaussian_noise(img, mean=0, var=0.01, **kwargs):

    img_shape = img.shape
    # img_shape_non_padded = (img_shape_true[0] - xsize, img_shape_true[1], img_shape_true[2])

    sigma = var ** 0.5

    gauss = np.random.normal(mean, 0.05, size=img_shape)
    gauss = gauss.reshape(img_shape) * img.max()

    noisy_img = img + gauss
    return noisy_img


def _low_res_rescale(img, **kwargs):
    down_scaling_amount = np.random.uniform(1, 2)

    x0 = img.shape[0]
    y0 = img.shape[1]
    z0 = img.shape[2]

    x1 = int(x0 / down_scaling_amount)
    y1 = int(y0 / down_scaling_amount)
    z1 = int(z0 / down_scaling_amount)

    img = resize(img, [x1, y1, z1], order=1, mode='reflect', anti_aliasing=True)
    img = resize(img, [x0, y0, z0], order=1, mode='reflect', anti_aliasing=True)

    return img



def _intensity_perturbation(img, alpha=(0.5, 2), mean=(10, 30)):
    """
    :param img: 3D numpy array of scan volume
    :param alpha: size of linear component
    :param mean: windows size of running mean filter
    :return: intensity remapped volume
    """

    rescale = False
    # img must be between 0 and 255 for this to work. The goal is to see if the maximum value in img is 1 or 255.
    # However, since this step is after possible affine augmentation, there is a chance that the maximum value in
    # img is not 1, but something close to 1 (ie, 0.989).
    if 0 <= img.max() <= 1:
        img = img * (255 * img.max())
        rescale = True

    intensities = img.flatten().astype(np.uint8)

    max_intensity = intensities.max()


    random_alpha = np.random.uniform(alpha[0], alpha[1])

    random_mean = np.random.randint(mean[0], mean[1])


    # make random noise curve and smooth it
    y = np.random.rand(286)
    y_av = running_mean(y, random_mean)[20:]  # remove first 20 as these are not well smoothed


    # determine sign of linear component
    sign = np.random.choice([-1, 1])

    # calculate for each pixel remapping component
    y_av = np.array(y_av)
    # print('check this: ' + str(intensities1.shape))
    noise_comp = y_av[intensities].astype(np.float32)


    # combine remapping component with linear comp
    i_remapped = (noise_comp + sign * random_alpha * (intensities / max_intensity)).astype(np.float32)



    # rescale between 0 and 255
    int_scale1 = max_intensity * (i_remapped - i_remapped.min()) / (i_remapped.max() - i_remapped.min())  # scale between 0 and 255

    val = (filters.threshold_otsu(img) * 0.5).astype(np.float32)

    res = img > val

    res2 = filters.gaussian(res, sigma=1).astype(np.float32)

    img_perturbed = int_scale1.reshape(img.shape)*res2

    if rescale:
        img_perturbed = img_perturbed / img_perturbed.max()

    return img_perturbed


def running_mean(l, N):
    """
    :param l: input list of numbers
    :param N: window of running mean
    :return:
        running mean filtered list
    """
    sum = 0
    result = list(0 for x in l)

    for i in range(0, N):
        sum = sum + l[i]
        result[i] = sum / (i + 1)

    for i in range(N, len(l)):
        sum = sum - l[i - N] + l[i]
        result[i] = sum / N
    return result


class IntensityAugmentation:
    def __init__(self, intensity_augmentations, augmentation_params=None):

        if isinstance(intensity_augmentations, str):
            intensity_augmentations = [intensity_augmentations]

        if isinstance(augmentation_params, dict):
            augmentation_params = [augmentation_params]

        if augmentation_params is None:
            augmentation_params = [{}] * len(intensity_augmentations)


        self.intensity_augmentations = intensity_augmentations
        self.augmentation_params = augmentation_params


        self.intensity_func_dict = {'contrast': _contrast_stretching,
                                    'hist_eq': _histogram_equalizer,
                                    'adapt_eq': _adaptive_equalization,
                                    'gaussian': _gaussian_noise,
                                    'low_res': _low_res_rescale,
                                    'intensity_perturbation': _intensity_perturbation}

    def __call__(self, img_list):

        spine_img = img_list[0]
        seg_img = img_list[1] if len(img_list) > 1 else None

        #
        # x_pad_index = np.where(np.amax(spine_img, axis=(1, 2)) == 0)[0]
        #
        # xmin = x_pad_index[0]
        # xmax = x_pad_index[-1] + 1
        # xsize = xmax - xmin
        #
        # # spine_img[xmin:xmax, :, :] = np.nan
        #
        # spine_block_a = spine_img[0:xmin, :, :]
        # spine_block_b = spine_img[xmin:xmax, :, :]
        # spine_block_c = spine_img[xmax - 1:-1, :, :]
        #
        # # One of these will be size = 0
        # spine_blocks = []
        # if spine_block_a.size == 0:
        #     spine_blocks = [spine_block_b, spine_block_c]
        #
        # elif spine_block_c.size == 0:
        #     spine_blocks = [spine_block_a, spine_block_b]
        #
        # # One of the remaining 2 blocks will be the padded block, determine which it is
        # non_padded_idx = 1 if spine_blocks[0].max() == 0 else 0


        for aug, params in zip(self.intensity_augmentations, self.augmentation_params):

            # spine_blocks[non_padded_idx] = self.intensity_func_dict[aug.lower()](spine_blocks[non_padded_idx], **params)

            aug_img = self.intensity_func_dict[aug.lower()](spine_img, **params)

        # spine_img = np.concatenate(spine_blocks)
        #
        # return spine_img

        return aug_img


if __name__ == '__main__':

    import SimpleITK as sitk

    img = sitk.ReadImage('../JupyterNotebooks/001_4month_self_supervised.nii.gz')

    data = sitk.GetArrayFromImage(img)

    data = (data - data.min()) / (data.max() - data.min()) * 255

    data = data.astype(np.uint8)

    data_aug = _intensity_perturbation(data)

    img_aug = sitk.GetImageFromArray(data_aug)

    img_aug.CopyInformation(img)

    sitk.WriteImage(img_aug, 'hold.nii.gz')






