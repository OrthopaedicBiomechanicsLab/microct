import SimpleITK as sitk

def resample_img(img, img_type, spacingOut):

    resample = sitk.ResampleImageFilter()
    resample.SetReferenceImage(img)

    resampling_interp = {'scan': sitk.sitkLinear,
                         'seg': sitk.sitkNearestNeighbor}

    resample.SetInterpolator(resampling_interp[img_type])

    resample.SetGlobalDefaultCoordinateTolerance(1e-12)
    resample.SetGlobalDefaultDirectionTolerance(1e-12)

    shapeIn = img.GetSize()
    spacingIn = img.GetSpacing()
    newSize = [int(shapeIn[0] * spacingIn[0] / spacingOut[0]),
               int(shapeIn[1] * spacingIn[1] / spacingOut[1]),
               int(shapeIn[2] * spacingIn[2] / spacingOut[2])]

    resample.SetSize(newSize)
    resample.SetOutputSpacing(spacingOut)
    img_resampled = resample.Execute(img)

    return img_resampled