import numpy as np
import SimpleITK as sitk
import math

# TODO Clean up cropping code
# TODO Make the crop and pad a single function with a binary toggle to determine if to pad, crop or both

def __crop_img(spine_img, seg_img, blastic_img, lytic_img, sample_size, translation=0):
    # Find where the segmentation first is in the image

    seg_idx = np.argwhere(seg_img > 0)

    (zmin, ymin, xmin), (zmax, ymax, xmax) = seg_idx.min(0), seg_idx.max(0) + 1


    # The size of the segmentation only

    xlength_vert = xmax - xmin
    ylength_vert = ymax - ymin
    zlength_vert = zmax - zmin


    z_img_length, y_img_length, x_img_length = seg_img.shape

    x_sample_size = sample_size[2]
    y_sample_size = sample_size[1]
    z_sample_size = sample_size[0]

    # The first is to crop in the z, or tranverse direction. This will be different than the x and y direction, since it
    # is necessary to locate where the vertebrae of interest is. Then, the cropping is to crop around the vertebrae of
    # interest given the sample size in the z direction. This sample size should be large enough that it could
    # potentially encompass part of adjacent vertebrae.

    # If the size of the vertebrae is larger than the sample size, crop the vertebrae in the axial direction.

    lower_crop_x = 0
    upper_crop_x = seg_img.shape[2]

    lower_crop_y = 0
    upper_crop_y = seg_img.shape[1]

    upper_crop_z = 0
    lower_crop_z = 0

    if z_img_length > z_sample_size:

        upper_crop_z = zmax
        lower_crop_z = zmin

        # First, see if the scan needs to be cropped at all, ie, could have already been cropped. At this stage, the
        # scan does need to be cropped and isolated for the vertebrae.

        # The second stage is to see where around the vertebral body the scan should be cropped.

        if z_sample_size > zlength_vert:


            if (zlength_vert - z_sample_size) % 2 == 0:
                upper_crop_z_addition = int((z_sample_size - zlength_vert) / 2)
                lower_crop_z_subtraction = int((z_sample_size - zlength_vert) / 2)

                upper_crop_z = upper_crop_z + upper_crop_z_addition
                lower_crop_z = lower_crop_z - lower_crop_z_subtraction


            else:

                upper_crop_z_addition = int((z_sample_size - zlength_vert) / 2) + 1
                lower_crop_z_subtraction = int((z_sample_size - zlength_vert) / 2)

                upper_crop_z = upper_crop_z + upper_crop_z_addition
                lower_crop_z = lower_crop_z - lower_crop_z_subtraction


            if translation:
                # Translate the cropping in the z-direction +/- 5 from where it is

                z_shift = np.random.random_integers(-5, 5)

                lower_crop_z = lower_crop_z + z_shift

                upper_crop_z = upper_crop_z + z_shift



    # Potential for the L5 of T6 vertebrae to be too close to the edge of the scan, so cropping would be outside of
    # scan region. Check to see if lower_crop_z is negative, and/or if upper_crop_z is outside of scan region. If so,
    # set lower_crop_z to be 0, and upper_crop_z to be image size

    crop_z_outside = None

    if lower_crop_z < 0:
        lower_crop_z = 0
        crop_z_outside = 'below'

    if upper_crop_z > seg_img.shape[0]:
        upper_crop_z = seg_img.shape[0]
        crop_z_outside = 'above'

    # See if vertebrae needs to be cropped at all in the sagittal and coronal directions (ie, x and y directions).
    # Based on total transerve plane size, not just isolated vertebrae. The difference than the above is that the upper
    # lower will "squeeze" in the cropping size, rather than expand it. This is because previously, the box needed to
    # expand to surround the vertebrae.


    if x_img_length > x_sample_size:

        if (x_img_length - x_sample_size) % 2 == 0:
            upper_crop_x_subtraction = int((x_img_length - x_sample_size) / 2)
            lower_crop_x_addition = int((x_img_length - x_sample_size) / 2)

            lower_crop_x = lower_crop_x + lower_crop_x_addition
            upper_crop_x = upper_crop_x - upper_crop_x_subtraction

        else:
            upper_crop_x_subtraction = int((x_img_length - x_sample_size) / 2) + 1
            lower_crop_x_addition = int((x_img_length - x_sample_size) / 2)

            lower_crop_x = lower_crop_x + lower_crop_x_addition
            upper_crop_x = upper_crop_x - upper_crop_x_subtraction


        # Check to see if a random translation will be applied to the image

        if translation:
            # First necessary to determine the bounds for the random translation, which depend on the size of the
            # object, the size of the sample and the size of the original image

            lower_crop_x = np.random.randint(xmax - x_sample_size, xmin)

            upper_crop_x = lower_crop_x + x_sample_size

            # If the window is large compared to the source, there is a potential that the translation can force the
            # cropping to outside the main image bounds

            if lower_crop_x < 0:
                lower_crop_x = 0
                upper_crop_x = x_sample_size

            elif upper_crop_x > x_img_length:
                upper_crop_x = x_img_length
                lower_crop_x = x_img_length - x_sample_size



    if y_img_length > y_sample_size:

        if (y_img_length - y_sample_size) % 2 == 0:
            upper_crop_y_subtraction = int((y_img_length - y_sample_size) / 2)
            lower_crop_y_addition = int((y_img_length - y_sample_size) / 2)

            lower_crop_y = lower_crop_y + lower_crop_y_addition
            upper_crop_y = upper_crop_y - upper_crop_y_subtraction

        else:

            upper_crop_y_subtraction = int((y_img_length - y_sample_size) / 2) + 1
            lower_crop_y_addition = int((y_img_length - y_sample_size) / 2)

            lower_crop_y = lower_crop_y + lower_crop_y_addition
            upper_crop_y = upper_crop_y - upper_crop_y_subtraction

        # Check to see if a random translation will be applied to the image

        if translation:
            # First necessary to determine the bounds for the random translation, which depend on the size of the
            # object, the size of the sample and the size of the original image

            lower_crop_y = np.random.randint(ymax - y_sample_size, ymin)

            upper_crop_y = lower_crop_y + y_sample_size

            # If the window is large compared to the source, there is a potential that the translation can force the
            # cropping to outside the main image bounds

            if lower_crop_y < 0:

                lower_crop_y = 0
                upper_crop_y = y_sample_size

            elif upper_crop_y > y_img_length:
                upper_crop_y = y_img_length
                lower_crop_y = y_img_length - y_sample_size



    # Determine size of crops based on starting and endings

    x_crop_size = int(upper_crop_x - lower_crop_x)
    y_crop_size = int(upper_crop_y - lower_crop_y)
    z_crop_size = int(upper_crop_z - lower_crop_z)





    spine_img_cropped = spine_img[
                        lower_crop_z:upper_crop_z,
                        lower_crop_y: upper_crop_y,
                        lower_crop_x: upper_crop_x]

    seg_img_cropped = seg_img[
                      lower_crop_z:upper_crop_z,
                      lower_crop_y: upper_crop_y,
                      lower_crop_x: upper_crop_x]

    if blastic_img is not None:
        blastic_img_cropped = blastic_img[
                              lower_crop_z:upper_crop_z,
                              lower_crop_y: upper_crop_y,
                              lower_crop_x: upper_crop_x]
    else:
        blastic_img_cropped = None


    if lytic_img is not None:
        lytic_img_cropped = lytic_img[
                              lower_crop_z:upper_crop_z,
                              lower_crop_y: upper_crop_y,
                              lower_crop_x: upper_crop_x]
    else:
        lytic_img_cropped = None

    return spine_img_cropped, seg_img_cropped, blastic_img_cropped, lytic_img_cropped, crop_z_outside


def __crop_img_centered(spine_img, seg_img, blastic_img, lytic_img, sample_size, translation=0):

    # Find where the segmentation first is in the image

    seg_idx = np.argwhere(seg_img > 0)

    (zmin, ymin, xmin), (zmax, ymax, xmax) = seg_idx.min(0), seg_idx.max(0) + 1


    # The size of the segmentation only

    xlength_vert = xmax - xmin
    ylength_vert = ymax - ymin
    zlength_vert = zmax - zmin


    z_img_length, y_img_length, x_img_length = seg_img.shape

    x_sample_size = sample_size[2]
    y_sample_size = sample_size[1]
    z_sample_size = sample_size[0]

    # The first is to crop in the z, or tranverse direction. This will be different than the x and y direction, since it
    # is necessary to locate where the vertebrae of interest is. Then, the cropping is to crop around the vertebrae of
    # interest given the sample size in the z direction. This sample size should be large enough that it could
    # potentially encompass part of adjacent vertebrae.

    # If the size of the vertebrae is larger than the sample size, crop the vertebrae in the axial direction.

    lower_crop_x = 0
    upper_crop_x = seg_img.shape[2]

    lower_crop_y = 0
    upper_crop_y = seg_img.shape[1]

    # upper_crop_z = zmax
    # lower_crop_z = zmin

    lower_crop_z = 0
    upper_crop_z = seg_img.shape[0]

    # if z_img_length > z_sample_size:
    #
    #     hz = int((z_sample_size - zlength_vert)/2)
    #
    #     lower_crop_z = zmin - hz
    #
    #     if lower_crop_z < 0:
    #         lower_crop_z = 0
    #
    #     upper_crop_z = lower_crop_z + z_sample_size


    if z_img_length > z_sample_size:

        upper_crop_z = zmax
        lower_crop_z = zmin

        # First, see if the scan needs to be cropped at all, ie, could have already been cropped. At this stage, the
        # scan does need to be cropped and isolated for the vertebrae.

        # The second stage is to see where around the vertebral body the scan should be cropped.

        if z_sample_size > zlength_vert:


            if (zlength_vert - z_sample_size) % 2 == 0:
                upper_crop_z_addition = int((z_sample_size - zlength_vert) / 2)
                lower_crop_z_subtraction = int((z_sample_size - zlength_vert) / 2)

                upper_crop_z = upper_crop_z + upper_crop_z_addition
                lower_crop_z = lower_crop_z - lower_crop_z_subtraction
            else:

                upper_crop_z_addition = int((z_sample_size - zlength_vert) / 2) + 1
                lower_crop_z_subtraction = int((z_sample_size - zlength_vert) / 2)

                upper_crop_z = upper_crop_z + upper_crop_z_addition
                lower_crop_z = lower_crop_z - lower_crop_z_subtraction


            if translation:
                # Translate the cropping in the z-direction +/- 5 from where it is

                z_shift = np.random.random_integers(-5, 5)

                lower_crop_z = lower_crop_z + z_shift

                upper_crop_z = upper_crop_z + z_shift



    # Potential for the L5 of T6 vertebrae to be too close to the edge of the scan, so cropping would be outside of
    # scan region. Check to see if lower_crop_z is negative, and/or if upper_crop_z is outside of scan region. If so,
    # set lower_crop_z to be 0, and upper_crop_z to be image size

    crop_z_outside = None

    if lower_crop_z < 0:
        lower_crop_z = 0
        crop_z_outside = 'below'

    if upper_crop_z > seg_img.shape[0]:
        upper_crop_z = seg_img.shape[0]
        crop_z_outside = 'above'

    # See if vertebrae needs to be cropped at all in the sagittal and coronal directions (ie, x and y directions).
    # Based on total transerve plane size, not just isolated vertebrae. The difference than the above is that the upper
    # lower will "squeeze" in the cropping size, rather than expand it. This is because previously, the box needed to
    # expand to surround the vertebrae.



    # Similar x-axis cropping, but adding in a centering option since the previous method assumes that the image is
    # centered in the total image frame.

    if x_img_length > x_sample_size:

        hx = int((x_sample_size - xlength_vert)/2)

        lower_crop_x = xmin - hx

        if lower_crop_x < 0:
            lower_crop_x = 0

        upper_crop_x = lower_crop_x + x_sample_size

        # Check to see if a random translation will be applied to the image

        if translation:
            # First necessary to determine the bounds for the random translation, which depend on the size of the
            # object, the size of the sample and the size of the original image

            lower_crop_x = np.random.randint(xmax - x_sample_size, xmin)

            upper_crop_x = lower_crop_x + x_sample_size

            # If the window is large compared to the source, there is a potential that the translation can force the
            # cropping to outside the main image bounds

            if lower_crop_x < 0:
                lower_crop_x = 0
                upper_crop_x = x_sample_size

            elif upper_crop_x > x_img_length:
                upper_crop_x = x_img_length
                lower_crop_x = x_img_length - x_sample_size


    # Similar y-axis cropping, but adding in a centering option since the previous method assumes that the image is
    # centered in the total image frame.

    if y_img_length > y_sample_size:

        hy = int((y_sample_size - ylength_vert)/2)

        lower_crop_y = ymin - hy

        if lower_crop_y < 0:

            lower_crop_y = 0

        upper_crop_y = lower_crop_y + y_sample_size

        # Check to see if a random translation will be applied to the image

        if translation:
            # First necessary to determine the bounds for the random translation, which depend on the size of the
            # object, the size of the sample and the size of the original image

            lower_crop_y = np.random.randint(ymax - y_sample_size, ymin)

            upper_crop_y = lower_crop_y + y_sample_size

            # If the window is large compared to the source, there is a potential that the translation can force the
            # cropping to outside the main image bounds

            if lower_crop_y < 0:

                lower_crop_y = 0
                upper_crop_y = y_sample_size

            elif upper_crop_y > y_img_length:
                upper_crop_y = y_img_length
                lower_crop_y = y_img_length - y_sample_size

    # Determine size of crops based on starting and endings

    x_crop_size = int(upper_crop_x - lower_crop_x)
    y_crop_size = int(upper_crop_y - lower_crop_y)
    z_crop_size = int(upper_crop_z - lower_crop_z)



    spine_img_cropped = spine_img[
                        lower_crop_z: upper_crop_z,
                        lower_crop_y: upper_crop_y,
                        lower_crop_x: upper_crop_x]

    seg_img_cropped = seg_img[
                      lower_crop_z: upper_crop_z,
                      lower_crop_y: upper_crop_y,
                      lower_crop_x: upper_crop_x]

    if blastic_img is not None:
        blastic_img_cropped = blastic_img[
                              lower_crop_z:upper_crop_z,
                              lower_crop_y: upper_crop_y,
                              lower_crop_x: upper_crop_x]
    else:
        blastic_img_cropped = None


    if lytic_img is not None:
        lytic_img_cropped = lytic_img[
                              lower_crop_z:upper_crop_z,
                              lower_crop_y: upper_crop_y,
                              lower_crop_x: upper_crop_x]
    else:
        lytic_img_cropped = None

    return spine_img_cropped, seg_img_cropped, blastic_img_cropped, lytic_img_cropped, crop_z_outside


def __pad_img(spine_img, seg_img, blastic_img, lytic_img, sample_size, crop_z_outside):

    # Follows the same idea as the above cropping image function, but does not need to isolate around the segmentation
    # in the z-direction first. The real difference is the SimpleITK function is different (not ROI), and the if/else
    # conditions are reversed.



    x_img_length = seg_img.shape[2]
    y_img_length = seg_img.shape[1]
    z_img_length = seg_img.shape[0]

    x_sample_size = sample_size[2]
    y_sample_size = sample_size[1]
    z_sample_size = sample_size[0]

    # Determine if the (potentially) cropped vertebrae sample needs additional padding as well. Could be needed if the x
    # and y sizes were smaller than the sample size. Could also be needed if the cropping in the z-direction was too
    # close to either edge that it would be outside the same size. In this situation, we want to pad the z only in the
    # direction where it was going to be cut-off. Ie, if the upper_crop_z was outside the image, we only want to pad the
    # sample above. This is why cropping needs to be done before padding.

    upper_pad_x = 0
    lower_pad_x = 0

    upper_pad_y = 0
    lower_pad_y = 0

    upper_pad_z = 0
    lower_pad_z = 0

    if x_sample_size > x_img_length:

        if (x_sample_size - x_img_length) % 2 == 0:
            upper_pad_x = int((x_sample_size - x_img_length) / 2)
            lower_pad_x = int((x_sample_size - x_img_length) / 2)

        else:

            upper_pad_x = int((x_sample_size - x_img_length) / 2) + 1
            lower_pad_x = int((x_sample_size - x_img_length) / 2)



    if y_sample_size > y_img_length:

        if (y_sample_size - y_img_length) % 2 == 0:
            upper_pad_y = int((y_sample_size - y_img_length) / 2)
            lower_pad_y = int((y_sample_size - y_img_length) / 2)

        else:

            upper_pad_y = int((y_sample_size - y_img_length) / 2) + 1
            lower_pad_y = int((y_sample_size - y_img_length) / 2)



    if (z_sample_size > z_img_length) and (crop_z_outside is None):

        if (z_sample_size - z_img_length) % 2 == 0:
            upper_pad_z = int((z_sample_size - z_img_length) / 2)
            lower_pad_z = int((z_sample_size - z_img_length) / 2)

        else:

            upper_pad_z = int((z_sample_size - z_img_length) / 2) + 1
            lower_pad_z = int((z_sample_size - z_img_length) / 2)



    if crop_z_outside is 'above':

        upper_pad_z = int(z_sample_size - z_img_length)

    elif crop_z_outside is 'below':

        lower_pad_z = int(z_sample_size - z_img_length)


    # Apply padding.
    # Based on if/else statements above, this will only crop if necessary.

    spine_img_padded = np.pad(spine_img, ((lower_pad_z, upper_pad_z),
                                          (lower_pad_y, upper_pad_y),
                                          (lower_pad_x, upper_pad_x)),
                              mode='constant', constant_values=0)

    seg_img_padded = np.pad(seg_img, ((lower_pad_z, upper_pad_z),
                                      (lower_pad_y, upper_pad_y),
                                      (lower_pad_x, upper_pad_x)),
                            mode='constant', constant_values=0)

    if blastic_img is not None:
        blastic_img_padded = np.pad(blastic_img, ((lower_pad_z, upper_pad_z),
                                                  (lower_pad_y, upper_pad_y),
                                                  (lower_pad_x, upper_pad_x)),
                                    mode='constant', constant_values=0)
    else:
        blastic_img_padded = None


    if lytic_img is not None:
        lytic_img_padded = np.pad(lytic_img, ((lower_pad_z, upper_pad_z),
                                          (lower_pad_y, upper_pad_y),
                                          (lower_pad_x, upper_pad_x)),
                                mode='constant', constant_values=0)
    else:
        lytic_img_padded = None

    return spine_img_padded, seg_img_padded, blastic_img_padded, lytic_img_padded


def crop_and_pad_img(spine_img, seg_img, sample_size, blastic_img=None, lytic_img=None, translation=False, centered=False):

    # To ensure that cropping and padding are done in the proper order (cropping then padding) the functions are called
    # from here respectively.

    # translation = False
    # centered = True

    if centered is False:

        (spine_img_cropped, seg_img_cropped,
         blastic_img_cropped, lytic_img_cropped, crop_z_outside) = __crop_img(spine_img, seg_img,
                                                                              blastic_img, lytic_img,
                                                                              sample_size, translation)
    else:
        (spine_img_cropped, seg_img_cropped,
         blastic_img_cropped, lytic_img_cropped, crop_z_outside) = __crop_img_centered(spine_img, seg_img,
                                                                                       blastic_img, lytic_img,
                                                                                       sample_size, translation)

    (spine_img_padded, seg_img_padded,
     blastic_img_padded, lytic_img_padded) = __pad_img(spine_img_cropped, seg_img_cropped,
                                                       blastic_img_cropped, lytic_img_cropped,
                                                       sample_size, crop_z_outside)

    return spine_img_padded, seg_img_padded

class CropPadImg:

    def __init__(self, sample_size, translation=False, centered=False):

        # self.spine_img = spine_img
        # self.seg_img = seg_img
        self.sample_size = sample_size
        #
        # self.blastic_img = blastic_img
        # self.lytic_img = lytic_img

        self.translation = translation
        self.centered = centered

    def __call__(self, img_list):

        spine_img = img_list[0]
        seg_img = img_list[1]
        blastic_img = img_list[2] if len(img_list) > 2 else None
        lytic_img = img_list[3] if len(img_list) > 3 else None

        spine_img, seg_img = crop_and_pad_img(spine_img, seg_img,
                                              sample_size=self.sample_size,
                                              translation=self.translation,
                                              centered=self.centered,
                                              blastic_img=blastic_img,
                                              lytic_img=lytic_img)

        return [spine_img, seg_img]


def determine_padding_amount(img_shape, output_size):
    x_img_length = img_shape[0]
    y_img_length = img_shape[1]
    z_img_length = img_shape[2]

    x_output_length = output_size[0]
    y_output_length = output_size[1]
    z_output_length = output_size[2]

    upper_x = 0
    lower_x = 0

    upper_y = 0
    lower_y = 0

    upper_z = 0
    lower_z = 0

    if x_output_length > x_img_length:
        if (x_output_length - x_img_length) % 2 == 0:
            upper_x = int((x_output_length - x_img_length) / 2)
            lower_x = int((x_output_length - x_img_length) / 2)
        else:
            upper_x = int((x_output_length - x_img_length) / 2) + 1
            lower_x = int((x_output_length - x_img_length) / 2)

    if y_output_length > y_img_length:
        if (y_output_length - y_img_length) % 2 == 0:
            upper_y = int((y_output_length - y_img_length) / 2)
            lower_y = int((y_output_length - y_img_length) / 2)
        else:
            upper_y = int((y_output_length - y_img_length) / 2) + 1
            lower_y = int((y_output_length - y_img_length) / 2)

    if z_output_length > z_img_length:

        if (z_output_length - z_img_length) % 2 == 0:
            upper_z = int((z_output_length - z_img_length) / 2)
            lower_z = int((z_output_length - z_img_length) / 2)

        else:
            upper_z = int((z_output_length - z_img_length) / 2) + 1
            lower_z = int((z_output_length - z_img_length) / 2)

    return lower_x, upper_x, lower_y, upper_y, lower_z, upper_z

class PadNearestPower:
    def __call__(self, img_list):

        spine_img = img_list[0]
        seg_img = img_list[1]

        sample_size = np.array(spine_img.shape)

        nearest_sample_size = 2**np.ceil(np.log2(sample_size)).astype(np.int)

        x0, x1, y0, y1, z0, z1 = determine_padding_amount(sample_size, nearest_sample_size)

        spine_padded = np.pad(spine_img, ((x0, x1),
                                          (y0, y1),
                                          (z0, z1)),
                              mode='constant', constant_values=spine_img.min())


        seg_padded = np.pad(seg_img, ((x0, x1),
                                      (y0, y1),
                                      (z0, z1)),
                            mode='constant', constant_values=seg_img.min())

        blastic_img = None
        lytic_img = None

        return spine_padded, seg_padded, blastic_img, lytic_img

class PadImg:
    def __init__(self, output_sample_size):
        self.output_sample_size = output_sample_size
    def __call__(self, img_list):
        spine_img = img_list[0]
        seg_img = img_list[1]

        input_sample_size = spine_img.shape
        x0, x1, y0, y1, z0, z1 = determine_padding_amount(input_sample_size, self.output_sample_size)

        spine_padded = np.pad(spine_img, ((x0, x1),
                                          (y0, y1),
                                          (z0, z1)),
                              mode='constant', constant_values=spine_img.min())

        seg_padded = np.pad(seg_img, ((x0, x1),
                                      (y0, y1),
                                      (z0, z1)),
                            mode='constant', constant_values=seg_img.min())

        blastic_img = None
        lytic_img = None

        return spine_padded, seg_padded, blastic_img, lytic_img


def determine_cropping_amount(img_shape, output_size):
    x_img_length = img_shape[0]
    y_img_length = img_shape[1]
    z_img_length = img_shape[2]

    x_output_length = output_size[0]
    y_output_length = output_size[1]
    z_output_length = output_size[2]

    upper_x = x_img_length
    lower_x = 0

    upper_y = y_img_length
    lower_y = 0

    upper_z = z_img_length
    lower_z = 0


    if x_img_length > x_output_length:
        if (x_img_length - x_output_length) % 2 == 0:
            upper_x = int(x_img_length - int((x_img_length - x_output_length) / 2))
            lower_x = int((x_img_length - x_output_length) / 2)
        else:
            upper_x = int(x_img_length - int((x_img_length - x_output_length) / 2 + 1))
            lower_x = int((x_img_length - x_output_length) / 2)

    if y_img_length > y_output_length:
        if (y_img_length - y_output_length) % 2 == 0:
            upper_y = int(y_img_length - int((y_img_length - y_output_length) / 2))
            lower_y = int((y_img_length - y_output_length) / 2)
        else:
            upper_y = int(y_img_length - int((y_img_length - y_output_length) / 2 + 1))
            lower_y = int((y_img_length - y_output_length) / 2)

    if z_img_length > z_output_length:
        if (z_img_length - z_output_length) % 2 == 0:
            upper_z = int(z_img_length - int((z_img_length - z_output_length) / 2))
            lower_z = int((z_img_length - z_output_length) / 2)
        else:
            upper_z = int(z_img_length - int((z_img_length - z_output_length) / 2 + 1))
            lower_z = int((z_img_length - z_output_length) / 2)

    return lower_x, upper_x, lower_y, upper_y, lower_z, upper_z



class UniformCropPad:
    def __init__(self, output_sample_size):
        self.output_sample_size = output_sample_size

    def __call__(self, img_list):

        # spine_img = img_list[0]
        # seg_img = img_list[1]
        #
        # spine_img = self.crop(spine_img)
        # seg_img = self.crop(seg_img)
        #
        # spine_img = self.pad(spine_img)
        # seg_img = self.pad(seg_img)
        #
        # return spine_img, seg_img

        return_list = []
        for img in img_list:

            img = self.crop(img)
            img = self.pad(img)

            return_list.append(img)


        if len(return_list) == 1:

            return return_list[0]

        else:
            return return_list


    def crop(self, img):

        input_sample_size = img.shape

        if img.ndim > len(self.output_sample_size):
            input_sample_size = input_sample_size[1:]
            multi_channel = True
        else:
            multi_channel = False

        x0, x1, y0, y1, z0, z1 = determine_cropping_amount(input_sample_size, self.output_sample_size)

        if multi_channel:
            img_cropped = img[:, x0:x1, y0:y1, z0:z1]
        else:
            img_cropped = img[x0:x1, y0:y1, z0:z1]

        return img_cropped

    def pad(self, img):

        input_sample_size = img.shape

        if img.ndim > len(self.output_sample_size):
            input_sample_size = input_sample_size[1:]
            multi_channel = True
        else:
            multi_channel = False

        x0, x1, y0, y1, z0, z1 = determine_padding_amount(input_sample_size, self.output_sample_size)

        if multi_channel:
            img_padded = np.pad(img, ((0, 0),
                                      (x0, x1),
                                      (y0, y1),
                                      (z0, z1)),
                                mode='constant', constant_values=img.min())

        else:
            img_padded = np.pad(img, ((x0, x1),
                                      (y0, y1),
                                      (z0, z1)),
                                mode='constant', constant_values=img.min())

        return img_padded

class DirectionalCropPad:
    def __init__(self, output_sample_size, pad_direction=None, crop_direction=None):
        self.output_sample_size = output_sample_size
        self.pad_direction = pad_direction
        self.crop_direction = crop_direction

    def __call__(self, img_list):
        spine_img = img_list[0]
        seg_img = img_list[1]

        spine_img = self.crop(spine_img)
        seg_img = self.crop(seg_img)

        spine_img = self.pad(spine_img)
        seg_img = self.pad(seg_img)

        blastic_img = None
        lytic_img = None

        return spine_img, seg_img, blastic_img, lytic_img

    def crop(self, img):

        input_sample_size = img.shape
        x0, x1, y0, y1, z0, z1 = determine_cropping_amount(input_sample_size, self.output_sample_size)

        if self.crop_direction == '+z':
            img_cropped = img[x0:x1, y0:y1, 0:z1 - z0]
        elif self.crop_direction == '-z':
            img_cropped = img[x0:x1, y0:y1, -(z1 - z0):]

        elif self.crop_direction == '+y':
            img_cropped = img[x0:x1, 0:y1 - y0, z0:z1]
        elif self.crop_direction == '+y':
            img_cropped = img[x0:x1, -(y1 - y0):, z0:z1]

        elif self.crop_direction == '+x':
            img_cropped = img[0:x1 - x0, y0:y1, z0:z1]
        elif self.crop_direction == '+x':
            img_cropped = img[-(x1 - x0):, y0:y1, z0:z1]

        else:
            img_cropped = img[x0:x1, y0:y1, z0:z1]



        return img_cropped

    def pad(self, img):

        input_sample_size = img.shape
        x0, x1, y0, y1, z0, z1 = determine_padding_amount(input_sample_size, self.output_sample_size)

        if self.pad_direction == '+z':
            img_padded = np.pad(img, ((x0, x1),
                                      (y0, y1),
                                      (0, z1 - z0)),
                                mode='constant', constant_values=img.min())
        elif self.pad_direction == '-z':
            img_padded = np.pad(img, ((x0, x1),
                                      (y0, y1),
                                      (z1 - z0, 0)),
                                mode='constant', constant_values=img.min())

        elif self.pad_direction == '+y':
            img_padded = np.pad(img, ((x0, x1),
                                      (0, y1 - y0),
                                      (z0, z1)),
                                mode='constant', constant_values=img.min())
        elif self.pad_direction == '-y':
            img_padded = np.pad(img, ((x0, x1),
                                      (y1 - y0, 0),
                                      (z0, z1)),
                                mode='constant', constant_values=img.min())


        elif self.pad_direction == '+x':
            img_padded = np.pad(img, ((0, x1 - x0),
                                      (y0, y1),
                                      (z0, z1)),
                                mode='constant', constant_values=img.min())
        elif self.pad_direction == '-x':
            img_padded = np.pad(img, ((x1 - x0, 0),
                                      (y0, y1),
                                      (z0, z1)),
                                mode='constant', constant_values=img.min())


        else:
            img_padded = np.pad(img, ((x0, x1),
                                      (y0, y0),
                                      (z0, z1)),
                                mode='constant', constant_values=img.min())


        return img_padded


class UniformCropPadSimpleITK:
    def __init__(self, output_sample_size):
        self.output_sample_size = output_sample_size

    def __call__(self, img_list):
        # spine_img = img_list[0]
        # seg_img = img_list[1]
        #
        # spine_img = self.crop(spine_img)
        # seg_img = self.crop(seg_img)
        #
        # spine_img = self.pad(spine_img)
        # seg_img = self.pad(seg_img)
        return_img_list = []
        for img in img_list:
            img = self.crop(img)
            img = self.pad(img)

            return_img_list.append(img)

        if len(return_img_list) == 1:
            return return_img_list[0]
        else:
            return return_img_list

    def crop(self, img):
        img_shape = img.GetSize()
        x0, x1, y0, y1, z0, z1 = determine_cropping_amount(img_shape, self.output_sample_size)

        xsize = x1 - x0
        ysize = y1 - y0
        zsize = z1 - z0

        roi_filter = sitk.RegionOfInterestImageFilter()

        roi_filter.SetIndex((x0, y0, z0))
        roi_filter.SetSize((xsize, ysize, zsize))

        img_cropped = roi_filter.Execute(img)

        return img_cropped

    def pad(self, img):
        padding = sitk.ConstantPadImageFilter()

        img_shape = img.GetSize()

        x0, x1, y0, y1, z0, z1 = determine_padding_amount(img_shape, self.output_sample_size)

        padding.SetPadLowerBound((x0, y0, z0))
        padding.SetPadUpperBound((x1, y1, z1))

        img_data = sitk.GetArrayFromImage(img)
        constant = img_data.min()

        padding.SetConstant(constant.astype('float'))

        img_padded = padding.Execute(img)

        return img_padded

if __name__ == '__main__':


    a = np.ones((145, 157, 398))
    output_sample_size = (128, 128, 256)
    test = DirectionalCropPad(output_sample_size=output_sample_size, crop_direction='+z')([a, a])