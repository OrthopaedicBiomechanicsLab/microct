import os
import numpy as np
import pandas as pd
import SimpleITK as sitk


from log.logging_dict_configuration import logging_dict_config
import logging
from logging.config import dictConfig

import Utils.filename_constants as FileConstUtil
import Trainer.GridSearch_Consts as GS_Util
from Models import create_model
from Dataset import select_dataset
from Parameters import construct_params

class Predictor:
    """this class uses a pre-trained model and evaluates the model on the
    test set. It also predicts the outputs and
    """

    def __init__(self,
                 input_source_dir,
                 gt_source_dir,
                 df_testing=None,
                 df_main=None,
                 x_test=None,
                 y_test=None,
                 k_fold=None,
                 model_weights_filename=None,
                 model_directory=None,
                 epoch=None,
                 params=None,
                 params_dict=None,
                 params_json=None,
                 time_stamp=None,
                 save_predictions=False,
                 pred_output_directory=None,
                 predicting_log_name=None,
                 main_output_directory=None,
                 gpu_ids=None,
                 cross_fold_identifier=None,
                 eval_cross_fold=False,
                 params_updates=None,
                 best_epoch=False):
        """
        This is the constructor of the Predictor class

        :param model_weights_filename: the full file name of the model
            weigths(h5 file)
        :param model_json_filename: the full filename of the model json file
        :param input_directory: the test batches live here
        :param output_directory: the predicted masks and the evaluation csv file
            will be saved here
        """

        self.gpu_ids = gpu_ids
        self.params = params
        self.params_dict = params_dict
        self.params_json = params_json

        self.model_weights_filename = model_weights_filename
        self.main_output_directory = main_output_directory
        self.model_directory = model_directory

        self.df_testing = df_testing
        self.df_main = df_main
        self.x_test = x_test
        self.y_test = y_test

        self.cross_fold_identifier = cross_fold_identifier

        self.input_source_dir = input_source_dir
        self.gt_source_dir = gt_source_dir

        self.save_predictions = save_predictions
        self.time_stamp = time_stamp

        self.k_fold = k_fold
        self.epoch = epoch

        self.predicting_log_name = predicting_log_name

        self.pred_output_directory = pred_output_directory

        self.eval_cross_fold = eval_cross_fold
        self.params_updates = params_updates

        self.best_epoch = best_epoch

        # if self.predicting_log_name is None:
        #     if self.time_stamp is not None:
        #         self.predicting_log_name = self.main_output_directory + '/' + self.time_stamp + '/' + self.model_output_directory + '/' + 'predict.log'
        #     else:
        #         self.predicting_log_name = self.main_output_directory + '/' + self.model_output_directory + '/' + 'predict.log'
        self.predicting_log_name = 'test_log.log'
        self.logging_dict = logging_dict_config(self.predicting_log_name)
        dictConfig(self.logging_dict)
        self.logger = logging.getLogger(__name__)
        self.logger_format = logging.Formatter(fmt=self.logging_dict['formatters']['f']['format'],
                                               datefmt=self.logging_dict['formatters']['f']['datefmt'])


        # self.logger.info('evaluation_filename is ' + self.evaluation_filename)


    def load_model(self):
        """
        this method loads the pre-trained model in two steps
        first it loads the json file and then it loads the weights. The
        compilation parameters are also set, which are the same as the
        training time
        """

        self.logger.info('loading the model')
        self.model = create_model(self.params, self.logger)

        if self.model_weights_filename is None:
            # now load weights
            if self.k_fold is not None:
                self.model_name = self.params.model_name + '_k_fold_' + str(self.k_fold)
            else:
                self.model_name = self.params.model_name

            if self.best_epoch is False:
                if self.epoch is not None:
                    weight_filename = self.model_name + '_' + str(self.epoch) + '_weights.pt'
                else:
                    weight_filename = self.model_name + '_' + str(self.params.epochs) + '_weights.pt'

            else:
                weight_filename = self.model_name + '_best_weights.pth'

            self.model_directory = self.params.model_output_directory + '/' + self.model_name

            if os.path.exists(self.params.model_output_directory + '/' + self.model_name):
                self.model_directory = self.params.model_output_directory + '/' + self.model_name

            elif os.path.exists(self.params.model_output_directory):
                self.model_directory = self.params.model_output_directory
            else:
                print('Cant find model directory location')


            self.model_weights_directory = self.model_directory + '/' + weight_filename

        else:
            assert (isinstance(self.model_directory, str) and isinstance(self.model_weights_filename, str)), 'Both must be str'
            self.model_weights_directory = self.model_directory + '/' + self.model_weights_filename

            self.model_name = self.params.model_name

        try:
            self.model.load_weights(self.model_weights_directory)
        except FileNotFoundError:
            try:
                self.model.load_weights(self.model_weights_filename)
            except FileNotFoundError:
                pass
        self.logger.info('model loaded')

    def create_predicted_batch_filename(self, test_batch_filename, predictions_test_target, output_directory=None):
        """
        this method generates a filename for the predictions to be saved to
        the filename is built based on the model_weights_filename and
        test_batch_filename. The file is located at the output_directory

        :param test_batch_filename: this is the filename of the test batch

        """

        if output_directory is None:
            output_directory = self.params.model_output_directory

        base_filename = test_batch_filename.split('.')[0]

        pred_batch_main_path = os.path.join(output_directory, 'prediction')

        if os.path.exists(pred_batch_main_path) is False:
            os.makedirs(pred_batch_main_path)

        pred_batch_filename = pred_batch_main_path + '/' + base_filename + predictions_test_target\
                              + FileConstUtil.NIFTI_EXTENSION()


        return pred_batch_filename

    def load_parameters(self):
        # Must pass some type of params (where dict, class or file) for predictor
        assert (self.params is not None) or (self.params_dict is not None) or (self.params_json is not None)

        if self.params is not None:
            return

        if self.params_json is not None:
            self.params = construct_params(json_file=self.params_json, params_updates=self.params_updates)
        elif self.params_dict is not None:
            self.params = construct_params(params_dict=self.params_dict, params_updates=self.params_updates)
        else:
            raise ValueError('self.params is type None. Must pass params (can be either a json file, dict or params class')





    def create_evaluation_filename(self):
        """
        this method creates a filename for the evaluation results.
        the file is located in the self.output_directory and it is
        built based on the name of the self.model_weights_filename

        Example:
            if the following values are set for the output_directory and the
            model_weights_filename,
            self.output_directory = 'd:/output/'
            self.model_weights_filename = 'd:/weights/weights123.h5'
            then, the self.evaluation_filename will be
            'd:/output/weights123.csv'
        """


        # create a csv filename inside the output_directory
        if self.epoch is not None:
            self.evaluation_filename = os.path.join(self.model_directory,
                                                    ('evaluation_' + self.model_name + '_' + str(self.epoch)))
        else:
            self.evaluation_filename = os.path.join(self.model_directory,
                                                    ('evaluation_' + self.model_name + '_' + str(self.params.epochs)))


    def extract_base_filename(self, filename):
        # get the filename
        base_filename = os.path.basename(filename)
        # remove the extension
        base_filename = os.path.splitext(base_filename)[0].split('_weights')[0]
        return str(base_filename)

    def save_evaluation_file(self, custom_file_name=None):
        """
        this method saves the results of the evaluation into a csv file
        """

        if custom_file_name is not None:
            try:
                self.measures_df.to_csv(custom_file_name + '.csv', index=False)
                self.measures_df.to_pickle(custom_file_name + '.pickle')

                self.logger.info('evaluation measures are saved to:' + custom_file_name)
            except:
                self.logger.error('can not save evaluation measures to: ' + custom_file_name)

        else:
            try:
                self.measures_df.to_csv(self.evaluation_filename + '.csv', index=False)
                self.measures_df.to_pickle(self.evaluation_filename + '.pickle')

                self.logger.info('evaluation measures are saved to:' + self.evaluation_filename)
            except:
                self.logger.error('can not save evaluation measures to: ' + self.evaluation_filename)

    def predict_and_evaluate(self):
        self.load_parameters()
        self.params.gpu_ids = self.gpu_ids
        # load the model


        # Find all model folders based on model name and k_fold values in the params
        try:
            params_k_fold = int(self.params.k_fold)
        except ValueError:
            params_k_fold = self.params.k_fold
        except TypeError:
            params_k_fold = self.params.k_fold

        folds_in_df = self.df_main[self.cross_fold_identifier].unique()

        k_fold_measures_list = []

        self.append_model_list = False
        self.use_model_list = False

        if (isinstance(params_k_fold, int) or isinstance(params_k_fold, float)) and (self.eval_cross_fold is True):
            self.models_list = []
            self.k_fold_total = int(self.params.k_fold)
            self.append_model_list = True
            for fold in range(self.k_fold_total):
                self.k_fold = fold
                self.df_testing = self.df_main.loc[self.df_main[self.cross_fold_identifier].isin([fold])]

                if fold in folds_in_df:
                    fold_idx = np.where(fold == folds_in_df)
                    folds_in_df = np.delete(folds_in_df, fold_idx[0])

                self._run_prediction()

                k_fold_measures_list.append(self.measures_df)

            # Compute with remaining folds and use the average of all cross folds
            for remaining_fold in folds_in_df:

                self.df_testing = self.df_main.loc[ self.df_main[self.cross_fold_identifier].isin([remaining_fold])]

                self.append_model_list = False
                self.use_model_list = True

                self.k_fold = remaining_fold

                self._run_prediction()
                k_fold_measures_list.append(self.measures_df)

            # create a csv filename inside the output_directory
            if self.epoch is not None:
                full_k_fold_eval_filename = os.path.join(self.params.model_output_directory,
                                                         ('evaluation_' + self.params.model_name + '_' + str(self.epoch)))
            else:
                full_k_fold_eval_filename = os.path.join(self.params.model_output_directory,
                                                         ('evaluation_' + self.params.model_name + '_' + str(
                                                            self.params.epochs)))
            self.measures_df = pd.concat(k_fold_measures_list, ignore_index=True)

            self.save_evaluation_file(custom_file_name=full_k_fold_eval_filename)

        else:
            if (self.df_testing is None) and (self.df_main is not None):
                self.df_testing = self.df_main
            self._run_prediction()





    def _run_prediction(self):
        """
        this method evaluates the pre-trained model on the batches of the
        test images, the ground truth masks are also available.
        the evaluations on the batches are all saved in the a file.
        For more information on the name of the evaluation file, refer to
        self.create_evaluation_filename
        """

        # load the model
        if self.use_model_list is False:
            self.load_model()

        if (self.eval_cross_fold is True) and (self.append_model_list is True):
            self.models_list.append(self.model)

        self.measures_df = pd.DataFrame(
            columns=['test_file'] + ['ground_truth'] + ['dataframe_idx'] + self.model.metric_names)

        self.create_evaluation_filename()

        # load the test data
        self.logger.info('evaluation started')

        dataset_fn = select_dataset(self.params)

        test_generator = dataset_fn(self.params, self.df_testing, self.input_source_dir, self.gt_source_dir,
                                    isTrain=False)


        # x_test = self.df_testing['spine_filename'].to_list()
        # y_test = self.df_testing['seg_filename'].to_list()

        idx = 0
        prediction = None
        for df_idx, row in self.df_testing.iterrows():
            print(('loading batch ' + str(idx + 1) + ' of ' + str(len(self.df_testing))))

            test_batch = test_generator.get_batch(idx, batch_size=1)

            self.logger.info('Prediction and evaluating batch ' + str(idx + 1))

            if self.save_predictions is True:
                if self.use_model_list is False:
                    prediction, results = self.model.test_and_eval(test_batch)
                else:
                    prediction_sum = 0
                    num_mods = 0
                    for num_mods, model in enumerate(self.models_list):
                        prediction = model.eval(test_batch)
                        prediction_sum = prediction_sum + prediction
                    prediction = prediction_sum / (num_mods + 1)
                    results = self.model.calc_metrics_with_inputs(prediction, test_batch['label'])
            else:
                if self.use_model_list is False:
                    results = self.model.test(test_batch)
                else:
                    prediction_sum = 0
                    num_mods = 0
                    for num_mods, model in enumerate(self.models_list):
                        prediction = model.eval(test_batch)
                        prediction_sum = prediction_sum + prediction
                    prediction = prediction_sum / (num_mods + 1)
                    results = self.model.calc_metrics_with_inputs(prediction, test_batch['label'])


            sample_name, label_name, additional = test_generator.get_sample_names(idx)

            dict_hold = {'test_file': sample_name,
                         'ground_truth': label_name,
                         'dataframe_idx': df_idx}


            if 'k_fold' in additional.keys():
                dict_hold['k_fold'] = additional['k_fold']

            for metric in results.keys():
                dict_hold[metric] = results[metric]
            self.measures_df = self.measures_df.append(dict_hold, ignore_index=True)

            if self.save_predictions is True:
                sample_filename = self.create_predicted_batch_filename(sample_name,
                                                                       FileConstUtil.SAMPLE_DATA(),
                                                                       output_directory=self.pred_output_directory)
                ground_truth_filename = self.create_predicted_batch_filename(sample_name, FileConstUtil.GROUND_TRUTH(),
                                                                             output_directory=self.pred_output_directory)
                predicted_filename = self.create_predicted_batch_filename(sample_name, FileConstUtil.PREDICTION(),
                                                                          output_directory=self.pred_output_directory)

                self.logger.info('Saving the predictions to ' + predicted_filename)

                if isinstance(prediction, list) or isinstance(prediction, tuple):
                    prediction = prediction[0]

                if self.gpu_ids is None:
                    pred_mask = prediction.numpy()
                else:
                    pred_mask = prediction.cpu().numpy()


                sample_img = test_batch['image'].numpy()
                sample_gt = test_batch['label'].numpy()

                # soft_int = (pred_mask * sample_gt).sum()
                # soft_dsc = (2. * soft_int) / (np.sum(sample_gt) + np.sum(pred_mask))
                #
                pred_mask = np.where(pred_mask > 0.5, 1, 0)
                #
                # hard_int = (pred_mask * sample_gt).sum()
                # hard_dsc = (2. * hard_int) / (np.sum(sample_gt) + np.sum(pred_mask))



                predicted_masks_img = sitk.GetImageFromArray(pred_mask[0, 0, :, :, :].astype(np.float32))
                ground_truth_img = sitk.GetImageFromArray(sample_gt[0, 0, :, :, :])
                sample_img = sitk.GetImageFromArray(sample_img[0, 0, :, :, :])


                sitk.WriteImage(predicted_masks_img, predicted_filename)
                sitk.WriteImage(ground_truth_img, ground_truth_filename)
                sitk.WriteImage(sample_img, sample_filename)

                self.logger.info(
                'Evaluation completed on batch ' + str(idx + 1))

            idx += 1
        if self.save_predictions is False:
            self.logger.info('Predictions were not saved as save_predictions = ' + str(self.save_predictions))

        self.save_evaluation_file()
