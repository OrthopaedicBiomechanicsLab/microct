import torch
import numpy as np
import random

np.random.seed(1)
torch.random.manual_seed(1)
random.seed(1)

from Trainer import Trainer
import pandas as pd

if __name__ == '__main__':

    # Define where the samples and segmentation files are located. These could be the same or different locations
    sample_main_path = '/home/hmahdi/Microct/MicroCT/Machine Learning/downsampled_new'
    seg_main_path = '/home/hmahdi/Microct/MicroCT/Machine Learning/downsampled_new'
    output_dir = "micro_ct"
    # Load the dataframe which has sample and corresponding segmentation file names. I load a single dataframe and split
    # the dataframe based on some predefined organizations.
    df_main = pd.read_csv('Dataframes/microCT_osteo_data.csv') #osteokneedata was old data that wasn't cropped


    df_train = df_main.loc[(df_main['cross_fold_number'] == 0) |
                           (df_main['cross_fold_number'] == 'training')]

    df_val = df_main.loc[df_main['cross_fold_number'] == 1]

    augmentation_params = {"rot_ang": None, "shear": None, "scale_factor": (0.9, 1.1), "x_flip": True, "y_flip": True, "z_flip": False, "elastic": True}
    training_params = dict(
        model= 'UNet', #'R2AttUNet',  # The model to be run.
        sample_size=(64, 128, 128),  # Sample sizes. Does not include batch or channel dimensions.
        epochs=500,  # Total number of epochs
        depth=5,  # Depth of the UNet
        base_filter=16,  # Base filter size for UNet. Increases with each depth
        kernel_size=3,  # kernel_size for UNet
        num_in_channels=1,  # Number of input channels. Can remove, default is 1
        num_out_channels=1,  # Number of output channels. Can remove, default is 1
        batch_size=6,  # Batch size. This is the total batch size during each epoch for all GPUs.
            # ie, 6 imgs / 2 GPU = 3 imgs / GPU
        lr=1e-3,  # Learning rate
        normalization=True,  # Boolean for normalization
        norm_type='batch',  # Normalization type. Options are batch or instance
        augmentation=True,  # Boolean for affine augmentation
        augmentation_params=augmentation_params,
        #batch_normalization = True,
        intensity_augmentation=False,
        translation_augmentation=True,  # Boolean to determine if translation augmentation works.
        dropout=0,
        #loss=['tversky_loss'],  # The loss function(s) for training. bce_logits is binary cross entropy with logits was bce_logits
        loss =  ['volume_ratio_loss', 'tversky_loss', 'focal_tversky_loss'],
        loss_weights = [1, 1, 0.5],
        metrics=['dsc', 'volume_ratio_gt', 'volume_ratio_pred'],  # The metrics you want to use. Can refer to LossMetricFunctions/loss_metric_utils.py
        dataset='OsteoKneeSeg',  # The data generator
        isTrain=True,  # Leave as True, remnant of some older workflow
        num_threads=8,  # Number of workers for the data loader
        shuffle=True,  # Leave as True, remnant of some older workflow. Not even sure this is used anymore.
        gpu_ids='0',  # Define what GPUs are being used. If unsure, set to 0 single GPU.
        is_distributed=False,  # Leave false. Not sure if distributed is working
        save_best_epoch =True, # need to test this. Note sure where to declare the parameter but it's used in base model
        )

    # Where you want the files to end up. Based on the relative location of this file (ie, the training script file)


    trainer = Trainer(training_params=training_params,
                      output_directory=output_dir,
                      df_training=df_train,
                      df_validation=df_val,
                      input_source_dir=sample_main_path,
                      gt_source_dir=seg_main_path,
                      CV=False,  # If running cross validation
                      run_validation=False,  # Runs a separate validation after training with the validation data. This
                        # is not always necessary and is not mutually exclusive with after epoch validation runs.
                      cross_fold_identifier='cross_fold_number',  # When running with cross validation, tells the
                        # pipline which column in the dataframe to split across. Necessary when CV == False.
                      relative_save_weight_period=10,  # Refer to Trainer/Trainer.py for a complete explanation.
                      verbose=1,  # Just leave at 1.
                      )

    trainer.train()
