# MicroCT

## This repo is dedicated for automatic segmentation and analysis of microct images from mice knees

***
***

## Training
A sample training script, [`sample_training_script_microCT.py`](sample_training_script_microCT.py), shows how to
run the pipeline. To get the pipeline working a data generator needs to defined by the 
user. This can be placed anywhere. A sample generator 
[`OsteoKneeSeg.py`](Dataset/OsteoKneeSeg.py) shows an example of what to do. 

The generator functions are based on pandas dataframes which are passed into the
generator from the [`Trainer`](Trainer/Trainer.py) class. These can be as simple as a single
column dataframe.

### Important TODO:
Currently the generator [`OsteoKneeSeg.py`](Dataset/OsteoKneeSeg.py) requires a hardcoded path to be changed for saving nrrds. This is suboptimal because you would need to change it everytime you switch machines etc. Please edit so that it gets a path from the training script.  





## Prediction
For prediction, modify [`microCT_pred_runfile.py`](microCT_pred_runfile.py), and run it.


## Analysis
For stereological analysis run the [`OBL_scripts_microct_analysis/threshold_and_morphometry.py`](OBL_scripts_microct_analysis/threshold_and_morphometry.py) inside hamza_scripts_microct_analysis. hamza_scripts_microct_analysis also contains other scripts such as upsampling predictions and doing statistical analysis. It is recommended to upsample your predictions before doing any analysis on them. There is a jupyter notebook for upsampling predictions: [`OBL_scripts_microct_analysis/upsample_predictions.ipynb`](OBL_scripts_microct_analysis/upsample_predictions.ipynb) 


Please remember to change the paths to load and save data to in the analysis scripts.


## Dataset
The dataset is under the `MicroCT/Machine Learning/downsampled_new` folder. This is a downsampled version of the original scans/segmentations. Please check tacitus for the original dataset.


## Current Models
Please see the `micro_ct/UNet` folder for the latest model/results. The trainer stores logs of previous training iterations if you'd like to dive deep into model analysis.

## Requirements
Here are some essential libraries and their versions at the time of release. Some secondary requirements may be missing from this list:
 
 
 
 package             | version     
---------------------|-------------
 python              | 3.7.7       
 pytorch             | 1.6.0       
 torchvision         | 0.7.0       
 cudatoolkit         | 10.2.89     
 jupyterlab          | 2.3.2       
 itk                 | 5.2.1.post1 
 itk-bonemorphometry | 1.3.0       
 itk-core            | 5.2.1.post1 
 itk-filtering       | 5.2.1.post1 
 itk-io              | 5.2.1.post1 
 itk-numerics        | 5.2.1.post1 
 itk-registration    | 5.2.1.post1 
 itk-segmentation    | 5.2.1.post1 
 matplotlib-base     | 3.3.1       
 pandas              | 1.2.4       
 scikit-image        | 0.17.2      
 scipy               | 1.6.2       
 torchio             | 0.18.70     
 nibabel             | 3.2.1       
 numpy               | 1.19.1      
 numpy-base          | 1.19.1      
 simpleitk           | 2.1.1       

A full list of the requirements can be seen in the [`conda_requirements.txt`](conda_requirements.txt)
and an associated `yaml` file can be seen in [`conda_env.yml`](conda_env.yml).

