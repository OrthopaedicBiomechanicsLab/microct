import os
# import multiprocessing as mp
# from multiprocessing import Process, Queue

import torch.multiprocessing as mp

from logging.handlers import QueueHandler, QueueListener
import logging

from log import logging_dict_config
from logging.config import dictConfig as logging_dictConfig
import logging.config
import threading


class BaseModel:

    def __init__(self, logger):
        self.logger = logger

        self.num_gpus = 2

    @staticmethod
    def logger_thread(q):
        while True:
            record = q.get()
            if record is None:
                break
            logger = logging.getLogger(record.name)
            logger.handle(record)

    def distributed_training_hold(self, gpu, queue):
        queue_handler = logging.handlers.QueueHandler(queue)

        root = logging.getLogger()
        root.setLevel(logging.NOTSET)
        root.addHandler(queue_handler)

        logger = logging.getLogger()
        logger.addHandler(queue_handler)
        logger.info('fuckls')

        logging.info(f'GPU: {gpu}')

    def fit(self):
        """ Train the model based on the inputs."""


        os.environ["MASTER_ADDR"] = "127.0.0.1"
        os.environ["MASTER_PORT"] = "29500"


        self.num_process_per_node = self.num_gpus
        self.num_nodes = 1
        self.node_rank = 0  # Ranking within the different nodes. This would need to be edited for multi-node.
        self.world_size = self.num_process_per_node * self.num_nodes
        self.num_gpus = self.num_gpus

        # mp.set_start_method('spawn')
        queue_logger = mp.Queue(-1)
        workers = []


        lp = threading.Thread(target=self.logger_thread, args=(queue_logger,))
        lp.start()

        # for gpu in range(self.num_gpus):
        #     wp = Process(target=self.distributed_training_hold, args=(gpu, queue_logger))
        #     workers.append(wp)
        #     wp.start()

        mp.spawn(self.distributed_training_hold, nprocs=self.num_gpus, args=(queue_logger, ))


        # At this point, the main process could do some useful work of its own
        # Once it's done that, it can wait for the workers to terminate...
        # for wp in workers:
        #     wp.join()
        # And now tell the logging thread to finish up, too
        queue_logger.put(None)
        lp.join()





if __name__ == '__main__':
    mp.set_start_method('spawn')
    logging_dict = logging_dict_config('log.log')
    logging_dictConfig(logging_dict)
    logger = logging.getLogger(__name__)
    logger_format = logging.Formatter(fmt=logging_dict['formatters']['f']['format'],
                                           datefmt=logging_dict['formatters']['f']['datefmt'])
    logger.info('Training data can be found in: ' + 'log.log')


    base = BaseModel(logger)

    base.fit()


    # q = Queue()
    # # d = {
    # #     'version': 1,
    # #     'formatters': {
    # #         'detailed': {
    # #             'class': 'logging.Formatter',
    # #             'format': '%(asctime)s %(name)-15s %(levelname)-8s %(processName)-10s %(message)s'
    # #         }
    # #     },
    # #     'handlers': {
    # #         'console': {
    # #             'class': 'logging.StreamHandler',
    # #             'level': 'INFO',
    # #         },
    # #         'file': {
    # #             'class': 'logging.FileHandler',
    # #             'filename': 'mplog.log',
    # #             'mode': 'w',
    # #             'formatter': 'detailed',
    # #         },
    # #         'foofile': {
    # #             'class': 'logging.FileHandler',
    # #             'filename': 'mplog-foo.log',
    # #             'mode': 'w',
    # #             'formatter': 'detailed',
    # #         },
    # #         'errors': {
    # #             'class': 'logging.FileHandler',
    # #             'filename': 'mplog-errors.log',
    # #             'mode': 'w',
    # #             'level': 'ERROR',
    # #             'formatter': 'detailed',
    # #         },
    # #     },
    # #     'loggers': {
    # #         'foo': {
    # #             'handlers': ['foofile']
    # #         }
    # #     },
    # #     'root': {
    # #         'level': 'DEBUG',
    # #         'handlers': ['console', 'file', 'errors']
    # #     },
    # # }
    # # logging.config.dictConfig(d)
    #
    #
    #
    # workers = []
    # for i in range(5):
    #     wp = Process(target=BaseModel.worker_process, name='worker %d' % (i + 1), args=(q,))
    #     workers.append(wp)
    #     wp.start()
    #
    # lp = threading.Thread(target=BaseModel.logger_thread, args=(q,))
    # lp.start()
    # # At this point, the main process could do some useful work of its own
    # # Once it's done that, it can wait for the workers to terminate...
    # for wp in workers:
    #     wp.join()
    # # And now tell the logging thread to finish up, too
    # q.put(None)