import torch.multiprocessing as mp



def process_A(gpu, cond, state_value):

    a = 1
    b = 2
    c = 4

    print(f'testing, {gpu}')


    for idx in range(100):
        print(idx, gpu)
        state_value.value = 0
        with cond:
            cond.notify_all()
            cond.wait_for(lambda: state_value.value == 1)




def process_B(cond, state_value):
    with cond:
        while(True):
            cond.wait_for(lambda: state_value.value != 1)

            print(mp.current_process().name)

            if state_value.value == 0:
                print('Doing Process B')
                state_value.value = 1
                cond.notify_all()

            else:
                print('Exiting Process B')
                state_value.value = -1
                break



if __name__ == '__main__':

    mp.set_start_method('spawn')

    condition = mp.Condition()
    state = mp.Value('i', 1)


    # proc_a = mp.Process(target=process_A, args=(condition, state))

    proc_b = mp.Process(target=process_B, args=(condition, state))
    proc_b.start()
    mp.spawn(process_A, nprocs=2, args=(condition, state))

    # proc_a.start()

    with condition:
        state.value = -1
        condition.notify_all()

    # print(f'Exited cond, {gpu}')


    # proc_a.join()
    proc_b.join()

    proc_b.close()
    # proc_a.close()
