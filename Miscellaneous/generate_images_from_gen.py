from Dataset.SpineSelfSupervised import SpineSelfSupervised
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


if __name__ == '__main__':

    import numpy as np
    import torch

    np.random.seed(81)
    torch.random.manual_seed(81)

    from Parameters import construct_params
    import SimpleITK as sitk

    df = pd.read_pickle('../Dataframes/spine_seg_dataframe_self_super.pickle')

    spine_main_path = 'W:/SpineSamplesBlocked/numpy'
    seg_main_path = 'W:/SpineSamplesBlocked/numpy'

    sample_size = (256, 128, 128)

    affine_aug_params = {'rot_ang': (15, 15, 15),
                         'shear': None,
                         'scale_factor': (0.8, 1.2),
                         'x_flip': True,
                         'y_flip': True,
                         'z_flip': True,
                         'elastic': True}


    params_dict = dict(model='UNet',
                       sample_size=sample_size,
                       augmentation=False,
                       augmentaiton_params=affine_aug_params,
                       translation_augmentation=False,
                       load_all_data=False,
                       translation_args={'img_in_frame': False,
                                         'center': False},
                       dataset='SpineSelfSupervised',
                       # intensity_augmentation='intensity_perturbation',
                       pre_train_method='remove_slices',
                       # pre_train_method_args={'num_slices': 5,
                       #                        'num_blocks': 4},
                       pre_train_method_args={'num_slices': 5,
                                              'num_blocks': 6},
                       )

    params = construct_params(params_dict=params_dict)

    data_gen = SpineSelfSupervised(params, df, spine_main_path, seg_main_path, isTrain=True)

    # random_idx = np.random.randint(0, df.shape[0])
    # print(i)

    random_idx = 22

    sample = data_gen.__getitem__(random_idx=random_idx)

    img_out = sample['image'].numpy()[0]
    label_out = sample['label'].numpy()[0]

    # orig_img = sample['original'].numpy()

    # img = sitk.GetImageFromArray(img_out)
    # label = sitk.GetImageFromArray(label_out)
    # sitk.WriteImage(label, 'label.nii.gz')
    # sitk.WriteImage(img, 'hold.nii.gz')

    slice_idx = 60



    # Create 2x2 sub plots
    gs = gridspec.GridSpec(1, 2)

    fig = plt.figure()
    # ax1 = fig.add_subplot(gs[0, 0])
    # ax1.imshow(orig_img[::-1, :, slice_idx], cmap='gray')
    # plt.title('Original Image')
    # plt.axis('off')

    ax2 = fig.add_subplot(gs[0, 0])
    ax2.imshow(img_out[::-1, :, slice_idx], cmap='gray')
    plt.title('Input')
    plt.axis('off')

    ax3 = fig.add_subplot(gs[0, 1])
    ax3.imshow(label_out[::-1, :, slice_idx], cmap='gray')
    plt.axis('off')
    plt.title('Ground Truth')


    plt.savefig('self_supervised_ex.png', bbox_inches='tight', pad_inches=0.25, dpi=800)

    plt.show()