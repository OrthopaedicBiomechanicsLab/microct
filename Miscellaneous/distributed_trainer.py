import torch
import numpy as np
import pandas as pd
from Parameters import construct_params
import os
import torch.multiprocessing as mp
import torch.distributed as dist
import torch.nn as nn
from Models.UNet.unet_base import UNet
from Dataset import CustomDataLoader
from Dataset import select_dataset

from datetime import datetime

from Visualization.Utils import ProgressBar

from LossMetricFunctions.loss_metric_utils import dsc as DSC_func
from torch.utils.data import DataLoader


def train(gpu, params):
    # Training complete in: 0:45:45.832184
    #
    # Training complete in (with dataset loading): 0:46:10.379139

    start_full = datetime.now()

    np.random.seed(1)
    torch.random.manual_seed(1)

    batch_size = params.batch_size
    epochs = params.epochs

    df_main = pd.read_pickle('Dataframes/spine_seg_dataframe_with_tumour_numpy.pickle')
    df_main = df_main.loc[df_main['seg_filename'].str.endswith('TumourVBnoCortSeg.npy')]

    spine_main_path = '/localdisk1/GeoffKlein/SegmentationData/Segmentation_Data_1_1_1/SpineSamples_With_Tumour_Fractured_Numpy'
    seg_main_path = '/localdisk1/GeoffKlein/SegmentationData/Segmentation_Data_1_1_1/SegSamples_With_Tumour_Fractured_Numpy'

    df_train = df_main.loc[(df_main['cross_fold_number'] == 0) |
                           (df_main['cross_fold_number'] == 1) |
                           (df_main['cross_fold_number'] == 2) |
                           (df_main['cross_fold_number'] == 3)]


    # df_val = df_main.loc[df_main['cross_fold_number'] == 4]

    gpu_ids = ['0', '1', '2']


    device = torch.device('cuda:0')

    net = UNet(params)

    torch.cuda.set_device(gpu)
    net = torch.nn.DataParallel(net)
    net.to(device)
    dataset_fn = select_dataset(params)

    dataset_train = dataset_fn(params, df_train, spine_main_path, seg_main_path,
                               isTrain=True)

    train_loader = CustomDataLoader(params, dataset_train,
                                    num_threads=params.num_threads, shuffle=True,
                                    drop_last=True)

    criterion = nn.BCELoss().to(device)

    optimizer = torch.optim.Adam(net.parameters(), 1e-3)

    start_training = datetime.now()
    total_step = len(train_loader)

    n_train = int(len(dataset_train) + 1)
    pbar = ProgressBar(batch_size, n_train, epochs, verbose=1)

    for epoch in range(epochs):

        pbar.initialize_progbar(epoch)
        batch_step = 0
        train_metrics = {'dsc': []}
        running_metrics = {'dsc': []}

        train_metrics['loss'] = []
        running_metrics['loss'] = []

        for i, sample in enumerate(train_loader):
            images = sample['image'].to(device)
            labels = sample['label'].to(device)
            # Forward pass
            outputs = net(images)
            loss = criterion(outputs, labels)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            batch_metric = {}

            batch_metric['loss'] = loss.item()
            batch_metric['dsc'] = float(DSC_func(outputs, labels, device=None))

            for metric in batch_metric.keys():
                running_metrics[metric].append(batch_metric[metric])

            batch_step += 1

            if gpu == 0:
                pbar.update(batch_step, running_metrics)

            # if (i + 1) % len(dataset_train) == 0 and gpu == 0:
            #     print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(
            #         epoch + 1,
            #         epochs,
            #         i + 1,
            #         total_step,
            #         loss.item())
            #        )

        pbar.close()
    if gpu == 0:
        close_time = datetime.now()
        print("Training complete in: " + str(close_time - start_training))
        print()
        print("Training complete in (with dataset loading): " + str(close_time - start_full))

    # torch.save(net.state_dict(), 'normal_parallel.pt')

    # checkpoint = {'model': net,
    #               'state_dict': net.state_dict(),
    #               'optimizer': optimizer.state_dict()}
    #
    # torch.save(checkpoint, 'checkpoint.pth')

    torch.save(net, 'save_total.pt')

def train_dist(gpu, params):

    # Training complete in: 0:46: 09.510225
    #
    # Training complete in (with dataset loading): 0:46: 24.955369

    start_full = datetime.now()

    rank = params.nr * params.gpus + gpu

    dist.init_process_group(
        backend='nccl',
        init_method='env://',
        world_size=params.world_size,
        rank=rank)

    np.random.seed(1)
    torch.random.manual_seed(1)

    batch_size = params.batch_size
    epochs = params.epochs

    batch_size = params.batch_size
    epochs = params.epochs

    df_main = pd.read_pickle('Dataframes/spine_seg_dataframe_with_tumour_numpy.pickle')
    df_main = df_main.loc[df_main['seg_filename'].str.endswith('TumourVBnoCortSeg.npy')]

    spine_main_path = '/localdisk1/GeoffKlein/SegmentationData/Segmentation_Data_1_1_1/SpineSamples_With_Tumour_Fractured_Numpy'
    seg_main_path = '/localdisk1/GeoffKlein/SegmentationData/Segmentation_Data_1_1_1/SegSamples_With_Tumour_Fractured_Numpy'

    df_train = df_main.loc[(df_main['cross_fold_number'] == 0) |
                           (df_main['cross_fold_number'] == 1) |
                           (df_main['cross_fold_number'] == 2) |
                           (df_main['cross_fold_number'] == 3)]

    df_val = df_main.loc[df_main['cross_fold_number'] == 4]

    dataset_fn = select_dataset(params)

    dataset_train = dataset_fn(params, df_train, spine_main_path, seg_main_path,
                               isTrain=True)


    net = UNet(params)
    torch.cuda.set_device(gpu)
    net.cuda(gpu)

    criterion = nn.BCELoss().cuda(gpu)
    optimizer = torch.optim.Adam(net.parameters(), 1e-3)


    net = nn.parallel.DistributedDataParallel(net, device_ids=[gpu])


    trainer_sampler = torch.utils.data.distributed.DistributedSampler(dataset_train,
                                                                      num_replicas=params.world_size,
                                                                      rank=rank)

    train_loader = DataLoader(
        dataset=dataset_train,
        batch_size=params.batch_size,
        shuffle=False,
        num_workers=int(params.num_threads),
        pin_memory=True,
        sampler=trainer_sampler)


    start_training = datetime.now()
    total_step = len(train_loader)

    n_train = int(len(dataset_train) + 1)

    if gpu == 0:
        pbar = ProgressBar(batch_size, n_train, epochs, verbose=1)

    for epoch in range(epochs):
        if gpu == 0:
            pbar.initialize_progbar(epoch)
        batch_step = 0
        train_metrics = {'dsc': []}
        running_metrics = {'dsc': []}

        train_metrics['loss'] = []
        running_metrics['loss'] = []

        for i, sample in enumerate(train_loader):
            images = sample['image'].cuda(non_blocking=True)
            labels = sample['label'].cuda(non_blocking=True)
            # Forward pass
            outputs = net(images)
            loss = criterion(outputs, labels)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            batch_metric = {}

            batch_metric['loss'] = loss.item()
            batch_metric['dsc'] = float(DSC_func(outputs, labels, device=None))

            for metric in batch_metric.keys():
                running_metrics[metric].append(batch_metric[metric])

            batch_step += 1

            if gpu == 0:
                pbar.update(batch_step, running_metrics)

            # if (i + 1) % len(dataset_train) == 0 and gpu == 0:
            #     print('Epoch [{}/{}], Step [{}/{}], Loss: {:.4f}'.format(
            #         epoch + 1,
            #         epochs,
            #         i + 1,
            #         total_step,
            #         loss.item())
            #        )

        if gpu == 0:
            pbar.close()

    if gpu == 0:
        close_time = datetime.now()
        print("Training complete in: " + str(close_time - start_training))
        print()
        print("Training complete in (with dataset loading): " + str(close_time - start_full))


    torch.save(net.state_dict(), 'distributed_parallel.pt')


def main():

    # load_all_data must be False for distributed on Oberon. Unclear if this is necessary if more memory is available.

    params_dict = dict(model='UNet',
                       sample_size=(64, 128, 128),
                       epochs=80,
                       depth=5,
                       base_filter=16,
                       num_classes=1,
                       num_out_channels=1,
                       batch_size=12,
                       load_all_data=True,
                       lr=1e-3,
                       batch_normalization=True,
                       augmentation=True,
                       # residuals=False,
                       # dropout=0,
                       loss=['bce'],
                       metrics=['dsc'],
                       dataset='SpineSegDataSet',
                       isTrain=True,
                       num_threads=10,
                       shuffle=True,
                       gpu_ids='0,1,2',
                       single_vert_crop=True,
                       crop_pad=True)

    os.environ['CUDA_VISIBLE_DEVICES'] = '0,1,2'

    params = construct_params(params_dict=params_dict)

    nproc_per_node = len(params.gpu_ids)
    nnodes = 1

    dist_world_size = nproc_per_node * nnodes

    params.nodes = 1
    params.gpus = 2
    params.nr = 0  # Ranking within the different nodes

    params.world_size = dist_world_size

    current_env = os.environ.copy()
    os.environ["MASTER_ADDR"] = "127.0.0.1"
    os.environ["MASTER_PORT"] = "29500"
    # current_env["WORLD_SIZE"] = str(dist_world_size)



    mp.spawn(train_dist, nprocs=len(params.gpu_ids), args=(params,))

    # train(0, params)


def load_checkpoint(filepath='checkpoint.pth'):

    params_dict = dict(model='UNet',
                       sample_size=(64, 128, 128),
                       epochs=1,
                       depth=3,
                       base_filter=8,
                       num_classes=1,
                       num_out_channels=1,
                       batch_size=3,
                       load_all_data=False,
                       lr=1e-3,
                       batch_normalization=True,
                       augmentation=True,
                       # residuals=False,
                       # dropout=0,
                       loss=['bce'],
                       metrics=['dsc'],
                       dataset='SpineSegDataSet',
                       isTrain=True,
                       num_threads=5,
                       shuffle=True,
                       gpu_ids='0',
                       single_vert_crop=True,
                       crop_pad=True)

    os.environ['CUDA_VISIBLE_DEVICES'] = '0'

    params = construct_params(params_dict=params_dict)



    df_main = pd.read_pickle('Dataframes/spine_seg_dataframe_with_tumour_numpy.pickle')
    df_main = df_main.loc[df_main['seg_filename'].str.endswith('TumourVBnoCortSeg.npy')]

    spine_main_path = '/localdisk1/GeoffKlein/SegmentationData/Segmentation_Data_1_1_1/SpineSamples_With_Tumour_Fractured_Numpy'
    seg_main_path = '/localdisk1/GeoffKlein/SegmentationData/Segmentation_Data_1_1_1/SegSamples_With_Tumour_Fractured_Numpy'

    df_val = df_main.loc[df_main['cross_fold_number'] == 4]

    device = torch.device('cuda:0')

    net = UNet(params)

    net.to(device)
    dataset_fn = select_dataset(params)

    dataset_val = dataset_fn(params, df_val, spine_main_path, seg_main_path,
                             isTrain=False)


    sample = dataset_val.__getitem__(0)

    images = sample['image'].to(device)
    labels = sample['label'].to(device)

    images = images.unsqueeze(0)
    labels = labels.unsqueeze(0)

    checkpoint = torch.load(filepath)
    model = checkpoint['model']
    model.load_state_dict(checkpoint['state_dict'])
    for parameter in model.parameters():
        parameter.requires_grad = False

    model.eval()

    pred = model(images)

    dsc = float(DSC_func(pred, labels, device=None))

    print(dsc)

if __name__ == '__main__':

    main()

    # load_checkpoint()