import numpy as np
import torch.nn.functional as F
ALPHA = 0.2
BETA = 0.8


def TverskyLoss(self, inputs, targets, smooth=1, alpha=ALPHA, beta=BETA):
    # comment out if your model contains a sigmoid or equivalent activation layer
    # inputs = F.sigmoid(inputs)

    # flatten label and prediction tensors
    inputs = inputs.view(-1)
    targets = targets.view(-1)

    # True Positives, False Positives & False Negatives
    TP = (inputs * targets).sum()
    FP = ((1 - targets) * inputs).sum()
    FN = (targets * (1 - inputs)).sum()

    Tversky = (TP + smooth) / (TP + alpha * FP + beta * FN + smooth)

    return 1 - Tversky
