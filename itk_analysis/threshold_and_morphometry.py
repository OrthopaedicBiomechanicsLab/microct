import pandas as pd
import numpy as np
import SimpleITK as sitk
from torch.utils import data
import nibabel as nib
from skimage.filters import threshold_minimum
from skimage.filters import threshold_otsu
from skimage.filters import threshold_isodata
from skimage.filters import threshold_li
from skimage.filters import threshold_triangle
from skimage.filters import threshold_yen 
from skimage.filters import threshold_mean
from skimage.filters import try_all_threshold
import numpy.ma as ma
import matplotlib.pyplot as plt
# python -m pip install itk-bonemorphometry
import itk
import torchio as tio
import os

# module from https://github.com/gregpost/ITK-SimpleITK-Converter
from itk_sitk_converter import ConvertSimpleItkImageToItkImage
from itk_sitk_converter import CopyImageMetaInformationFromSimpleItkImageToItkImage


# threshold_methods = {'min': threshold_minimum, 'otsu': threshold_otsu, 'isodata': threshold_isodata,
# 'li': threshold_li, 'triangle': threshold_triangle, 'yen': threshold_yen, 'mean': threshold_mean}
threshold_methods = {'mean': threshold_mean}

seg_mode = 'full'

# paths to data
image_dir = r"C:\Users\NEW-PC-1\Documents\microCT\Dataset\original_res\original_scans"
seg_dir = r'C:\Users\NEW-PC-1\Documents\microCT\Dataset\original_res\upsampled_prediction_unet_volume_ratio_mean'
analysis_dir = r'C:\Users\NEW-PC-1\Documents\microCT\Dataset\Analysis\auto_itk'



'''
morphometry using the itk library
'''
def calc_morphometry(sitk_image, sitk_mask, thresh):
    # morphometry module wants itk images. This seems very slow 
    itk_image = ConvertSimpleItkImageToItkImage(sitk_image, itk.F)
    itk_mask = ConvertSimpleItkImageToItkImage(sitk_mask, itk.UC)
    itk_mask = CopyImageMetaInformationFromSimpleItkImageToItkImage(itk_mask,sitk_image, itk.UC) # copies origin and spacing info

    # itk_image = itk.GetImageFromArray(np.ascontiguousarray(im))
    # itk_mask = itk.GetImageFromArray(np.ascontiguousarray(mask))
    filtr = itk.BoneMorphometryFeaturesFilter.New(itk_image)
    filtr.SetMaskImage(itk_mask)
    filtr.SetThreshold(int(thresh))
    filtr.Update()
    return (filtr.GetBVTV(),  filtr.GetTbN(), filtr.GetTbTh(), filtr.GetTbSp(), filtr.GetBSBV())

'''
Takes in a segmentation alongside the original bone and calcultes the total volume and the bone volume using the spacing in mm
'''
def calc_volumes(image, segmentation, threshold, spacing):
    image[segmentation==0] = 0 # delete all outside the image 
    bone = image>threshold
    return np.sum(bone) * spacing, np.sum(segmentation==1)*spacing


def pre_load_files(image_dir, seg_dir):
    img_files = [f for f in os.listdir(image_dir) if os.path.isfile(os.path.join(image_dir, f))]
    img_files.sort()
    print(img_files)
    seg_files = [f for f in os.listdir(seg_dir) if os.path.isfile(os.path.join(seg_dir, f))]
    seg_files.sort()
    print(seg_files)

    if len(img_files) != len(seg_files):
        print("Error: Different number of scan and segmentation files.")
        sys.exit(1)

    imgs = []
    segs = []
    # tio is a lazy loader hence preload
    for img in img_files:
        imgs.append(tio.ScalarImage(os.path.join(image_dir,img)))
    for seg in seg_files:
        segs.append(tio.LabelMap(os.path.join(seg_dir, seg)))
    
    return imgs, segs, img_files



imgs, segs, names = pre_load_files(image_dir, seg_dir)
for threshold_method in threshold_methods.keys():
    BVTV_list = []
    TbN_list = []
    TbTh_list = []
    TbSp_list = []
    BSBV_list = [] 
    BV_list = []
    TV_list = []
    threshold_list = []

    total_data = []

    # code for global threshold
    # for x_data, seg_data, filename in zip(generator.sample_img_list, generator.label_img_list, generator.sample_filename_list):
    #     x_data = np.where(x_data < -1024, -1024, x_data)
    #     x_data_masked = ma.array(x_data, mask=np.logical_not(seg_data))
    #     total_data.extend(x_data_masked.compressed())
    # threshold = threshold_methods[threshold_method](np.array(total_data))

    for  x_data, seg_data, filename in zip(imgs, segs, names):
        try:
            x_data_np = x_data.numpy()
            seg_data_np = seg_data.numpy()
            # Intensities from the CT scan images need to be both normalized and potentially windowed. The CT images
            # Hounsfield intensities should start at -1024 for air, but some have background values of -3024, most
            # likely an artifact of the machines calibration. The if statements control whether to simply remove the
            # -3024 Hounsfield intensity and normalize, or window the Hounsfield units and normalize.
            x_data_np = np.where(x_data_np < -1024, -1024, x_data_np)
            
            seg_data_np = seg_data_np.astype(np.uint8) # segdata should alwuas be int 
            #plot_histogram_image_raw(x_data, seg_data, filename)

            x_data_masked = ma.array(x_data_np, mask=np.logical_not(seg_data_np))
            threshold = threshold_methods[threshold_method](x_data_masked.compressed())


            threshold_list.append(threshold)
            #BVTV,  TbN, TbTh, TbSp, BSBV = calc_bvtv(x_data, seg_data, threshold)
            BVTV,  TbN, TbTh, TbSp, BSBV = calc_morphometry(x_data.as_sitk(), seg_data.as_sitk(), threshold)
            BV, TV = calc_volumes(x_data_np, seg_data_np, threshold, np.prod(x_data.spacing))
        except:
            BVTV,  TbN, TbTh, TbSp, BSBV =0, 0, 0, 0, 0
            BV, TV = 0, 0
            print("failed at file: "+ filename)
        
        BV_list.append(BV)
        TV_list.append(TV)
        BVTV_list.append(BVTV)
        TbN_list.append(TbN)
        TbTh_list.append(TbTh)
        TbSp_list.append(TbSp)
        BSBV_list.append(BSBV) 

    df = pd.DataFrame(list(zip(names, BV_list, TV_list, BVTV_list, TbN_list, TbTh_list, TbSp_list, BSBV_list, threshold_list)),
                columns =['Name', 'BV', 'TV', 'BVTV', 'TbN','TbTh', 'TbSp', 'BSBV', 'Threshold level'])

    if seg_mode == 'full':
        df.to_csv(os.path.join(analysis_dir, threshold_method+'_thresholding_output.csv'))
    elif seg_mode == 'lat':
        df.to_csv(os.path.join(analysis_dir, 'lat/' + threshold_method+'_thresholding_output.csv'))
