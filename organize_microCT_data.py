import pandas as pd
import SimpleITK as sitk
import numpy as np
import os

if __name__ == '__main__':


    main_dir = '/home/hmahdi/Microct/MicroCT/Machine Learning'

    column_names = ['sample_name', 'seg_name', 'cross_fold_number']

    df = pd.DataFrame(columns=column_names)


    train_file_list = os.listdir(main_dir + '/train')

    val_file_list = os.listdir(main_dir + '/val')


    for file in train_file_list:

        if '_labels' in file:
            continue

        sample_name = file.split('.nii')[0]

        seg_name = sample_name + '_labels'

        df_dict = {'sample_name': sample_name, 'seg_name': seg_name, 'cross_fold_number': 0}

        df = df.append(df_dict, ignore_index=True)


    for file in val_file_list:

        if '_labels' in file:
            continue

        sample_name = file.split('.nii')[0]

        seg_name = sample_name + '_labels'

        df_dict = {'sample_name': sample_name, 'seg_name': seg_name, 'cross_fold_number': 1}

        df = df.append(df_dict, ignore_index=True)


    df.to_csv('microCT_osteo_data.csv', index=False)
    df.to_pickle('microCT_osteo_data.pickle')
