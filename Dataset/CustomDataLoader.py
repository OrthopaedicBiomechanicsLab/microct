from Dataset.seg_to_bbox import SegToBBox

from Dataset.BaseDataset import BaseDataset
from torch.utils.data import DataLoader
import torch
import numpy as np


class CustomDataLoader:
    """Wrapper for Datasets that can perform multi-threaded data loading"""

    def __init__(self, params, dataset, num_threads=0, shuffle=False, batch_size=None, drop_last=False, sampler=None):
        """
        Initialize the custom dataloader

        Step 1: create a dataset instance given the name [dataset_mode]
        Step 2: create a multi-threaded data loader.

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model, training and dataset
        dataset : Dataset.BaseDataset.BaseDataset
        num_threads : int
            Number of threads to use for the custom data loader
        """

        import sys
        gettrace = getattr(sys, 'gettrace', None)
        if gettrace is None:
            num_threads = num_threads
        elif gettrace():
            num_threads = 0


        self.params = params
        self.dataset = dataset
        self.drop_last = drop_last
        if batch_size is None:
            self.batch_size = self.params.batch_size
        else:
            self.batch_size = batch_size

        # a = 1
        #
        collate_fn = SegToBBox(self.params.num_classes, self.params.get_rois_from_seg_flag, self.params.class_specific_seg_flag, self.params) if self.params.dataset == 'RetinaUNet' else None

        self.dataloader = DataLoader(
            self.dataset,
            batch_size=self.batch_size,
            shuffle=shuffle,
            num_workers=int(num_threads),
            # num_workers=0,
            pin_memory=True,
            drop_last=self.drop_last,
            worker_init_fn=self.worker_init_fn,
            collate_fn=collate_fn,
            sampler=sampler,
        )

    def __call__(self, *args, **kwargs):
        return self.dataloader

    def worker_init_fn(self, worker_id):
        # pass
        np.random.seed(np.random.get_state()[1][0] + worker_id)
        # np.random.seed(0)
        # worker_info = torch.utils.data.get_worker_info()
        #
        # dataset = worker_info.dataset
        # worker_id = worker_info.id
        # split_size = dataset.__len__() // worker_info.num_workers
        #
        # dataset.sample_list = dataset.sample_list[worker_id * split_size: (worker_id + 1) * split_size]


    def load_data(self):
        return self

    def __len__(self):
        """Return the number of data in the dataset"""
        return min(len(self.dataset), self.params.max_dataset_size)

    # def __iter__(self):
    #     """Return a batch of data"""
    #     for i, data in enumerate(self.dataloader):
    #         if i * self.batch_size >= self.params.max_dataset_size:
    #             break
    #         yield data

    def __getitem__(self, index):
        for i, data in enumerate(self.dataloader):
            if i == index - 1:
                hold = 1
            if i != index:
                continue
            return data