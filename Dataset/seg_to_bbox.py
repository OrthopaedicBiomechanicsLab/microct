import numpy as np
# import cupy as np
from scipy.ndimage.measurements import label as lb
from ImageUtils.ToTensorMultiInput import ToTensorMultiInput

from time import time
from skimage.measure import regionprops
from skimage.measure import label as sk_label

import SimpleITK as sitk
import torch


class_dict = {
    1: 'C1',
    2: 'C2',
    3: 'C3',
    4: 'C4',
    5: 'C5',
    6: 'C6',
    7: 'C7',
    8: 'T1',
    9: 'T2',
    10: 'T3',
    11: 'T4',
    12: 'T5',
    13: 'T6',
    14: 'T7',
    15: 'T8',
    16: 'T9',
    17: 'T10',
    18: 'T11',
    19: 'T12',
    20: 'L1',
    21: 'L2',
    22: 'L3',
    23: 'L4',
    24: 'L5',
    25: 'L6',
}


def stack_dict(data_dict, batch_size):
    data_dict_hold = {}

    for keys in data_dict[0].keys():
        data_dict_hold[keys] = []

    for b in range(batch_size):
        for key in data_dict[b].keys():
            data_dict_hold[key].append(data_dict[b][key])


    for key in data_dict_hold.keys():
        data_dict_hold[key] = np.stack(data_dict_hold[key], axis=0)


    return data_dict_hold

class SegToBBox:
    '''
    This function generates bounding box annotations from given pixel-wise annotations.
    :param data_dict: Input data dictionary as returned by the batch generator.
    :param dim: Dimension in which the model operates (2 or 3).
    :param get_rois_from_seg: Flag specifying one of the following scenarios:
    1. A label map with individual ROIs identified by increasing label values, accompanied by a vector containing
    in each position the class target for the lesion with the corresponding label (set flag to False)
    2. A binary label map. There is only one foreground class and single lesions are not identified.
    All lesions have the same class target (foreground). In this case the Dataloader runs a Connected Component
    Labelling algorithm to create processable lesion - class target pairs on the fly (set flag to True).
    :param class_specific_seg_flag: if True, returns the pixelwise-annotations in class specific manner,
    e.g. a multi-class label map. If False, returns a binary annotation map (only foreground vs. background).
    :return: data_dict: same as input, with additional keys:
    - 'bb_target': bounding box coordinates (b, n_boxes, (y1, x1, y2, x2, (z1), (z2)))
    - 'roi_labels': corresponding class labels for each box (b, n_boxes, class_label)
    - 'roi_masks': corresponding binary segmentation mask for each lesion (box). Only used in Mask RCNN. (b, n_boxes, y, x, (z))
    - 'seg': now label map (see class_specific_seg_flag)
    '''
    def __init__(self,
                 num_classes,
                 get_rois_from_seg_flag=False,
                 class_specific_seg_flag=False,
                 params=None):

        self.num_classes = num_classes - 1
        self.get_rois_from_seg_flag = get_rois_from_seg_flag
        self.class_specific_seg_flag = class_specific_seg_flag

        self.params = params

    def __call__(self, data_dict):


        bb_target = []
        roi_masks = []
        roi_labels = []


        # Organize the data_dict to have batch_size integrated. Not totally relevant currently
        data_dict = stack_dict(data_dict, batch_size=len(data_dict))

        # Check if using the Verse Dataset. If so, modify segmentations to be just vertebral body.
        if False:
            if self.num_classes == 25:
                vb_label_range = range(1, self.num_classes + 1)

                total_seg_list = []

                for b_idx in range(data_dict['seg'].shape[0]):

                    seg_data = data_dict['seg'][b_idx, 0]

                    seg_list = []

                    for lab_idx, label_value in enumerate(vb_label_range):
                        # label_value = 19
                        vb_label = label_value
                        inner_label = label_value + self.num_classes

                        if vb_label in seg_data:
                            seg_data_label = np.where(seg_data == vb_label, 1,
                                                      np.where(seg_data == inner_label, 2, 0))

                            seg_data_label_2 = sk_label(seg_data_label)

                            # find second largest segmentation
                            sum_array = []
                            for sub_label in np.unique(seg_data_label_2):
                                if sub_label == 0:
                                    continue
                                sum_array.append(seg_data_label_2[seg_data_label_2 == sub_label].astype(np.bool).sum())

                            sorted_sum_array_idx = np.argsort(sum_array)[::-1]

                            vb_sub_label = sorted_sum_array_idx[0] + 1
                            inner_sub_label = sorted_sum_array_idx[1] + 1

                            region_properties = regionprops(seg_data_label_2)

                            inner_mid = None
                            seg_data_label_3 = seg_data_label_2.copy().astype(np.int32)
                            for prop in region_properties:

                                if prop.label != inner_sub_label:
                                    continue

                                inner_mid = prop.centroid
                                inner_bbox = prop.bbox

                                seg_data_label_3[int(inner_mid[0]):, :, :] = 0
                                # seg_data_label_3[:, int(inner_bbox[4]):, :] = 0
                                seg_data_label_3[:, :, inner_bbox[5]-2:] = 0

                            seg_data_label_3 = np.where(seg_data_label_3 == vb_sub_label, 1, 0)
                            seg_list.append(seg_data_label_3 * vb_label)

                    seg_list = np.sum(np.array(seg_list), axis=0)

                    total_seg_list.append(seg_list)

                total_seg = np.expand_dims(np.array(total_seg_list), 1)

                # sitk.WriteImage(sitk.GetImageFromArray(data_dict['data'][0, 0]), self.params.model_output_directory + '/hold.nii.gz')
                # sitk.WriteImage(sitk.GetImageFromArray(data_dict['seg'][0, 0]), self.params.model_output_directory + '/hold_seg.nii.gz')
                # sitk.WriteImage(sitk.GetImageFromArray(total_seg[0, 0].astype('float')), self.params.model_output_directory + '/hold_seg_2.nii.gz')

                data_dict['seg'] = total_seg

        out_seg = np.copy(data_dict['seg'])
        for b in range(data_dict['seg'].shape[0]):

            image_shape = data_dict['seg'].shape[-3:]

            roi_labels_padded = np.zeros(shape=(self.num_classes,))
            bb_target_padded = np.zeros(shape=(self.num_classes, 6))
            roi_masks_padded = np.zeros(shape=(self.num_classes, 1) + image_shape)


            # roi_labels_padded = np.zeros(shape=(25,))
            # bb_target_padded = np.zeros(shape=(25, 6))
            # roi_masks_padded = np.zeros(shape=(25, 1) + image_shape)

            p_coords_list = []
            p_roi_masks_list = []
            p_roi_labels_list = []

            if np.sum(data_dict['seg'][b] != 0) > 0:
                if self.get_rois_from_seg_flag:
                    clusters, n_cands = lb(data_dict['seg'][b])
                    data_dict['class_target'][b] = [data_dict['class_target'][b]] * n_cands
                else:
                    n_cands = int(np.max(data_dict['seg'][b]))
                    clusters = data_dict['seg'][b]

                rois = np.array([(clusters == ii) * 1 for ii in range(1, n_cands + 1)])  # separate clusters and concat
                for rix, r in enumerate(rois):
                    if np.sum(r != 0) > 0:  # check if the lesion survived data augmentation
                        seg_ixs = np.argwhere(r != 0)
                        coord_list = [np.min(seg_ixs[:, 1]) - 1, np.min(seg_ixs[:, 2]) - 1, np.max(seg_ixs[:, 1]) + 1,
                                      np.max(seg_ixs[:, 2]) + 1]

                        coord_list.extend([np.min(seg_ixs[:, 3]) - 1, np.max(seg_ixs[:, 3]) + 1])

                        p_coords_list.append(coord_list)
                        p_roi_masks_list.append(r)
                        # add background class = 0. rix is a patient wide index of lesions. since 'class_target' is
                        # also patient wide, this assignment is not dependent on patch occurrances.
                        p_roi_labels_list.append(data_dict['class_target'][b][rix + 1])

                    if self.class_specific_seg_flag:
                        out_seg[b][data_dict['seg'][b] == rix + 1] = data_dict['class_target'][b][rix] + 1

                if not self.class_specific_seg_flag:
                    out_seg[b][data_dict['seg'][b] > 0] = 1

                roi_labels_hold = np.array(p_roi_labels_list)
                bb_target_hold = np.array(p_coords_list)
                roi_masks_hold = np.array(p_roi_masks_list).astype('uint8')

                for label in range(1, self.num_classes + 1):
                # for label in range(1, 25 + 1):
                    if label in roi_labels_hold:

                        idx = np.argwhere(label == roi_labels_hold)[0][0]

                        roi_labels_padded[label - 1] = roi_labels_hold[idx]
                        bb_target_padded[label - 1] = bb_target_hold[idx]
                        roi_masks_padded[label - 1] = roi_masks_hold[idx]


                roi_labels.append(roi_labels_padded)
                bb_target.append(bb_target_padded)
                roi_masks.append(roi_masks_padded.astype('uint8'))



            else:
                roi_labels.append(np.array([-1]))
                bb_target.append([])
                roi_masks.append(np.zeros_like(data_dict['seg'][b])[None])



        if self.get_rois_from_seg_flag:
            data_dict.pop('class_target', None)

        roi_labels_hold = np.array(roi_labels)
        roi_labels = np.where(roi_labels_hold > 0, 1, 0)

        data_dict['roi_labels'] = np.array(roi_labels)
        # data_dict['roi_labels_hold'] = np.array(roi_labels_hold)
        data_dict['bb_target'] = np.array(bb_target)
        data_dict['roi_masks'] = np.array(roi_masks)


        # rect_np = np.zeros((128, 128, 384))
        #
        # gt_bb_class_targets = data_dict['roi_labels'][data_dict['roi_labels'] > 0].astype(np.int64)
        # gt_class_list = []
        #
        # bb_target_list = data_dict['bb_target'][data_dict['roi_labels'] > 0].astype(np.int64)
        #
        # for idx, box in enumerate(bb_target_list):
        #     x1 = box[0]
        #     y1 = box[1]
        #
        #     x2 = box[2]
        #     y2 = box[3]
        #
        #     z1 = box[4]
        #     z2 = box[5]
        #
        #     # rect_np[x1:x2, y1:y2, 384 - z2: 384 - z1] = box['box_pred_class_id']
        #     rect_np[x1:x2, y1:y2, z1:z2] = gt_bb_class_targets[idx]
        #     gt_class_list.append(class_dict[gt_bb_class_targets[idx]])
        #
        # sitk.WriteImage(sitk.GetImageFromArray(rect_np), 'hold_rect.nii.gz')


        # roi_labels_arr = np.array(roi_labels)
        # bb_target_arr = np.array(bb_target)
        # roi_masks_arr = np.array(roi_masks)
        #
        #
        # non_zero_idx = np.argwhere(roi_labels_arr > 0)
        #
        # roi_labels_arr = roi_labels_arr[non_zero_idx[:, 0], non_zero_idx[:, 1]].astype(np.int64)
        # bb_target_arr = bb_target_arr[non_zero_idx[:, 0], non_zero_idx[:, 1]].astype(np.int64)
        # roi_masks_arr = roi_masks_arr[non_zero_idx[:, 0], non_zero_idx[:, 1]].astype(np.uint8)
        #
        # roi_labels_hold = np.load('roi_labels_hold.npy')[0]
        # bb_target_hold = np.load('bb_target_hold.npy')[0]
        # roi_masks_hold = np.load('roi_masks_hold.npy')[0]
        #
        # a = roi_masks_arr - roi_masks_hold
        # b = bb_target_arr - bb_target_hold
        # c = roi_labels_arr - roi_labels_hold

        data_dict['seg'] = out_seg

        # sitk.WriteImage(sitk.GetImageFromArray(data_dict['data'][0, 0]), 'hold.nii.gz')
        # sitk.WriteImage(sitk.GetImageFromArray(data_dict['seg'][0, 0]), 'hold_seg.nii.gz')


        for key in data_dict.keys():
            if key == 'pid':
                continue
            else:
                data_dict[key] = ToTensorMultiInput()(data_dict[key])
        

        return data_dict

