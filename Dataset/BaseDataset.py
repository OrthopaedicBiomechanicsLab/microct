import numpy as np
import torch
import torch.utils.data as data
import torchvision.transforms as transforms
from abc import ABC, abstractmethod



from ImageUtils.CropPadImg import CropPadImg, PadNearestPower, PadImg, UniformCropPad, DirectionalCropPad
from ImageUtils import IntensityAugmentation
from ImageUtils.ShapeAugmentation import ShapeAugmentationClass
from ImageUtils.TranslationAugmentation import TranslationAugmentation
from ImageUtils.ToTensorMultiInput import ToTensorMultiInput

class BaseDataset(data.Dataset, ABC):
    """
    This is an abract base class (ABC) for dataset creattion
    """
    def __init__(self, params, isTrain=False):
        """

        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model, training and dataset
        """
        self.params = params
        self.isTrain = isTrain

    @abstractmethod
    def __len__(self):
        """
        Length of total number of samples
        """
        return 0

    @abstractmethod
    def __getitem__(self, sample_idx):
        """
        Return a single sample

        Parameters
        ----------
        sample_idx : int
            an integer for the sample to be pulled from the source. The source can be a list, dataframe, or other.

        Returns
        -------

        """
        pass

    def get_sample_names(self, sample_idx):
        sample_name = None
        label_name = None
        additional = {}
        return sample_name, label_name, additional

    def get_batch(self, sample_idx, batch_size=None):
        if batch_size is None:
            batch_size = self.params.batch_size
        sample_idx_range = range(sample_idx, sample_idx + batch_size)
        image_list = []
        label_list = []
        multi_label_output = False

        for idx in sample_idx_range:
            sample = self.__getitem__(idx)
            image_list.append(sample['image'])
            if isinstance(sample['label'], list) or isinstance(sample['label'], tuple):
                    multi_label_output = True
            label_list.append(sample['label'])
        image_batch = torch.stack(image_list, dim=0)

        if multi_label_output is True:
            label_batch = []
            label_list_multi_label = list(map(list, zip(*label_list)))
            for label_type in label_list_multi_label:
                label_batch.append(torch.stack(label_type, dim=0))
        else:
            label_batch = torch.stack(label_list, dim=0)
        batch_sample = {'image': image_batch,
                        'label': label_batch}
        return batch_sample


def get_transforms(params, isTrain, pad_direction=None, crop_direction=None):

    transform_list = []

    # if params.intensity_augmentation is not None:
    #     transform_list.append(IntensityAugmentation(intensity_augmentations=params.intensity_augmentation,
    #                                                 augmentation_params=params.intensity_aug_params))

    if params.single_vert_crop is True:
        if (params.translation_augmentation is True) and (isTrain or params.transform_aug_on_val_test):
            transform_list.append(CropPadImg(sample_size=params.sample_size,
                                             translation=True,
                                             centered=True))
        elif params.crop_pad is True:
            transform_list.append(CropPadImg(sample_size=params.sample_size,
                                             translation=False,
                                             centered=True))

        if (params.affine_augmentation is True) and (isTrain or params.transform_aug_on_val_test):
            augmentation_params = params.augmentation_params
            transform_list.append(ShapeAugmentationClass(sample_size=params.sample_size,
                                                         **augmentation_params))

        return transforms.Compose(transform_list)

    else:
        if (params.affine_augmentation is True) and (isTrain or params.transform_aug_on_val_test):
            augmentation_params = params.augmentation_params
            transform_list.append(ShapeAugmentationClass(sample_size=params.sample_size,
                                                         **augmentation_params))

        if (params.translation_augmentation is True) and (isTrain or params.transform_aug_on_val_test):
            transform_list.append(TranslationAugmentation(sample_size=params.sample_size,
                                                          translation_args=params.translation_args))

        else:
            transform_list.append(UniformCropPad(output_sample_size=params.sample_size))

        # elif params.directional_pad_crop is True:
        #     transform_list.append(DirectionalCropPad(output_sample_size=params.sample_size,
        #                                              pad_direction=pad_direction,
        #                                              crop_direction=crop_direction))
        #
        # else:
        #     pass


        return transforms.Compose(transform_list)

