import pandas as pd
import numpy as np
import SimpleITK as sitk

import torch
from torch.utils import data
import torchio as tio
from Dataset.BaseDataset import BaseDataset
from ImageUtils.ToTensorMultiInput import ToTensorMultiInput
from ImageUtils import IntensityAugmentation
from ImageUtils import TranslationAugmentation
from ImageUtils.ShapeAugmentation import ShapeAugmentationClass
from ImageUtils.CropPadImg import UniformCropPad


class OsteoKneeSeg(BaseDataset):

    def __init__(self, params, df, scan_main_path, seg_main_path, isTrain=False):
        """
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
                Parameters for model, training and dataset
        df : pd.Dataframe
            Dataframe used to track samples
        scan_main_path : str
            String for the location of the samples
        seg_main_path : str
            String for the location of the labels
        """
        BaseDataset.__init__(self, params, isTrain)

        self.df = df

        self.sample_main_path = scan_main_path
        self.label_main_path = seg_main_path

        #self.sample_size = params.sample_size #TODO: why does the other file not have this?
        #self.n_channels = params.num_channels
        #self.n_labels = params.num_classes #TODO: Why does the other file not have this?
        #self.translation_augmentation = params.translation_augmentation
        #self.affine_augmentation = params.affine_augmentation
        #self.low_res_augmentation = params.low_res_augmentation

        self.sample_filename_list = df['sample_name'].to_list()
        self.label_filename_list = df['seg_name'].to_list()

        #self.sample_list = list(zip(self.sample_filename_list, self.label_filename_list))

        self.num_repeats = 1 #TODO: other file doesn't have this

        self.sample_img_list = []
        self.label_img_list = []

        self.sample_img_list_sitk = []
        self.label_img_list_sitk = []


        self.sample_idx_list = np.arange(0, len(self.sample_filename_list))
        self.sample_idx_list = np.tile(self.sample_idx_list, self.num_repeats)

        if self.params.load_all_data is True:
            _, self.unique_sample_idx, self.unique_scan_inv = np.unique(self.sample_filename_list,
                                                                        return_index=True,
                                                                        return_inverse=True)

            _, self.unique_seg_idx, self.unique_seg_inv = np.unique(self.label_filename_list,
                                                                    return_index=True,
                                                                    return_inverse=True)

            # Load only the unique scan img files. Files could be in .npy or .npz, so if statements are used.
            for scan_idx in self.unique_sample_idx:
                sample_filename = self.sample_filename_list[scan_idx] + '.nii'
                print(sample_filename)
                # scan_img_hold = np.load(self.sample_main_path + '/' + scan_filename).astype(np.float32)

                sample_img_hold = sitk.ReadImage(self.sample_main_path + '/' + sample_filename)
                self.sample_img_list_sitk.append(sample_img_hold)


                sample_img_hold = sitk.GetArrayFromImage(sample_img_hold)
                # Store scan images in a list, used during batch generation.
                self.sample_img_list.append(sample_img_hold)

            # Load the ground truth segmentation img files. Files could be in .npy or .npz, so if statements are used.
            for seg_idx in self.unique_seg_idx:
                label_filename = self.label_filename_list[seg_idx] + '.nii'
                print(label_filename)

                # seg_img_hold = np.load(self.label_main_path + '/' + seg_filename).astype(np.uint8)

                seg_img_hold = sitk.ReadImage(self.label_main_path + '/' + label_filename)
                self.label_img_list_sitk.append(seg_img_hold)

                seg_img_hold = sitk.GetArrayFromImage(seg_img_hold)
                # Store ground truth segmentations in a list, used during batch generation.
                self.label_img_list.append(seg_img_hold)

    def __len__(self):
        """
        Length of total number of samples
        """
        return len(self.label_filename_list)

    def __getitem__(self, random_idx):
        """
        Generates a single sample
        """

        # scan_sample, seg_sample = sample

        # if torch.is_tensor(sample_idx):
        #     sample_idx = sample_idx.tolist()

        # Obtain the scan and segmentation filename/index for a single sample.
        sample_idx = self.sample_idx_list[random_idx]

        sample_filename = self.sample_filename_list[sample_idx]
        label_filename = self.label_filename_list[sample_idx]

        if self.params.load_all_data is True:
            x_data = self.sample_img_list[self.unique_scan_inv[sample_idx]]
            seg_data = self.label_img_list[self.unique_seg_inv[sample_idx]]
        else:
            # resizing in the training loop is very slow so just do it prior
            # resizer = tio.Resize((128, 128, 64))
            # x_data =  np.squeeze(resizer(tio.ScalarImage(self.sample_main_path +\
            #      '/' + sample_filename + '.nii')).numpy())
            # seg_data =  np.squeeze(resizer(tio.LabelMap(self.label_main_path +\
            #      '/' + label_filename + '.nii')).numpy())
            
            
            x_img = sitk.ReadImage(self.sample_main_path + '/' + sample_filename + '.nii')
            seg_img = sitk.ReadImage(self.label_main_path + '/' + label_filename + '.nii')

            x_data = sitk.GetArrayFromImage(x_img)
            seg_data = sitk.GetArrayFromImage(seg_img)
            

        # Intensities from the CT scan images need to be both normalized and potentially windowed. The CT images
        # Hounsfield intensities should start at -1024 for air, but some have background values of -3024, most
        # likely an artifact of the machines calibration. The if statements control whether to simply remove the
        # -3024 Hounsfield intensity and normalize, or window the Hounsfield units and normalize.
        #
        # Normalized afterwards to scale values from 0 to 255.

        x_data = np.where(x_data < -1024, -1024, x_data)
        # Normalized
        x_data = (x_data - x_data.min()) / (x_data.max() - x_data.min())

        ground_truth_img = sitk.GetImageFromArray(seg_data)
        sample_img = sitk.GetImageFromArray(x_data)

        sitk.WriteImage(sample_img, '/home/hmahdi/Microct/MicroCT/Machine Learning' + '/' + label_filename+'-before.nii')
        sitk.WriteImage(ground_truth_img, '/home/hmahdi/Microct/MicroCT/Machine Learning' + '/' + sample_filename+'-before.nii')

        # Affine transformation.
        # TODO: Look at this one?
        if (self.params.affine_augmentation is True) and (self.isTrain or self.params.transform_aug_on_val_test):
            augmentation_params = self.params.augmentation_params
            shape_aug = ShapeAugmentationClass(sample_size=self.params.sample_size, **augmentation_params)
            x_data, seg_data, _ = shape_aug(spine_list=[x_data], seg_list=[seg_data])

        # Intensity Augmentation
        if self.params.intensity_augmentation and (self.isTrain or self.params.transform_aug_on_val_test):
            intensity_aug_random = np.random.randint(0, 1 + 1)
            if intensity_aug_random == 1:
                intensity_aug = IntensityAugmentation(intensity_augmentations=self.params.intensity_augmentation,
                                                      augmentation_params=self.params.intensity_aug_params)

                x_data = intensity_aug([x_data])

        # Translational Augmentation (differs depending on the dataset)
        #TODO: Look at this one
        if self.params.translation_augmentation and (self.isTrain or self.params.transform_aug_on_val_test):
            trans_aug = TranslationAugmentation(sample_size=self.params.sample_size,
                                                translation_args=self.params.translation_args)

            x_data, seg_data = trans_aug([x_data, seg_data])

        else:
            crop_pad = UniformCropPad(output_sample_size=self.params.sample_size)
            x_data, seg_data = crop_pad([x_data, seg_data])

        ground_truth_img = sitk.GetImageFromArray(seg_data)
        sample_img = sitk.GetImageFromArray(x_data)

        sitk.WriteImage(ground_truth_img, '/home/hmahdi/Microct/MicroCT/Machine Learning' + '/' + label_filename+'.nii')
        sitk.WriteImage(sample_img, '/home/hmahdi/Microct/MicroCT/Machine Learning' + '/' + sample_filename+'.nii')


        # Since the data being used in gray scale, an extra dimension for the channel dimension is necessary for pytorch
        x_data = np.expand_dims(x_data, 0)
        seg_data = np.expand_dims(seg_data, 0)

        # Convert the numpy arrays to torch tensors and return the sample
        x_data, seg_data = ToTensorMultiInput()([x_data, seg_data])

        y_data = seg_data

        sample = {'image': x_data,
                  'label': y_data}

        return sample

    def get_sample_names(self, sample_idx):
        row = self.df.iloc[sample_idx]

        sample_name = row['sample_name']
        label_name = row['seg_name']

        k_fold = row['cross_fold_number']

        additional = {'k_fold': k_fold}
        return sample_name, label_name, additional


if __name__ == '__main__':
    from Parameters import construct_params
    #from Parameters import ModelParameters
    import SimpleITK as sitk

    #np.random.seed(1337) #TODO: Their data used random manual seed -- not sure how that fits in Geoffs model
    #torch.random.manual_seed(1337)


    df = pd.read_pickle('/home/hmahdi/SpineSegmentation/MicroCT/osteoknee_data.pickle')

    df_train = df.loc[df['cross_fold_number'] == 0]  # TODO: Where do I put in Traversky loss -- send separate to Geoff

    sample_size = (64, 128, 128)

    scan_main_path = '/home/hmahdi/SpineSegmentation/MicroCT/Tina data/scans'
    seg_main_path = '/home/hmahdi/SpineSegmentation/MicroCT/Tina data/segs'

    affine_aug_params = {'rot_ang': True,  # TODO: what needs to go here? An angle or just true?
                         'shear': True,
                         'scale_factor': (0.8, 1.2),
                         'x_flip': False,  # May want to change this later - old code had this as a TODO.
                         'y_flip': False,
                         'z_flip': False,
                         'elastic': False}  # TODO: What is this? Ask Geoff

    params_dict = dict(model='UNet',
                       sample_size=sample_size,
                       augmentation=True,
                       augmentaiton_params=affine_aug_params,
                       translation_augmentation=True,
                       dataset='OsteoKneeSeg',
                       intensity_augmentation=False,
                       )

    params = construct_params(params_dict=params_dict)

    data_gen = OsteoKneeSeg(params, df_train, scan_main_path, seg_main_path, isTrain=True)

    num = 0

    sample = data_gen.__getitem__(num)

    img_out = sample['image'].numpy()[0]
    label_out = sample['label'].numpy()[0]

    img = sitk.GetImageFromArray(img_out)
    label = sitk.GetImageFromArray(label_out)
    sitk.WriteImage(label, 'label.nii.gz')
    sitk.WriteImage(img, 'hold.nii.gz')
