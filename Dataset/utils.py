import numpy as np

def build_rpn_targets(gt_boxes, gt_class_ids, anchors, params):
    """Given the anchors and GT boxes, compute overlaps and identify positive
    anchors and deltas to refine them to match their corresponding GT boxes.

    anchors: [num_anchors, (x1, y1, x2, y2)]
    gt_class_ids: [num_gt_boxes] Integer class IDs.
    gt_boxes: [num_gt_boxes, (x1, y1, x2, y2)]

    params : Parameters.ParameterBuilder.Parameters

    Returns:
    rpn_match: [N] (int32) matches between anchors and GT boxes.
               1 = positive anchor, -1 = negative anchor, 0 = neutral
    rpn_class_match: [N] (int32) class that corresponds to the positive or neutral anchor overlaps.
    rpn_bbox: [N, (dx, dy, log(dw), log(dh))] Anchor bbox deltas.
    """
    # RPN Match: 1 = positive anchor, -1 = negative anchor, 0 = neutral

    rpn_match = np.zeros([anchors.shape[0]], dtype=np.int8)

    # RPN bounding boxes: [max anchors per image, (dx, dy, log(dw), log(dh))]
    rpn_bbox = np.zeros((params.rpn_train_anchors_per_image, 4))

    gt_class_ids_bbox = np.zeros((params.rpn_train_anchors_per_image))


    # Reshape gt_boxes to work with the overlap function.
    gt_boxes = np.reshape(gt_boxes, (-1, gt_boxes.shape[1]))


    # Compute overlaps [num_anchors, num_gt_boxes]
    overlaps = compute_overlap(anchors, gt_boxes)

    # Match anchors to GT Boxes
    # If an anchor overlaps a GT box with IoU >= 0.7 then it's positive.
    # If an anchor overlaps a GT box with IoU < 0.3 then it's negative.
    # Neutral anchors are those that don't match the conditions above,
    # and they don't influence the loss function.
    # However, don't keep any GT box unmatched (rare, but happens). Instead,
    # match it to the closest anchor (even if its max IoU is < 0.3).
    #
    # 1. Set negative anchors first. They get overwritten below if a GT box is
    # matched to them. Skip boxes in crowd areas.


    # Determine the best anchor for each gt_bbox.=
    anchor_iou_argmax = np.argmax(overlaps, axis=1)
    anchor_iou_max = overlaps[np.arange(overlaps.shape[0]), anchor_iou_argmax]

    # The max IoU for each anchor have already been selected. If the max values are below the specified threshold
    # set the rpn id to -1.

    # rpn_match specifies what bbox is positive, negative or neutral for a particular anchor box.
    # rpn_class_match specifies the class the positive/neutral anchor boxes overlap with in the image.

    rpn_match[(anchor_iou_max < params.neg_iou_threshold)] = -1
    rpn_class_match = np.copy(anchor_iou_argmax)
    rpn_class_match[(anchor_iou_max < params.neg_iou_threshold)] = -1

    # 2. Set an anchor for each GT box (regardless of IoU value).
    # These may be overwritten later if the max value is also above the pos_iou_threshold

    gt_iou_argmax = np.argmax(overlaps, axis=0)
    # rpn_match[gt_iou_argmax] = 1

    rpn_class_match[gt_iou_argmax] = np.arange(len(gt_class_ids))

    # 3. Set anchors with high overlap as positive.
    rpn_match[anchor_iou_max >= params.pos_iou_threshold] = 1

    # rpn_class_match is broken. Does not work
    rpn_class_match[(anchor_iou_max >= params.neg_iou_threshold) & (anchor_iou_max < params.pos_iou_threshold)] = 0

    # Subsample to balance positive and negative anchors
    # Don't let positives be more than half the anchors. Matters for rpn_match
    ids_positive = np.where(rpn_match == 1)[0]


    # # If more positive anchors exists than the amount of positive anchors per image,
    # # randomly set additional ones to zero
    # extra_positive = len(ids_positive) - (params.rpn_train_anchors_per_image // 2)
    #
    # if extra_positive > 0:
    #     # Reset the extra ones to neutral
    #     ids_positive = np.random.choice(ids_positive, extra_positive, replace=False)
    #     rpn_match[ids_positive] = 0
    #
    # # Same for negative proposals
    # ids_negative = np.where(rpn_match == -1)[0]
    # extra_negative = len(ids_negative) - (params.rpn_train_anchors_per_image - np.sum(rpn_match == 1))
    #
    # if extra_negative > 0:
    #     # Rest the extra ones to neutral
    #     ids_negative = np.random.choice(ids_negative, extra_negative, replace=False)
    #     rpn_match[ids_negative] = 0

    # For positive anchors, compute shift and scale needed to transform them
    # to match the corresponding GT boxes.
    ids = np.where(rpn_match == 1)[0]
    ix = 0  # index into rpn_bbox

    rpn_bbox_list = []
    gt_hold = []
    a_hold = []
    for i, a in zip(ids, anchors[ids]):
        # Closest gt box (it might have IoU < 0.7)

        # gt are in [x1, y1, x2, y2]
        gt = gt_boxes[anchor_iou_argmax[i]]

        gt_class_ids_bbox[ix] = gt_class_ids[anchor_iou_argmax[i]]

        # Convert coordinates to center plus width/height.

        # GT Box
        gt_w = gt[2] - gt[0]
        gt_h = gt[3] - gt[1]
        gt_center_x = gt[0] + (0.5 * gt_w)
        gt_center_y = gt[1] + (0.5 * gt_h)

        # Anchor
        a_w = a[2] - a[0]
        a_h = a[3] - a[1]
        a_center_x = a[0] + (0.5 * a_w)
        a_center_y = a[1] + (0.5 * a_h)

        # # Compute the bbox refinement that the RPN should predict.
        # rpn_bbox[ix] = [
        #     (gt_center_x - a_center_x) / a_w,
        #     (gt_center_y - a_center_y) / a_h,
        #     np.log(gt_w / a_w),
        #     np.log(gt_h / a_h)]

        #
        hold = np.array([(gt_center_x - a_center_x) / a_w,
                         (gt_center_y - a_center_y) / a_h,
                         np.log(gt_w / a_w),
                         np.log(gt_h / a_h)])

        a_hold.append(np.array([a_center_x,
                                a_center_y,
                                a_w,
                                a_h]))

        gt_hold.append(np.array([gt_center_x,
                                 gt_center_y,
                                 gt_w,
                                 gt_h]))

        #
        # hold = (hold - params.rpn_bbox_avg) / (params.rpn_bbox_std_dev)
        #
        rpn_bbox_list.append(hold)


        # Normalize
        # rpn_bbox[ix] /= config.RPN_BBOX_STD_DEV

        # rpn_bbox[ix] = (rpn_bbox[ix] - params.rpn_bbox_avg) / (params.rpn_bbox_std_dev)

        ix += 1


    try:
        rpn_bbox = np.stack(rpn_bbox_list, axis=0)
    except ValueError:
        rpn_bbox = np.stack([[np.nan, np.nan, np.nan, np.nan]], axis=0)
        a = 1
    # rpn_bbox = np.stack(rpn_bbox_list, axis=0)
    # gt_hold = np.stack(gt_hold, axis=0)
    # a_hold = np.stack(a_hold, axis=0)

    return rpn_match, rpn_bbox, gt_class_ids_bbox, rpn_class_match



def compute_overlap(boxes1, boxes2):
    """Computes IoU overlaps between two sets of boxes.
    boxes1, boxes2: [N, (x1, y1, x2, y2)].
    For better performance, pass the largest set first and the smaller second.
    """
    # Areas of anchors and GT boxes
    area1 = (boxes1[:, 2] - boxes1[:, 0]) * (boxes1[:, 3] - boxes1[:, 1])
    area2 = (boxes2[:, 2] - boxes2[:, 0]) * (boxes2[:, 3] - boxes2[:, 1])

    # Compute overlaps to generate matrix [boxes1 count, boxes2 count]
    # Each cell contains the IoU value.
    overlaps = np.zeros((boxes1.shape[0], boxes2.shape[0]))

    for i in range(overlaps.shape[1]):
        box2 = boxes2[i]
        overlaps[:, i] = compute_iou(box2, boxes1, area2[i], area1)

    return overlaps


def compute_iou(box, boxes, box_area, boxes_area):
    """Calculates IoU of the given box with the array of the given boxes.
    box: 1D vector [x1, y1, x2, y2]
    boxes: [boxes_count, (x1, y1, x2, y2)]
    box_area: float. the area of 'box'
    boxes_area: array of length boxes_count.
    Note: the areas are passed in rather than calculated here for
          efficency. Calculate once in the caller to avoid duplicate work.
    """
    # Calculate intersection areas
    x1 = np.maximum(box[0], boxes[:, 0])
    x2 = np.minimum(box[2], boxes[:, 2])
    y1 = np.maximum(box[1], boxes[:, 1])
    y2 = np.minimum(box[3], boxes[:, 3])

    intersection = np.maximum(x2 - x1, 0) * np.maximum(y2 - y1, 0)

    # intersection = np.maximum((x2 - x1)*(y2 - y1), 0)


    union = box_area + boxes_area[:] - intersection[:]
    iou = intersection / union
    return iou
