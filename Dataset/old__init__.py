from .BaseDataset import BaseDataset
from .SpineSegDataset import SpineSegDataSet
from .CustomDataLoader import CustomDataLoader
import importlib

def select_dataset(params):

    if hasattr(params.dataset, '__class__'):
        return params.dataset


    dataset_name = params.dataset

    dataset_name = dataset_name.lower()
    dict_of_datasets = {'verse': 'VerseSegDataset',
                        'versesegdataset': 'VerseSegDataset',
                        'verseseg': 'VerseSegDataset',

                        'spineseg': 'SpineSegDataset',
                        'spinesegdataset': 'SpineSegDataset',

                        'versepatcheddataset': 'VersePatchedDataset',
                        'versesegdownsampleddataset': 'VerseSegDownSampledDataset',
                        'versesegdownsampleddatasetdetect': 'VerseSegDownSampledDatasetDetect',
                        'fullspinesegdataset': 'FullSpineSegDataset',
                        'spineselfsupervised': 'SpineSelfSupervised',
                        'spinelocalizationdataset': 'SpineLocalizationDataset',

                        'fasterrcnnspine': 'FasterRCNNSpine',
                        'retinaunet': 'RetinaUNet',
                        'versefullsegdataset': 'VerseFullSegDataset',
                        'sarcopeniaseg': 'SarcopeniaSeg',
                        'spinecentroiddataset': 'SpineCentroidDataset',}


    dataset_filename = dict_of_datasets[dataset_name]
    dataset_filename = 'Dataset.' + dataset_filename
    dataset_lib = importlib.import_module(dataset_filename)

    dataset = None
    for name, cls in dataset_lib.__dict__.items():
        if (name.lower() == params.dataset.lower()) and (issubclass(cls, BaseDataset)):
            dataset = cls

    if dataset is None:
        raise ImportError('No accepted dataset name has been passed. Please refer to documentation of a list of '
                          'accepted dataset names, with appropriate formatting and case sensitivity.')

    return dataset