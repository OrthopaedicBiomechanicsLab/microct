from skimage.filters.thresholding import threshold_mean
import torch
import torch.nn as nn
from torch.nn import functional as F
from abc import ABC, abstractmethod
import numpy as np
from skimage.filters import threshold_otsu
from skimage.filters import threshold_minimum
from skimage.filters import threshold_isodata
import importlib
import itk
import numpy.ma as ma
def dsc_loss(pred, target, device, **kwargs):
    return torch.tensor(1, dtype=torch.float32, device=device) - dsc(pred, target, device, neg=True, **kwargs)

def dsc(pred, target, device, neg=False, **kwargs):
    # target = target[:, 1]

    if isinstance(pred, np.ndarray):
        module = np
    elif isinstance(pred, torch.Tensor):
        module = torch

    if (pred.shape[1] > 1) and pred.ndim == 5:
        pred = F.softmax(pred, dim=1)[:, 1]
    else:
        pred = torch.sigmoid(pred)


    y_true_f = target.reshape(target.shape[0], -1)
    y_pred_f = pred.reshape(pred.shape[0], -1)
    # intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    intersection = module.sum(y_true_f * y_pred_f, -1)
    smooth = 1e-7
    # dsc = (2. * intersection) / (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1) + smooth)
    dsc = (2. * intersection) / (module.sum(y_true_f.type(torch.float32), -1) + module.sum(y_pred_f.type(torch.float32), -1) + smooth)
    # if neg == True:
    #     dsc = 1 - dsc

    return dsc.mean()
"""
def Tversky_dsc(pred, target, device, neg=False, **kwargs):
    # target = target[:, 1]

    if isinstance(pred, np.ndarray):
        module = np
    elif isinstance(pred, torch.Tensor):
        module = torch

    if (pred.shape[1] > 1) and pred.ndim == 5:
        pred = F.softmax(pred, dim=1)[:, 1]
    else:
        pred = torch.sigmoid(pred)


    y_true_f = target.reshape(target.shape[0], -1)
    y_pred_f = pred.reshape(pred.shape[0], -1)
    # intersection = torch.sum(y_true_f * y_pred_f, dim=-1)
    intersection = module.sum(y_true_f * y_pred_f, -1)
    smooth = 1e-7
    # dsc = (2. * intersection) / (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1) + smooth)
    dsc = (2. * intersection) / (module.sum(y_true_f.type(torch.float32), -1) + module.sum(y_pred_f.type(torch.float32), -1) + smooth)
    # if neg == True:
    #     dsc = 1 - dsc

    return dsc.mean()
"""

def concurrency(pred, target, device, **kwargs):
    smooth = torch.tensor(1e-7, device=device)
    pred_threshold = torch.tensor(0.5, device=device)

    zero_const = torch.tensor([0], dtype=pred.dtype, device=device)
    pred = torch.where(pred > pred_threshold, pred, zero_const)

    y_true_f = target.reshape(target.shape[0], -1)
    y_pred_f = pred.reshape(pred.shape[0], -1)
    intersection = torch.sum(y_true_f * y_pred_f, dim=-1)

    con = 1 / 2 * (intersection * (torch.sum(y_true_f, dim=-1) + torch.sum(y_pred_f, dim=-1))) / (
            torch.sum(y_true_f, dim=-1) * torch.sum(y_pred_f, dim=-1) + smooth)
    return con.mean()


def bce_loss(pred, target, device, weight=None, reduction='mean', **kwargs):
    return F.binary_cross_entropy(pred, target, weight=weight, reduction=reduction)


def bce_logits_loss(pred, target, device, weight=None, reduction='mean', **kwargs):
    return F.binary_cross_entropy_with_logits(pred, target, weight=weight, reduction=reduction)

def tversky(pred, target, device, **kwargs):
    # comment out if your model contains a sigmoid or equivalent activation layer
    smooth = 1
    alpha = 0.2
    beta = 0.8
    inputs = F.sigmoid(pred)
    # flatten label and prediction tensors
    inputs = inputs.view(-1)
    target = target.view(-1)
    # True Positives, False Positives & False Negatives
    TP = (inputs * target).sum()
    FP = ((1 - target) * inputs).sum()
    FN = (target * (1 - inputs)).sum()
    tversky = (TP + smooth) / (TP + alpha * FP + beta * FN + smooth)
    return tversky

def tversky_loss(pred, target, device, **kwargs):
    return 1-tversky(pred, target, device)

def focal_tversky_loss(pred, target, device, gamma=1.5, **kwargs):
    return torch.pow(tversky_loss(pred, target, device), gamma)


'''
Takes in a segmentation alongside the original bone and calcultes the ratio of 
the volume of the trabecular bone over the total volume (trabecular bone+trabecular 
separation (gaps))
'''
def volume_ratio(segmentation, gt_segmentation, image, device, mode = 'pred'):
    
    image = image.view(-1)
    if mode == 'pred':
        # do prediction so you have binary
        pred = F.sigmoid(segmentation)
        pred_threshold = torch.tensor(0.5, device=device)
        pred = pred > pred_threshold
        
        # flatten label and prediction tensors
        pred = pred.view(-1)

        x_data_masked = ma.array(image.cpu(), mask=np.logical_not(pred.cpu())).compressed()
        threshold = threshold_mean(np.array(x_data_masked))

        image[pred==0] = 0 # delete all outside the image 
        #threshold = threshold_otsu(np.array(image.cpu())) # replace with math later
        
        bone = image>threshold
        tissue = image<= threshold
        return torch.tensor(torch.sum(bone).item()/torch.sum(pred==1).item(),
        device = device)
    else:
        gt_segmentation = gt_segmentation.view(-1)
        
        x_data_masked = ma.array(image.cpu(), mask=np.logical_not(gt_segmentation.cpu())).compressed()
        threshold = threshold_mean(np.array(x_data_masked))

        image[gt_segmentation==0] = 0 # delete all outside the image 

        bone = image>threshold
        tissue = image<= threshold
        return torch.tensor(torch.sum(bone).item()/torch.sum(gt_segmentation==1).item(),
        device = device)


# should use GPU but currently the function is only implemented in cpu (see source below):
# https://codebrowser.bddppq.com/pytorch/pytorch/aten/src/ATen/native/Histogram.cpp.html
# I checked with pytorch 1.10 
# If you want to implement it in the future, make sure you have a cuda image
# to have a cuda image, edit BaseModel/base_model.py to have image = self.image instead of image_np
def fast_volume_ratio(segmentation, gt_segmentation, image, device, mode = 'pred'):
    image = image.view(-1)
    if mode == 'pred':
        # do prediction so you have binary
        pred = F.sigmoid(segmentation)
        pred_threshold = torch.tensor(0.5, device=device)
        pred = pred > pred_threshold
        
        # flatten label and prediction tensors
        pred = pred.view(-1)
        img_hist = torch.histogram(image, bins=256, range=(0., 1.), weight=pred)
        bins = torch.linspace(0,1.,256)

        # formula for mean thresh is from: An Analysis of Histogram-Based Thresholding Algorithms
        A = torch.sum(img_hist.hist)
        B = torch.sum(torch.mul(img_hist.hist,bins))
        threshold_mean_fast = B/A

        image[pred==0] = 0 # delete all outside the image 
        #threshold = threshold_otsu(np.array(image.cpu())) # replace with math later
        
        bone = image>threshold_mean_fast
        tissue = image<= threshold_mean_fast
        return torch.tensor(torch.sum(bone).item()/torch.sum(pred==1).item(),
        device = device)
    else:
        gt_segmentation = gt_segmentation.view(-1)
        
        img_hist = torch.histogram(image, bins=256, range=(0., 1.), weight=gt_segmentation)
        bins = torch.linspace(0,1.,256)
        # formula for mean thresh is from: An Analysis of Histogram-Based Thresholding Algorithms
        A = torch.sum(img_hist.hist)
        B = torch.sum(torch.mul(img_hist.hist,bins))
        threshold_mean_fast = B/A

        image[gt_segmentation==0] = 0 # delete all outside the image 

        bone = image>threshold_mean_fast
        tissue = image<= threshold_mean_fast
        return torch.tensor(torch.sum(bone).item()/torch.sum(gt_segmentation==1).item(),
        device = device)

def volume_ratio_loss(segmentation, gt_segmentation, image, device):
    
    gt_volume = volume_ratio(segmentation, gt_segmentation, image, device, mode = 'gt')
    pred_volume = volume_ratio(segmentation, gt_segmentation, image, device, mode = 'pred')
    return (gt_volume-pred_volume)**2

# def sag_shape_loss(pred, target, device, **kwargs):
#
#     ones = torch.tensor(1., device=pred.device)
#     zeros = torch.tensor(0., device=pred.device)
#
#     pred = torch.where(pred > 0.5, ones, zeros)
#
#
#     c_vec_sag = torch.arange(1, pred.shape[3] + 1).to(pred)
#     c_vec_cor = torch.arange(1, pred.shape[4] + 1).to(pred)
#
#     c_vec_sag_2 = torch.stack([c_vec_sag] * 128, dim=1).to(pred)
#     c_vec_cor_2 = torch.stack([c_vec_cor] * 128, dim=0).to(pred)
#
#     raw_out_sag = pred * c_vec_sag_2
#     # raw_out_cor = pred * c_vec_cor_2
#
#     sum_mat_sag = torch.matmul(c_vec_cor_2, pred)
#
#     m_sum = torch.sum(pred, dim=3, keepdim=True)  # calculate Nx
#     avg = sum_mat_sag / (m_sum + 1e-5)  # calculate mean x position for each z
#     avg_out = pred * avg  # encode mean x position into mask
#
#     idx = torch.nonzero(avg_out, as_tuple=True)
#     raw_out_sag = raw_out_sag[idx]
#     avg_out = avg_out[idx]
#
#     mse = (raw_out_sag - avg_out) ** 2
#
#     mse_loss = torch.mean(mse)
#
#     mse_loss = torch.where(torch.isnan(mse_loss), zeros, mse_loss)
#
#     return mse_loss


def sag_shape_loss(pred, target, device, **kwargs):

    ones = torch.tensor(1., device=pred.device)
    zeros = torch.tensor(0., device=pred.device)

    pred = torch.where(pred > 0.5, ones, zeros)

    c_vec_sag = torch.arange(1, pred.shape[3] + 1).to(pred)
    c_vec_cor = torch.arange(1, pred.shape[4] + 1).to(pred)

    c_vec_sag_2 = torch.stack([c_vec_sag] * 128, dim=1).to(pred)
    c_vec_cor_2 = torch.stack([c_vec_cor] * 128, dim=0).to(pred)

    raw_out_sag = pred * c_vec_sag_2
    # raw_out_cor = pred * c_vec_cor_2

    sum_mat_sag = torch.matmul(c_vec_cor_2, pred)

    m_sum = torch.sum(pred, dim=3, keepdim=True)  # calculate Nx
    avg = sum_mat_sag / (m_sum + 1e-5)  # calculate mean x position for each z
    avg_out = pred * avg  # encode mean x position into mask

    # idx = torch.nonzero(avg_out, as_tuple=True)
    # raw_out_sag = raw_out_sag[idx]
    # avg_out = avg_out[idx]

    # mse = (raw_out_sag - avg_out) ** 2
    # mse_loss = torch.mean(mse)
    # mse_loss = torch.where(torch.isnan(mse_loss), zeros, mse_loss)

    sum_mat_sag_tar = torch.matmul(c_vec_cor_2, target)

    m_sum_tar = torch.sum(target, dim=3, keepdim=True)  # calculate Nx
    avg_tar = sum_mat_sag_tar / (m_sum_tar + 1e-5)  # calculate mean x position for each z
    avg_out_tar = target * avg_tar  # encode mean x position into mask


    mse = (avg_out_tar - avg_out) ** 2
    mse_loss = torch.mean(mse)
    mse_loss = torch.where(torch.isnan(mse_loss), zeros, mse_loss)

    return mse_loss





def bce_loss_windowed(pred, target, device, weight=None, reduction='mean', **kwargs):

    batch_size = target.shape[0]

    buffer = 5

    zero_padding = 10

    loss_hold = []

    target_batch_2 = []
    pred_batch_2 = []

    for batch in range(batch_size):
        target_hold = target[batch, 0, :, :, :].detach().cpu().numpy()

        target_idx = np.argwhere(target_hold == 1)

        (zmin, ymin, xmin), (zmax, ymax, xmax) = target_idx.min(0), target_idx.max(0) + 1

        if zmin > buffer:
            zmin = zmin - buffer

        if zmax < (target.shape[2] - buffer):
            zmax = zmax + buffer

        padded_shape = target[batch, 0, zmin:zmax, :, :].shape

        padded_shape = (padded_shape[0] + (zero_padding * 2), padded_shape[1], padded_shape[2])

        pred_batch = torch.zeros(padded_shape).to(pred)
        target_batch = torch.zeros(padded_shape).to(target)

        rel_zmin = zero_padding
        rel_zmax = zmax - zmin + zero_padding

        target_batch[rel_zmin:rel_zmax, :, :] = target[batch, 0, zmin:zmax, :, :]
        pred_batch[rel_zmin:rel_zmax, :, :] = pred[batch, 0, zmin:zmax, :, :]

        f = F.binary_cross_entropy(pred_batch, target_batch, weight=weight, reduction=reduction)

        if torch.isnan(f):
            print()

        loss_hold.append(f)

    loss = torch.mean(torch.stack(loss_hold))

    return loss

def ce_loss(pred, target, device,
            weight=None, size_average=None,
            ignore_index=-100, reduce=None, reduction='mean', **kwargs):
    # target[0, :, 60, 57, 65]
    input = pred.permute(0, 2, 3, 4, 1).contiguous().view(-1, pred.shape[1])
    tar = target.permute(0, 2, 3, 4, 1).contiguous().view(-1).type(torch.long)
    return F.cross_entropy(input,
                           tar,
                           weight=weight, size_average=size_average,
                           ignore_index=ignore_index, reduce=reduce, reduction=reduction)



def accuracy(pred, target, device, **kwargs):
    pred_label = pred.argmax(dim=-1)

    correct_labels = torch.eq(pred_label, target)

    return correct_labels.type(torch.float32).mean()


def l1_loss(pred, target, device,
            size_average=None, reduce=None, reduction='mean', **kwargs):

    metric = F.l1_loss(pred, target,
                       size_average=size_average,
                       reduce=reduce, reduction=reduction)

    return metric


def l2_loss(pred, target, device,
            size_average=None, reduce=None, reduction='mean', **kwargs):

    metric = F.mse_loss(pred, target,
                        size_average=size_average,
                        reduce=reduce,
                        reduction=reduction)
    return metric


def l12_loss(pred, target, device,
            size_average=None, reduce=None, reduction='mean', **kwargs):

    l1 = F.l1_loss(pred, target,
                   size_average=size_average,
                   reduce=reduce, reduction=reduction)

    l2 = F.mse_loss(pred, target,
                    size_average=size_average,
                    reduce=reduce,
                    reduction=reduction)

    metric = (l1 + l2) / 2

    return metric


# def ssim(pred, target, device,
#          window_size=11, window=None, channel=1, size_average=True, full=False, val_range=None, **kwargs):
#
#
#     if val_range is None:
#         if torch.max(pred) > 128:
#             max_val = 255
#         else:
#             max_val = 1
#
#         if torch.min(pred) < -0.5:
#             min_val = -1
#         else:
#             min_val = 0
#         range_length = max_val - min_val
#     else:
#         range_length = val_range
#
#     padd = 0
#
#     (_, channel, height, width, depth) = pred.size()
#
#     if window is None:
#         real_size = min(window_size, height, width, depth)
#         window = create_window3D(real_size, channel=channel).to(device)
#
#     mu_pred = F.conv3d(pred, window, padding=padd, groups=channel)
#     mu_target = F.conv3d(target, window, padding=padd, groups=channel)
#
#     mu_pred_sq = mu_pred.pow(2)
#     mu_target_sq = mu_target.pow(2)
#
#     mu_pred_target = mu_pred * mu_target
#
#
#     sigma_pred_sq = F.conv3d(pred * pred, window, padding=padd, groups=channel) - mu_pred_sq
#     sigma_target_sq = F.conv3d(target * target, window, padding=padd, groups=channel) - mu_target_sq
#     sigma_pred_target = F.conv3d(pred * target, window, padding=padd, groups=channel) - mu_pred_target
#
#     C1 = (0.01 * range_length) ** 2
#     C2 = (0.03 * range_length) ** 2
#
#
#     v1 = 2.0 * sigma_pred_target + C2
#     v2 = sigma_pred_sq + sigma_target_sq + C2
#
#     cs = torch.mean(v1 / v2)  # contrast sensitivity
#
#
#     ssim_map = ((2 * mu_pred_target + C1) * v1) / ((mu_pred_sq + mu_target_sq + C1) * v2)
#
#
#     # ssim_map = ((2 * mu_pred_target + C1) * (2.0 * sigma_pred_target + C2)) / ((mu_pred_sq + mu_target_sq + C1) * (sigma_pred_sq + sigma_target_sq + C2))
#     # ssim_map = ((2 * mu_pred_target + C1) * (2.0 * sigma_pred_target + C2)) / ((mu_pred_sq + mu_target_sq + C1) * (sigma_pred_sq + sigma_target_sq + C2))
#
#     if size_average:
#         ret = ssim_map.mean()
#     else:
#         ret = ssim_map.mean(1).mean(1).mean(1)
#
#     if full:
#         return ret, cs
#     return ret



class ssim:
    """
    """

    def __init__(self, params, device,
                 window_size=11, window=None, channel=1, size_average=True, full=False, val_range=None, **kwargs):
        """
        Parameters
        ----------
        params : Parameters.ParameterBuilder.Parameters
            Parameters for model creating and training purposes.

        """
        num_out_channels = params.num_out_channels

        sample_size = params.sample_size


        self.window_size = window_size
        self.window = window
        self.channel = channel
        self.size_average = size_average
        self.full = full
        self.val_range = val_range


        height, width, depth = params.sample_size

        if self.window is None:
            real_size = min(self.window_size, height, width, depth)
            self.window = create_window3D(real_size, channel=self.channel).to(device)


    def __call__(self, pred, target, device, **kwargs):

        if self.val_range is None:
            if torch.max(pred) > 128:
                max_val = 255
            else:
                max_val = 1

            if torch.min(pred) < -0.5:
                min_val = -1
            else:
                min_val = 0
            range_length = max_val - min_val
        else:
            range_length = self.val_range

        padd = 0

        # (_, self.channel, height, width, depth) = pred.size()
        #
        # if self.window is None:
        #     real_size = min(self.window_size, height, width, depth)
        #     window = create_window3D(real_size, channel=self.channel).to(device)
        # else:
        #     window = self.window

        mu_pred = F.conv3d(pred, self.window, padding=padd, groups=self.channel)
        mu_target = F.conv3d(target, self.window, padding=padd, groups=self.channel)

        mu_pred_sq = mu_pred.pow(2)
        mu_target_sq = mu_target.pow(2)

        mu_pred_target = mu_pred * mu_target


        sigma_pred_sq = F.conv3d(pred * pred, self.window, padding=padd, groups=self.channel) - mu_pred_sq
        sigma_target_sq = F.conv3d(target * target, self.window, padding=padd, groups=self.channel) - mu_target_sq
        sigma_pred_target = F.conv3d(pred * target, self.window, padding=padd, groups=self.channel) - mu_pred_target

        C1 = (0.01 * range_length) ** 2
        C2 = (0.03 * range_length) ** 2


        v1 = 2.0 * sigma_pred_target + C2
        v2 = sigma_pred_sq + sigma_target_sq + C2

        cs = torch.mean(v1 / v2)  # contrast sensitivity


        ssim_map = ((2 * mu_pred_target + C1) * v1) / ((mu_pred_sq + mu_target_sq + C1) * v2)


        # ssim_map = ((2 * mu_pred_target + C1) * (2.0 * sigma_pred_target + C2)) / ((mu_pred_sq + mu_target_sq + C1) * (sigma_pred_sq + sigma_target_sq + C2))
        # ssim_map = ((2 * mu_pred_target + C1) * (2.0 * sigma_pred_target + C2)) / ((mu_pred_sq + mu_target_sq + C1) * (sigma_pred_sq + sigma_target_sq + C2))

        if self.size_average:
            ret = ssim_map.mean()
        else:
            ret = ssim_map.mean(1).mean(1).mean(1)

        if self.full:
            return ret, cs
        return ret



# def ssim_loss(pred, target, device,
#               window_size=11, window=None, channel=1, size_average=True, full=False, val_range=None, **kwargs):
#
#     return torch.tensor(1, dtype=torch.float32, device=device) - ssim(pred, target, device,
#                                                                       window_size=window_size, window=window, channel=channel,
#                                                                       size_average=size_average, full=False, val_range=val_range, **kwargs)

class ssim_loss:

    def __init__(self, params, device,
                 window_size=11, window=None, channel=1, size_average=True, full=False, val_range=None, **kwargs):

        self.ssim_fn = ssim(params=params, device=device,
                            window_size=window_size, window=window, channel=channel,
                            size_average=size_average, full=full, val_range=val_range, **kwargs)

    def __call__(self, pred, target, device):

        return torch.tensor(1, dtype=torch.float32, device=device) - self.ssim_fn(pred, target, device)


def mssim(pred, target, device,
          window_size=11, window=None, channel=1, size_average=True, val_range=None, normalize=False, **kwargs):

    weights = torch.tensor([0.0448, 0.2856, 0.3001, 0.2363, 0.1333], dtype=torch.float32).to(device)
    levels = weights.size()[0]
    mssim = []
    mcs = []
    for _ in range(levels):
        sim, cs = ssim(pred, target, window_size=window_size, size_average=size_average, full=True, val_range=val_range)
        mssim.append(sim)
        mcs.append(cs)


        pred = F.avg_pool3d(pred, (2, 2, 2))
        target = F.avg_pool3d(target, (2, 2, 2))

    mssim = torch.stack(mssim)
    mcs = torch.stack(mcs)


    # Normalize (to avoid NaNs during training unstable models, not compliant with original definition)
    if normalize:
        mssim = (mssim + 1) / 2
        mcs = (mcs + 1) / 2

    pow1 = mcs ** weights
    pow2 = mssim ** weights
    output = torch.prod(pow1[:-1] * pow2[-1])
    return output


def mssim_loss(pred, target, device,
               window_size=11, window=None, channel=1, size_average=True, val_range=None, normalize=False, **kwargs):

    return torch.tensor(1, dtype=torch.float32, device=device) - mssim(pred, target, device,
                                                                       window_size=window_size, window=window, channel=channel, size_average=size_average,
                                                                       val_range=val_range, normalize=normalize, **kwargs)


def create_window3D(window_size=11, channel=1, sigma=1.5):

    x_center = window_size // 2
    y_center = window_size // 2
    z_center = window_size // 2

    gauss = np.zeros((window_size, window_size, window_size))

    for x_idx in range(11):
        for y_idx in range(11):
            for z_idx in range(11):
                gauss[x_idx, y_idx, z_idx] = np.exp(-(
                                                    (x_idx - x_center) ** 2 / (2 * sigma ** 2) +
                                                    (y_idx - y_center) ** 2 / (2 * sigma ** 2) +
                                                    (z_idx - z_center) ** 2 / (2 * sigma ** 2)))


    gauss = gauss / gauss.sum()
    gauss = torch.tensor(gauss, dtype=torch.float32)
    gauss = gauss.expand(channel, 1, window_size, window_size, window_size).contiguous()

    return gauss


def create_window2D(window_size=11, channel=1, sigma=1.5):
    x_center = window_size // 2
    y_center = window_size // 2
    gauss = np.zeros((window_size, window_size))

    for x_idx in range(11):
        for y_idx in range(11):
            gauss[x_idx, y_idx] = np.exp(-(
                    (x_idx - x_center) ** 2 / (2 * sigma ** 2) +
                    (y_idx - y_center) ** 2 / (2 * sigma ** 2)))

    gauss = gauss / gauss.sum()

    gauss = torch.tensor(gauss)
    gauss = gauss.expand(channel, 1, window_size, window_size).contiguous()
    return gauss



def batch_pack_graph(x, counts, num_rows):
    """Picks different number of values from each row
    in x depending on the values in counts.
    """
    outputs = []
    for i in range(num_rows):
        outputs.append(x[i, :counts[i]])
    return torch.cat(outputs, dim=0)


def rpn_class_loss(pred, target, device):
    """
    RPN anchor classifier loss.
    :param rpn_match: [batch, anchors, 1]. Anchor match type. 1=positive, -1=negative, 0=neutral anchor.
    :param rpn_class_logits: [batch, anchors, 2]. RPN classifier logits for FG/BG.
            I believe this is a type from the documentation. Should be BG/FG.
    """

    rpn_match, rpn_class_logits = pred

    const_1 = torch.tensor(1, device=device)
    const_0 = torch.tensor(0, device=device)

    rpn_match = torch.squeeze(rpn_match, -1)


    # Get anchor classes. Convert the -1/+1 match to 0/1 values.
    # anchor_class = K.cast(K.equal(rpn_match, 1), tf.int32)
    anchor_class = torch.where(rpn_match == const_1, const_1, const_0)

    # Positive and Negative anchors contribute to the loss, but neutral anchors (match value = 0) don't.
    # indices = tf.where(K.not_equal(rpn_match, 0))
    indices = torch.where(rpn_match != const_0)

    # Pick rows that contribute to the loss and filter out the rest.

    rpn_class_logits = rpn_class_logits[indices]
    anchor_class = anchor_class[indices]

    # Cross-entropy loss
    # Change rpn_class_logits to a single channel and use binary_cross_entropy
    if anchor_class.shape[0] > 0:
        # loss = F.cross_entropy(input=rpn_class_logits, target=anchor_class)
        loss = F.binary_cross_entropy(input=rpn_class_logits, target=anchor_class.to(rpn_class_logits))
    else:
        loss = torch.tensor(0, device=device, dtype=torch.float32)

    if torch.isnan(loss):
        loss = torch.tensor(0, device=device, dtype=torch.float32)

    return loss


def rpn_bbox_loss(pred, target, device): #(config, target_bbox, rpn_match, rpn_bbox):
    """
    Return the RPN bounding box loss graph.
    :param config: the model config object.
    :param target_bbox: [batch, max positive anchors, (dy, dx, log(dh), log(dw))].
                        Uses 0 padding to fill in unsed bbox deltas.
    :param rpn_match: [batch, anchors, 1]. Anchor match type. 1=positive, -1=negative, 0=neutral anchor.
    :param rpn_bbox: [batch, anchors, (dy, dx, log(dh), log(dw))]
    """

    const_1 = torch.tensor(1, device=device)
    const_0 = torch.tensor(0, device=device)

    rpn_match, rpn_gt_bbox_deltas, rpn_bbox_deltas = pred


    # target_bbox = utils.normalize_coordinates(target_bbox, config)


    # Positive anchors contribute to the loss, but negative and
    # neutral anchors (match value of 0 or -1) don't.
    indices = torch.where(rpn_match == 1)

    # Pick bbox deltas that contribute to the loss
    rpn_bbox_deltas = rpn_bbox_deltas[indices]

    # Trim target bounding box deltas to the same length as rpn_bbox.
    # Number of positive anchors for per batch
    batch_counts = torch.where(rpn_match == 1, const_1, const_0).sum(dim=-1)

    # Unsure if this is fully necessary. Essentially, removing the padding in the gt_bbox.
    # Could be done another way, but this works for now. Unclear if it will work. Try both methods and see

    # target_bbox = batch_pack_graph(rpn_gt_bbox, batch_counts, batch_counts.shape[0])

    batch_idx = torch.repeat_interleave(torch.arange(rpn_match.shape[0]).to(rpn_bbox_deltas), batch_counts).type(torch.long)
    batch_counts_idx = torch.cat([torch.arange(count) for count in batch_counts], dim=0).type(torch.long)

    target_bbox_deltas = rpn_gt_bbox_deltas[batch_idx, batch_counts_idx]


    if target_bbox_deltas.shape[0] > 0:
        # loss = smooth_l1_loss(rpn_bbox_deltas, target_bbox_deltas, device=device)
        # loss = F.l1_loss(rpn_bbox_deltas, target_bbox_deltas)
        loss = F.smooth_l1_loss(rpn_bbox_deltas, target_bbox_deltas)
    else:
        loss = torch.tensor(0, device=device, dtype=torch.float32)

    if torch.isnan(loss):
        loss = torch.tensor(0, device=device, dtype=torch.float32)

    if torch.isinf(loss):
        loss = torch.tensor(0, device=device, dtype=torch.float32)

    return loss


def frcnn_class_loss_graph(pred, target, device):
    """Loss for the classifier head of Mask RCNN.
    target_class_ids: [batch, num_rois]. Integer class IDs. Uses zero
        padding to fill in the array.
    pred_class_logits: [batch, num_rois, num_classes]
    active_class_ids: [batch, num_classes]. Has a value of 1 for
        classes that are in the dataset of the image, and 0
        for classes that are not in the dataset.
    """

    target_class_ids, pred_class_logits = pred

    # # Ensure the class ids are in the data type
    # target_class_ids = tf.cast(target_class_ids, tf.int32)

    # Find predictions of classes that are not in the dataset.
    # Based on the N+1 outputs for the predicting the class (where N is the number of classes and the +1 for background)
    # find which predicting produced the largest possible output.
    # pred_class_ids is [batch, num_rois] where each is which of the index for the active class ids
    pred_class_ids = torch.argmax(pred_class_logits, dim=-1)


    # Loss
    # Compute the cross_entropy between the predicted classes from logits and ground truth

    try:
        loss = F.cross_entropy(input=pred_class_logits.contiguous().view(-1, pred_class_logits.shape[-1]),
                               target=target_class_ids.contiguous().view(-1).type(torch.long))
    except:
        a = 1
    if torch.isnan(loss):
        loss = torch.tensor(0, device=device, dtype=torch.float32)

    return loss



def frcnn_bbox_loss_graph(pred, target, device):
    """
    Loss for Mask R-CNN bounding box refinement.
    :param target_bbox: [batch, num_rois, (dy, dx, log(dh), log(dw))]
    :param target_class_ids: [batch, num_rois]. Integer class IDs.
    :param pred_bbox: [batch, num_rois, num_classes, (dy, dx, log(dh), log(dw))]
    """

    target_bbox_deltas, target_class_ids, pred_bbox_deltas = pred

    # Reshape to merge batch and roi dimensions for simplicity.
    # target_class_ids = K.reshape(target_class_ids, (-1,))
    # target_bbox_deltas = K.reshape(target_bbox_deltas, (-1, 4))
    # pred_bbox_deltas = K.reshape(pred_bbox_deltas, (-1, K.int_shape(pred_bbox_deltas)[2], 4))

    # # Only positive ROIs contribute to the loss. And only the right class_id of each ROI. Get their indicies.
    # # Find index for all positive ROIs
    # positive_roi_ix = tf.where(target_class_ids > 0)
    #
    # # Collect the target_class_ids for the positive ROIs and ensure tf.int64
    # positive_roi_class_ids = tf.cast(tf.gather_nd(target_class_ids, positive_roi_ix), tf.int64)

    positive_roi_ix = torch.where(target_class_ids > 0)
    positive_roi_class_ids = target_class_ids[positive_roi_ix].type(torch.long)


    # Combine the positive index and class id for each positive ROI
    indices = torch.stack([*positive_roi_ix, positive_roi_class_ids], dim=1)

    indices = torch.chunk(indices, indices.shape[1], dim=-1)

    # Gather the deltas (predicted and true) that contribute to loss
    target_bbox_deltas = target_bbox_deltas[positive_roi_ix]
    pred_bbox_deltas = pred_bbox_deltas[indices].squeeze(dim=1)


    # Smooth-L1 Loss

    # loss = smooth_l1_loss(pred_bbox_deltas, target_bbox_deltas, device=device)

    # loss = F.l1_loss(pred_bbox_deltas, target_bbox_deltas)
    loss = F.smooth_l1_loss(pred_bbox_deltas, target_bbox_deltas)


    if torch.isnan(loss):
        loss = torch.tensor(0, device=device, dtype=torch.float32)

    return loss


def smooth_l1_loss(pred, target, device, **kwargs):

    const_1 = torch.tensor(1).to(pred)
    const_0 = torch.tensor(0).to(pred)
    const_05 = torch.tensor(0.5).to(pred)
    const_2 = torch.tensor(2).to(pred)

    diff = torch.abs(pred - target)

    less_than_one = torch.where(diff < const_1, const_1, const_0)

    # loss = (less_than_one * 0.5 * diff ** 2) + (1 - less_than_one) * (diff - 0.5)

    loss = (less_than_one * const_05 * (diff ** const_2)) + (const_1 - less_than_one) * (diff - const_05)

    return loss.mean()




if __name__ == '__main__':

    import SimpleITK as sitk

    gt_img = sitk.ReadImage('/localdisk1/GeoffKlein/self_supervised/prediction/018_4month_0_gt.nii.gz')
    pred_img = sitk.ReadImage('/localdisk1/GeoffKlein/self_supervised/prediction/018_4month_0_prediction.nii.gz')

    gt_data = sitk.GetArrayFromImage(gt_img)
    pred_data = sitk.GetArrayFromImage(pred_img)

    pred_data_expand = np.expand_dims(np.expand_dims(pred_data, axis=0), axis=0)
    gt_data_expand = np.expand_dims(np.expand_dims(gt_data, axis=0), axis=0)

    pred_tensor = torch.tensor(pred_data_expand, dtype=torch.float32).to('cuda')
    gt_tensor = torch.tensor(gt_data_expand, dtype=torch.float32).to('cuda')

    a = ssim(pred_tensor, gt_tensor, gt_tensor.device)
    print(a)

