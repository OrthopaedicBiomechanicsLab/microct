from .loss_metric_utils import *
# from . import loss_metric_utils
# import torch
from torch import nn
# Dictionary of accepted typos and names with corresponding loss functions.
dict_function_name_mappings = {'dsc': 'DSC',
                               'DSC': 'DSC',
                               'dice': 'DSC',

                               'dice_loss': 'DSCLoss',
                               'dsc_loss': 'DSCLoss',
                               'DSCLoss': 'DSCLoss',

                               'sag_shape_loss': 'SagShapeLoss',

                               'bce_loss': 'BCELoss',
                               'bce': 'BCELoss',
                               'bce_logits': 'BCELogitsLoss',
                               'bce_logits_loss': 'BCELogitsLoss',
                               'bce_window': 'BCEWindowLoss',
                               'ce': 'CrossEntropyLoss',
                               'cross_entropy': 'CrossEntropyLoss',
                               #'tversky_loss' : 'tversky_loss',

                               'concurrency': 'CONCURRENCY',
                               'acc': 'ACCURACY',
                               'accuracy': 'ACCURACY',
                               'ACCURACY': 'ACCURACY',
                               'acc_class': 'ACCURACYClass',
                               'acc_comp': 'ACCURACYComp',
                               'smooth_l1_loss': 'SmoothL1Loss',
                               'l1': 'L1Loss',
                               'l1_loss': 'L1Loss',
                               'l2_loss': 'L2Loss',
                               'mse': 'L2Loss',
                               'MeanSquareError': 'L2Loss',
                               'MSE': 'L2Loss',
                               'l12_loss': 'L12Loss',
                               'ssim': 'SSIM',
                               'ssim_loss': 'SSIMLoss',


                               'rpn_class_loss': 'RPNClassLoss',
                               'rpn_bbox_loss': 'RPNBoxLoss',
                               'frcnn_class_loss_graph': 'RCNNClassLoss',
                               'frcnn_bbox_loss_graph': 'RCNNBoxLoss',
                               'volume_ratio' : 'volume_ratio',
                               'volume_ratio_gt' : 'volume_ratio',
                               'volume_ratio_pred' : 'volume_ratio'
                               }

# custom_function_name_list = ['DSCLoss', 'DSC']


# class BaseLossMetric:
#     """
#     This is the base class for loss and metric functions
#     """
#     def __init__(self, fn, pred_index=None, label_index=None, **fn_params):
#         self.fn = fn
#         self.fn_params = fn_params
#         self.pred = None
#         self.target = None
#         self.label_index = label_index
#         self.pred_index = pred_index
#     # def initialize_fn(self):
#     #     """
#     #     Define loss function, either directly or calling another defined function
#     #     """
#     #     self.loss_fn = None
#     def __call__(self, pred, target, **kwargs):
#         if self.label_index is not None:
#             target = target[self.label_index]
#         if self.pred_index is not None:
#             pred = pred[self.pred_index]
#         return self.fn(pred, target, **self.fn_params, **kwargs)


class BaseLossMetric(nn.Module):
    """
    This is the base class for loss and metric functions
    """
    def __init__(self, fn, fn_params, pred_index=None, label_index=None, device=None):
        super(BaseLossMetric, self).__init__()
        self.fn = fn
        self.fn_params = fn_params
        self.pred = None
        self.target = None
        self.device = device
        self.label_index = label_index
        self.pred_index = pred_index

    def forward(self, pred, target, **kwargs):
        if self.label_index == 'skip':
            target = None
        elif self.label_index is not None:
            if isinstance(self.label_index, int):
                target = target[self.label_index]
            else:
                target = [target[idx] for idx in self.label_index]

        if self.pred_index == 'skip':
            pred = None
        elif self.pred_index is not None:
            if isinstance(self.pred_index, int):
                pred = pred[self.pred_index]
            else:
                pred = [pred[idx] for idx in self.pred_index]
        if 'image' in kwargs: # if the loss function requires the original image
            image =  kwargs.pop('image', None)
            if 'mode' in kwargs: #this is a specific check for the volume ratio (bone/total)
                mode =  kwargs.pop('mode', None)
                return self.fn(pred, target, image, self.device, mode = mode, **self.fn_params, **kwargs)  
            else:
                return self.fn(pred, target, image, self.device, **self.fn_params, **kwargs)    
        return self.fn(pred, target, self.device, **self.fn_params, **kwargs)
